package eu.digient.billfold.payment.mb


enum MBStatus {
    /**
     * This status is sent when the transaction is processed and the funds have been received on your Moneybookers account.
     */
    Processed(2),

    /**
     * This status is sent when the customers pays via the pending bank transfer option. Such transactions will auto-process IF the bank transfer is received by Moneybookers. We strongly recommend that you do NOT process the order/transaction in your system upon receipt of a pending status from Moneybookers.
     */
    Pending(0),

    /**
     * Pending transactions can either be cancelled manually by the sender in their online account history or they will auto-cancel after 14 days if still pending.
     */
    Cancelled(-1),

    /**
     * This status is sent when the customer tries to pay via Credit Card or Direct Debit but our provider declines the transaction. If you do not accept Credit Card or Direct Debit payments via Moneybookers (see page 17) then you will never receive the failed status.
     */
    Failed(-2),
    /**
     * This status could be received only if your account is configured to receive chargebacks. If this is the case, whenever a chargeback is received by Moneybookers, a -3 status will be posted on the status_url for the reversed transaction.
     */
    Chargeback(-3)

    private int value
    private MBStatus(int value) {
        this.value = value
    }

    static MBStatus toMBStatus(Integer value) {
        switch(value) {
            case 2:
                return Processed
            case 0:
                return Pending
            case -1:
                return Cancelled
            case -2:
                return Failed
            case -3:
                return Chargeback
        }
        throw new IllegalArgumentException()
    }

    static MBStatus toMBStatus(String value) {
        Integer v = Integer.valueOf(value)
        toMBStatus(v)
    }

    static MBStatus valueOf(int statusCode) {
        toMBStatus(statusCode)
    }

    public int getValue() {
        return value;
    }

}
