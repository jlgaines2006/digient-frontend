package eu.digient.billfold.payment.mb

enum ReturnTarget {
    top(1),
    parent(2),
    self(3),
    blank(4)

    private final int value

    private ReturnTarget(final int value) {
        this.value = value
    }
}
