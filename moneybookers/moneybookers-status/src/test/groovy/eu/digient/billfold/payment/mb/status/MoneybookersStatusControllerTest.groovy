package eu.digient.billfold.payment.mb.status

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.payment.service.PaymentService
import eu.digient.billfold.session.web.ParamHelper
import eu.digient.billfold.support.service.BillfoldConfigService
import org.junit.Test
import eu.digient.billfold.payment.mb.MBStatus
import org.junit.Before
import eu.digient.test.AbstractBaseTest
import eu.digient.billfold.payment.provider.DepositNotificationHandler
import org.easymock.EasyMock
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse

class MoneybookersStatusControllerTest extends AbstractBaseTest {
    MoneybookersStatusController target
    DepositNotificationHandler delegator
    BillfoldConfigService configService
    PaymentService paymentService
    ParamHelper paramHelper

    MockHttpServletRequest req
    MockHttpServletResponse res

    @Before void setUp() {
        target  = new MoneybookersStatusController()

        delegator = EasyMock.createMock(DepositNotificationHandler)
        addMock(target, 'delegator', delegator)

        paramHelper = EasyMock.createMock(ParamHelper)
        addMock(target, 'paramHelper', paramHelper)

        configService = EasyMock.createMock(BillfoldConfigService)
        addMock(target, 'configService', configService)

        paymentService = EasyMock.createMock(PaymentService)
        addMock(target, 'paymentService', paymentService)

        req = new MockHttpServletRequest()
        res = new MockHttpServletResponse()
    }

    @Test
    void testInvalidMD5() {
        EasyMock.expect(paramHelper.getIpAddress(req)).andReturn('some-ip')
        EasyMock.expect(paymentService.getPaymentByReference('TX_123')).andReturn(new Payment(paymentId: 1L, status: PaymentStatus.pending, siteId: 1))
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SECRET_WORD')).andReturn('123')
        delegator.handleError(1L, 'some-ip', 'Security error: MD5 do not match.', [:])

        req.addParameter('pay_to_email', 'to@pay.com')
        req.addParameter('pay_from_email', 'from@pay.com')
        req.addParameter('merchant_id', '123456')
        req.addParameter('transaction_id', 'TX_123')
        req.addParameter('mb_transaction_id', 'MB_TX_123')
        req.addParameter('mb_amount', '12.34')
        req.addParameter('mb_currency', 'EUR')
        req.addParameter('status', '2')
        req.addParameter('failed_reason_code', '')
        req.addParameter('md5sig', 'WRONG-MD5')
        req.addParameter('amount', '12.34')
        req.addParameter('currency', 'EUR')
        req.addParameter('payment_type', '')
        req.addParameter('pay_to_email', 'pay_to_email')

        replay()
        target.notification(req, res)
    }

    @Test
    void testOK() {
        EasyMock.expect(paramHelper.getIpAddress(req)).andReturn('some-ip')
        EasyMock.expect(paymentService.getPaymentByReference('TX_123')).andReturn(new Payment(status: PaymentStatus.pending, paymentId: 1L, siteId: 1))
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SECRET_WORD')).andReturn('123')
        def event = [pay_to_email: 'to@pay.com', pay_from_email: 'from@pay.com', merchantId: '123456', transaction_id: 'TX_123', mbTransactionId: 'MB_TX_123', mbAmount: new BigDecimal('1'), mbCurrency: 'EUR', mbStatus: MBStatus.Processed, md5sig: 'CE38EA9010DDFDADC34192695E972C74', amount: new BigDecimal('1'), currency: 'EUR', paymentType: null, failedReasonCode: null, rec_payment_id: null]
        delegator.handleOk(1L, new BigDecimal('1'), 'EUR', 'some-ip', event)
        EasyMock.expectLastCall().once()

        req.addParameter('pay_to_email', 'to@pay.com')
        req.addParameter('pay_from_email', 'from@pay.com')
        req.addParameter('merchant_id', '123456')
        req.addParameter('transaction_id', 'TX_123')
        req.addParameter('mb_transaction_id', 'MB_TX_123')
        req.addParameter('mb_amount', '1')
        req.addParameter('mb_currency', 'EUR')
        req.addParameter('status', '2')
        req.addParameter('md5sig', 'CE38EA9010DDFDADC34192695E972C74')
        req.addParameter('amount', '1')
        req.addParameter('currency', 'EUR')

        replay()
        target.notification(req, res)
    }

    @Test
    void testPending() {
        EasyMock.expect(paramHelper.getIpAddress(req)).andReturn('some-ip')
        EasyMock.expect(paymentService.getPaymentByReference('TX_123')).andReturn(new Payment(status: PaymentStatus.pending, paymentId: 1L, siteId: 1))
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SECRET_WORD')).andReturn('123')
        def event = [pay_to_email: 'to@pay.com', pay_from_email: 'from@pay.com', merchantId: '123456', transaction_id: 'TX_123', mbTransactionId: 'MB_TX_123', mbAmount: new BigDecimal('12.34'), mbCurrency: 'EUR', mbStatus: MBStatus.Pending, md5sig: 'A138F1AAA515B3942778068BA131CF8E', amount: new BigDecimal('12.34'), currency: 'EUR', paymentType: null, failedReasonCode: null, rec_payment_id: null]
        delegator.handleStore(1L, 'some-ip', event)
        EasyMock.expectLastCall().once()

        req.addParameter('pay_to_email', 'to@pay.com')
        req.addParameter('pay_from_email', 'from@pay.com')
        req.addParameter('merchant_id', '123456')
        req.addParameter('transaction_id', 'TX_123')
        req.addParameter('mb_transaction_id', 'MB_TX_123')
        req.addParameter('mb_amount', '12.34')
        req.addParameter('mb_currency', 'EUR')
        req.addParameter('status', '0')
        req.addParameter('md5sig', 'A138F1AAA515B3942778068BA131CF8E')
        req.addParameter('amount', '12.34')
        req.addParameter('currency', 'EUR')

        replay()
        target.notification(req, res)
    }

    @Test
    void testCancel() {
        EasyMock.expect(paramHelper.getIpAddress(req)).andReturn('some-ip')
        EasyMock.expect(paymentService.getPaymentByReference('TX_123')).andReturn(new Payment(status: PaymentStatus.pending, paymentId: 1L, siteId: 1))
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SECRET_WORD')).andReturn('123')
        def event = [pay_to_email: 'to@pay.com', pay_from_email: 'from@pay.com', merchantId: '123456', transaction_id: 'TX_123', mbTransactionId: 'MB_TX_123', mbAmount: new BigDecimal('12.34'), mbCurrency: 'EUR', mbStatus: MBStatus.Cancelled, md5sig: '4F1912702061D5C4335655AC9DE890B4', amount: new BigDecimal('12.34'), currency: 'EUR', paymentType: null, failedReasonCode: null, rec_payment_id: null]
        delegator.handleCancel(1L, 'some-ip', event)
        EasyMock.expectLastCall().once()

        req.addParameter('pay_to_email', 'to@pay.com')
        req.addParameter('pay_from_email', 'from@pay.com')
        req.addParameter('merchant_id', '123456')
        req.addParameter('transaction_id', 'TX_123')
        req.addParameter('mb_transaction_id', 'MB_TX_123')
        req.addParameter('mb_amount', '12.34')
        req.addParameter('mb_currency', 'EUR')
        req.addParameter('status', '-1')
        req.addParameter('md5sig', '4F1912702061D5C4335655AC9DE890B4')
        req.addParameter('amount', '12.34')
        req.addParameter('currency', 'EUR')

        replay()
        target.notification(req, res)
    }

    @Test
    void testPaymentNotPending() {
        EasyMock.expect(paymentService.getPaymentByReference('TX_123')).andReturn(new Payment(status: PaymentStatus.ok))
        req.addParameter('transaction_id', 'TX_123')

        replay()
        target.notification(req, res)
    }
}
