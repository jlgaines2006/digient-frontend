package eu.digient.billfold.payment.mb.status

import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.user.service.PaymentDetailService
import eu.digient.billfold.user.service.UserService
import eu.digient.sdk.util.StringUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpServletRequest
import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.mb.MBStatus
import eu.digient.billfold.payment.status.AbstractBaseStatusController

import javax.servlet.http.HttpServletResponse

@Controller
@RequestMapping(value = 'moneybookers')
class MoneybookersStatusController extends AbstractBaseStatusController {
    private static final def INPUT_OUTPUT = LoggerFactory.getLogger('INPUT_OUTPUT_MONEYBOOKERS')
    private static final def LOG = LoggerFactory.getLogger(MoneybookersStatusController)

    @Autowired PaymentDetailService paymentDetailService

    @RequestMapping(value = '/notification-1-tap.html', method = [RequestMethod.POST, RequestMethod.GET])
    @ResponseBody
    void notificationFor1Tap(final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        String ip = getRequestIpAddress(request)
        String pv = getParameters(request)

        LOG.debug "**************** S notification for 1-tap ****************"
        LOG.debug "*** from IP: " + ip
        LOG.debug "*** parameter: " + pv
        LOG.debug "**************** E notification for 1-tap ****************"
    }

    @RequestMapping(value = '/notification.html', method = [RequestMethod.POST])
    @ResponseBody
    void notification(final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        long x1 = System.currentTimeMillis()
        try {
            def event = [:]
            String pay_to_email = request.getParameter('pay_to_email')
            String pay_from_email = eu.digient.sdk.util.StringUtil.trimNull(request.getParameter('pay_from_email'))
            String merchant_id = request.getParameter('merchant_id')
            String transaction_id = request.getParameter('transaction_id')
            String mb_transaction_id = request.getParameter('mb_transaction_id')
            String mb_amountString = request.getParameter('mb_amount')
            String mb_currency = request.getParameter('mb_currency')
            String statusString = request.getParameter('status')

            // If the transaction is with status -2 (failed), this field will contain a code detailing the reason for the failure.
            String failed_reason_code = request.getParameter('failed_reason_code')

            String md5sig = request.getParameter('md5sig')
            String amount = request.getParameter('amount')
            String currency = request.getParameter('currency')
            String payment_type = request.getParameter('payment_type')
            String rec_payment_id = request.getParameter('rec_payment_id')

            def original = getOriginal(transaction_id)
            Long paymentId = original?.paymentId
            if (original?.status == PaymentStatus.pending) {
                def ip = getRequestIpAddress(request)
                if (validateMD5(original.siteId, merchant_id, transaction_id, mb_amountString, mb_currency, statusString, md5sig)) {
                    BigDecimal mbAmount = new BigDecimal(mb_amountString)
                    MBStatus mbStatus = MBStatus.toMBStatus(statusString)

                    event.pay_to_email = pay_to_email
                    event.pay_from_email = pay_from_email
                    event.merchantId = merchant_id
                    event.transaction_id = transaction_id
                    event.mbTransactionId = mb_transaction_id
                    event.mbAmount = mbAmount
                    event.mbCurrency = mb_currency
                    event.mbStatus = mbStatus
                    event.md5sig = md5sig
                    event.amount = new BigDecimal(amount)
                    event.currency = currency
                    event.paymentType = payment_type
                    event.failedReasonCode = failed_reason_code
                    event.rec_payment_id = rec_payment_id

                    process(original, ip, event)

                    try {
                        if (!eu.digient.sdk.util.StringUtil.isEmptyString(pay_from_email)) {
                            LOG.debug 'Updating Skrill email to {} for user {}', pay_from_email, original.userId
                            paymentDetailService.updateMoneybookersEmail(original.userId, pay_from_email)
                        }

                        if (!eu.digient.sdk.util.StringUtil.isEmptyString(rec_payment_id)) {
                            LOG.debug 'Updating Skrill recurring payment ID to {} for user {}', rec_payment_id, original.userId
                            paymentDetailService.updateMoneybookersEmail(original.userId, pay_from_email)
                            paymentDetailService.updateMoneybookersrRecPaymentId(original.userId, rec_payment_id)
                        }

                    } catch (Exception e) {
                        String msg = 'Failed to update skirll email to '+pay_from_email+' for user ' + original.userId
                        LOG.warn msg, e
                    }
                }
                else {
                    handleError(paymentId, ip, 'Security error: MD5 do not match.', event)
                }
            } else {
                LOG.error('[reference: {}, status: {}] Payment not found or payment is not pending.', transaction_id, original?.status)
            }
        } finally {
            def time = System.currentTimeMillis() - x1
            INPUT_OUTPUT.debug('[NOTIFICATION][MONEYBOOKERS][POST_PARAMS]{}[TIME]{}ms[END]', getParameters(request), time)
        }
    }

    private void process(final Payment payment, final String ip, final Map<String, Object> event) {
        Long paymentId = payment.paymentId
        switch (event.mbStatus) {
            case MBStatus.Processed:
                handleOk(paymentId, event.amount as BigDecimal, event.currency as String, ip, event)
                break
            case MBStatus.Pending:
                handleStore(paymentId, ip, event)
                break
            case MBStatus.Cancelled:
                handleCancel(paymentId, ip, event)
                break
            case MBStatus.Failed:
                handleReject(paymentId, ip, event)
                break
            default:
                handleError(paymentId, ip, 'Status: ' + event.mbStatus + ' not supported', event)
        }
    }

    private boolean validateMD5(final Integer siteId, final String merchant_id, final String transaction_id, final String mb_amount, final String mb_currency, final String status, String md5sig) {
        def secretWord = configService.getPropertyAsString(siteId, 'PAYMENT_MONEYBOOKERS_SECRET_WORD')
        String plain = merchant_id + transaction_id + StringUtil.MD5(secretWord).toUpperCase() + mb_amount + mb_currency + status
        String calculatedMD5 = StringUtil.MD5(plain).toUpperCase()
        (md5sig != null && !"".equals(md5sig) && md5sig.equals(calculatedMD5))
    }

}