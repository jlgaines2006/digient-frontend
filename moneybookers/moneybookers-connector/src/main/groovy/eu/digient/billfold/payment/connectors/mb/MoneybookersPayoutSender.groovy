package eu.digient.billfold.payment.connectors.mb

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.payment.PayoutSendResult
import eu.digient.billfold.payment.PayoutSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
@Qualifier(value = 'skrill_withdrawal')
class MoneybookersPayoutSender implements PayoutSender {
    @Autowired MoneybookersServiceInvoker invoker

    PayoutSendResult send(final Payment payment) {
        try {
            def r = invoker.send(payment.siteId, payment.reference, payment.bankAmount, payment.bankCurrency, payment.moneybookersEmail, payment.language)

            return new PayoutSendResult(success: true, providerReference: r)
        } catch (PaymentGatewayException e) {
            return new PayoutSendResult(success: false, errorText: e.errorMessage)
        }
    }

    @Override
    Payment.PaymentProviderName getName() {
        Payment.PaymentProviderName.skrill_withdrawal
    }
}
