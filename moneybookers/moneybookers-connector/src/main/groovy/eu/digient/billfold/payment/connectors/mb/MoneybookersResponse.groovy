package eu.digient.billfold.payment.connectors.mb

import eu.digient.sdk.lang.annotation.Equals
import eu.digient.sdk.lang.annotation.EqualsStrategy
import eu.digient.sdk.lang.annotation.HashCode
import eu.digient.sdk.lang.annotation.HashCodeStrategy
import eu.digient.sdk.lang.annotation.ToString
import eu.digient.sdk.lang.annotation.ToStringStrategy

class MoneybookersResponse implements Serializable {
    private static final long serialVersionUID = 1L
    @ToString @Equals @HashCode String sid
    @ToString @Equals @HashCode String id
    @ToString @Equals @HashCode String currency
    @ToString @Equals @HashCode BigDecimal amount
    @ToString @Equals @HashCode Integer status
    @ToString @Equals @HashCode String statusMsg
    @ToString @Equals @HashCode String errorMsg

    @Override
    String toString() {
        ToStringStrategy.byAnnotation(MoneybookersResponse, this)
    }

    @Override
    boolean equals(Object that) {
        EqualsStrategy.byAnnotation(MoneybookersResponse, this, that)
    }

    @Override
    int hashCode() {
        HashCodeStrategy.byAnnotation(MoneybookersResponse, this)
    }

}
