package eu.digient.billfold.payment.connectors.mb

import eu.digient.billfold.payment.connector.AbstractBaseXmlParser
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

interface MoneybookersResponseXmlParser {
    MoneybookersResponse parse(String entity)
}

@Component
class MoneybookersResponseXmlParserImpl extends AbstractBaseXmlParser implements MoneybookersResponseXmlParser {
    private static final def LOG = LoggerFactory.getLogger(MoneybookersResponseXmlParser)

    MoneybookersResponse parse(final String entity) {
        try {
            def data = new XmlSlurper().parseText(entity)
            def response = new MoneybookersResponse()
            response.sid = parseString('sid', data)
            response.errorMsg = parseString('error_msg', data.error)
            response.id = parseString('id', data.transaction)
            response.currency = parseString('currency', data.transaction)
            response.amount = parseBigDecimal('amount', data.transaction)
            response.status = parseInteger('status', data.transaction)
            response.statusMsg = parseString('status_msg', data.transaction)
            return  response
        }  catch (Exception e) {
            LOG.error 'Parsing error, xml: {}', e, entity
            throw new IllegalArgumentException()
        }
    }

}
