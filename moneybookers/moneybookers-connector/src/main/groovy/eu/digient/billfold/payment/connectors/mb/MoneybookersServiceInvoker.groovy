package eu.digient.billfold.payment.connectors.mb

import eu.digient.billfold.payment.CreateDepositResult
import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.connector.AbstractBaseConnector
import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.sdk.util.StringUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

interface MoneybookersServiceInvoker {
    String send(Integer siteId, String reference, BigDecimal amount, String currency, String moneybookersEmail, String language) throws PaymentGatewayException
    CreateDepositResult send1Tab(Integer siteId, String reference, BigDecimal amount, String currency, String recPaymentId, String language) throws PaymentGatewayException
}

@Component
class MoneybookersServiceInvokerImpl extends AbstractBaseConnector implements MoneybookersServiceInvoker {
    private static final def LOG = LoggerFactory.getLogger(MoneybookersServiceInvoker)
    private static final def INPUT_OUTPUT = LoggerFactory.getLogger('INPUT_OUTPUT_MONEYBOOKERS')

    @Autowired MoneybookersResponseXmlParser parser

    MoneybookersServiceInvokerImpl() {
        name = Payment.PaymentProviderName.skrill_withdrawal
    }

    String send(final Integer siteId, final String reference, final BigDecimal amount, final String currency, String moneybookersEmail, final String language) throws PaymentGatewayException {
        String exception = null

        String url = ''

        String prepareResponse = ''
        Map prepareParams = [:]

        String transferResponse = ''
        Map transferParams = [:]

        try {
            url = getPropertyAsString(siteId, 'URL')
            prepareParams.action = 'prepare'
            def emails = getPropertyAsMetadata(siteId, 'EMAIL')
            prepareParams.email = emails.get(currency)
            if (prepareParams.email == null) {
                prepareParams.email = emails.get('default')
            }
            if (prepareParams.email?.size() < 1) {
                def exp = new PaymentGatewayException()
                exp.errorMessage = 'PAYMENT_SKRILL_WITHDRAWAL_EMAIL is not configured for currency: ' + currency
                throw exp
            }

            prepareParams.password = StringUtil.MD5(getPropertyAsString(siteId, 'PASSWORD'))
            prepareParams.amount = amount.toPlainString()
            prepareParams.currency = currency
            prepareParams.bnf_email = moneybookersEmail
            prepareParams.subject = getMessage('payment.moneybookers.payout.subject', language)
            prepareParams.note = getMessage('payment.moneybookers.payout.note', language)
            prepareParams.frn_trn_id = reference

            prepareResponse = post(url, prepareParams)
            def prepareR = parser.parse(prepareResponse)

            def msg = ''
            if (prepareR?.sid != null) {
                transferParams.action = 'transfer'
                transferParams.sid = prepareR.sid
                transferResponse = post(url, transferParams)
                def transferR = parser.parse(transferResponse)
                if (transferR.status == 1 || transferR.status == 2) {
                    return transferR.id
                }
                else {
                    msg += 'Transfer failed, sid: ' + prepareR.sid + '.'
                    if (transferR?.status != null) {
                        msg += ' Status: ' + transferR.status + '.'
                    }
                    if (transferR?.statusMsg?.size() > 0) {
                        msg += ' StatusMsg: ' + transferR.statusMsg + '.'
                    }
                    if (transferR?.errorMsg?.size()> 0) {
                        msg += ' ErrorMsg: ' + transferR.errorMsg + '.'
                    }
                }
            }
            else {
                msg += 'Prepare failed.'
                if (prepareR?.errorMsg?.size()> 0) {
                    msg += ' ErrorMsg: ' + prepareR.errorMsg + '.'
                }
            }
            LOG.error('Something went wrong: {}', msg)
            throw new PaymentGatewayException(errorMessage: msg)
        } catch (PaymentGatewayException e) {
            throw e
        } catch (Exception e) {
            exception = 'Exception: ' + e.message
            LOG.error('Exception occurred when handling call to Moneybookers', e)
            throw new PaymentGatewayException(errorMessage: 'Error when sending payout')
        } finally {
            if (exception != null) {
                INPUT_OUTPUT.debug('[SEND][URL]{}[ERROR]{}[PREPARE_PARAMS]{}[PREPARE_RESPONSE]{}[TRANSFER_PARAMS]{}[TRANSFER_RESPONSE]{}[END]', url, exception, prepareParams, prepareResponse, transferParams, transferResponse)
            } else {
                INPUT_OUTPUT.debug('[SEND][URL]{}[PREPARE_PARAMS]{}[PREPARE_RESPONSE]{}[TRANSFER_PARAMS]{}[TRANSFER_RESPONSE]{}[END]', url, prepareParams, prepareResponse, transferParams, transferResponse)
            }
        }
    }

    CreateDepositResult send1Tab(final Integer siteId, final String reference, final BigDecimal amount, final String currency, String recPaymentId, final String language) throws PaymentGatewayException {
        String exception = null

        String url = ''

        String prepareResponse = ''
        Map prepareParams = [action: 'prepare']

        String requestResponse = ''
        Map requestParams = [action: 'request']

        try {
            url = configService.getPropertyAsString(siteId, 'PAYMENT_MONEYBOOKERS_ONE_TAP_URL')
            prepareParams.email = configService.getPropertyAsString(siteId, 'PAYMENT_MONEYBOOKERS_ONE_TAP_EMAIL')
            prepareParams.password = StringUtil.MD5(configService.getPropertyAsString(siteId, 'PAYMENT_MONEYBOOKERS_ONE_TAP_PASSWORD'))
            prepareParams.action = 'prepare'
            prepareParams.amount = amount.toPlainString()
            prepareParams.currency = currency
            prepareParams.frn_trn_id = reference
            prepareParams.rec_payment_id = recPaymentId

            prepareResponse = post(url, prepareParams)
            def prepareR = parser.parse(prepareResponse)

            def msg = ''
            if (prepareR?.sid != null) {
                requestParams.action = 'request'
                requestParams.sid = prepareR.sid
                requestResponse = post(url, requestParams)
                def requestR = parser.parse(requestResponse)
                if (requestR.status == 2) {
                    return new CreateDepositResult(success: true, synchronous: false, providerReference: requestR.id)
                }
                else {
                    msg += 'request failed, sid: ' + prepareR.sid + '.'
                    if (requestR?.status != null) {
                        msg += ' Status: ' + requestR.status + '.'
                    }
                    if (requestR?.statusMsg?.size() > 0) {
                        msg += ' StatusMsg: ' + requestR.statusMsg + '.'
                    }
                    if (requestR?.errorMsg?.size()> 0) {
                        msg += ' ErrorMsg: ' + requestR.errorMsg + '.'
                    }
                    LOG.debug("error message: {}", msg)
                    return new CreateDepositResult(success: false, errorText: msg)
                }
            }
            else {
                msg += 'Prepare failed.'
                if (prepareR?.errorMsg?.size()> 0) {
                    msg += ' ErrorMsg: ' + prepareR.errorMsg + '.'
                }
                LOG.debug("prepare error message: {}", msg)
                return new CreateDepositResult(success: false, errorText: msg)
            }
        } catch (PaymentGatewayException e) {
            throw e
        } catch (Exception e) {
            exception = 'Exception: ' + e.message
            LOG.error('Exception occurred when handling call to Moneybookers', e)
            throw new PaymentGatewayException(errorMessage: 'Error when sending payout')
        } finally {
            if (exception != null) {
                INPUT_OUTPUT.debug('[SEND][URL]{}[ERROR]{}[PREPARE_PARAMS]{}[PREPARE_RESPONSE]{}[REQUEST_PARAMS]{}[REQUEST_RESPONSE]{}[END]', url, exception, prepareParams, prepareResponse, requestParams, requestResponse)
            } else {
                INPUT_OUTPUT.debug('[SEND][URL]{}[PREPARE_PARAMS]{}[PREPARE_RESPONSE]{}[REQUEST_PARAMS]{}[REQUEST_RESPONSE]{}[END]', url, prepareParams, prepareResponse, requestParams, requestResponse)
            }
        }

    }
}
