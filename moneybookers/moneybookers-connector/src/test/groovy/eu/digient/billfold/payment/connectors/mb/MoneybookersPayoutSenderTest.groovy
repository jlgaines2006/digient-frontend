package eu.digient.billfold.payment.connectors.mb

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.test.AbstractBaseTest
import org.easymock.EasyMock
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class MoneybookersPayoutSenderTest extends AbstractBaseTest{
    MoneybookersPayoutSender target
    MoneybookersServiceInvoker invoker

    @Before void setUp() {
        target = new MoneybookersPayoutSender()

        invoker = EasyMock.createMock(MoneybookersServiceInvoker)
        addMock(target, 'invoker', invoker)
    }

    @Test void send() {
        EasyMock.expect(invoker.send(1, 'some-ref', new BigDecimal('1'), 'some-cur', 'some-email', 'some-lang')).andReturn('some-prov-ref')

        replay()
        def r = target.send(new Payment(siteId: 1, reference: 'some-ref', bankAmount: new BigDecimal('1'), bankCurrency: 'some-cur', moneybookersEmail: 'some-email', language: 'some-lang'))
        assertEquals 'some-prov-ref', r.providerReference
        assertTrue r.success
    }

    @Test void send_failed() {
        EasyMock.expect(invoker.send(1, 'some-ref', new BigDecimal('1'), 'some-cur', 'some-email', 'some-lang')).andThrow(new PaymentGatewayException(errorMessage: 'some-error'))

        replay()
        def r = target.send(new Payment(siteId: 1, reference: 'some-ref', bankAmount: new BigDecimal('1'), bankCurrency: 'some-cur', moneybookersEmail: 'some-email', language: 'some-lang'))
        assertEquals 'some-error', r.errorText
        assertFalse r.success
    }
}
