package eu.digient.billfold.payment.connectors.mb

import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.billfold.support.service.HttpConnectionHelper
import eu.digient.test.AbstractBaseTest
import org.easymock.EasyMock
import org.junit.Before
import org.junit.Test
import org.springframework.context.MessageSource

import static org.junit.Assert.assertEquals
import static org.junit.Assert.fail

class MoneybookersServiceInvokerTest extends AbstractBaseTest {
    MoneybookersServiceInvoker target

    MoneybookersResponseXmlParser parser
    HttpConnectionHelper httpConnectionHelper
    MessageSource messageSource
    BillfoldConfigService configService

    @Before void setUp() {
        target = new MoneybookersServiceInvokerImpl()

        parser = EasyMock.createMock(MoneybookersResponseXmlParser)
        addMock(target, 'parser', parser)

        httpConnectionHelper = EasyMock.createMock(HttpConnectionHelper)
        addMock(target, 'httpConnectionHelper', httpConnectionHelper)

        messageSource = EasyMock.createMock(MessageSource)
        addMock(target, 'messageSource', messageSource)

        configService = EasyMock.createMock(BillfoldConfigService)
        addMock(target, 'configService', configService)
    }

    @Test void sendMoney_no_currency() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_URL')).andReturn('some-url')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_SKRILL_WITHDRAWAL_EMAIL')).andReturn(['some-cur': 'some-email'])

        replay()
        try {
            target.send(1, 'some-ref', new BigDecimal('10'), 'wrong-cur', 'some-bnf-email', 'en')
            fail()
        } catch (PaymentGatewayException e) {
            assertEquals('PAYMENT_SKRILL_WITHDRAWAL_EMAIL is not configured for currency: wrong-cur', e.errorMessage)
        }
    }

    @Test void sendMoney() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_URL')).andReturn('some-url')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_SKRILL_WITHDRAWAL_EMAIL')).andReturn(['some-cur': 'some-email'])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_PASSWORD')).andReturn('some-password')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.payout.subject', null, new Locale('en'))).andReturn('some-subject')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.payout.note', null, new Locale('en'))).andReturn('some-note')
        def params = [action: 'prepare', email: 'some-email', password: '3a6e8affacd419b3dd24d8e4298e5799', amount: '10', currency: 'some-cur', bnf_email: 'some-bnf-email', subject: 'some-subject', note: 'some-note', frn_trn_id: 'some-ref']
        EasyMock.expect(httpConnectionHelper.post('some-url', params)).andReturn('some-xml')
        EasyMock.expect(parser.parse('some-xml')).andReturn(new MoneybookersResponse(sid: 'some-sid'))

        def params2 = [action: 'transfer', sid: 'some-sid']
        EasyMock.expect(httpConnectionHelper.post('some-url', params2)).andReturn('some-xml-2')
        EasyMock.expect(parser.parse('some-xml-2')).andReturn(new MoneybookersResponse(status: 1, statusMsg: 'some-msg', id: 'some-id', currency: 'some-cur', amount: new BigDecimal('1')))

        replay()
        def r = target.send(1, 'some-ref', new BigDecimal('10'), 'some-cur', 'some-bnf-email', 'en')
        assertEquals('some-id', r)
    }

    @Test void sendMoney_transfer_fails() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_URL')).andReturn('some-url')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_SKRILL_WITHDRAWAL_EMAIL')).andReturn(['some-cur': 'some-email'])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_PASSWORD')).andReturn('some-password')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.payout.subject', null, new Locale('en'))).andReturn('some-subject')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.payout.note', null, new Locale('en'))).andReturn('some-note')
        def params = [action: 'prepare', email: 'some-email', password: '3a6e8affacd419b3dd24d8e4298e5799', amount: '10', currency: 'some-cur', bnf_email: 'some-bnf-email', subject: 'some-subject', note: 'some-note', frn_trn_id: 'some-ref']
        EasyMock.expect(httpConnectionHelper.post('some-url', params)).andReturn('some-xml')
        EasyMock.expect(parser.parse('some-xml')).andReturn(new MoneybookersResponse(sid: 'some-sid'))

        def params2 = [action: 'transfer', sid: 'some-sid']
        EasyMock.expect(httpConnectionHelper.post('some-url', params2)).andReturn('some-xml-2')
        EasyMock.expect(parser.parse('some-xml-2')).andReturn(new MoneybookersResponse(status: 3, statusMsg: 'some-msg', errorMsg: 'some-error'))

        replay()
        try {
             target.send(1, 'some-ref', new BigDecimal('10'), 'some-cur', 'some-bnf-email', 'en')
            fail()
        } catch (PaymentGatewayException e) {
            assertEquals('Transfer failed, sid: some-sid. Status: 3. StatusMsg: some-msg. ErrorMsg: some-error.', e.errorMessage)
        }
    }

    @Test void sendMoney_prepare_fails() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_URL')).andReturn('some-url')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_SKRILL_WITHDRAWAL_EMAIL')).andReturn(['some-cur': 'some-email'])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_SKRILL_WITHDRAWAL_PASSWORD')).andReturn('some-password')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.payout.subject', null, new Locale('en'))).andReturn('some-subject')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.payout.note', null, new Locale('en'))).andReturn('some-note')
        def params = [action: 'prepare', email: 'some-email', password: '3a6e8affacd419b3dd24d8e4298e5799', amount: '10', currency: 'some-cur', bnf_email: 'some-bnf-email', subject: 'some-subject', note: 'some-note', frn_trn_id: 'some-ref']
        EasyMock.expect(httpConnectionHelper.post('some-url', params)).andReturn('some-xml')
        EasyMock.expect(parser.parse('some-xml')).andReturn(new MoneybookersResponse(errorMsg: 'some-error'))

        replay()
        try {
            target.send(1, 'some-ref', new BigDecimal('10'), 'some-cur', 'some-bnf-email', 'en')
            fail()
        } catch (PaymentGatewayException e) {
            assertEquals('Prepare failed. ErrorMsg: some-error.', e.errorMessage)
        }
    }

}
