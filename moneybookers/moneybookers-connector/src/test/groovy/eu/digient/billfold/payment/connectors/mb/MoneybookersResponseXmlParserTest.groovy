package eu.digient.billfold.payment.connectors.mb

import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

class MoneybookersResponseXmlParserTest {
    MoneybookersResponseXmlParser target

    @Before void setUp() {
        target = new MoneybookersResponseXmlParserImpl()
    }

    @Test void parseSid() {
        def xml = '''<?xml version="1.0" encoding="UTF-8"?> <response>
<sid>some-sid</sid> </response>'''

        def r = target.parse(xml)
        assertEquals 'some-sid', r.sid
    }

    @Test void parseError() {
        def xml = '''<?xml version="1.0" encoding="UTF-8"?> <response>
<error> <error_msg>some-error</error_msg>
     </error></response>'''

        def r = target.parse(xml)
        assertEquals 'some-error', r.errorMsg
    }

    @Test void parseTransaction() {
        def xml = '''<?xml version="1.0" encoding="UTF-8"?> <response>
<transaction> <amount>1.00</amount>
<currency>some-cur</currency> <id>some-id</id>
<status>2</status> <status_msg>processed</status_msg>
</transaction> </response>'''

        def r = target.parse(xml)
        assertTrue r.amount.compareTo(new BigDecimal('1.00')) == 0
        assertEquals 'some-cur', r.currency
        assertEquals 'some-id', r.id
        assertEquals 2, r.status
        assertEquals 'processed', r.statusMsg
    }
}
