package eu.digient.billfold.payment.providers.mb

import eu.digient.billfold.account.Product
import eu.digient.billfold.payment.CreateDepositResult
import eu.digient.billfold.payment.NoPaymentDetailsFilledException
import eu.digient.billfold.payment.PaymentMethod
import eu.digient.billfold.payment.PaymentMethodAttribute
import eu.digient.billfold.payment.PaymentProvider
import eu.digient.billfold.payment.connectors.mb.MoneybookersServiceInvoker
import eu.digient.billfold.support.annotations.BillfoldPropertyConfig
import eu.digient.billfold.support.annotations.BillfoldPropertyValue
import eu.digient.billfold.support.annotations.BillfoldPropertyValueContainer
import eu.digient.billfold.user.service.PaymentDetailService
import eu.digient.billfold.user.service.UserService
import eu.digient.sdk.lang.GStrings
import org.slf4j.LoggerFactory

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.PspProvider
import eu.digient.billfold.payment.Payment.PaymentProviderName
import eu.digient.billfold.payment.provider.AbstractPspProvider
import eu.digient.billfold.user.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.text.SimpleDateFormat

@Component(value = 'moneybookers')
@BillfoldPropertyConfig
class MoneybookersProvider extends AbstractPspProvider implements PspProvider {
    private static final def PROVIDER_LOG = LoggerFactory.getLogger('PROVIDER_LOG')
    private static final def LOG = LoggerFactory.getLogger(MoneybookersProvider)

    @Autowired UserService userService
    @Autowired PaymentDetailService paymentDetailService
    @Autowired MoneybookersServiceInvoker invoker

    MoneybookersProvider() {
        name = PaymentProviderName.moneybookers
    }

    CreateDepositResult createDeposit(Payment payment, User user, Map<String, Object> userInput) throws PaymentGatewayException, NoPaymentDetailsFilledException {
        switch (payment.paymentMethodId) {
            case 70002:
                return regularHPPPayment(payment, user, userInput)
            case 70003:
                return oneTab(payment, user, userInput)
        }
        LOG.error('[paymentMethodId: {}] is not supported by moneybookers provider', payment.paymentMethodId)
        throw new IllegalArgumentException()
    }

    private CreateDepositResult regularHPPPayment(Payment payment, User user, Map<String, Object> userInput) {
        def params = [:]
        def usedEmail = null
        def oldEmail = user.paymentDetail?.moneybookersEmail
        if (userInput?.containsKey('moneybookersEmail')) {
            params.pay_from_email = userInput.moneybookersEmail
            usedEmail = params.pay_from_email
        }

        if (params.pay_from_email == null) {
            params.pay_from_email = user.paymentDetail?.moneybookersEmail
            usedEmail = params.pay_from_email
        }

        if (params.pay_from_email == null) {
            LOG.error('[userId: {}] moneybookersEmail is not provided', user.userId)
            throw new NoPaymentDetailsFilledException(fieldName: 'moneybookersEmail')
        }

        params.currency = payment.currency
        params.amount = payment.amount
        params.transaction_id = payment.reference
        params.firstname = user.firstName
        params.lastname = user.lastName
        params.address = user.address
        params.city = user.city
        params.postal_code = user.zipCode
        params.country = user.country
        params.phone_number = user.phoneNumber
        params.rid = getPropertyAsString(user.siteId, 'RID')
        params.pay_to_email = getPayToEmail(user.siteId, payment.currency)
        params.cancel_url = getCancelledUrl(user.siteId)
        params.return_url = getOkUrl(user.siteId)
        params.status_url = getStatusUrl(user.siteId, 'moneybookers/notification.html')
        params.status_url2 = getStatusEmail(user.siteId)
        params.redirectUrl = getRedirectUrl(user.siteId)

        params.language = user.language.toUpperCase()
        def supportedLanguages = GStrings.parseList(configService.getPropertyAsString(user.siteId, 'PAYMENT_MONEYBOOKERS_SUPPORTED_LANGUAGES'))
        if (!supportedLanguages.contains(params.language)) {
            params.language = 'EN'
        }
        def BIRTH_DATE_FORMAT = new SimpleDateFormat("ddMMyyyy")
        params.date_of_birth = BIRTH_DATE_FORMAT.format(user.birthDate)
        def lang = user.language
        params.recipient_description = getMessage('payment.moneybookers.recipient_description', lang)
        params.return_url_text = getMessage('payment.moneybookers.return_url_text', lang)
        params.detail1_description = getMessage('payment.moneybookers.detail1_description', lang)
        params.detail1_text = getMessage('payment.moneybookers.detail1_text', lang)
        params.detail2_description = getMessage('payment.moneybookers.detail2_description', lang, null)
        params.detail2_text = getMessage('payment.moneybookers.detail2_text', lang, null)
        params.detail3_description = getMessage('payment.moneybookers.detail3_description', lang, null)
        params.detail3_text = getMessage('payment.moneybookers.detail3_text', lang, null)
        params.detail4_description = getMessage('payment.moneybookers.detail4_description', lang, null)
        params.detail4_text = getMessage('payment.moneybookers.detail4_text', lang, null)
        params.detail5_description = getMessage('payment.moneybookers.detail5_description', lang, null)
        params.detail5_text = getMessage('payment.moneybookers.detail5_text', lang, null)

        if (oldEmail != usedEmail) {
            paymentDetailService.updateMoneybookersEmail(user.userId, usedEmail)
        }

        PROVIDER_LOG.info '[{}] [{}] createDeposit [SUCCESS] [{}]', payment.reference, name, payment.amount
        new CreateDepositResult(formParams: params, formatUrls: ['cancel_url', 'return_url'], providerReference: params.transaction_id, redirectUrl: params.redirectUrl, updatePayment: [moneybookersEmail: usedEmail])
    }

    private CreateDepositResult oneTab(Payment payment, User user, Map<String, Object> userInput) {
        def r = null
        if (user.paymentDetail.moneybookersrRecPaymentId?.size() > 0) {
            r = subsequentOneTapPayment(payment, user)
        }
        else {
            r = initOneTapPayment(payment, user, userInput)
            r.redirectUrl = getRedirectUrl(user.siteId)
        }

        r
    }

    private CreateDepositResult initOneTapPayment(Payment payment, User user, Map<String, Object> userInput) {
        def createDepositResult = regularHPPPayment(payment, user, userInput)

        createDepositResult.formParams.ondemand_max_amount = payment.maxAmount
        createDepositResult.formParams.ondemand_max_currency = payment.currency
        createDepositResult.formParams.ondemand_note = getMessage('payment.moneybookers.ondemand.note', user.language)
        createDepositResult.dynamicFormFields = ['ondemand_max_amount', 'ondemand_max_currency', 'ondemand_note']

        createDepositResult
    }

    private CreateDepositResult subsequentOneTapPayment(Payment payment, User user) {
        def createDepositResult = invoker.send1Tab(user.siteId, payment.reference, payment.amount, payment.currency, user.paymentDetail.moneybookersrRecPaymentId, user.language)
        if (createDepositResult.success) {
            createDepositResult.redirectUrl = getOkUrl(user.siteId)
        }
        else {
            createDepositResult.redirectUrl = getFailedUrl(user.siteId)
        }

        createDepositResult.formTarget = "_top"
        createDepositResult.formMethod = "REDIRECT"

        // set formField to empty list will prevent PaymentService fetching
        // the configuration (PAYMENT_MONEYBOOKERS_FORM_FIELDS) from database
        createDepositResult.formFields = []

        createDepositResult
    }

    private String getPayToEmail(final Integer siteId, final String currency) {
        def payTo = getPropertyAsMetadata(siteId, 'PAY_TO_EMAIL')
        if (payTo.containsKey(currency)) {
            return payTo.get(currency)
        }
        payTo.get('default')
    }


    @Override
    Collection<Product> getProductConfiguration() {
        [
                new Product(productId: 11101L, name: 'moneybookers', productCategory: Product.ProductCategory.payment),
                new Product(productId: 11404L, name: 'skrill_withdrawal', productCategory: Product.ProductCategory.payment)
        ]
    }

    @Override
    Collection<PaymentProvider> getProviderConfiguration()  {
        [
                new PaymentProvider(provider: PaymentProviderName.moneybookers, name: 'moneybookers',
                        paymentMethods: [
                                new PaymentMethod(paymentMethodId: 70002L, productId: 11101L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'Skrill pay from email', direction: Payment.PaymentDirection.deposit, paymentDetailType: 3,
                                        fields: [
                                                new PaymentMethodAttribute(field: 'moneybookersEmail',  optional: false, minLength: 1, maxLength: 100, fieldType: 'text', userField: 'moneybookersEmail', targetField: 'moneybookersEmail', paymentField: 'moneybookersEmail')
                                        ]),
                                new PaymentMethod(paymentMethodId: 70003L, productId: 11101L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'Skrill 1-tab', direction: Payment.PaymentDirection.deposit)
                        ]
                ),
                new PaymentProvider(provider: PaymentProviderName.skrill_withdrawal, name: 'skrill_withdrawal',
                        paymentMethods: [
                                new PaymentMethod(paymentMethodId: 1003L, productId: 11404L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'Skrill payout', supportDirect: true, directEnabled: true, direction: Payment.PaymentDirection.withdraw, paymentDetailType: 3,
                                        fields: [
                                                new PaymentMethodAttribute(field: 'moneybookersEmail',  optional: false, minLength: 1, maxLength: 100, fieldType: 'text', userField: 'moneybookersEmail', targetField: 'moneybookersEmail', paymentField: 'moneybookersEmail')
                                        ])
                        ]
                )
        ]
    }

    @BillfoldPropertyValueContainer(values = [
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_PAY_TO_EMAIL',
                    defaultValue = 'EUR=some-pay-to-email;USD=some-pay-to-email;GBP=some-pay-to-email'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_SECRET_WORD',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_RID',
                    defaultValue = '0'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_FORM_METHOD',
                    defaultValue = 'POST'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_FORM_TARGET',
                    defaultValue = '_top'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_FORM_FIELDS',
                    defaultValue = 'amount,address,cancel_url,city,country,currency,date_of_birth,detail1_description,detail1_text,firstname,language,lastname,pay_from_email,pay_to_email,phone_number,postal_code,recipient_description,return_url,return_url_text,status_url,rid,transaction_id'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_PROD',
                    defaultValue = 'https://www.moneybookers.com/app/payment.pl'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_TEST',
                    defaultValue = 'https://www.moneybookers.com/app/payment.pl'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_ONE_TAP_EMAIL',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.36',
                    key = 'PAYMENT_MONEYBOOKERS_ONE_TAP_PASSWORD',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.37',
                    key = 'PAYMENT_MONEYBOOKERS_ONE_TAP_URL',
                    defaultValue = 'https://www.moneybookers.com /app/ondemand_request.pl'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 25,
                    version = '1.37',
                    key = 'PAYMENT_MONEYBOOKERS_SUPPORTED_LANGUAGES',
                    defaultValue = 'EN,DE,ES,FR,IT,PL,GR,RO,RU,CN,CZ,NL,DA,SV,FI'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 26,
                    version = '1.36',
                    key = 'PAYMENT_SKRILL_WITHDRAWAL_EMAIL',
                    defaultValue = 'EUR=some-pay-from-email;USD=some-pay-from-email;GBP=some-pay-from-email'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 26,
                    version = '1.36',
                    key = 'PAYMENT_SKRILL_WITHDRAWAL_PASSWORD',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 26,
                    version = '1.36',
                    key = 'PAYMENT_SKRILL_WITHDRAWAL_URL',
                    defaultValue = 'https://www.moneybookers.com/app/pay.pl'
            ),
    ])
    void billfoldProperties() {}
}