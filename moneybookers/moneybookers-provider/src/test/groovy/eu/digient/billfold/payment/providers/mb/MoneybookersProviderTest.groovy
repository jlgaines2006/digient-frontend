package eu.digient.billfold.payment.providers.mb

import eu.digient.billfold.payment.CreateDepositResult
import eu.digient.billfold.payment.NoPaymentDetailsFilledException
import eu.digient.billfold.support.service.SiteApplicationConfig
import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PspProvider
import eu.digient.billfold.payment.connectors.mb.MoneybookersServiceInvoker
import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.billfold.support.service.TimeService
import eu.digient.billfold.user.PaymentDetail
import eu.digient.billfold.user.service.PaymentDetailService
import eu.digient.billfold.user.service.UserService
import eu.digient.test.AbstractBaseTest
import org.easymock.EasyMock
import org.junit.Before
import org.junit.Test
import eu.digient.billfold.user.User
import org.springframework.context.MessageSource

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

class MoneybookersProviderTest extends AbstractBaseTest {
    PspProvider target

    TimeService timeService
    UserService userService
    BillfoldConfigService configService
    PaymentDetailService paymentDetailService
    MessageSource messageSource
    MoneybookersServiceInvoker invoker

    @Before void setUp() {
        target = new MoneybookersProvider()

        timeService = EasyMock.createMock(TimeService)
        addMock(target, 'timeService', timeService)

        userService = EasyMock.createMock(UserService)
        addMock(target, 'userService', userService)

        messageSource = EasyMock.createMock(MessageSource)
        addMock(target, 'messageSource', messageSource)

        configService = EasyMock.createMock(BillfoldConfigService)
        addMock(target, 'configService', configService)

        invoker = EasyMock.createMock(MoneybookersServiceInvoker)
        addMock(target, 'invoker', invoker)

        paymentDetailService = EasyMock.createMock(PaymentDetailService)
        addMock(target, 'paymentDetailService', paymentDetailService)
    }

    @Test
    void createDeposit_moneybookersEmail_from_user_profile() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_RID')).andReturn('some-rid')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_MONEYBOOKERS_PAY_TO_EMAIL')).andReturn(['default': 'pay-to-email'])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_CANCELLED')).andReturn('url-cancelled')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).andReturn('url-ok')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_NOTIFICATION_BASE')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_EMAIL_NOTIFICATION')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsBoolean(1, 'PAYMENT_MODE')).andReturn(false)
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_TEST')).andReturn('some-redirect-url')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SUPPORTED_LANGUAGES')).andReturn('en')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.recipient_description', null, new Locale('en'))).andReturn('some-1')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.return_url_text', null, new Locale('en'))).andReturn('some-2')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_description', null, new Locale('en'))).andReturn('detail-1')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_text', null, new Locale('en'))).andReturn('text-1')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_description', null, new Locale('en'))).andReturn('detail-2')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_text', null, new Locale('en'))).andReturn('text-2')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_description', null, new Locale('en'))).andReturn('detail-3')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_text', null, new Locale('en'))).andReturn('text-3')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_description', null, new Locale('en'))).andReturn('detail-4')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_text', null, new Locale('en'))).andReturn('text-4')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_description', null, new Locale('en'))).andReturn('detail-5')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_text', null, new Locale('en'))).andReturn('text-5')

        replay()
        def pmt = new Payment(paymentMethodId: 70002, paymentIp: 'some-ip', reference: 'some-ref', currency: 'EUR')
        def user = new User(siteId: 1, birthDate: new Date(1L), userId: 1L, language: 'en', paymentDetail: new PaymentDetail(moneybookersEmail: 'some-email'))
        def r = target.createDeposit(pmt, user, [:])
        assertEquals 'some-email', r.updatePayment.moneybookersEmail
        assertEquals 'some-redirect-url', r.redirectUrl
        assertEquals 'some-ref', r.providerReference
        assertEquals 'email-noti/moneybookers/notification.html', r.formParams.status_url
        assertEquals 'some-rid', r.formParams.rid
        assertEquals 'email-noti', r.formParams.status_url2
        assertEquals 'url-cancelled', r.formParams.cancel_url
        assertEquals 'url-ok', r.formParams.return_url
        assertEquals '01011970', r.formParams.date_of_birth
        assertEquals 'EN', r.formParams.language
        assertEquals 'pay-to-email', r.formParams.pay_to_email
        assertEquals 'some-email', r.formParams.pay_from_email
        assertEquals 'some-1', r.formParams.recipient_description
        assertEquals 'some-2', r.formParams.return_url_text
        assertEquals 'detail-1', r.formParams.detail1_description
        assertEquals 'text-1', r.formParams.detail1_text
        assertEquals 'detail-2', r.formParams.detail2_description
        assertEquals 'text-2', r.formParams.detail2_text
        assertEquals 'detail-3', r.formParams.detail3_description
        assertEquals 'text-3', r.formParams.detail3_text
        assertEquals 'detail-4', r.formParams.detail4_description
        assertEquals 'text-4', r.formParams.detail4_text
        assertEquals 'detail-5', r.formParams.detail5_description
        assertEquals 'text-5', r.formParams.detail5_text
        assertEquals(['cancel_url', 'return_url'], r.formatUrls)
    }

    @Test
    void createDeposit_moneybookersEmail_from_user_input() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_RID')).andReturn('some-rid')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_MONEYBOOKERS_PAY_TO_EMAIL')).andReturn([:])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_CANCELLED')).andReturn('url-cancelled')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).andReturn('url-ok')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_NOTIFICATION_BASE')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_EMAIL_NOTIFICATION')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsBoolean(1, 'PAYMENT_MODE')).andReturn(false)
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_TEST')).andReturn('some-redirect-url')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SUPPORTED_LANGUAGES')).andReturn('en')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.recipient_description', null, new Locale('en'))).andReturn('some-1')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.return_url_text', null, new Locale('en'))).andReturn('some-2')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_description', null, new Locale('en'))).andReturn('some-3')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_text', null, new Locale('en'))).andReturn('some-4')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_text', null, new Locale('en'))).andReturn(null)

        paymentDetailService.updateMoneybookersEmail(1L, 'some-email')
        EasyMock.expectLastCall().once()

        replay()

        def pmt = new Payment(paymentMethodId: 70002, paymentIp: 'some-ip', currency: 'EUR')
        def user = new User(siteId: 1, birthDate: new Date(1L), userId: 1L, language: 'en', paymentDetail: new PaymentDetail())

        def r = target.createDeposit(pmt, user, [moneybookersEmail: 'some-email', password: 'some-pwd'])
        assertEquals 'some-email', r.updatePayment.moneybookersEmail
    }

    @Test
    void createDeposit_moneybookersEmail_from_user_input_but_same_as_old() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_RID')).andReturn('some-rid')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_MONEYBOOKERS_PAY_TO_EMAIL')).andReturn(['EUR': 'pay-to-email'])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_CANCELLED')).andReturn('url-cancelled')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).andReturn('url-ok')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_NOTIFICATION_BASE')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_EMAIL_NOTIFICATION')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsBoolean(1, 'PAYMENT_MODE')).andReturn(false)
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_TEST')).andReturn('some-redirect-url')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SUPPORTED_LANGUAGES')).andReturn('en')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.recipient_description', null, new Locale('en'))).andReturn('some-1')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.return_url_text', null, new Locale('en'))).andReturn('some-2')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_description', null, new Locale('en'))).andReturn('some-3')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_text', null, new Locale('en'))).andReturn('some-4')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_text', null, new Locale('en'))).andReturn(null)

        replay()

        def pmt = new Payment(paymentMethodId: 70002, paymentIp: 'some-ip', currency: 'EUR')
        def user = new User(siteId: 1, birthDate: new Date(1L), userId: 1L, language: 'en', paymentDetail: new PaymentDetail(moneybookersEmail: 'some-email'))

        def r = target.createDeposit(pmt, user, [moneybookersEmail: 'some-email'])
        assertEquals 'some-email', r.updatePayment.moneybookersEmail
    }

    @Test
    void createDeposit_no_moneybookersEmailProvided() {
        replay()

        def pmt = new Payment(paymentMethodId: 70002)
        try {
            target.createDeposit(pmt, new User(siteId: 1), null)
        } catch (NoPaymentDetailsFilledException e) {
            assertEquals('moneybookersEmail', e.fieldName)
        }
    }

    @Test
    void createDeposit_1Tap_firstTime() {
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_RID')).andReturn('some-rid')
        EasyMock.expect(configService.getPropertyAsMetadata(1, 'PAYMENT_MONEYBOOKERS_PAY_TO_EMAIL')).andReturn(['some-cur': 'pay-to-email'])
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_CANCELLED')).andReturn('url-cancelled')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).andReturn('url-ok')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_NOTIFICATION_BASE')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_EMAIL_NOTIFICATION')).andReturn('email-noti')
        EasyMock.expect(configService.getPropertyAsBoolean(1, 'PAYMENT_MODE')).andReturn(false)
        EasyMock.expect(configService.getPropertyAsBoolean(1, 'PAYMENT_MODE')).andReturn(false)
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_TEST')).andReturn('some-redirect-url')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_REDIRECT_URL_TEST')).andReturn('some-redirect-url')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_MONEYBOOKERS_SUPPORTED_LANGUAGES')).andReturn('en')

        EasyMock.expect(messageSource.getMessage('payment.moneybookers.recipient_description', null, new Locale('en'))).andReturn('some-1')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.return_url_text', null, new Locale('en'))).andReturn('some-2')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_description', null, new Locale('en'))).andReturn('some-3')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail1_text', null, new Locale('en'))).andReturn('some-4')
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail2_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail3_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail4_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_description', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.detail5_text', null, new Locale('en'))).andReturn(null)
        EasyMock.expect(messageSource.getMessage('payment.moneybookers.ondemand.note', null, new Locale('en'))).andReturn('some-translation')

        replay()

        def pmt = new Payment(maxAmount: new BigDecimal('1'), paymentMethodId: 70003, paymentIp: 'some-ip', currency: 'some-cur')
        def user = new User(siteId: 1, birthDate: new Date(1L), userId: 1L, language: 'en', paymentDetail: new PaymentDetail(moneybookersEmail: 'some-email'))

        def r = target.createDeposit(pmt, user, [:])
        assertTrue(r.formParams.ondemand_max_amount.compareTo(new BigDecimal('1')) == 0)
        assertEquals(r.formParams.ondemand_max_currency, 'some-cur')
        assertEquals(r.formParams.ondemand_note, 'some-translation')
        assertEquals('some-redirect-url', r.redirectUrl)
    }

    @Test
    void createDeposit_1Tap_secondTime_success() {
        EasyMock.expect(invoker.send1Tab(1, 'some-ref', new BigDecimal('1'), 'some-cur', 'some-rec-id', 'some-lang')).andReturn(new CreateDepositResult(success: true))
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).andReturn('some-url')

        replay()

        def pmt = new Payment(reference: 'some-ref', paymentMethodId: 70003, paymentIp: 'some-ip', amount: new BigDecimal('1'), currency: 'some-cur')
        def user = new User(siteId: 1, language: 'some-lang', paymentDetail: new PaymentDetail(moneybookersrRecPaymentId: 'some-rec-id'))

        def r = target.createDeposit(pmt, user, [:])
        assertEquals(r.formTarget, '_top')
        assertEquals(r.formMethod, 'REDIRECT')
        assertEquals('some-url', r.redirectUrl)
    }

    @Test
    void createDeposit_oneTap_secondTime_error() {
        EasyMock.expect(invoker.send1Tab(1, 'some-ref', new BigDecimal('1'), 'some-cur', 'some-rec-id', 'some-lang')).andReturn(new CreateDepositResult(success: false))
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_FAILED')).andReturn('some-url')

        replay()

        def pmt = new Payment(reference: 'some-ref', paymentMethodId: 70003, paymentIp: 'some-ip', amount: new BigDecimal('1'), currency: 'some-cur')
        def user = new User(siteId: 1, language: 'some-lang', paymentDetail: new PaymentDetail(moneybookersrRecPaymentId: 'some-rec-id'))

        def r = target.createDeposit(pmt, user, [:])
        assertEquals(r.formTarget, '_top')
        assertEquals(r.formMethod, 'REDIRECT')
        assertEquals(0, r.formFields.size())
        assertEquals('some-url', r.redirectUrl)
    }

}
