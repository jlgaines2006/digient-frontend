// Here are listed the game provider -specific window widths, heights and aspect ratios that are needed when opening games.
// The height and aspect ratio include the possible footer of the window.
// NOTE: USED ONLY WHEN GAMES ARE OPENED IN NEW BROWSER WINDOWS

// Return the string of parameters for the game window and based on the provider-specific aspect ratio
function calculateWindowDimensions(options) {

    options = options || {};

    windowRatio = parseFloat(options.windowRatio) || window['windowAspectRatio_default'] || 1;
    fixedWidth = parseInt(options.fixedWidth, 10) || 0;
    fixedHeight = parseInt(options.fixedHeight, 10) || 0;
    widthOffset = parseInt(options.widthOffset, 10) || 0;
    heightOffset = parseInt(options.heightOffset, 10) || window['windowHeightOffset_default'] || 0;

	var windowWidth, windowHeight, windowZoom = 0.8,
        w = window, d = document, e = d.documentElement, g = d.getElementsByTagName('body')[0],
		screenHeight = screen.height || w.innerHeight || e.clientHeight || g.clientHeight,
        screenWidth  = screen.width  || w.innerWidth  || e.clientWidth  || g.clientWidth,
        windowMaxHeight = Math.floor(screenHeight * windowZoom),
        windowMaxWidth = Math.floor(screenWidth * windowZoom),
        screenRatio  = screenWidth / screenHeight;

    if (fixedWidth > 0 && fixedHeight > 0) {
        windowWidth = fixedWidth < screenWidth ? fixedWidth : screenWidth;
        windowHeight = fixedHeight < screenHeight ? fixedHeight : screenHeight;
    }
    else if (fixedWidth > 0) {
        windowWidth = fixedWidth < screenWidth ? fixedWidth : screenWidth;
        windowHeight = Math.floor(windowWidth / windowRatio);
        windowHeight = windowHeight < screenHeight ? windowHeight : screenHeight;
    }
    else if (fixedHeight > 0) {
        windowHeight = fixedHeight < screenHeight ? fixedHeight : screenHeight;
        windowWidth = Math.floor(windowHeight * windowRatio);
        windowWidth = windowWidth < screenWidth ? windowWidth : screenWidth
    }
    else {
        if (screenRatio > windowRatio) {
            windowHeight = windowMaxHeight;
            windowWidth = Math.floor((windowHeight - heightOffset) * windowRatio + widthOffset);
            if (windowWidth > windowMaxWidth) {
                windowWidth = windowMaxWidth;
                windowHeight = Math.floor((windowWidth - widthOffset) / windowRatio + heightOffset);
            }
        }
        else {
            windowWidth = windowMaxWidth;
            windowHeight = Math.floor((windowWidth - widthOffset) / windowRatio + heightOffset);
            if (windowHeight > windowMaxHeight) {
                windowHeight = windowMaxHeight;
                windowWidth = Math.floor((windowHeight - heightOffset) * windowRatio + widthOffset);
            }
        }
    }

    return 'width=' + windowWidth + ',height=' + windowHeight + ',top=' + ((screenHeight - windowHeight) / 2) + ',left=' + ((screenWidth - windowWidth) / 2);
    
}

// Return the values of width, height and positional parameters and based on the specific aspect ratio
function calculateBoxDimensions(options) {

    options = options || {};

    boxRatio = parseFloat(options.boxRatio) || window['windowAspectRatio_default'] || 1;
    widthOffset = parseInt(options.widthOffset, 10) || 0;
    heightOffset = parseInt(options.heightOffset, 10) || 0;

    var boxWidth, boxHeight, boxZoom = 1,
        w = window, d = document, e = d.documentElement, g = d.getElementsByTagName('body')[0],
        wrapperHeight = w.innerHeight || e.clientHeight || g.clientHeight,
        wrapperWidth  = w.innerWidth  || e.clientWidth  || g.clientWidth,
        boxMaxHeight = Math.floor(wrapperHeight * boxZoom),
        boxMaxWidth = Math.floor(wrapperWidth * boxZoom),
        wrapperRatio  = wrapperWidth / wrapperHeight;

    if (wrapperRatio > boxRatio) {
        boxHeight = boxMaxHeight;
        boxWidth = Math.floor((boxHeight - heightOffset) * boxRatio + widthOffset);
        if (boxWidth > boxMaxWidth) {
            boxWidth = boxMaxWidth;
            boxHeight = Math.floor((boxWidth - widthOffset) / boxRatio + heightOffset);
        }
    }
    else {
        boxWidth = boxMaxWidth;
        boxHeight = Math.floor((boxWidth - widthOffset) / boxRatio + heightOffset);
        if (boxHeight > boxMaxHeight) {
            boxHeight = boxMaxHeight;
            boxWidth = Math.floor((boxHeight - heightOffset) * boxRatio + widthOffset);
        }
    }

    return {
        width: boxWidth,
        height: boxHeight,
        top: (wrapperHeight - boxHeight) / 2,
        left: (wrapperWidth - boxWidth) / 2
    };

}


// Aspect ratios (4:3 = 1.333, 16:9 = 1.778)
var windowAspectRatio_default       = 1.333,
    windowAspectRatio_provider_1    = 1.333,    // Microgaming
    windowAspectRatio_provider_2    = 1.778,    // Airdice (billfold)
    windowAspectRatio_provider_3    = 1.333,    // Play'n GO
    windowAspectRatio_provider_4    = 1.548,    // Stream
    windowAspectRatio_provider_6    = 1.333,    // Betsoft Gaming
    windowAspectRatio_provider_10   = 1.333,    // Betradar Gaming
    windowAspectRatio_provider_13   = 1.778,    // HPG
    windowAspectRatio_provider_14   = 1.333,    // SVG
    windowAspectRatio_provider_15   = 1.778,    // SVG
    windowAspectRatio_provider_19   = 1.778;    // GGL

// Fixed width
var windowFixedWidth_provider_4     = 960,      // Ezugi
    windowFixedWidth_provider_10    = 1125;     // Betradar Gaming

// Fixed height
var windowFixedHeight_provider_4    = 620;      // Ezugi

// Height of the game window bottom panel
var windowHeightOffset_default      = 60;       // px