//----------------------------------------------------------------------------------------------------------------------
// General validation
//

// Check if element "id" is empty
function checkEmpty(id) {

    var element = $("#" + id),
        length = element.val().length;

    if( length < 1 || element.val().replace(/(\s)/g, '').length == 0 ) {
        return false;
    }

    return true;
}

function checkEmptyElement(element) {

    var length = element.val().length;

    if( length < 1 || element.val().replace(/(\s)/g, '').length == 0 ) {
        return false;
    }

    return true;
}

function validateElementLength(element, requiredMinLength) {
    var length = element.val().length;
    if (length < requiredMinLength) return false;
    else return true;
}

function checkEmptyAndShowResult(id) {
    if (checkEmpty(id)) {
        $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        $("#" + id + "_error").html("");
        return true;
    }
    else {
        $("#"+ id +"_validOK").html("");
        $("#" + id + "_error").html(msgValidatorEmpty);
        return false;
    }
}

// Removal of duplicate spaces and change their to one
function removeDuplicatedSpaces(id) {

    var element = $("#" + id);

    return element.val().trim().replace(/[\s]{2,}/g, ' ');
}

// Check for spaces
function noSpaces(id) {

    var element = $("#" + id);
    if (element.val().length > 0 && element.val().replace(/(\s)/g, '').length == 0) return false;

    return true;
}

// Remove the spaces
function removeSpaces(id) {

    var element = $("#" + id);

    return element.val().replace(/(\s)/g, '');
}

// Check if email is valid
function isEmail(id) {

	var isemail = (/^\w+((-\w+)|(\.\w+)|(\+\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/);
    var email = $("#" + id).val();

    if(!isemail.test(email)) {
        return false;
    }

    return true;
}

// Validation of email
function validateEmailAndShowResult(id) {
    if (!checkEmptyAndShowResult(id)) return false;
    if (!isEmail(id)) {
        $("#"+ id +"_validOK").html("");
        $("#" + id + "_error").html(msgValidatorEmail);
        return false;
    } else {
        $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        $("#" + id + "_error").html("");
        return true;
    }
}

// Validation of text inputs - numbers, decimals and phone numbers
function validateInputs(event, typeOfInput) {

    event = event || window.event;

    // Don't process special keys except Shift
    if (event.ctrlKey || event.altKey || event.metaKey) return;

    var regex;
    switch (typeOfInput) {
        case 'alphabetical':
            regex = /^[a-zA-Z]$/;
        break;
        case 'number':
            regex = /^[0-9]$/;
        break;
        case 'phone':
            regex = /^[+0-9\-\s]$/;
        break;
        case 'date':
            regex = /^[0-9\-\s]$/;
        break;
        case 'decimal':
            regex = /^[.0-9]$/;
        break;
        case 'bankInfo':
            regex = /^[a-zA-Z0-9\-\s]$/;
        break;
        case 'zipCode':
            regex = /^[^\s]$/;
        break;
        case 'passportMRZ':
            regex = /^[a-zA-Z0-9<]$/;
        break;
        case 'userAccounts':
            regex = /^[a-zA-Z0-9\.,\-_]$/;
            break;
        default:
            regex = /^.$/;
        break;
    }

    var char = getKeypressChar(event);
    if (char == null || regex.test(char)) return;

    event.preventDefault ? event.preventDefault() : (event.returnValue=false);

}

//----------------------------------------------------------------------------------------------------------------------
// Registration related validation
//

// Check if register field "id" is empty
function checkEmptyRegisterfield(id) {
	if (showRegisterErrorMessages) {
		$('#' + id + '_error').empty();
		$('#' + id + '_validOK').empty();
	}

    // Dealing with ssn separate entry fields
    if (id == ssnID) {
		var ssnYear = $('#' + ssnYearID).val() || '',
			ssnMonth = $('#' + ssnMonthID).val() || '',
			ssnDate = $('#' + ssnDateID).val() || '',
			ssnNumber = $('#' + ssnNumberID).val() || '';
		$('#' + ssnID).val(ssnYear + ssnMonth + ssnDate + ssnNumber);
    }

	if (!checkEmpty(id)) {
		if (showRegisterErrorMessages) {
			$('#' + id + '_error').html(msgValidatorEmpty);
			showRegisterErrorMessages = false;
		}
		return false;
	}

	$('#' + id + '_validOK').html('<i title="OK" class="fa fa-check fa-lg"></i>');
	return true;
}

// Check if password is long enough
function checkRegisterMinFiledLength(id, minLength) {

	var element = $("#" + id),
        length = $("#" + id).val().length;

	if(showRegisterErrorMessages) {
		$("#"+ id +"_error").html("");
		$("#"+ id +"_validOK").html("");
	}

	if(length >= minLength && element.val().replace(/(\s)/g, '').length >= minLength) {

		if(showRegisterErrorMessages) {
			$("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
		}
		return true;
	}

	if(showRegisterErrorMessages) {
		var error_string = replaceParams(msgValidatorMinlength,[minLength.toString()]);
		$("#"+ id +"_error").html(error_string);
	}
	return false;
}

function checkSSN() {
	if (!checkEmptyRegisterfield(ssnID)) {
		return false;
	}

	var ssnYear = $('#' + ssnYearID).val() || '',
		ssnMonth = $('#' + ssnMonthID).val() || '',
		ssnDate = $('#' + ssnDateID).val() || '',
		ssnNumber = $('#' + ssnNumberID).val() || '',
		ssn = ssnYear + ssnMonth + ssnDate + ssnNumber;

	$('#' + ssnID).val(ssn);

	if (ssn.length !== 11) {
		if (showRegisterErrorMessages) {
			$('#' + ssnID + '_error').html(msgValidatorInvalidData);
			showRegisterErrorMessages = false;
		}
		$('#' + ssnID + '_validOK').empty();
		return false;
	}

	$('#' + ssnID + '_error').empty();
	$('#' + ssnID + '_validOK').html('<i title="OK" class="fa fa-check fa-lg"></i>');
	return true;
}

function checkCitizenshipNumber(id, checkCorrespondence) {
	checkCorrespondence = !!checkCorrespondence;

	var $error = $('#' + id + '_error'),
		$mark = $('#' + id + '_validOK'),
		value = $('#' + id).val() || '',
		errorMessage = !value ? msgValidatorEmpty :
		               !isChecksumValid() ? msgValidatorCitizenshipNumber :
		               !doesCorrespondToBirthDate() ? msgValidatorCitizenshipNumberBirthday :
		               !doesCorrespondToGender() ? msgValidatorCitizenshipNumberGender : '';

	if (showRegisterErrorMessages) {
		$error.html(errorMessage);
		$mark.html(!errorMessage ? '<i title="OK" class="fa fa-check fa-lg"></i>' : '');
	}

	return !errorMessage;

	function isChecksumValid() {
		var checksumTable = [2, 4, 8, 5, 10, 9, 7, 3, 6, 0],
			sum = 0;

		if (!/^\d{10}$/.test(value)) {
			return false;
		}
		for (var i = 0; i < value.length; i++) {
			sum += checksumTable[i] * value[i];
		}

		return sum % 11 % 10 == value[9];
	}

	function doesCorrespondToBirthDate() {
		if (!checkCorrespondence) return true;

		var year = $('#' + birthYearID).val() || '0000',
			month = ('0' + ((+$('#' + birthMonthID).val() + (year > 1999 ? 40 : 0)) || '00')).slice(-2),
			day = ('0' + ($('#' + birthDateID).val() || '00')).slice(-2),
			date = (year).slice(-2) + month + day;

		return date === '000000' || date === value.substring(0, 6);
	}

	function doesCorrespondToGender() {
		if (!checkCorrespondence) return true;

		var gender = $('input[name=gender]:radio:checked').val() || '',
			isEven = !(value[8] % 2);

		return !gender || isEven && gender === 'male' || !isEven && gender === 'female';
	}
}

// Check the password safety based on the selected "strength":
// strong: password must contain at least one capital letter and one number
// medium: it is enough that the password is 6+ characters (we can skip this validation)
function checkPasswordSafety(id) {

	if(passwordStrength === "strong") {

		var pwd = $("#" + id).val();

		if ( pwd.match(/[A-Z]/) && pwd.match(/\d/) ) {
			return true;
		}
		else {

			if(showRegisterErrorMessages) {
				$("#"+ id +"_validOK").html('');
				$("#"+ id +"_error").html(msgPwdNotSafe);
			}
			return false;
		}
	}
	else if(passwordStrength === "medium") {

		return true;
	}
}

// Check if password1 is same as password2 (they must be)
function checkConfirmPassword() {

	var pwd1 = $("#" + pwd1ID).val(),
		pwd2 = $("#" + pwd2ID).val();

	if(showRegisterErrorMessages) {
		$("#"+ pwd2ID +"_error").html("");
		$("#"+ pwd2ID +"_validOK").html("");
	}

	if(pwd1.length != pwd2.length || pwd1 != pwd2) {

		if(showRegisterErrorMessages) {
			var error_string = replaceParams(msgValidatorConfirmPassword);
			$("#"+ pwd2ID +"_error").html(error_string);
		}
		return false;
	}

	if(showRegisterErrorMessages) {
		$("#"+ pwd2ID +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
	}
	return true;
}

// Check if email-field in register form has same values as confirmed email -field (they must be)
function checkConfirmEmail() {

	var email1 = $("#" + emailID).val(),
		email2 = $("#" + emailConfID).val();

	if(showRegisterErrorMessages) {
		$("#"+ emailConfID +"_error").html("");
		$("#"+ emailConfID +"_validOK").html("");
	}

	if(email1.length != email2.length || email1 != email2) {

		if(showRegisterErrorMessages) {
			var error_string = replaceParams(msgValidatorConfirmEmail);
			$("#"+ emailConfID +"_error").html(error_string);
		}
		return false;
	}

	if(showRegisterErrorMessages) {
		$("#"+ emailConfID +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
	}
	return true;
}

// Check if value of element "id" is alphanumeric
function isAlphaNumeric(id) {

	var element = $("#" + id);
	var usern = /^[a-zA-Z0-9_]{1,}$/;

	if(showRegisterErrorMessages) {
		$("#" + id + "_error").html("");
		$("#"+ id +"_validOK").html("");
	}

	if (!usern.test(element.val())) {

		if(showRegisterErrorMessages) {
			var error_string = msgValidatorAlphanumeric;
			$("#"+ id +"_error").html(error_string);
		}
		return false;
	}

	return true;
}

// Check if the given field is duplicate or not
function checkDuplication(fieldId, forcibly) {

	var value = $("#" + fieldId).val();
	var notDuplicate = false;

	value = value.replace(/\+/g,'%2B');

	if(showRegisterErrorMessages) {
		$("#"+ fieldId +"_error").html("");
		$("#"+ fieldId +"_validOK").html("");
	}

	var urlParam = "";

	if(fieldId == emailID) { urlParam = "email"; }
	else if(fieldId == nnameID) { urlParam = "nickname"; }
    else if(fieldId == unameID) { urlParam = "userName"; }

	$.ajax({

		url: '/billfold-api/player/' + urlParam,
		type: 'post',
		async: false,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		dataType: 'json',
		data: urlParam + '=' + value,
		actualize: !!forcibly,
		success: function(msg) {

			if(msg != null) {

				if(typeof msg.success != "undefined") {

					if(msg.success == true) {

						if(showRegisterErrorMessages) {
							$("#"+ fieldId +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
						}
						notDuplicate = true;
					}
					else {

						if(showRegisterErrorMessages) {
							$("#"+ fieldId  +"_error").html(msgValidatorDuplicate);
						}
						notDuplicate = false;
					}
				}
			}
		}
	});

	return notDuplicate;
}

// Validate e-mail
function validateEmail(emailfield) {

    if (checkEmptyRegisterfield(emailfield)) {
        if (isRegisterEmail(emailfield)) {
            if (emailfield == emailID) {
                if (checkEmpty(emailConfID)) {
                    return checkDuplication(emailfield) && checkConfirmEmail();
                }
                return checkDuplication(emailfield);
            }
            if (emailfield == emailConfID) {
                return checkConfirmEmail();
            }
        }
    }
    return false;
}

// Validate nickname
function validateNickName() {

    if(checkEmptyRegisterfield(nnameID)) {

        if(isAlphaNumeric(nnameID)) {
            return checkDuplication(nnameID);
        }
    }

    return false;
}

// Validate User name
function validateUserName() {

    if(checkEmptyRegisterfield(unameID)) {

        if(isAlphaNumeric(unameID)) {
            return checkDuplication(unameID);
        }
    }

    return false;
}

// Validate password
function validatePassword(pwdID) {

    if (checkEmptyRegisterfield(pwdID)) {
        if (checkRegisterMinFiledLength(pwdID, 6) && checkPasswordSafety(pwdID)) {
            if (pwdID == pwd1ID) {
                if (checkEmpty(pwd2ID)) {
                    return checkConfirmPassword();
                }
                return true;
            }
            if (pwdID == pwd2ID) {
                return checkConfirmPassword();
            }
        }
    }

    return false;
}

// First, Middle and Last name validation
function validatePersonalName(id) {
	var minLength = $('#' + id).prop('minLength') || 2;

    if (checkEmptyRegisterfield(id) && checkRegisterMinFiledLength(id, minLength)) return true;
    else return false;
}

// Validate user accounts fields: Skype ID, WeChat ID, QQ ID
function validateUserAccounts(id) {

	if (checkEmptyRegisterfield(id)) {
		if (!/^[a-zA-Z0-9\.,\-_]+$/.test($('#' + id).val())) {
			if (showRegisterErrorMessages) {
				$('#' + id + '_error').html(msgUserAccountsValidationError);
				showRegisterErrorMessages = false;
			}
			$('#' + id + '_validOK').empty();
			return false;
		}
		else {
			return true;
		}
	}

	return false;

}

// Check if the value of email-field really is an e-mail address
function isRegisterEmail(id) {

	var result = isEmail(id);

	if(showRegisterErrorMessages) {
		$("#"+ id +"_error").html("");
		$("#"+ id +"_validOK").html("");
	}

	if(!result) {

		if(showRegisterErrorMessages) {
			$("#"+ id +"_error").html(msgValidatorEmail);
		}
	}

	return result;
}

// Validate house number
function validateAddressField(addressId, houseNumberId, errorPostfix) {
	if (!checkEmpty(addressId) || !checkEmpty(houseNumberId)) {
		if (showRegisterErrorMessages) {
			$('#' + addressId + '_' + errorPostfix).html(msgValidatorEmpty);
			showRegisterErrorMessages = false;
		}
		$('#' + addressId + '_validOK').empty();
		return false;
	}

	$('#' + addressId + '_' + errorPostfix).empty();
	$('#' + addressId + '_validOK').html('<i title="OK" class="fa fa-check fa-lg"></i>');
	return true;
}

// Check birthday field
function checkBirthdayField() {

	if (showRegisterErrorMessages) {
		$('#birthdayField_error').empty();
	}

	var inputYear = parseInt($('#registerBirthyear').val()),
		inputMonth = parseInt($('#registerBirthmonth').val()) - 1,
		inputDate = parseInt($('#registerBirthday').val());

	if (!checkDateValidity(inputYear, inputMonth, inputDate)) {
		if (showRegisterErrorMessages) {
			showErrorObj($('#birthdayField_error'), limitsSelectDate);
		}
		return false;
	}

	var birthDate = new Date(inputYear, inputMonth, inputDate);

	if (!checkUserIsOldEnough(birthDate)) {
		if (showRegisterErrorMessages) {
			var error = replaceParams(msgUserIsNotOldEnough, [currentAgeLimit]);
			showErrorObj($('#birthdayField_error'), error);
			showRegisterErrorMessages = false;
		}
		return false;
	}

	return true;
}

function checkDateValidity(year, month, date) {
	var inputDate = new Date(year, month, date);

	// Date is valid / not valid
	return inputDate.getDate() == date;
}

function checkUserIsOldEnough(date) {
	var today = new Date();
	var userDatePlusAgeLimit = date.setYear(date.getFullYear() + currentAgeLimit);

	if (isNaN(userDatePlusAgeLimit) || (today - userDatePlusAgeLimit) < 0) {
		return false;
	}
	return true;
}

function checkDocumentIssuedDateField(id) {
	var year = parseInt($('#' + id + 'Year').val()),
		month = parseInt($('#' + id + 'Month').val()),
		day = parseInt($('#' + id + 'Day').val());

	var today = new Date(),
		date = new Date(year, (month - 1), day);

	if (!+date) {
		if (showRegisterErrorMessages) {
			$('#' + id + '_error').html(msgValidatorEmpty);
			showRegisterErrorMessages = false;
		}
		return false;
	}
	if ((today - date) < 0) {
		if (showRegisterErrorMessages) {
			$('#' + id + '_error').html(msgValidatorInvalidData);
			showRegisterErrorMessages = false;
		}
		return false;
	}

	$('#' + id + '_error').empty();
	return true;
}

function checkPassportExpiryDateField(id) {
	var year = parseInt($('#' + id + 'Year').val()),
		month = parseInt($('#' + id + 'Month').val()),
		day = parseInt($('#' + id + 'Day').val());

	var today = new Date(),
		date = new Date(year, month, day);

	if (!+date) {
		if (showRegisterErrorMessages) {
			$('#' + id + '_error').html(msgValidatorEmpty);
			showRegisterErrorMessages = false;
		}
		return false;
	}
	if ((date - today) < 0) {
		if (showRegisterErrorMessages) {
			$('#' + id + '_error').html(msgValidatorInvalidData);
			showRegisterErrorMessages = false;
		}
		return false;
	}

	$('#' + id + '_error').empty();
	return true;
}

// Check that terms & conditions -checkbox is checked
function checkTerms(id) {

	var terms = $("#" + id).attr("checked");

	if(showRegisterErrorMessages) $("#"+ id +"_error").html("");

	if(terms != "checked"){

		if(showRegisterErrorMessages) {
			$("#"+ id +"_error").html(msgValidatorCheck);
            showRegisterErrorMessages = false;
		}
		return false;
	}

	return true;
}

function checkRadioSelector(name, errorID) {
	if (typeof $('input[name=' + name + ']:radio:checked').val() === 'undefined') {
		if (showRegisterErrorMessages) {
			$('#' + errorID).html(msgValidatorRequired);
            showRegisterErrorMessages = false;
		}
		return false;
	}

	if (showRegisterErrorMessages) {
		$('#' + errorID).empty();
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------------
// Upload documents name validation (name must be alphanumeric and at least 6 charactes long)
//
function validateDocumentName(id) {

    var element = $("#" + id);

    if(element.val().length > 2 && element.val().length < 11) {

        var usern = /^[a-zA-Z0-9_]{1,}$/;

        if (usern.test(element.val())) {

            return true;
        }
    }

    return false;
}


//----------------------------------------------------------------------------------------------------------------------
// Forgot password validation
//

// Empty fields when opening the popup
function emptyForgotElement() {

    $("#forgotPwdHead_error").hide();
    $('#forgotPwdEmail_error').hide();
    $("#forgotpasswordWrapper").show();
    $("#forgotpasswordSuccess").hide();
}

function validateForgotpwdEmail() {

    var resultEmpty = checkEmpty("forgotpassword_email"),
        resultEmail = isEmail("forgotpassword_email");

    if (resultEmpty) {

        if (!resultEmail) {

            showValidateError(true, msgValidatorEmail);
            return false;
        }
        else {

            showValidateError(false, "");
            return true;
        }

    } else {

        showValidateError(true, msgValidatorRequired);
        return false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
// Newsletter ordering functions
//

// Check if given e-mail is a valid e-mail
function isNewsEmail() {

    var result = isEmail("newEmail");

    if(!result) {

        $("#newsMsg").html(msgValidatorEmail);
        $("#newComfirmNo").hide();
        $("#newComfirmyes").hide();
        $("#newCancel").show();
        $("#newsletterOk").hide();

        return false;
    }

    $("#newsMsg").html(msgNewsletterConfirmAge);
    $("#newComfirmNo").show();
    $("#newComfirmyes").show();
    $("#newCancel").hide();
    $("#newsletterOk").hide();

    return true;
}

//----------------------------------------------------------------------------------------------------------------------
// Contact us validation
//

function checkContactUsEmail(id) {

    if(checkEmptyContactUs(id)) {

        var result = isEmail(id);

        if(showErrors == true) {
            $("#"+ id +"_error").html("");
            $("#"+ id +"_validOK").html("");
        }

        if(!result) {
            if(showErrors == true) $("#"+ id +"_error").html(msgValidatorEmail);
            return false;
        }

        if(showErrors == true) $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        return true;
    }
    else {
        return false;
    }
}

function checkEmptyContactUs(id) {

    if(showErrors == true) {
        $("#"+ id +"_error").html("");
        $("#"+ id +"_validOK").html("");
    }

    var result = checkEmpty(id);

    if(!result) {
        if(showErrors == true) $("#"+ id +"_error").html(msgValidatorEmpty);
    }
    else {
        if(showErrors == true) $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
    }

    return result;
}

//----------------------------------------------------------------------------------------------------------------------
// Refer A Friend -validation
//

function checkReferAFriendEmail(id) {

    if(checkEmptyReferAFriend(id)) {

        var result = isEmail(id);
        $("#"+ id +"_error").html("");

        if(!result) {
            $("#"+ id +"_error").html(msgValidatorEmail);
            return false;
        }

        return true;
    }
    else return false;
}

function checkEmptyReferAFriend(id) {

    $("#"+ id +"_error").html("");

    var result = checkEmpty(id);

    if(!result) {
        $("#"+ id +"_error").html(msgValidatorEmpty);
    }

    return result;
}

//----------------------------------------------------------------------------------------------------------------------
// Myacc-related validation
//

// Validate phone number
function validatePhoneNumber(id, notCleanErrors, notRequired) {

    notCleanErrors = !!notCleanErrors;
    notRequired = !!notRequired;

    if(!notCleanErrors) {
        $("#"+ id +"_error").html("");
    }

    var phoneNumber = $("#"+id).val(),
        phoneCode = window["phoneCode_"+playersCountry],
        regex = /^[+]{0,1}[0-9\-\s]+$/;

    if (regex.test(phoneNumber)
        && ((notRequired && phoneNumber == phoneCode) || (phoneNumber.replace(/[^0-9]/g,'').length >= 5))) {
        return true;
    }

    return false;

}

function validateRegistrationPhoneNumber(id, notCleanErrors) {
	if (!validatePhoneNumber(id, notCleanErrors)) {
		if (showRegisterErrorMessages) {
			$('#' + phoneNumberID + '_error').html(msgPhoneValidationError);
			showRegisterErrorMessages = false;
		}
		$('#' + phoneNumberID + '_validOK').empty();
		return false;
	}

	$('#' + phoneNumberID + '_error').empty();
	$('#' + phoneNumberID + '_validOK').html('<i title="OK" class="fa fa-check fa-lg"></i>');
	return true;
}

// Validate password in popup window
function validatePwdInPopup(id,errorId) {

    $("#"+errorId).html("");

    if(!checkEmpty(id)) {
        $("#"+errorId).html(msgValidatorEmpty);
        return false;
    }

    var element = $("#"+id);
    var length = $("#"+id).val().length;

    if(length >= 6 && element.val().replace(/(\s)/g, '').length >= 6) {
        return true;
    }
    else {
        var error_string = replaceParams(msgValidatorMinlength,["6"]);
        $("#"+errorId).html(error_string);
        return false;
    }
}

// Validate bank details
function validateBankDetails(formID) {

    formID = formID || 'updateMyBankDetails';

    $("#manageAccount_error").hide();

    var validatedFields = true,
        arrayOfInputsIds = [],
        arrayIndex = 0;

    // Creating an array of visible and editable inputs of type text and select fields for their further validation
    $("#"+formID+" input:text," + "#"+formID+" select").each(function() {

        var inputId = $(this).attr('id');

        if ( $(this).is(':visible') ) {
            arrayOfInputsIds[arrayIndex] = inputId;
            arrayIndex++;
        }
    });

    // Validate the required fields
    $.each(arrayOfInputsIds, function(index, visibleInputId) {

        // If the element is required
        var label =  $('#'+visibleInputId+'_label');
        if (label.text().indexOf('*') != -1) {

            // Validation of the international bank account
            if ( visibleInputId == "ibanAccount" ) {
                if (!validateBankAccount_IBAN("ibanAccount")) validatedFields = false;
            }
            else if (visibleInputId == "ibanBic") {
                if (!validateBankCode("ibanBic")) validatedFields = false;
            }
            // Bank Account
            else if (visibleInputId == 'bankAccount' ) {
                if (!validateBankAccount_BBAN("bankAccount")) validatedFields = false;
            }
            else if (visibleInputId == "bankBic" || visibleInputId == "bankClearingCode") {
                if ( $('#' + visibleInputId).prop("tagName").toLowerCase() !== 'select' ) {
                    if (!validateBankCode("bankBic")) {
                        if (!checkEmptyAndShowResult("bankClearingCode")) {
                            validatedFields = false;
                        }
                        validatedFields = false;
                    }
                    else {
                        $("#bankClearingCode_error").html('');
                        if ($("#bankClearingCode").val() != "") {
                            $("#bankClearingCode_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
                        }
                    }
                } else {
                    checkEmptyAndShowResult(visibleInputId);
                }
            }
            // Bank location (only in Germany!)
            else if (visibleInputId == 'bankLocation') {
                if (!validateBankLocation("bankLocation")) validatedFields = false;
            }
            // Skrill email
            else if (visibleInputId == 'moneybookersEmail') {
                if (!validateEmailAndShowResult(visibleInputId)) validatedFields = false;
            }
            else {
                if (!checkEmptyAndShowResult(visibleInputId)) validatedFields = false;
            }
        }
    });

    // Separate check for neteller
    if (selectedPaymentMethod.method == depositProvidersIds.neteller && !netellerCheck()) {
        validatedFields = false;
        secondDepositConfirm(null);
    }

    if(validatedFields == true) {
		// Don't show password for Gatewayserv (because we don't store any data)
		if (selectedPaymentMethod.method === 290001) {
			secondDepositConfirm(null);
			return;
		}

        if (document.getElementById('myaccPasswordConfirm') != null) {
            $.fancybox( { href: '#myaccPasswordConfirm', title: '' } );
        } else if (document.getElementById('withdrawalPasswordConfirm') != null) {
            $.fancybox( { href: '#withdrawalPasswordConfirm', title: '' } );
        }
        $("#myaccPasswordConfirm_error").html("");
        $("#myaccPasswordConfirm_pwd").val("");
    }

}

// This function checks if account id is saved and same as before, we are not asking pwd
function netellerCheck() {
    var fieldToCheck = 'netellerAccountId',
        result = true;
    $.each(selectedPaymentMethod.fields, function(index, field) {
        if (field.field == fieldToCheck && field.defaultValue == $('#' + fieldToCheck).val()) {
            result = false;
            return false;
        }
    });
    return result;
}

// Validation of bank location (if the player is from Germany)
function validateBankLocation(id) {

    var element = $("#" + id);
    var length = element.length;

    $("#"+ id +"_validOK").html('');
    $("#" + id + "_error").html("");

    if (length == 0) {

        $("#" + id + "_error").html( msgValidatorEmpty );
        return false;

    }
    else if (length < 8) {

        $("#" + id + "_error").html( msgShouldBe12Digits );
        return false;
    }
    else {

        $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        return true;
    }
}

// Validate bank details on withdrawal page
/*function validateWithdrawalBankDetails() {

 var validatedFields = true;

 if (!validateBankAccount_IBAN("internationalBankAccount")) {
 validatedFields = false;
 }

 if ( !validateBankCode("bicString")) {
 validatedFields = false;
 }

 if ( validatedFields ) {
 $.fancybox( { href: '#myaccPasswordConfirm', title: '' } )
 $("#myaccPasswordConfirm_error").html("");
 $("#myaccPasswordConfirm_pwd").val("");
 }

 return validatedFields;
 }*/

// Validation of the IBAN
function validateBankAccount_IBAN(id) {

    var element = $("#" + id);
    var iban = element.val().replace(/(\-)/g, '').replace(/\s+/g, '');
    var length = iban.length;

    $("#"+ id +"_validOK").html('');
    $("#" + id + "_error").html("");

    if (length == 0) {

        $("#" + id + "_error").html( msgValidatorEmpty );
        return false;
    } else if (length < 8) {

        $("#" + id + "_error").html( msgShouldBe12Digits );
        return false;
    } else {

        $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        return true;
    }
}

// Validation of the BBAN (not currently in use)
function validateBankAccount_BBAN(id) {

    var element = $("#" + id),
        bban = element.val().replace(/(\-)/g, '').replace(/\s+/g, ''),
        length = bban.length;

    $("#"+ id +"_validOK").html('');
    $("#" + id + "_error").html("");

    if (length == 0) {

        $("#" + id + "_error").html( msgValidatorEmpty );
        return false;

    }

    else if (length < 8) {

        $("#" + id + "_error").html( msgShouldBe12Digits );
        return false;
    }
    else {
        $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        return true;
    }
    /*
     else {

     var first = parseInt(bban.substr(0, 10));
     var end = parseInt(bban.substr(10, 2));
     var step1 = first - end;
     var step2 = step1 % 97;
     if (step2 == 0) {
     $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
     return true;
     } else {
     $("#" + id + "_error").html( msgInvalidBankAccount );
     return false;
     }
     }
     */
}

// Validate BIC or SWIFT
function validateBankCode(id) {

    var element = $("#" + id);
    element = element.val().replace(/(\-)/g, '').replace(/\s+/g, '');
    var length = element.length;
    $("#"+ id +"_validOK").html('');
    $("#" + id + "_error").html("");

    if (length == 0) {

        $("#" + id + "_error").html( msgValidatorEmpty );
        return false;
    }
    else if (length != 8 && length != 11) {

        $("#" + id + "_error").html( msgRequiredLengthForBicAndSwift );
        return false;
    }
    else {

        $("#"+ id +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
        return true;
    }
}

// Check validity of passwords on MyAccount-page
function savePassword() {

    var pwdOld = "oldPassword";
    pwd1ID = "newPassword";
    pwd2ID = "confirmNewPassword";
    var isOK = true;

    if(!checkEmptyRegisterfield(pwdOld) || !checkRegisterMinFiledLength(pwdOld, 6)) {
        isOK = false;
    }

    if(!validatePassword(pwd1ID)) {
        isOK = false;
    }

    if(!validatePassword(pwd2ID)) {
        isOK = false;
    }

    if (isOK) {
        return true;
    }
    else {
        return false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
// Limits-related validation
//

// Check empty limits fields
function isLimitsFieldEmpty(id) {

    var result = checkEmpty(id);

    if(!result) {
        $("#" + id).val("");
    }

    return !result;
}

//validation of end date (datepicker plug-in is used)
function validateEndDate(validateId){

    var dateField = $("#" + validateId).datepicker("getDate"),
        endYear = dateField.getFullYear(),
        endMonth = dateField.getMonth()+ 1,
        endDate = dateField.getDate();

    var now = new Date(),
        nowYear = now.getFullYear(),
        nowMonth = now.getMonth() + 1,
        nowDate = now.getDate();

    var result = false;

    if(endYear > nowYear){
        result = true;
    }
    else if(endYear == nowYear && endMonth > nowMonth){
        result = true;
    }
    else if(endYear == nowYear && endMonth == nowMonth && endDate > nowDate){
        result = true;
    }

    $("#"+ validateId +"_validOK").html("");
    $("#"+validateId+"_error").html("");

    if(!result){
        $("#" + validateId+ "_error").html(limitDateValidation);
    } else {
        $("#"+ validateId +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
    }

    return result;
}

//validate amount fields
function validateAmount(validateId, limitMinAmount, limitMaxAmount) {

	var amount = $("#" + validateId).val();
	$("#"+ validateId +"_validOK").html("");
	$("#"+validateId+"_error").html("");
	amount = parseFloat(amount);

	if (limitMinAmount == limitMaxAmount) {

		$("#"+validateId+"_error").html(limitReached);
		return false;
	}
	else if(isLimitsFieldEmpty(validateId)) {

		$("#"+validateId+"_error").html(msgValidatorEmpty);
		return false;
	}
	else if(amount >= limitMaxAmount && validateId === "pdl_limit_amount") {

		$("#"+validateId+"_error").html(replaceParams(limitCannotBeHigher+" "+limitAmountValidation, [parseFloat(limitMinAmount).toFixed(2), parseFloat(limitMaxAmount).toFixed(2)]));
		return false;
	}
	else {

		if(!isNaN(amount) && (amount >= limitMinAmount) && (amount <= limitMaxAmount)) {

			$("#" + validateId).val(amount.toString().replace(/[ ]/g,""));
			$("#"+ validateId +"_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
			return true;
		}
		else {

			$("#"+validateId+"_error").html(replaceParams(limitAmountValidation, [parseFloat(limitMinAmount).toFixed(2), parseFloat(limitMaxAmount).toFixed(2)]));
			return false;
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
// Deposit-related validation
//

function validateDepositAmount(value, method) {

    var minValue = parseFloat(method.minAmount);
    var maxValue = parseFloat(method.maxAmount);
    var result = true;

    if (value == "")
    {
        $("#paymentSum_error").html(depositSelectAmount);
        $("#paymentSum_errorWrapper").show();
        result = false;
    }
    else
    {
        value = parseFloat(value);

        if ( (isNaN(value)) || (value < minValue) || (value > maxValue) ) {
            $("#paymentSum_error").html(depositWrongAmount);
            $("#paymentSum_errorWrapper").show();
            result = false;
        }
    }

    return result;
}

//----------------------------------------------------------------------------------------------------------------------
// Transactions-related validation
//

// Using date picker
function validateDateRange(startDateId, endDateId) {

    var startDate = $("#" + startDateId).datepicker("getDate"),
        endDate = $("#" + endDateId).datepicker("getDate");

    var correctRange = true;

    if (endDate - startDate < 0) {
        correctRange = false;
    }

    $("#"+endDateId+"_error").html("");

    if(!correctRange){
        $("#" + endDateId+ "_error").html(wrongDatePeriod);
    }

    return correctRange;
}

// Get char from keycode (for keypress event only)
function getKeypressChar(event) {

  if (event.which == null) {   // IE
    if (event.keyCode < 32) return null;   // special symbol
    return String.fromCharCode(event.keyCode);
  }

  if (event.which != 0 && event.charCode != 0) {   // all browsers except IE
    if (event.which < 32) return null;   // special symbol
    return String.fromCharCode(event.which);   // other symbols
  }

  return null;   // special symbol

}

// String purification before use in RegExp
function preg_quote(str){
    return (str+"").replace(new RegExp("[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]","g"),"\\$&");
}
