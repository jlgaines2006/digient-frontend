$.extend({
    languageSelector : function(config) {
        var defaults = {
            c_name : "Cookie_Name",
            c_value : "Cookie_Value",
            path: "/",
            exdays : 0
        };
        config = config != null ? jQuery.extend(defaults, config) : defaults;

        this.setCookie(config.c_name, config.c_value, config.path, config.exdays);
    },

    setCookie : function(c_name,value,path,exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((path==null) ? "" : "; path="+path) +((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    },

    getCookie : function(c_name) {
        var i,x,y,ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x = x.replace(/^\s+|\s+$/g,"");
            if (x == c_name) {
                return unescape(y);
            }
        }
    }
});

function changeLanguage(id) {
    var config = {
        c_name : "_lang",
        c_value : id,
        path:"/",
        exdays : 100
    };
    jQuery.languageSelector(config);
}

function changeLanguageInCookieAndUser(id) {
    var config = {
        c_name : "_lang",
        c_value : id,
        path:"/",
        exdays : 100
    };
    jQuery.languageSelector(config);

    $.ajax({
        url: "/billfold-api/player/changeLanguage",
        type: "POST",
        data: { language: id },
        dataType: "json",
        async: false,
        success: function(map) {
            //ignore
        }
    });
}

function chooseLanguage(id) {

    var prevLang = $.getCookie("_lang"),
        newPath = "",
        pathname = location.pathname,
        href = window.location.href;

    changeLanguageInCookieAndUser(id);

    var subStr = "/" +  prevLang,
        newStr = "/" + id;
    var subStrLen = subStr.length;

    // Case 1: /en/xxx
    if (href.indexOf(subStr + "/") != -1) {
        newPath = pathname.replace(subStr + "/", newStr + "/");
    }
    // Case 2: /en? or /en?xxx
    else if (href.indexOf(subStr + "?") != -1) {
        newPath = pathname.substr(0, pathname.length-subStrLen) + newStr;
    }
    // Case 3: /en
    else if (href.split(subStr)[1] == "") {
        newPath = pathname.substr(0, pathname.length-subStrLen) + newStr;
    }
    // Case 4: /xxx...
    else {
        newPath = newStr + pathname;
    }

    // Generating parameters of the url
    var params;

    if(currentSlidePageOpen != null) {
        params = 'currentSlide=' + currentSlidePageOpen;

        if (href.indexOf("?") != -1) {
            params = '?' + href.split("?")[1];
            var updatedParams = params.split("currentSlide=")[0];
            if (updatedParams[updatedParams.length-1] == "&") {
                params = updatedParams;
            } else {
                params = updatedParams + '&';
            }
            params = params + 'currentSlide=' + currentSlidePageOpen;
        } else {
            params = '?' + params;
        }
    }
    else {
        var queryString = href.split('?')[1] || '';
        params = queryString ? '?' + queryString : '';
    }

    if(tokenValue) {
        params = params + "&token=" + tokenValue + "&user_id=" + myAffiliateId;
    }

    var targetUrl;

    params= params || '';
    if(window.isIndexPage && oGamesList.activeGroup.name) {
        targetUrl = location.protocol + '//' + location.host + newStr + "/gamegroup/" + oGamesList.activeGroup.name + params;
    }
    else {
        targetUrl = location.protocol + '//' + location.host + newPath + params;
    }

    window.location.href = targetUrl;
}