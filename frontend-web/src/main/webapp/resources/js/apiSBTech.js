(function (whl, $, window, document) {
    'use strict';

    whl.errorMessages = {};
    whl.errorMessages[-1] = 'Unknown error.';
    whl.errorMessages[-100] = 'Invalid input parameters.';
    whl.errorMessages[-102] = 'Username is not entered.';
    whl.errorMessages[-104] = 'Password is not entered.';
    whl.errorMessages[-205] = 'Username/password is not correct.';
    whl.errorMessages[-7] = 'User account is locked.';
    whl.siteURL = 'http://????.testing.sbtech.com';
    whl.linkLogin = whl.siteURL + '/Login.html';
    whl.linkStatus = whl.siteURL + '/Status.html';
    whl.linkLogout = whl.siteURL + '/Logout.html';
    whl.linkReg = '/register.html';
    whl.linkDeposits = '/deposit.html';
    whl.linkWithdrawals = '/withdraw.html';
    whl.linkChangePassword = '/changePassword.html';
    whl.linkProfile = '/profile.html';
    whl.DEF_PANEL_WIDTH = 800;
    whl.DEF_PANEL_HEIGHT = 550;
    whl.PANEL_NAME = 'panel_popup';

    // STATUS

    whl.status = function (callback) {
        callback = callback || function () {};

        callback({
            uid: '',
            token: '',
            status: 'real',
            message: '',
            balance: '100 $'
        });
    };

    // REFRESH

    whl.refreshSession = function (callback) {
        callback = callback || function () {};

        callback({
            status: 'success',
            message: '',
            balance: '100 $'
        });
    };

    // LOGIN

    whl.login = function (username, password, callback) {
        callback = callback || function () {};

        callback({
            token: '',
            status: 'error',
            message: '',
            balance: '100 $'
        });
    };

    // LOGOUT

    whl.logout = function () {

    };

    // MESSAGES

    $(postContentHeight);
    $(window).bind('load resize', postContentHeight);

    function postContentHeight() {
        var body = document.body,
            html = document.documentElement,
            height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);

        window.parent.postMessage({
            height: height
        }, '*');
    }

})(window.whl = window.whl || {}, jQuery, window, document);