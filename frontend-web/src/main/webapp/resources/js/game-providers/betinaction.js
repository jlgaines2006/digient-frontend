// ---------------------------
function listener(event) {
    var iframe = $("#gameWindow").length > 0 ? $("#gameWindow") : $("#transactions_betting").find('iframe');
    var regexp = new RegExp(event.origin, "i");
    if (regexp.test(iframe.attr('src')) == false) return;
    if (event.data.method != null && event.data.method.length != 0)
    switch(event.data.method){
        case 'autoheight':
            iFrame_autoheight(iframe, event.data.params)
            break;
        case 'redirectToLive':
            iFrame_redirectToLive(iframe, event.data.params)
            break;
        case 'showLoginBox':
            openLoginRegisterPopup();
            break;
    }
}

// autoheight
function iFrame_autoheight(iframe, params){
    if (params == null || params.length == 0) return;
    var height = /number|string/i.test(typeof params.height) == true ? parseInt(params.height) : 0;
    if (isNaN(height) == false && height > 0 && iframe)
    iframe.height(height + 'px');
}

// redirectToLive
function iFrame_redirectToLive(iframe, params){
    if (params == null || params.length == 0) return;
    var lang = $.getCookie('_lang') || defaultLanguage;
    window.location.href = '/' + lang + '/betting/live/' + (userIsLogged ? 'real' : 'demo') +'#eventid=' + params.eventid;
}

function getBettingTransactions() {
    if (window.addEventListener) window.addEventListener("message", listener, false);
    else window.attachEvent("onmessage", listener);

    $.when(getGameParams(250011,'real')).done(function(map) {
        $('#transactions_betting').find('iframe').attr('src', map.url);
    });
}