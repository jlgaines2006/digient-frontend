/*console.log("Detect OS = ");
console.log(fingerprint_os());
console.log("Detect OS bits = ");
console.log(fingerprint_bt());
console.log("Detect Timezone = ");
console.log(fingerprint_timezone());
console.log("Detect Display = ");
console.log(fingerprint_display());
console.log("Detect UserAgent = ");
console.log(fingerprint_useragent());
console.log("Detect Browser = ");
console.log(fingerprint_browser());
console.log("Detect Browser Plugins = ");
console.log(fingerprint_plugins());
console.log("Detect Languages = ");
console.log(fingerprint_language());
console.log("Detect Fonts = ");
console.log(fingerprint_fonts());
console.log("Detect Fonts MD5 Hash = ");
console.log(md5(fingerprint_fonts()));
console.log("Detect Flash Version = ");
console.log(fingerprint_flash());
console.log("Detect Silverlight Version = ");
console.log(fingerprint_silverlight());
console.log("Detect Java Enabled = ");
console.log(fingerprint_java());
console.log("Detect Cookies Enabled = ");
console.log(fingerprint_cookie());*/

var deviceFingerprint = readCookie("devicefingerprint");

$(document).ready(function(){

	if(!deviceFingerprint) {

		deviceFingerprint = md5(fingerprint_os()+fingerprint_bt()+fingerprint_timezone()+fingerprint_display()+fingerprint_useragent()+fingerprint_browser()+fingerprint_plugins()+
			fingerprint_language()+fingerprint_fonts()+fingerprint_flash()+fingerprint_silverlight()+fingerprint_java()+fingerprint_cookie());

		createCookie("devicefingerprint",deviceFingerprint,365);
	}
});