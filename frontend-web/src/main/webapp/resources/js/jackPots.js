var jackPots = new Array();

// Get jackpot-data for Play2Go -games
function populateJackpots(firstload)  {

	// Update jackpot values in the scroller (create the scroller on the first time)
	if(firstload == true) {

		// Add jackpots to template
		if($("#progressivejackpot").length > 0) {

			$("#jackpotslidertextwrapper").append($("#progressiveJackpotTemplate").tmpl(jackPots));

			// Define how the game is opened (live- and other games)
			defineGameOpening(".gameLink",gameWindowOpening);
			defineGameOpening(".liveGameLink",liveGameWindowOpening);

			// How we present the progressive jackpots? With scroller or slider?
			$('#jackpotslidertextwrapper').css('visibility','visible');

			if(jackpotDisplayType === "scroller") {

				$('#jackpotslider').SetScroller({
					velocity: 	 55,
					direction: 	 'vertical',
					startfrom: 	 'bottom',
					loop:		 'infinite',
					movetype: 	 'linear',
					onmouseover: 'pause',
					onmouseout:  'play',
					onstartup: 	 'play',
					cursor:      'default'
				});
			}
			else if(jackpotDisplayType === "slider") {

				$('#jackpotslidertextwrapper').cycle({
					fx: 'fade',
					timeout:  4000,
					sync:     0,
					containerResize: false,
					slideResize: false,
					fit: 1
				});
			}
		}
	}

	// Update initial jackpot values to game the listing and scroller
	$.each(jackPots, function(jackpotIndex,jackpotObj) {

		var displayValue = jackpotObj.value;

		// The value in game thumbnail
		$('#gameListJackpot_'+jackpotObj.name).html(jackpotObj.currency+'<span class="jackpotgame_'+jackpotObj.name+'">'+numberWithCommas(displayValue.toFixed(2))+'</span>');
		$('#gameJackpot_' + jackpotObj.name).toggle(!!jackpotObj.value);

		// The value in slider/scroller
		if(jackpotObj.showInSlider == true) {
			$(".jackpotitemvalue .jackpotgame_"+jackpotObj.name).html(numberWithCommas(displayValue.toFixed(2)));
		}
	});

	// Increase jackpot values where they are displayed
	var currentValue = null;

	// How much each jackpot value changes during second
	var changeAmount = parseFloat(1/300);

	// Clear the previous timer
	if(jackPotsUpdaterTimer) {
		clearInterval(jackPotsUpdaterTimer);
	}

	// Update the visible jackpot values on the site
	jackPotsUpdaterTimer = setInterval(function() {

		$.each(jackPots, function(jackpotIndex,jackpotObj) {

			currentValue = (jackpotObj.value+changeAmount);
			jackpotObj.value = currentValue;
			$('.jackpotgame_'+jackpotObj.name).html(numberWithCommas(currentValue.toFixed(2)));
		});

	}, 1000);

}

// Fetch the jackpots by ajax-call
function fetchJackpotsFromServer(currency) {

	// Reset the jackpots-array
	jackPots = [];

	// Do some operations just on the first load (that is, when jackPotsShown=false)
	var firstload = false;

	if(jackPotsShown == false) firstload = true;

	jackPotsShown = true;

	$.ajax({
		url: "/billfold-api/jackpots?currency="+currency,
		type: "get",
		dataType: "json",
		success: function(map) {

			if(map) {

				var amount = "";
				var currencyParam = "";
				var showInSlider = false;

				// Loop all the games of the site for which jackpots are configured
				$.each(map, function(index,obj) {

					// Amount of jackpot
					if(obj.amount) {
						amount = parseInt(obj.amount);
					}
					else {
						amount = 0;
					}

					// Do we show jackpot in the slider or not
					if((obj.metadata || {}).hasOwnProperty('jackpotSlider')) {

						if(obj.metadata.jackpotSlider == "true") {
							showInSlider = true;
						}
					}
					else {
						showInSlider = false;
					}

					currencyParam = getCurrencySymbol(obj.currency);

					var jackpotData = {
						"name" : obj.name,
						"displayName" : getGameDisplayName(obj.name),
						"provider" : obj.provider,
						"currency" : currencyParam,
						"hideDemoUrl" : obj.hideDemoUrl,
						"promoMoneyEnabled" : obj.promoMoneyEnabled,
						"value" : amount,
						"showInSlider": showInSlider
					}

					jackPots.push(jackpotData);
				});

				populateJackpots(firstload);
			}
		}
	});
}

// Get game-specific jackpots and update them every 5. minute
function getGameJackpots(currency) {

	if(jackPotsShown == false) {

		fetchJackpotsFromServer(currency);

		setInterval(function() {
			fetchJackpotsFromServer(currency);
		}, 300000);
	}
}

// Get total jackpot and display it on the site
function getTotalJackpot(currency) {

	$.ajax({
		url: "/billfold-api/totalJackpots?currency="+currency,
		type: "get",
		dataType: "json",
		success: function(map) {

			if(map && typeof map[0] != "undefined") {

				var currencyParam = "";

				if(map[0].currency) {
					currencyParam = getCurrencySymbol(map[0].currency);
				}

				var value, randomValue;

				if(map[0].amount) {
					value = parseFloat(map[0].amount);
				}
				else {
					value = 0;
				}

				// Update the visible total jackpot value on the site
				setInterval(function() {

					randomValue = parseFloat(Math.random());
					value = parseFloat(value+randomValue);

					$('#totaljackpotValue').html(currencyParam+numberWithCommas(value.toFixed(2)));

				}, 1000);
			}

		}

	});
}

// Format jackpot values to include commas (after every thousand)
function numberWithCommas(x) {

	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}