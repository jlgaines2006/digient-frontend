var ajaxCalls = {

    /********** Home page **********/

    // Languages
    getLanguages: function() {
        return $.ajax({
            url: "/billfold-api/config/languages",
            type: "GET",
            dataType: "json",
            async: false
        });
    },

    /********** Games **********/

    // Add to favourites
    addToFavourites: function (gameId) {
        return $.ajax({
            url: '/billfold-api/games/favourites/add',
            type: 'POST',
            dataType: 'json',
            data: { gameId: gameId }
        });
    },

    // Remove from favourites
    removeFromFavourites: function (gameId) {
        return $.ajax({
            url: '/billfold-api/games/favourites/remove',
            type: 'POST',
            dataType: 'json',
            data: { gameId: gameId }
        });
    },

    /********** My account **********/

    // Profile tab
    playerDetailsRequest: function() {
        return $.ajax({
            url: "/billfold-api/player",
            type: "GET",
            dataType: "json"
        });
    },

    // Payment Details tab

    // Password tab

    // Documents tab

    // Loyalty tab

    // RTP tab
    getRTPInfo: function () {
        return $.ajax({
            url: '/billfold-api/player/rtp',
            type: 'GET',
            dataType: 'json'
        });
    },

    // Bonuses tab
    getBonusInfo: function (status, data) {
        status = status || 'all';
        return $.ajax({
            url: '/billfold-api/player/bonuses?status='+status,
            type: 'GET',
            dataType: 'json',
            data: data
        });
    },

    // Login History tab
    getLoginHistoryInfo: function (data) {
        return $.ajax({
            url: '/billfold-api/player/activity',
            type: 'GET',
            dataType: 'json',
            data: data
        });
    }

    /******* End of my account *******/

};