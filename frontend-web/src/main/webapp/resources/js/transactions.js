var transactionsItemsPerPage = 15; // This is just a default value - it is updated when transactions are fetched
var transactionTabName = "";
var transactionsMap = {};
var pageIndex = 0;

// Show x number of transactions on the given page (in paginator)
function updateTransactions(page_index, pagination_container) {

	progressIndicator(true);
    $("#transactionError").hide();
    $('#transactionsTableBody_'+transactionTabName).empty();

    // Updating paginator
    clearAndRemovePaginator();

    pageIndex = parseInt(page_index+1);

    var ajaxCall,
        data = "page=" + pageIndex;

    if (transactionTabName != "loyalty") {
        data += getPeriod();
    }

    // Fetching game transactions
    if (transactionTabName == "game") {
        ajaxCall = "fetchGameTransactions";
        data = generateDataForGameTranscations(data);
    }
    else if (transactionTabName == "promo") {
        ajaxCall = "fetchPromoTransactions";
    }
    else if (transactionTabName == "deposit") {
        ajaxCall = "fetchDepositTransactions";
        data += getTransStatus();
    }
    else if (transactionTabName == "withdrawal") {
        ajaxCall = "fetchWithdrawalTransactions";
        data += getTransStatus();
    }
    else if (transactionTabName == "loyalty") {
        ajaxCall = "fetchLoyaltyTransactions";
    }
    else if (transactionTabName == "commission") {
        ajaxCall = "fetchCommissionTransactions";
        data += getTransCommissionType();
    }

    $.when(window[ajaxCall](data)).done(function(map) {

        if (!hasErrors(map)) {

            transactionsMap = map.transactions;

            // Case 1: Transactions are fetched
            if(typeof map.transactions != "undefined") {

                // Update info message on the page
                setInfoMessage(map);

                // Add the paginator; First Parameter: number of items; Second Parameter: options object
                transactionsItemsPerPage = map.size;

                if(map.count > transactionsItemsPerPage) {

                    $("#transactions_"+transactionTabName).append('<div id="transactionsPaginator_'+transactionTabName+'" class="pagination"></div>');
                    $("#transactionsPaginator_"+transactionTabName).pagination(map.count, {

                        items_per_page: transactionsItemsPerPage,
                        current_page: pageIndex-1,
                        callback: updateTransactions
                    });

                    // Update the pagination icons to use current language
                    $(".prev").html(transactionPrevious);
                    $(".next").html(transactionNext);
                }

                // Filling in the data to the table
                populateTable(map);
            }
        }

        // Case 2: Error occurred
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#transactionError_" + transactionTabName), error);
        }

		progressIndicator(false);
        document.getElementById("loadingContent").style.visibility = "visible";
    });
}

// Populating table with the transactions
function populateTable(map) {

    $("#transactionsTableBody_" + transactionTabName + " > tr").remove();

    var newRow = "";

    $.each(map.transactions, function(index, item) {

        if(transactionTabName == "game") {

            var roundId = item.roundId;
            if (roundId == null) roundId = "-";

            if (index % 2 == 0) {
                newRow = "<tr class='transactionTableEvenRow'>";
            }
            else {
                newRow = "<tr>";
            }

            newRow = newRow +
                "<td>" + getFormattedDate(item.createdTime) + "</td>" +
                "<td>" + (window[item.productName] || item.productName) + "</td>" +
                "<td>" + getCurrencySymbol(item.currency) + " " + item.betAmount.toFixed(2) + "</td>" +
                "<td>" + getCurrencySymbol(item.currency) + " " + item.winAmount.toFixed(2) + "</td>" +
                "<td>" + getType(item.status) + "</td>" +
                "<td title='" + roundId + "'>"+ concateRef(roundId) +"</td>" +
                "</tr>";
        }
        else if (transactionTabName == "deposit") {
            // Created time link
            var createdTime = $('<a class="transDetailLink"/>')
                .html(getFormattedDate(item.createdTime))
                .click(function() {showDepositTransDetails(index)});

            newRow = $('<tr/>')
                .append($('<td/>').append(createdTime))
                .append("<td>" + getPaymentProv(item.provider,item.method) + "</td>")
                .append("<td>" + getCurrencySymbol(item.currency) + " " + parseFloat(item.amount).toFixed(2) + "</td>")
                .append("<td>" + (retrieveCampaignBonusAmount(item, getCurrencySymbol(item.currency)).bonusAmount || '&mdash;') + "</td>")
                .append("<td>" + getStatus(item.status) + "</td>")
                .append("<td title='" + item.reference + "'>" + concateRef(item.reference) + "</td>");
        }
		else if (transactionTabName == "withdrawal") {
            // Created time link
            var createdTime = $('<a class="transDetailLink"/>')
                .html(getFormattedDate(item.createdTime))
                .click(function(){showWithdrawalTransDetails(index)});

            newRow = $('<tr/>')
                .append($('<td/>').append(createdTime))
                .append("<td>" + getWithdrawalMethodName(item.method) + "</td>")
                .append("<td>" + getCurrencySymbol(item.currency) + " " + parseFloat(item.bankAmount).toFixed(2) + "</td>")
                .append("<td>" + getStatus(item.status) + "</td>")

            if (item.status == "pending" && item.canCancel) {
                newRow.append("<td><input class='btn btn-default btn-xs' type='button' id='" + item.reference + "\' value='" + cancel + "\' /></td>");
            } else {
                newRow.append($('<td/>'));
            }
		}
        else if (transactionTabName == "promo") {
            newRow = "<tr>" +
                "<td>" + getFormattedDate(item.createdTime) + "</td>" +
                "<td>" + getProductName(item.productId) + "</td>" +
                "<td>" + getCurrencySymbol(item.currency) + " " + parseFloat(item.amount).toFixed(2) + "</td>" +
                "<td>" + getType(item.type) + "</td>" +
                "<td title='" + item.reference + "'>" + concateRef(item.reference) + "</td>" +
                "</tr>";
        }
        else if (transactionTabName == "loyalty") {
            var statusPoint = item.statusPoint,
                awardPoint = item.awardPoint;
            if (statusPoint == null) statusPoint = "-";
            if (awardPoint == null) awardPoint = "-";
            newRow = "<tr>" +
                "<td>" + getFormattedDate(item.createdTime) + "</td>" +
                "<td>" + getPointType(item.pointType) + "</td>" +
                "<td>" + getPointDirection(item.direction) + "</td>" +
                "<td>" + statusPoint + "</td>" +
                "<td>" + awardPoint + "</td>" +
                "</tr>";
        }
        else if (transactionTabName == "commission") {
            var currency = getCurrencySymbol(item.currency) || '',
                payoutCurrency = getCurrencySymbol(item.payoutCurrency) || '',
                created = getFormattedDate(item.createdTime),
                type = getCommissionType(item.type),
                promo = item.promo != null ? (item.promo || 0).toFixed(2) + '&nbsp;' + currency : '&mdash;',
                cash = item.cash != null ? (item.cash || 0).toFixed(2) + '&nbsp;' + currency : '&mdash;',
                total = item.total != null ? (item.total || 0).toFixed(2) + '&nbsp;' + currency : '&mdash;',
                payout = item.payoutAmount != null ? (item.payoutAmount || 0).toFixed(2) + '&nbsp;' + payoutCurrency : '&mdash;',
                rate = item.exchangeRate != null ? (item.exchangeRate || 0) : '&mdash;';

            newRow = [
                '<tr>',
                    '<td>' + created + '</td>',
                    '<td>' + type + '</td>',
                    '<td>' + promo + '</td>',
                    '<td>' + cash + '</td>',
                    '<td>' + total + '</td>',
                    '<td>' + payout + '</td>',
                    '<td>' + rate + '</td>',
                '</tr>'
            ].join('');
        }

        $('#transactionsTableBody_' + transactionTabName).append(newRow);

        if (transactionTabName == "withdrawal" && item.canCancel) {
            $("#" + item.reference).click(function() {
                cancelWithdrawal(item.reference);
            });
        }
    });

	$(".transactionsTable tbody tr:even").addClass("transactionTableEvenRow");
}

// Clear the possible old content of the paginator wrapper element
function clearAndRemovePaginator() {

    if($("#transactionsPaginator_"+transactionTabName).length > 0) {
        $("#transactionsPaginator_"+transactionTabName).unbind();
        $("#transactionsPaginator_"+transactionTabName).remove();
    }
}

// Shorten reference id to fit to table
function concateRef(param) {

    if(param.length > 13) {
        param = param.substr(0,10)+"...";
    }

    return param;
}

function getPointType(type){
    if(window['pointType_'+ type]) {
        return window['pointType_' + type];
    } else {
        return type;
    }
}

function getPointDirection(direction){
    if(window['pointDirection_'+ direction]) {
        return window['pointDirection_' + direction];
    } else {
        return direction;
    }
}

// Shorten type to fit to table
function concateType(param) {

    if(param.length > 13) {
        param = param.substr(0,10)+"...";
    }

    return param;
}

// Get the transactions for the given tab
function getTransactionsData(transactionTabParam, notOnLoad) {

	transactionTabName = transactionTabParam;

	// Fetch game groups for the game transactions
	if (transactionTabName == "game") {

		// Fetch game groups - except if we are changing the game group or game name
		if(typeof notOnLoad == "undefined") {

			progressIndicator(true);

			setTimeout(function() {

				$.when(fetchGameGroups()).done(function(gameGroupMap) {

					// Populate the group selector
					if (!hasErrors(gameGroupMap)) {

						var select = document.getElementById('gameGroup');
						select.options.length = 0;
						select.options.add(new Option(getGameGroupDisplayName('all'), 'all'));

						$.each(gameGroupMap.groups, function(index, gameGroup) {
                            if (gameGroup != 'all') {
                                select.options.add(new Option(getGameGroupDisplayName(gameGroup), gameGroup));
                            }
						});

						if(styledInputs) {

							setTimeout(function() {
								stylizeDropdown("#gameGroup",10);
							}, 10);
						}
					}

					progressIndicator(false);

					updateTransactions(0,null);
				});

			},50);
		}
		else {
			updateTransactions(0,null);
		}
	}

	if (transactionTabName == "promo") {

		updateTransactions(0,null);
	}

	if (transactionTabName == "withdrawal" || transactionTabName == "deposit") {

		// Fetch statuses
		if (typeof notOnLoad == "undefined") {
			generateStatusesSelection();
		}

		updateTransactions(0,null);
	}

	if(transactionTabName == "loyalty") {

		updateTransactions(0,null);
	}

    if(transactionTabName == "commission") {
        // Fetch types
        if (!notOnLoad) {
            generateCommissionTypeSelection();
        }
        updateTransactions(0, null);
    }

}

// Generate statuses selector
function generateStatusesSelection() {
    var select = document.getElementById('status_' + transactionTabName),
        status = "";
    select.options.length = 0;

    if (transactionTabName == "withdrawal") {
        status = ["all","pending","checkedOut","confirmed","rejected","cancelled","error"];
    }
    else if (transactionTabName == "deposit") {
        status = ["all","pending","ok","cancelled","rejected","error"];
    }
    for (var i = 0; i < status.length; i++) {
        select.options.add(new Option(window["transactionStatus_"+status[i]], status[i]));
    }

    if(styledInputs) {

        setTimeout(function() {
            stylizeDropdown("#status_" + transactionTabName,10);
        }, 10);
    }
}

// Generate commission type selector
function generateCommissionTypeSelection() {
    var $typeSelector = $('#type_' + transactionTabName),
        types = ['payout', 'commission'],
        options = '<option value=""></option>';

    for (var i = 0; i < types.length; i++) {
        var type = types[i];
        options += '<option value="' + type + '">' + getCommissionType(type) + '</option>';
    }

    $typeSelector.html(options);

    if (styledInputs) {
        setTimeout(function() {
            stylizeDropdown('#type_' + transactionTabName, 10);
        }, 10);
    }
}

// Fetch game groups
function fetchGameGroups() {

    return $.ajax({
        url: '/billfold-api/groups',
        type: 'GET',
        async: false,
        dataType: 'json'
    });
}

// Get player's balance
function fetchBalance() {

    return $.ajax({
        url: "/billfold-api/player/balance",
        async: false,
        type: "get",
        dataType: 'json'
    });
}

// Fetch game transactions
function fetchGameTransactions(data) {

    return $.ajax({
        url: '/billfold-api/transactions/games',
        type: 'GET',
        async: false,
        dataType: 'json',
        data: data
    });
}

// Fetch promo transactions
function fetchPromoTransactions(data) {

    return $.ajax({
        url: '/billfold-api/transactions/promo',
        type: 'GET',
        async: false,
        dataType: 'json',
        data: data
    });
}

// Fetch deposit transactions
function fetchDepositTransactions(data) {

    return $.ajax({
        url: '/billfold-api/transactions/deposits',
        type: 'GET',
        async: false,
        dataType: 'json',
        data: data
    });
}

// Fetch withdrawal transactions
function fetchWithdrawalTransactions(data) {

    return $.ajax({
        url: '/billfold-api/transactions/withdrawals',
        type: 'GET',
        async: false,
        dataType: 'json',
        data: data
    });
}

function fetchLoyaltyTransactions(data) {

    return $.ajax({
        url: '/billfold-api/transactions/loyalty',
        type: 'GET',
        async: false,
        dataType: 'json',
        data: data
    });
}

// Fetch commission transactions
function fetchCommissionTransactions(data) {
    return $.ajax({
        url: '/billfold-api/transactions/commission',
        data: data
    });
}

// Called when changing the current group in game-tab. Populates the game names (in selector) related to that group
function changeGameGroupDynamicSelector() {

	var groupName = $('#gameGroup').val();

	// Populate game name selector
	if (groupName == 'all') {
		$("#gameNameRow").hide();

		// Send transcaction get call for the group
		getTransactionsData("game", true);
	}
	else if (groupName) {
		$("#gameNameRow").show();

		progressIndicator(true);

		setTimeout(function() {

			$.ajax({
				url: '/billfold-api/games/' + groupName,
				type: 'GET',
				async: false,
				dataType: 'json',
				success: function(map) {

					if(styledInputs) {

						$("#gameNameColumn").empty();
						$("#gameNameColumn").append('<select id="gameName" class="selectClass" onchange="changeGameSelector()"></select>');
					}

					var select = document.getElementById('gameName');
					select.options.length = 0;

					$.each(map.games, function() {

						if (!this.gameDisabled) {

							var gameDisplayName = getGameDisplayName(this.name),
								gameName = this.gameId;

							select.options.add(new Option(gameDisplayName, gameName));
						}
					});

					sortGamesAlphabetically();

					// Add 'All games' selected option
					$("#gameName").prepend("<option value='all' selected='selected'>" + getGameGroupDisplayName('all') + "</option>");

					if(styledInputs) {

						setTimeout(function() {
							stylizeDropdown("#gameName",10);
						}, 10);
					}

					progressIndicator(false);

					// Send transcaction get call for the group
					getTransactionsData("game", true);
				}
			});

		},50);
	}

}

// Called when changing the current game in game-tab
function changeGameSelector() {
	getTransactionsData("game", true);
}

function changeCurrency(){
    getTransactionsData(transactionTabName, true);
}

function changeWithdrawalStatusSelector() {
    getTransactionsData("withdrawal", true);
}

function changeDepositStatusSelector() {
    getTransactionsData("deposit", true);
}

function changeCommissionTypeSelector() {
    getTransactionsData('commission', true);
}

function sortGamesAlphabetically() {

    var options = $('select.selectClass option');
    var arr = options.map(function(_, o) {
        return {
            t: $(o).text(),
            v: o.value
        };
    }).get();
    arr.sort(function(o1, o2) {
        return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
    });
    options.each(function(i, o) {
        o.value = arr[i].v;
        $(o).text(arr[i].t);
    });
}

// Set title message for the current transaction tab (how many transactions found etc.)
function setInfoMessage(map) {

    // Set title of transactions
    var titleString = "";

    if(map.count > 0) {

        $("#transactionsTable_"+transactionTabName).show();

        if(map.pageCount == 1) {
            titleString = replaceParams(transactionFoundOne,[map.count]);
        }
        else {
            titleString = replaceParams(transactionFoundMany,[map.count,map.size,map.pageCount]);
        }
    }
    else {
        $("#transactionsTable_"+transactionTabName).hide();
        titleString = transactionFoundZero;
    }
    $("#transactionsTitle_"+transactionTabName).html(titleString);
}

// Called when we want to see transactions for a defined period
function periodForTransactions() {

    // Get transcactions
    getTransactionsData(transactionTabName, true);
}

// Returns the period selected in the tab
function getPeriod() {

	var startDateId = "transactions_" + transactionTabName + "_from",
		endDateId = "transactions_" + transactionTabName + "_to",
		dateValue = "";

	var startDateValue = $("#" + startDateId).datepicker("getDate");

	// For the game tab the start- and end values are always same (you can see game transactions just for one day)
	if(transactionTabName == "game") {
		$("#" + endDateId).datepicker("setDate", startDateValue);
	}

	var endDateValue = $("#" + endDateId).datepicker("getDate");

	if (checkEmpty(startDateId) && checkEmpty(endDateId)) {

		validateDateRange(startDateId, endDateId);
		startDateValue = startDateValue.getFullYear()+"-"+(startDateValue.getMonth()+1)+"-"+startDateValue.getDate();
		endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();

		dateValue = "&startDate=" + startDateValue + "&endDate=" + endDateValue;
	}
	else if (checkEmpty(startDateId)) {
		startDateValue = startDateValue.getFullYear()+"-"+(startDateValue.getMonth()+1)+"-"+startDateValue.getDate();
		dateValue = "&startDate=" + startDateValue;
	}
	else if (checkEmpty(endDateId)) {
		endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();
		dateValue = "&endDate=" + endDateValue;
	}

	return dateValue;
}

function getCurrency() {

    var data = "",
        name = "currency_" + transactionTabName;

    if (document.getElementsByName(name) != null) {
        data = "&currency=" + $('input[name=' + name + ']:checked').val();
    }
    return data;
}

function getTransStatus() {

    var statusEl = document.getElementById("status_" + transactionTabName),
        status = statusEl.options[statusEl.selectedIndex].value,
        data = "";

    if (status != "all") {data = "&status=" + status};

    return data;
}

function getTransCommissionType() {
    var $typeSelector = $('#type_' + transactionTabName),
        type = $typeSelector.val() || '';

    return (type ? '&type=' + type : '');
}

// Return data needed for get transactions for game group and/or game
function generateDataForGameTranscations(data) {

    var groupName = $('#gameGroup').val(),
        gameName = $('#gameName').val();

    if (gameName && gameName != 'all') {
        data += '&gameName=' + gameName;
    }
    data += '&groupName=' + groupName;

    return data;

}

function showWithdrawalTransDetails(index) {

    var transaction = transactionsMap[index];
    $("#withdrawalTransTable").hide();
    $("#transactionsPaginator_"+transactionTabName).hide();
    $("#detailTimestamp").html(getFormattedDate(transaction.createdTime));
    $("#detailProvider").html(getWithdrawalMethodName(transaction.method));
    $("#detailBankAmount").html(getCurrencySymbol(transaction.currency) + " " + parseFloat(transaction.bankAmount).toFixed(2));
    transaction.fee != null ? $("#detailFee").html(getCurrencySymbol(transaction.currency) + " " + parseFloat(transaction.fee).toFixed(2)) : $("#detailFee").html('-');
    $("#detailAmount").html(getCurrencySymbol(transaction.currency) + " " + parseFloat(transaction.amount).toFixed(2));
    $("#detailStatus").html(getStatus(transaction.status));
    $("#detailTd").html('');


    if (transaction.status == "pending" && transaction.canCancel) {
        $("#detailTd").append('<input class=\'btn btn-default btn-xs\' type=\'button\' value=\"' + cancel + '\" id=\"cancelBtn' + transaction.reference + '\"/>');
        $("#cancelBtn"+transaction.reference).click(function() {
            cancelWithdrawal(transaction.reference);
        });
    }
    $("#detailRef").html(transaction.reference);
    $("#withdrawalTransDetail").show();
}

function closeWithdrawalTransDetail() {
    $("#withdrawalTransTable").show();
    $("#transactionsPaginator_"+transactionTabName).show();
    $("#withdrawalTransDetail").hide();
}

// Cancelling pending withdrawal transaction
function cancelWithdrawal(reference) {

    var data = "reference=" + reference;
    progressIndicator(true);
    $.when(cancelWithdrawalPost(data)).done(function(map) {
        if (!hasErrors(map)) {
            if (map.success) {

                // If it's the transaction page
                if (document.getElementById("withdrawalTransTable").style.display != 'none') {
                    updateTransactions(pageIndex-1,null);
                }

                // If it is the details page
                else {
                    $("#detailStatus").html(getStatus('cancelled'));
                    $("#detailTd").html('');
                    updateTransactions(pageIndex-1,null);
                    $("#transactionsPaginator_"+transactionTabName).hide();
                }
            }
            else {
                showErrorObj($("#transactionError_withdrawal"), cancelWithdrawError);
            }
        }
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#transactionError_withdrawal"), error);
        }
        progressIndicator(false);
    });
}

function cancelWithdrawalPost(data) {

	return $.ajax({
        url: "/billfold-api/payment/cancelWithdraw",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function showDepositTransDetails(index) {

    var transaction = transactionsMap[index],
        bankCurrency = transaction.bankCurrency || transaction.currency;
    $("#depositTransTable").hide();
    $("#transactionsPaginator_"+transactionTabName).hide();
    $("#depositDetailTimestamp").html(getFormattedDate(transaction.createdTime));
    $("#depositDetailProvider").html(getPaymentProv(transaction.provider,transaction.method));
    $("#depositDetailBankAmount").html(getCurrencySymbol(bankCurrency) + " " + parseFloat(transaction.bankAmount).toFixed(2));
    transaction.fee != null ? $("#depositDetailFee").html(getCurrencySymbol(transaction.currency) + " " + parseFloat(transaction.fee).toFixed(2)) : $("#depositDetailFee").html('-');
    $("#depositDetailAmount").html(getCurrencySymbol(transaction.currency) + " " + parseFloat(transaction.amount).toFixed(2));
    $("#depositDetailStatus").html(getStatus(transaction.status));
    $("#depositDetailRef").html(transaction.reference);
    $("#depositTransDetail").show();
}

function closeDepositTransDetail() {
    $("#depositTransTable").show();
    $("#transactionsPaginator_"+transactionTabName).show();
    $("#depositTransDetail").hide();
}
