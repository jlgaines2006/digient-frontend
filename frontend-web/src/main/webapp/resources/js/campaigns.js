var pageIndex = 0;

function campaignsOnLoad() {

	$(document).ready(function() {

		// Style upload type checkboxes (if needed)
		if(styledInputs) {

			setTimeout(function() {
				stylizeDropdown("#uploadTypeSelector",10);
			}, 10);
		}

		progressIndicator(true);

        // Loading player bonuses information
        $("input:radio[name=bonusStatus]:first").attr('checked', true);
        updateBonusesList(0, null);

        loadLoyaltyInfo();

        // Show the content
        progressIndicator(false);
        document.getElementById("loadingContent").style.visibility = "visible";

        var url = window.location.href.toString(),
            index = -1;
        if (url.indexOf('loyalty') != -1) {
            index = $('#loyalty_details_tab').index();
        }
        else if (url.indexOf('bonuses') != -1) {
            index = $('#profile_bonuses_tab').index();
        }
        else if (url.indexOf('promotions') != -1) {
            index = $('#profile_promotions_tab').index();
        }
        $('#tabs').tabs({ active: (index != -1 ? index : 0) });
    });
}

function loadLoyaltyInfo() {

    $.when(getLoyaltyInfo()).done(function(map){
        if (map.success) {

            // Feeding the picture
            $("#bronze").html(getUserLoyaltyTier('bronze'));
            $("#silver").html(getUserLoyaltyTier('silver'));
            $("#gold").html(getUserLoyaltyTier('gold'));
            $("#platinum").html(getUserLoyaltyTier('platinum'));
            $("#silverPoints").html(map.silver);
            $("#goldPoints").html(map.gold);
            $("#vipPoints").html(map.platinum);
            var color="",
                highestLevel = false;
            if (map.loyaltyTier == "bronze") {
                var el = document.getElementById("aBorderBronze");
                el.style.color = "#c16e35";
            }
            else if (map.loyaltyTier == "silver") {
                var el = document.getElementById("aBorderSilver");
                el.style.color = "#a5a5a5";
            }
            else if (map.loyaltyTier == "gold") {
                var el = document.getElementById("aBorderGold");
                el.style.color = "#f7c300";
            }
            else if (map.loyaltyTier == "platinum") {
                var el = document.getElementById("aBorderVip");
                el.style.color = "#fef601";
                highestLevel = true;
            }

            // Populating the table
            // Not the highest level
            if (!highestLevel) {
                $('#notHighestLevel').show();
                $("#currentTier").html(getUserLoyaltyTier(map.loyaltyTier));
                if (map.next) {
                    $("#nextTier").html(getUserLoyaltyTier(map.next));
                }
                else {
                    $("#nextTier").html('-');
                }
                $("#nextPoints").html(map.qualify);
                $("#currentPoints").html(map.statusPoints);
                $("#requiredPoints").html(map.needed);
                $("#awardPoints").html(map.awardPoints);

                if (map.periodExpiry) {
                    $("#periodExpiryTr").show();
                    $("#periodExpiry").html(getFormattedDate(map.periodExpiry));
                }
            }
            // Highest level
            else {
                $('#highestLevel').show();
                $("#currentTierPlatinum").html(getUserLoyaltyTier(map.loyaltyTier));
                $("#currentPointsPlatinum").html(map.statusPoints);
                $("#qualifyPointsPlatinum").html(replaceParams(points, [map.qualify]));
                $("#awardPointsPlatinum").html(map.awardPoints);
                $("#periodExpiryPlatinum").html(map.periodExpiry ? getFormattedDate(map.periodExpiry) : '&mdash;');
            }
        }
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#loyaltyDetails_error"), error);
        }
    });
}

// Show x number of bonuses on the given page (in paginator)
function updateBonusesList(page_index, pagination_container) {

    $("#bonusesTable").hide();
    $("#bonusesTitle").hide();
    $("#bonusesError").hide();
    $('#bonusesTableBody').empty();

    progressIndicator(true);

    // Updating paginator
    clearAndRemoveBonusPaginator();
    pageIndex = parseInt(page_index+1);

    var status,
        data = "page=" + pageIndex;
        status = getBonusStatusSelector();

    $.when(ajaxCalls.getBonusInfo(status,data)).done(function(map) {

        if (!hasErrors(map)) {

            // Case 1: Transactions are fetched
            if(typeof map.bonuses != "undefined") {

                // Update info message on the page
                setBonusInfoMessage(map.bonuses);

                // Add the paginator; First Parameter: number of items; Second Parameter: options object
                bonusesItemsPerPage = map.bonuses.size;

                if(map.bonuses.count > bonusesItemsPerPage) {

                    $("#bonusesInfo").append('<div id="bonusesPaginator" class="pagination"></div>');
                    $("#bonusesPaginator").pagination(map.bonuses.count, {

                        items_per_page: bonusesItemsPerPage,
                        current_page: pageIndex-1,
                        callback: updateBonusesList
                    });

                    // Update the pagination icons to use current language
                    $(".prev").html(transactionPrevious);
                    $(".next").html(transactionNext);
                }

                // Filling in the data to the table
                populatePlayerBonuses(map.bonuses);
            }
        }

        // Case 2: Error occurred
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#bonusesError"), error);
        }

        progressIndicator(false);

    });
}

// Set title message for the current transaction tab (how many bonuses found etc.)
function setBonusInfoMessage(map) {

    // Set title of transactions
    var titleString = "";

    if(map.count > 0) {

        $("#bonusesTable").show();

        if(map.pageCount == 1) {
            titleString = replaceParams(bonusesFoundOne,[map.count]);
        }
        else {
            titleString = replaceParams(bonusesFoundMany,[map.count,map.size,map.pageCount]);
        }
    }
    else {
        $("#bonusesTable").hide();
        titleString = bonusesFoundZero;
    }
    $("#bonusesTitle").html(titleString).show();
}

// Get and show all player bonuses
function populatePlayerBonuses(map) {

    var newRow = '',
        freeSpinsAmount,
        createdTime,
        expiredDate,
        bonusWin,
        turnoverFactor,
        total,
        gamesLink,
        cancelBtn,
        gamesArr;

    $.each(map.transactions, function(index, bonus) {

        createdTime = bonus.createdTime ? getFormattedDate(bonus.createdTime) : '&mdash;';
        expiredDate = bonus.expirationTime ? getFormattedDate(bonus.expirationTime) : '&mdash;';
        freeSpinsAmount = bonus.freeSpins != null ? bonus.freeSpins : bonusfreeSpinsNA;
        bonusWin = bonus.winnings ? bonus.winnings : 0;
        total = bonus.total ? bonus.total : 0;
        turnoverFactor = bonus.turnoverFactor ? bonus.turnoverFactor : 0,
        gamesArr = bonus.games || [];
        gamesLink = ((bonus.status == "active") && (gamesArr.length > 0)) ? "<a onclick='showFreespinsGames(" + bonus.freeSpinBonusId + ")'>" + bonusGamesLink + "</a>" : "&mdash;";

        if (bonus.status == "active" && ~[2,6].indexOf((gamesArr[0] || {}).provider)) {  // Only campaigns related to Betsoft and Airdice free spins can be cancelled
            cancelBtn = '<input class="btn btn-default btn-xs" type="button" id="campaign-' + bonus.freeSpinBonusId + '" value="' + cancel + '" />';
        } else {
            cancelBtn = '&nbsp';
        }

        newRow =
            '<tr>' +
            '<td>' + bonus.name + '</td>' +
            '<td>' + createdTime + '</td>' +
            '<td>' + freeSpinsAmount + '</td>' +
            '<td>' + total + '</td>' +
            '<td>' + replaceParams(amountMode, [bonusWin.toFixed(2), getCurrencySymbol(bonus.currency)]) + '</td>' +
            '<td>' + turnoverFactor + '</td>' +
            '<td>' + expiredDate + '</td>' +
            '<td>' + bonus.status + '</td>' +
            '<td>' + gamesLink + '</td>' +
            '<td>' + cancelBtn + '</td>' +
            '</tr>';

        $('#bonusesTableBody').append(newRow);

        $("#campaign-" + bonus.freeSpinBonusId).click(function() {
            cancelBonus(bonus.freeSpinBonusId);
        });
    });

}

function getBonusStatusSelector() {

    var status = $("input[name='bonusStatus']:checked").val() || $("#bonusStatusWrapper input[type='radio']:first-child").val();
    return status;
}

function clearAndRemoveBonusPaginator() {

    if($("#bonusesPaginator").length > 0) {
        $("#bonusesPaginator").unbind();
        $("#bonusesPaginator").remove();
    }
}

function cancelBonus(freeSpinBonusId) {

    var data = "freeSpinBonusId=" + freeSpinBonusId;
    progressIndicator(true);
    $.when(cancelBonusesPost(data)).done(function(map) {
        if (!hasErrors(map)) {
            if (map.success) {
                updateBonusesList(pageIndex-1, null);
            }
            else {
                showErrorObj($("#bonusesError"), cancelBonusError);
            }
        }
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#bonusesError"), error);
        }
        progressIndicator(false);
    });
}

function cancelBonusesPost(data) {

    return $.ajax({
        url: "/billfold-api/player/bonus/cancel",
        type: "POST",
        data: data,
        dataType: "json"
    });
}