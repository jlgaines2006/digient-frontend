//---------------------------------------------------------------------------------------------------------------------
// Game opening -related functions
//

var gameToBeStarted = null;
var gameModeToBeStarted = null;

// Put the site to HTML5-fullscreen -mode
var pfx = ["webkit", "moz", "ms", "o", ""];

function RunFullScreenMethod(obj, method) {

	var p = 0, m, t;

	if(method === "RequestFullScreen" && document.msFullscreenEnabled) {
		method = "RequestFullscreen";
	}
	else if(method === "CancelFullScreen" && document.msFullscreenEnabled) {
		method = "ExitFullscreen";
	}

	while (p < pfx.length && !obj[m]) {

		m = method;

		if (pfx[p] == "") {
			m = m.substr(0,1).toLowerCase() + m.substr(1);
		}

		m = pfx[p] + m;
		t = typeof obj[m];

		if (t != "undefined") {
			pfx = [pfx[p]];
			return (t == "function" ? obj[m]() : obj[m]);
		}

		p++;
	}
}

// Initialize fullscreen toggle
function initFullScreenToggle() {

	// Show fullscreen toggle -button just if fullscreen is supported (hidden as default)
	if(parent.document.fullscreenEnabled || parent.document.mozFullScreenEnabled || parent.document.webkitFullscreenEnabled || parent.document.msFullscreenEnabled) {

		$("#gamePageButton_fullscreen").show();

		if(!parent.document.fullscreen && !parent.document.mozFullScreen && !parent.document.webkitIsFullScreen && !parent.document.msFullscreenElement) {
			$("#gamePageButton_fullscreen").addClass("gamePageButton_fullscreen_off");
		}

		// Add listeners that change the button's appearance if fullscreen is toggled
		parent.document.addEventListener("fullscreenchange", function () {

			if(parent.document.fullscreen) {
				$("#gamePageButton_fullscreen").removeClass("gamePageButton_fullscreen_off");
			}
			else {
				$("#gamePageButton_fullscreen").addClass("gamePageButton_fullscreen_off");
			}

		}, false);

		parent.document.addEventListener("mozfullscreenchange", function () {

			if(parent.document.mozFullScreen) {
				$("#gamePageButton_fullscreen").removeClass("gamePageButton_fullscreen_off");
			}
			else {
				$("#gamePageButton_fullscreen").addClass("gamePageButton_fullscreen_off");
			}

		}, false);

		parent.document.addEventListener("webkitfullscreenchange", function () {

			if(parent.document.webkitIsFullScreen) {
				$("#gamePageButton_fullscreen").removeClass("gamePageButton_fullscreen_off");
			}
			else {
				$("#gamePageButton_fullscreen").addClass("gamePageButton_fullscreen_off");
			}

		}, false);

		parent.document.addEventListener("MSFullscreenChange", function () {

			if(parent.document.msFullscreenElement) {
				$("#gamePageButton_fullscreen").removeClass("gamePageButton_fullscreen_off");
			}
			else {
				$("#gamePageButton_fullscreen").addClass("gamePageButton_fullscreen_off");
			}

		}, false);

		// What happens when the button is clicked
		$("#gamePageButton_fullscreen").click(function() {

			if(parent.document.fullscreen || parent.document.mozFullScreen || parent.document.webkitIsFullScreen || parent.document.msFullscreenElement) {

				parent.RunFullScreenMethod(parent.document, "CancelFullScreen");
			}
			else {
				var fullScreenElem = parent.document.getElementById("fullScreenWindow");

				parent.RunFullScreenMethod(fullScreenElem, "RequestFullScreen");
			}

		});
	}
}

// Check if there is a game-specific cookie - if yes, start the game written in cookie and destroy then the cookie
function checkGameCookie(cookieName) {

	var gamevalue = readCookie(cookieName);

	if(gamevalue != null && gamevalue != "") {
		var gameName = decodeURIComponent((new RegExp('[?|&]game=' + '([^&;]+?)(&|#|;|$)').exec(gamevalue)||[,""])[1].replace(/\+/g, '%20'))||null;
		var gameMode = decodeURIComponent((new RegExp('[?|&]mode=' + '([^&;]+?)(&|#|;|$)').exec(gamevalue)||[,""])[1].replace(/\+/g, '%20'))||null;

		openGame(gameName,gameMode);
		eraseCookie(cookieName);

		return true;
	}

	return false;
}

// Set global variables to start game automatically - after login, if started the game from thumbnail
function setGameForAutostart(name,mode) {

	gameToBeStarted = name;
	gameModeToBeStarted = mode;
}

// Open the game given as a parameter
function openGame(name,mode) {

	$(document).ready(function() {

		$.getJSON("/billfold-api/games/all", null, function(map) {

			if(map != null) {

				// Fill game select dropdown list; Remove name of the current game
				$.each(map.games, function(index, item) {

					if(item.name === name) {

                        var a = $('<a/>')
                            .addClass('promoMoneyEnabled_'+item.promoMoneyEnabled)
                        // Stream or any other live lobby
                        if (item.provider === "4" || item.liveGame) a.addClass('tempLiveGameLink')
                        else a.addClass('tempGameLink')
                        $("body").append(a)

						if(mode == "demo" && item.hideDemoUrl != true) {

                            // Stream or any other live lobby
                            if(item.provider === "4" || item.liveGame) {
								a.attr('href', '/liveGameWindow?name='+item.name+'&provider='+item.provider+'&thisIsDemo=1');
                                a.click(function() {redirect('/liveGameWindow?name='+item.name+'&provider='+item.provider+'&thisIsDemo=1')})
                            }
                            else {
								a.attr('href', '/demoGame?name='+item.name+'&provider='+item.provider+'&thisIsDemo=1');
                                a.click(function() {redirect('/demoGame?name='+item.name+'&provider='+item.provider+'&thisIsDemo=1')})
                            }
						}
						else if(mode == "real") {

                            // Stream or any other live lobby
							if(item.provider === "4" || item.liveGame) {
								a.attr('href', '/liveGameWindow?name='+item.name+'&provider='+item.provider);
                                a.click(function() {redirect('/liveGameWindow?name='+item.name+'&provider='+item.provider)})
                            }
                            else {
								a.attr('href', '/realGame?name='+item.name+'&provider='+item.provider);
                                a.click(function() {redirect('/realGame?name='+item.name+'&provider='+item.provider)})
                            }
						}
					}

				});

				defineGameOpening(".tempGameLink",gameWindowOpening,mode);
				defineGameOpening(".tempLiveGameLink",liveGameWindowOpening,mode);

				$(".tempGameLink").trigger("click");
				$(".tempGameLink").remove();
			}

		});

	});
}

// How the browser popup-window is opened for the new games (if gameWindowOpening or liveGameWindowOpening = browser)
function defineWindowOpening(gameLink, mode) {

	mode = mode || '';
	$(gameLink).click(function() {

		if((($(this).hasClass('promoMoneyEnabled_false') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false))
			&& !$(this).hasClass('freeSpinsEnabled_true') && mode != 'demo') {

			$.fancybox([ {href : '#lockPopup'} ]);
			return false;
		}
		else {

            // Calculate window dimensions according to aspect ratios of provider's games
            var providerID, windowDimensions;
            // Try to find provider id in url
            if ($(this).attr('href')) {
                providerID = getUrlParameter($(this).attr('href'), 'provider');
            }
            // Try to find provider id in class
            if (!providerID && $(this).attr('class')) {
                providerID = $(this).attr('class').match(/provider_([a-zA-Z0-9]+)/);
                if (providerID != null) {
                    providerID = providerID[1];
                }
            }
            // Calculate game window dimensions
            if (window['windowAspectRatio_provider_' + providerID]) {
                windowDimensions = calculateWindowDimensions({
                    windowRatio: window['windowAspectRatio_provider_' + providerID],
                    fixedWidth: window['windowFixedWidth_provider_' + providerID],
                    fixedHeight: window['windowFixedHeight_provider_' + providerID]
                });
            }
            else {
                windowDimensions = calculateWindowDimensions();
            }

			// Let's check if the game should be opened in a new or existing window
			var windowName = "";
			if(multipleGameWindows == false) {

				windowName = "gameWindow";
				if(gameWindowElem) {
					gameWindowElem.close();
				}
			}

			if($("#slider").length > 0) {
				// The command to stop slider on index-page (depending on the library)
                $('#slider').flexslider("stop");
			}
			if($("#winnerList").length > 0) {
				// JUST IN SUPERGAME: Stop the winnerlist reels during games
				clearInterval(winnerReelTimer);
			}

			gameWindowElem = window.open($(this).prop('href'), windowName, 'directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,resizable=yes,scrollbars=1,'+windowDimensions);

			if (window.focus) {

				gameWindowElem.focus();
			}

			var timer = setInterval(function() {
				if(gameWindowElem.closed) {
					clearInterval(timer);

					if($("#slider").length > 0) {
						// The command to start slider on index-page (depending on the library)
                        $('#slider').flexslider("play");
					}
					if($("#winnerList").length > 0) {
						// JUST IN SUPERGAME: Restart the winnerlist reel timer
						initWinnerReelTimer();
					}
				}
			}, 1000);
		}

		return false;
	});

}

function defineGameOpening(gamelistlink,method,mode) {

	mode = mode || '';
	$(gamelistlink).unbind('click');

	if(method === "browserEmpty") {

		var gameurl = "";
		var newurl1 = "";
		var newurl2 = "";

		$(gamelistlink).each(function(i, obj) {

			gameurl = $(this).attr("href");

			newurl1 = gameurl.replace("demoGame","emptyGameWindow");
			newurl2 = newurl1.replace("realGame","emptyGameWindow");

			$(this).attr("href",newurl2);
		});

		// Open games in new browser popup-windows, not in tabs
		defineWindowOpening(gamelistlink,mode);
	}
	if(method === "browser") {

		// Open games in new browser popup-windows, not in tabs
		defineWindowOpening(gamelistlink,mode);
	}
	else if(method === "fancybox" || method === "fancyboxBkg") {

		var gameurl = "";
		var newurl1 = "";
		var newurl2 = "";

		$(gamelistlink).each(function(i, obj) {

			if((($(this).hasClass('promoMoneyEnabled_false') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false))
				&& !$(this).hasClass('freeSpinsEnabled_true') && mode != 'demo') {

				$(this).click(function() {
					$.fancybox([ {href : '#lockPopup'} ]);
					return false;
				});
			}
			else {

				$(this).attr("data-fancybox-type","iframe");
				$(this).attr("title","");
				$(this).addClass("fancyboxGameIframe");

				gameurl = $(this).attr("href");

				newurl1 = gameurl.replace("demoGame","emptyGameWindow");
				newurl2 = newurl1.replace("realGame","emptyGameWindow");

				$(this).attr("href",newurl2);

				// Show game-specific background under fancybox
				if(method === "fancyboxBkg") {

					$(this).click(function() {

						// Set game-specific background picture
						var gameName = getParamValue("name", $(this).attr("href"));
						$("#gameBackground").attr("src","../../cms/images/gameBackgrounds/gameBkg_"+gameName+".jpg");

						// Scroll to top
						window.scrollTo(0,0);
						var bkgOffset = $('#main').offset();

						$('html, body').delay(700).animate({scrollTop: bkgOffset.top}, 1000, function() {
							$("#gameBackground").slideDown(250);
						});

					});
				}
			}
		});
	}
	else if(method === "fullscreen") {

		var gameUrl = "";
		var fullScreenWindow = '<iframe id="fullScreenWindow"></iframe>';

		$(gamelistlink).each(function(i, obj) {

			if((($(this).hasClass('promoMoneyEnabled_false') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false))
				&& !$(this).hasClass('freeSpinsEnabled_true') && mode != 'demo') {

				$(this).click(function() {
					$.fancybox([ {href : '#lockPopup'} ]);
					return false;
				});
			}
			else {

				$(this).click(function() {

					// If the browser is IE, maximize the window (fullscreen not used)
					var isIe = detectIEversion();
					if(isIe) {

						window.moveTo(0,0);

						if (document.all) {
							top.window.resizeTo(screen.availWidth,screen.availHeight);
						}

						else if (document.layers||document.getElementById) {

							if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth) {
								top.window.outerHeight = screen.availHeight;
								top.window.outerWidth = screen.availWidth;
							}
						}
					}

					// Check game's mode and url
					var currentMode = "";

					gameUrl = $(this).attr("href");

					if(gameUrl.indexOf("thisIsDemo") != -1) {
						currentMode = "demo";
					}
					else {
						currentMode = "real";
					}

					var currentGame = getParamValue("name");

					// Load the game in iframe
					$("body").prepend(fullScreenWindow);

					var fullScreenElem = document.getElementById("fullScreenWindow");
					$("#fullScreenWindow").attr("src",gameUrl);
					$("#wrapper").hide();

					// Delay the showing of iframe to remove "flickering white" screen
					setTimeout(function() {
						$("#fullScreenWindow").css("visibility","visible");
					}, 500);

					RunFullScreenMethod(fullScreenElem, "RequestFullScreen");

					window.location.hash = "#gameroomview";

					fullscreentimer = setInterval(function() { checkFullScreenUrl(currentGame,currentMode); }, 100);

					return false;
				});
			}
		});

	}
	else if(method === "gameInPage") {

		$(gamelistlink).each(function(i, obj) {

			if((($(this).hasClass('promoMoneyEnabled_false') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false))
				&& !$(this).hasClass('freeSpinsEnabled_true') && mode != 'demo') {

				$(this).click(function() {
					$.fancybox([ {href : '#lockPopup'} ]);
					return false;
				});
			}
		});

	}
}

// Check if the url field of fullscreen doesn't include anymore "gameroom". If it's not there close fullscreen.
// This is used to check if the user has clicked back-button
function checkFullScreenUrl(currentGame,currentMode) {

	var urlHash = window.location.hash;

	if(urlHash.indexOf("gameroom") == -1) {

		closeFullScreenGame(currentGame,currentMode);
	}
}

// Close fullscreen -game and return back to normal view
function closeFullScreenGame(currentGame,currentMode) {

	var fullScreenElem = document.getElementById("fullScreenWindow");

	if(fullScreenElem) {

		clearInterval(fullscreentimer);

		RunFullScreenMethod(document, "CancelFullScreen");
		$("#wrapper").show();
		$("#fullScreenWindow").remove();
	}

}

// Redirect player to other game when he plays in fullscreen mode
function changeFullScreenUrl(url) {

	$("#fullScreenWindow").attr("src",url);
}

// Return the current tabname when the player is in fullscreen
function getCurrentTab() {

	return oGamesList.activeGroup.name;
}

var popupButtonClickedOnGamePage = false;
var bgcError = "";

// Launched when loading demo or real game (excluding the live games and situations where the games are opened in fancybox)
function gameOnload() {

	// Get parameters from url
	var isDemo = getUrlParameter(null, 'thisIsDemo') == 1,
        name = getUrlParameter(null, 'name'),
        provider = getUrlParameter(null, 'provider'),
        gameParams = null;

	// Define gamewindow's width and height and what happens when the screen is resized
	var windowWidth, windowHeight;

	if(gameWindowOpening === "gameInPage") {

		var ratio = windowAspectRatio_default;

		if(window['windowAspectRatio_provider_'+provider]) {
			ratio = window['windowAspectRatio_provider_'+provider];
		}

		windowWidth = $("#wrapper").width();
		windowHeight = Math.floor(windowWidth/ratio);
	}
	else {

		var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0];
        windowWidth=w.innerWidth||e.clientWidth||g.clientWidth;
        windowHeight=w.innerHeight||e.clientHeight||g.clientHeight;
	}

	// Erase the game launch cookie as default
	eraseCookie("gamestart");

	// Set the game-related cookie if Balance- or Deposit-button is clicked and open deposit-page
	$("#gamePageButton_cashier").click(function() {
		createCookie("gamestart","?game="+name+"&mode=real",0);
		payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');
	});

	if($('#gameWindow').length > 0) {

		$('#gameWindow, #gameWindowEmpty').height(windowHeight - window['windowHeightOffset_default']);

		// Resize game window's height according to the wrapper's dimensions
		$(window).resize(function() {

			if(gameWindowOpening === "gameInPage") {

				windowWidth = $("#wrapper").width();
				windowHeight = Math.floor(windowWidth/ratio);
			}
			else {

				var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0];
				windowHeight=w.innerHeight||e.clientHeight||g.clientHeight;
			}

			$('#gameWindow, #gameWindowEmpty').height(windowHeight - window['windowHeightOffset_default']);
		});
	}

	$('#gameWindow').removeClass('gameWindowHidden');
	$("#msg").hide();

	// Demo game
	if (isDemo) {
        gameParams = getGameParams(name, 'demo');
		fillOtherGameWindowElements(name, provider, 'demo');
	}
	// Real game
	else {
        gameParams = getGameParams(name, 'real');
		fillOtherGameWindowElements(name, provider, 'real');
	}

	// Fill iframe with the game content or show error if url was not found
	if (!gameParams) {

		$("#msg").show();
		$("#msg").html(bgcError);
		bgcError = "";
	}
	else {

		// Play'n GO - desktop games
		if ((provider == '3' || typeof gameParams.flashvars !== 'undefined') && gameParams.url.indexOf('mobile') == -1) {
			$('#gameWindow').attr('src', '/play2goGame?url=' + encodeURIComponent(gameParams.url) + '&flashvars=' + encodeURIComponent(gameParams.flashvars) + '&provider=' + provider);
		}
        // TopGame provider
        else if (provider == '22') {
            var url = gameParams.url,
                domain = getUrlParameter(url, 'domain'),
                authToken = getUrlParameter(url, 'authToken'),
                gameId = getUrlParameter(url, 'gameId'),
				technology = getUrlParameter(url, 'technology'),
				gameUrl = compileTopGameUrl(domain, authToken, gameId, (technology != null ? 'U' : 'F'));

            $('#gameWindow').attr('src', gameUrl);
        }
		// Betinaction provider. Events.
		else if (provider == '28') {
			// Register postMessage listener
			$(window).on('message onmessage', function (e) {
				var data = e.originalEvent.data,
					hash = '';
				if ($.isPlainObject(data)) {
					if (data.method === 'showEvents') {
						hash = data.params.id || '';
					}
					else if (data.method === 'showEventDetails') {
						hash = data.params.id ? 'eventid=' + data.params.id : '';
					}
				}
				if (hash) {
					if (history.pushState) {
						history.pushState(null, null, '#' + hash);
					}
					else {
						location.hash = '#' + hash;
					}
				}
			});
			// Transfer location hash into game url in order to handle events
			var hashedUrl = location.hash ? (gameParams.url || '').split('#')[0] + location.hash : gameParams.url;
			$('#gameWindow').attr('src', hashedUrl);
		}
		// Other games
		else {
			$('#gameWindow').attr('src', gameParams.url);
		}

		// Airdice
		if (provider == '2') {
			listenAirdiceExitEvent();
		}
	}

	// When player selects game from dropdown, start that game for him
	$("#gamePageDropdown").change(function() {

		var nextGame = $('#gamePageDropdown :selected').val();

		if(nextGame != "Other") {

			if(gameWindowOpening === "fullscreen") {
				parent.changeFullScreenUrl(nextGame);
			}
			else {
				window.location.href = nextGame;
			}

		}
	});

	// If fancyboxes (like register form) are opened on the Flash-window, make sure that flash window is hidden when the boxes are floating on it
	$("#loginRegisterPopup_registerbutton, #loginRegisterPopup_forgotpassword").click(function() {

		popupButtonClickedOnGamePage = true;
	});
}

// Called when loading Play2Go -game screen
function onGamePlay2GoLoad() {

	var playUrl = getUrlParameter(null, 'url');

	if (playUrl != null) {

		var ga = document.createElement("script");
		ga.type = "text/javascript";
		ga.src = playUrl;

		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(ga, s);

	}
}

function generateSWFObject() {

    var url = getUrlParameter(null, 'url'),
        params = {
            flashvars: getUrlParameter(null, 'flashvars'),
            quality: 'high',
            scale: 'exactfit',
            base: '.',
            allowScriptAccess: 'always',
            wmode: 'opaque'
        };

    swfobject.embedSWF(url, 'gamecontent', '640', '480', '10.0.42.34', 'expressInstall.swf', false, params);
}

// Get url of the game given as parameter
function getGameParams(name, demoOrReal, forcibly) {

	var gameParams = null;

	$.ajax({
		cache: false,
		url: '/billfold-api/game/'+demoOrReal+'/' + name,
		type: 'get',
		dataType: 'json',
		async: false,
		actualize: !!forcibly,
		success: function(map) {

            if (typeof map.error === 'undefined') {
                if (map.url) {
                    gameParams = map;
                }
                else {
                    bgcError = msgOpenGameError;
                }
            }
            else {
                bgcError = getErrorMessage(map);
            }

		}
	});

	return gameParams;
}

// Populate game dropdown list (in the bottom right corner)
function fillOtherGameWindowElements(name,provider,demoOrReal) {

	$.getJSON("/billfold-api/games/all", null, function(map) {

		if(map != null) {

			map.games.sort(function(a, b) {
				if (a.name == b.name) {
					return 0;
				} else if (a.name > b.name) {
					return 1;
				}
				return -1;
			});

			// Fill game select dropdown list; Remove name of the current game
			$.each(map.games, function(index, item) {

				if(item.gameDisabled != true) {

					if(demoOrReal == "demo" && item.hideDemoUrl != true) {

                        if((item.liveGame || item.provider === "4") && gameWindowOpening === liveGameWindowOpening)   // Stream or any other live lobby
                            $("#gamePageDropdown").append('<option data-name="'+item.name+'" value="/liveGameWindow?name='+item.name+'&provider='+item.provider+'&thisIsDemo=1">'+getGameDisplayName(item.name)+'</option>');
                        else if(item.provider != "4" && item.liveGame == null)
                            $("#gamePageDropdown").append('<option data-name="'+item.name+'" value="/demoGame?name='+item.name+'&provider='+item.provider+'&thisIsDemo=1">'+getGameDisplayName(item.name)+'</option>');
					}
					else if(demoOrReal == "real") {

						if((item.liveGame || item.provider === "4") && gameWindowOpening === liveGameWindowOpening)   // Stream or any other live lobby
							$("#gamePageDropdown").append('<option data-name="'+item.name+'" value="/liveGameWindow?name='+item.name+'&provider='+item.provider+'">'+getGameDisplayName(item.name)+'</option>');
						else if(item.provider != "4" && item.liveGame == null)
							$("#gamePageDropdown").append('<option data-name="'+item.name+'" value="/realGame?name='+item.name+'&provider='+item.provider+'">'+getGameDisplayName(item.name)+'</option>');
					}

				}

			});

			$("#gamePageDropdown").prepend('<option data-name="Other" value="Other">'+msgOtherGames+'</option>');
			$("#gamePageDropdown option[value=Other]").attr("selected","selected");

			$("#gamePageDropdown option[data-name='"+name+"']").remove();
		}

		if(gameWindowOpening === "fullscreen") {

			// Style game dropdown
			if(styledInputs) {
				$("#gamePageDropdown").msDropdown();
			}

			// Load the game-specific rules
			$("#rulesPopup").load("../../cms/languages/"+currentLanguage+"/pages/info/gamerules/"+name+"_gamerules.html", function(responseText, statusText, xhr) {

				if(statusText == "success") {

					if(responseText.indexOf("Error") >= 0 ) {
						$("#rulesPopup").html("<h4 class='h3'>Error! The file 'languages/"+currentLanguage+"/pages/info/gamerules/"+name+"_gamerules.html' was not found!</h4>");
					}
				}
				else if(statusText != "success") {
					$("#rulesPopup").html("<h4 class='h3'>Error! The file 'languages/"+currentLanguage+"/pages/info/gamerules/"+name+"_gamerules.html' was not found!</h4>");
				}
			});
		}

	});

	// Fill link url to "Play for real" -button in the demogame page
	if(demoOrReal == "demo" || demoOrReal == "point") {

		var url = "/realGame?name="+name+"&provider="+provider;

		$('#demoGameWindow_gamename').val(name);
		$('#demoGameWindow_provider').val(provider);

		if(gameWindowOpening === "fullscreen") {

			$("#gamePageButton2_demoLoggedIn").click(function() {

				parent.changeFullScreenUrl(url);
				return false;
			});
		}
		else {
			$("#gamePageButton2_demoLoggedIn").attr("href",url);
		}

	}
}

// Called when loading game in fancybox (empty means that there is not button-footer in the game window, just the game)
function emptyGameWindowOnLoad() {

	var isDemo = getUrlParameter(null, 'thisIsDemo') == 1,
        name = getUrlParameter(null, 'name'),
        provider = getUrlParameter(null, 'provider'),
        gameParams = null;

	// Demo game
	if(isDemo) {
        gameParams = getGameParams(name, 'demo');
	}
	// Real game
	else {
        gameParams = getGameParams(name, 'real');
	}

	if(!gameParams) {

		$("#msg").show();
		$("#msg").html(bgcError);

		if(gameWindowOpening === "fancybox" || gameWindowOpening === "fancyboxBkg") {

			alert(bgcError);
			parent.$.fancybox.close();
		}
		else if(gameWindowOpening === "browserEmpty") {

			alert(bgcError);
		}

		bgcError = "";
	}
	else {

		// Play'n GO - desktop games
		if ((provider == '3' || typeof gameParams.flashvars !== 'undefined') && gameParams.url.indexOf('mobile') == -1) {
            location.href = '/play2goGame?url=' + encodeURIComponent(gameParams.url) + '&flashvars=' + encodeURIComponent(gameParams.flashvars) + '&provider=' + provider;
        }
        // TopGame provider
        else if (provider == '22') {
            var url = gameParams.url,
                domain = getUrlParameter(url, 'domain'),
                authToken = getUrlParameter(url, 'authToken'),
                gameId = getUrlParameter(url, 'gameId'),
				technology = getUrlParameter(url, 'technology'),
				gameUrl = compileTopGameUrl(domain, authToken, gameId, (technology != null ? 'U' : 'F'));

            location.href = gameUrl;
        }
        // Other games
		else {
			location.href = gameParams.url;
		}
	}

}

// Launched when loading live game
function liveGameWindowOnLoad() {

    var isDemo = getUrlParameter(null, 'thisIsDemo') == 1 || isDemoGame || !userIsLogged,
        name = getUrlParameter(null, 'name') || liveGameName,
        provider = getUrlParameter(null, 'provider') || gameProvider,
        gameParams = null;

	// What happens if the screen is resized
	var windowWidth, windowHeight;

	if(liveGameWindowOpening === "gameInPage") {

		var ratio = windowAspectRatio_default;

		if(window['windowAspectRatio_provider_'+provider]) {
			ratio = window['windowAspectRatio_provider_'+provider];
		}

		windowWidth = $("#wrapper").width();
		windowHeight = Math.floor(windowWidth/ratio);
	}
	else {

		var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0];
		windowWidth=w.innerWidth||e.clientWidth||g.clientWidth;
		windowHeight=w.innerHeight||e.clientHeight||g.clientHeight;
	}

	if($('#gameWindow').length > 0) {

        var gameWindowElement = $("#gameWindow"),
			gameWindowEmptyElement = $("#gameWindowEmpty");

        gameWindowElement.height(gameWindowElement.width()/ratio);
		gameWindowEmptyElement.height(gameWindowEmptyElement.width()/ratio);

		$(window).resize(function() {
			if(liveGameWindowOpening === "gameInPage") {

				windowWidth = $("#gameWindow").width();
				windowHeight = Math.floor(windowWidth/ratio);
			}
			else {
				var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0];
				windowHeight=w.innerHeight||e.clientHeight||g.clientHeight;
			}

			$("#gameWindow").height(windowHeight);
			$("#gameWindowEmpty").height(windowHeight);
		});
	}

	// Demo game
	if(isDemo) {
        gameParams = getGameParams(name, 'demo');
	}
	// Real game
	else {
        gameParams = getGameParams(name, 'real');
	}

	if(!gameParams) {

		$("#msg").show();
		$("#msg").html(bgcError);

		if(liveGameWindowOpening === "fancybox" || liveGameWindowOpening === "fancyboxBkg") {

			alert(bgcError);
			parent.$.fancybox.close();
		}

		bgcError = "";
	}
	else {

		if(liveGameWindowOpening === "browser") {
			window.location.href = gameParams.url;
		}
		else {
            var src = gameParams.url;

            // For Betinaction
            if (provider == '28') {
                /*var eventId = getUrlParameter(null, 'eventId');
                if (eventId) {
                    src += '#eventId=' + eventId;
                }*/

                // Register postMessage listener
                $(window).on('message onmessage', function (e) {
                    var data = e.originalEvent.data,
                        hash = '';
                    if ($.isPlainObject(data)) {
                        if (data.method === 'showEvents') {
                            hash = data.params.id || '';
                        }
                        else if (data.method === 'showEventDetails') {
                            hash = data.params.id ? 'eventid=' + data.params.id : '';
                        }
                    }
                    if (hash) {
                        if (history.pushState) {
                            history.pushState(null, null, '#' + hash);
                        }
                        else {
                            location.hash = '#' + hash;
                        }
                    }
                });
                // Transfer location hash into game url in order to handle events
                var hashedUrl = location.hash ? (gameParams.url || '').split('#')[0] + location.hash : gameParams.url;
                $('#gameWindow').attr('src', hashedUrl);
            }
            else {
                $('#gameWindow').attr('src', src);
            }
		}
	}
}

// Set auto call to billfold to identify that the user is playing for NO_ACTIVITY_TIME_LIMIT
function recordGameActivity() {

    $(document).ready(function() {
        autoRecord();
    });
}

// Auto call when playing the game
function autoRecord() {
    $.ajax({
        url: "/billfold-api/game/playing",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (map) {

            // Relaunch the autoUpdate() in a time less than noActivityTimeLimit. NoActivityTimeLimit is in mins
            if (map.noActivityTimeLimit != null) {
                setTimeout(autoRecord, 60000*(map.noActivityTimeLimit - 0.5));
            }
        }
    });
}

// Returns launch url for a TopGame provider's game. Taken from http://finplay1.mobile.dev-env.biz/gs2c/common/js/lobby/GameLib.js
function compileTopGameUrl(domain, token, symbol, technology, platform, language, cashierUrl, lobbyURL, secureLogin) {
	var key = encodeURIComponent(['token=' + token, '&symbol=' + symbol, '&technology=' + technology, '&platform=' + platform, '&language=' + language, '&cashierUrl=' + cashierUrl, '&lobbyURL=' + lobbyURL].join(""));
	if (secureLogin) {
		return ['https://' + domain, '/gs2c/playGame.do?key=' + key + '&stylename=' + secureLogin].join("");
	}
	else {
		return ['https://' + domain, '/gs2c/playGame.do?key=' + key ].join("");
	}
}

// Intercept Airdice exit game event and close the game
function listenAirdiceExitEvent() {
	// Register postMessage listener
	$(window).on('message onmessage', function (e) {
		var data = e.originalEvent.data;
		if (data === 'closeGame') {
			if (gameWindowOpening === 'fancybox' || gameWindowOpening === 'fancyboxBkg') {  // Close fancybox
				window.parent.$.fancybox.close();
			}
			else if (gameWindowOpening === 'browser' || gameWindowOpening === 'browserEmpty') {  // Close window
				window.parent.open('', '_self').close();
			}
			else {  // Redirect to home page
				window.parent.location.href = '/';
			}
		}
	});
}