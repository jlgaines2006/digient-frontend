var depositProvidersIds = {skrill: 70002, neteller: 110002, adyenSkrill: 60013, adyenNeteller: 60021}

function prefillOnLoad() {

    $(document).ready(function() {

        // Changing styles of the buttons
        var url = window.location.href.toString();
        url = url.split("?")[1];

        // If it is prefill deposit page
        if (url == "deposit") {

            document.getElementById('depositPageButton').className = " accountviewButton accountviewbutton_active btn btn-default active";
            $('#depositHeader').show();
            $('#helpMessage').html ( depositHelp );
        }

        var requiredFieldsMap = {};
		progressIndicator(true);

		// Short timeout (delay) is needed that progress indicator can be visible also in Chrome
		setTimeout(function() {

        	$.when(initPrefillRequest(), ajaxCalls.playerDetailsRequest()).done(function (prefillResult, playerResult) {

                if (!hasErrors(prefillResult[0]) && !hasErrors(playerResult[0])) {

                    playersCountry = playerResult[0].country;

					progressIndicator(false);
                    document.getElementById("loadingContent").style.visibility = "visible";

                    prefillingPlayerInformation(prefillResult[0],playerResult[0],"deposit_savebutton1");
                }
            });

		},50);

    });
}

// Set button style to "disabled" if the validation fails
function setButtonStyle(buttonId, requiredFieldsMap) {

	var isOK = true;

	$.each(requiredFieldsMap, function(index, value) {

		// If this field is mandatory and exists on the page
		if(value.mandatory && $("#" + index).length > 0) {

			if(index === "address" || index === "houseNumber") {

				showRegisterErrorMessages = false;
				if(!validateAddressField("address","houseNumber","error")) {
					isOK = false;
				}
				showRegisterErrorMessages = true;
			}
			else {

				if (!checkEmpty(index) ) {
					isOK = false;
				}

				// Validation of phonenumber
				else if( index == "phoneNumber") {

					if(!validatePhoneNumber("phoneNumber", true)) {
						isOK = false;
					}
				}

				// Validation of bank details
				else if( index == "bicString") {

					if(!validateBankCode("bicString")) {
						isOK = false;
					}
				}
				else if( index == "internationalBankAccount") {

					if(!validateBankAccount_IBAN("internationalBankAccount")) {
						isOK = false;
					}
				}
			}
            // Remove duplicated spaces from all fields
            $('#' + index).val(removeDuplicatedSpaces(index));
		}
	});

    if(isOK) {
        $("#" + buttonId).removeClass("disabled");
    }
    else {
        $("#" + buttonId).addClass("disabled");
    }
}

// Go to the 2nd Deposit page
function beforeLoadProviders() {

    var emptyFields = false;

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

		// Get the fields
		$.when(initPrefillRequest()).done(function(map) {

			progressIndicator(false);

			if(!hasErrors(map)) {

				$.each(map, function(index, value) {

					$("#" + index + "_error").html("");

					// If this field is mandatory
					if(value.mandatory && $("#"+index).length > 0) {

						if(index != "address" && index != "houseNumber") {

							if (!checkEmpty(index) ) {
								$("#" + index + "_error").html(msgValidatorEmpty);
								emptyFields = true;
							}

							// Validation of phonenumber
							else if( index == "phoneNumber") {

								if(!validatePhoneNumber("phoneNumber")) {

									$("#"+ index +"_error").html(msgPhoneValidationError);
									emptyFields = true;
								}
							}
						}

						// Validation of address and housenumber
						else if(index === "address") {

							if(!validateAddressField(index,"houseNumber","error")) {
								emptyFields = true;
							}
						}
					}

                    // Remove duplicated spaces from all fields
                    $('#' + index).val(removeDuplicatedSpaces(index));
				});
			}

			if( !emptyFields ) {

				var data = $("#depositForm").serializeAndModify({
                    checkboxesAsBools: true,
                    modifier: afterSerialization
                });

				progressIndicator(true);

				// Add small delay to the sending of info to make sure that progress indicator is displayed
				setTimeout(function() {

					$.when(checkNoDuplicates(data)).done(function(map) {

						progressIndicator(false);

						if (map.success) {

							$.when(prefillUpdatePost(data)).done(function(map) {

								if(!hasErrors(map)) {

									// Redirecting to the right page
									var url = window.location.href.toString();
									url = url.split("?")[1];

									// If it is prefill deposit page
									var lang = $.getCookie("_lang");
									if (lang == null) lang = defaultLanguage;
									if (url.indexOf("deposit") != -1) {
										redirect('/' + lang + '/postlogin/payment/deposit');
									}
								}
								else {
                                    var error = getErrorMessage(map);
                                    showErrorObj($("#deposit_error"), error);
								}
							});
						}
						else {
                            var error = getErrorMessage(map);
                            showErrorObj($("#deposit_error"), msgDuplicatedData);
						}
					});

				},50);
			}
		});

	},50);
}

var selectedPaymentMethod = {},
    methods = {};

function depositOnLoad() {

    $(document).ready(function() {

		progressIndicator(true);

		// Short timeout (delay) is needed that progress indicator can be visible also in Chrome
		setTimeout(function() {

			$.when(getPaymentProviders()).done(function (map) {

                if(!hasErrors(map)) {

                    methods = map;

                    var currency = getCurrencySymbol(methods.currency),
                        supportedCurrency;

                    $("#currency_for_input").html(currency);
                    $("#deposit2_promocode").html(methods.bonusCode);

                    // On change supported currencies selector
                    $('#supportedCurrencies-select').change(function () {
                        supportedCurrency = $(this).val();
                        updateFixedAmountExchange(selectedPaymentMethod, supportedCurrency);
                        updateCustomAmountExchange(selectedPaymentMethod, supportedCurrency);
                    });

                    generatePaymentProvidersList(methods);

                    // Limits info displaying
                    if(methods.depositLimitActive) {

                        var limitMessage = replaceParams(depositAccumulatedLimitMessage,[getFormattedDate(methods.startTime), getFormattedDate(methods.endTime), parseFloat(methods.depositLimitAmount).toFixed(2), currency, parseFloat(methods.available).toFixed(2), currency]);
                        $("#depositLimitMessage").show();
                        $("#depositLimitMessage").html( limitMessage );
                    }

                    // Errors displaying
                    if (methods.isDepositLimitReaced) {
                        showErrorObj($("#mainDeposit_error"), msgPaymentLimitException, $("#mainDeposit_info"));
                        disableMainDepositConfirmBtn();
                        $("#depositLimitMessage").hide();
                    }

                    // Displaying the forms
                    $("#deposit2_forms").show();

                    // Displaying the payment results
                    var url = window.location.href.toString();
                    url = url.split("&")[0];
                    url = url.split("=")[1];
                    if (url != null) {
                        if (url == "pending" || url == "ok" || url.indexOf("bonus_ok") != -1) {
                            loadUserTrackingScript('deposit');

                            $("#mainDeposit_info").show();
                            var infoMess = 'msgDeposit' + capitalizeString(url);
                            $("#mainDeposit_info").html( eval(infoMess) );

                            // Check if there is cookie that will make the user to go back to the game immediately
                            setTimeout(function() {
                                checkGameCookie("gamestart");
                            },1000);
                        }
                        else if (url == "cancelled" || url.indexOf("failure") != -1 || url == "rejected" || url == "error" || url.indexOf("bonus_invalid_bonus_code") != -1 || url.indexOf("bonus_user_not_eligible") != -1 ) {
                            var error = 'msgDeposit' + capitalizeString(url),
                                error = eval(error);
                            showErrorObj($("#mainDeposit_error"), error, $("#mainDeposit_info"));
                        }
                    }

                    // Displaying info msg if there are pending withdrawals
                    if (methods.hasPendingWithdrawal) $("#pendingWithdrawals_info").show();

                    // Populate campaigns dropdown
                    populateCampaignDropdown(methods.campaigns, 'deposit2_campaignsFieldContainer', 'deposit2_promoFieldContainer', {}, currency);
                    // Populate campaigns dropdown on amount change
                    $('input[name="paymentSum"]:radio').change(function () {
                        populateCampaignDropdown(methods.campaigns, 'deposit2_campaignsFieldContainer', 'deposit2_promoFieldContainer', { amount: getDepositAmount() }, currency);
                    });
                    $('#deposit2_giveAmount, #deposit2_doublezeros').blur(function () {
                        populateCampaignDropdown(methods.campaigns, 'deposit2_campaignsFieldContainer', 'deposit2_promoFieldContainer', { amount: getDepositAmount() }, currency);
                    }).keyup(function () {
                        updateCustomAmountExchange(selectedPaymentMethod, supportedCurrency);
                    });

                    // Get url parameters for a prefilling of fields
                    var prefilledMethod = parseInt(getUrlParameter(null, 'method'), 10) || 0,
                        prefilledAmount = getUrlParameter(null, 'amount') || '';
                    // (order of conditions is important)
                    if (prefilledMethod) {
                        $('#' + prefilledMethod + '_radio').click();
                    }
                    if (prefilledAmount) {
                        if (prefilledMethod === 150001) {
                            $('#deposit2_promocode').val(prefilledAmount);
                        }
                        else {
                            prefilledAmount = parseFloat(prefilledAmount) || 0;
                            $('#deposit2_giveAmount').val(Math.floor(prefilledAmount)).focus();
                            $('#deposit2_doublezeros').val(prefilledAmount.toFixed(2).split('.', 2)[1]);
                        }
                    }

                }
                else {
                    if (map.errorCode === 125) {
                        // Redirect to prefill deposit page
                        return window.location.href = '/postlogin/payment/prefill?deposit';
                    }
                    else {
                        var error = getErrorMessage(map);
                        showErrorObj($("#mainDeposit_error"), error, $("#mainDeposit_info"));
                        disableMainDepositConfirmBtn();
                    }
                }

                progressIndicator(false);
                document.getElementById("loadingContent").style.visibility = "visible";
			});

		},50);

        $('#deposit2_giveAmount, #deposit2_doublezeros').on('focus', function () {
            $('#deposit2_amountRadioButton').attr('checked', true).trigger('change');
        });
    });
}

function disableMainDepositConfirmBtn() {
    document.getElementById('deposit_savebutton2').className = "btn btn-primary btn-lg disabled";
    document.getElementById('confirmButtonClicked').checked=true;
}

function capitalizeString(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function generatePaymentProvidersList(map) {

    if (map.methods.length == 0) {
        var error = getErrorMessage(map);
        showErrorObj($("#mainDeposit_error"), noDepositMethodsAvailable, $("#mainDeposit_info"));
    }
    else {
        for (var i = 0; i < map.methods.length; i++) {
            addPaymentProviderListElement(map.methods[i]);
        }
    }

    // If none of the methods is selected, select the 1st one
    if (!$('[name="paymentMethods"]').is(':checked')) {
        $('#' + map.methods[0].method + '_radio').attr('checked',true);
        providerOnClick(map.methods[0]);
    }
}

function addPaymentProviderListElement(method) {

	var provider = method.provider,
        methodId = method.method,
		element;

	if (method.defaultMethod) var checked = 'checked="checked"';

	element =
        '<li class="radio">' +
            '<label for="' + methodId + '_radio">' +
                '<input id="' + methodId + '_radio" class="depositRadio" type="radio" name="paymentMethods" value="' + methodId + '" ' + checked + ' />' +
                '<div class="deposit2_imgWrapper img-thumbnail">' +
                    '<img src="/resources/images/icons/' + provider + '_' + methodId + '.png" title="' + getPaymentProv(provider, methodId) + '" alt="' + getPaymentProv(provider, methodId) + '" class="depositImg" />' +
                '</div>' +
            '</label>' +
        '</li>';

	$("#deposit2_providers").append(element);

    // Offline bank
    if (methodId == '10000') {
        $.when(ajaxCalls.playerDetailsRequest()).done(function(map) {
            var spanText = $('#offline-bank-info-span').html();
            $('#offline-bank-info-span').html(spanText + '&nbsp;' + map.offlineReference);
        });
    }

	$("#" + methodId + "_radio").click(function() {
		providerOnClick(method);
	});

    // For the first loading
	if (method.defaultMethod) providerOnClick(method);
}

// Handle the event the player selects a method from the list
function providerOnClick(method) {

    selectedPaymentMethod = method;

    var currency = getCurrencySymbol(methods.currency),
		provider = selectedPaymentMethod.provider,
		methodId = selectedPaymentMethod.method,
        fixedAmount = !!method.fixedAmount;

	  $("#paymentSum_error").html("");
    $("#paymentSum_errorWrapper").hide();
	  $("#mainDeposit_error").hide();
	  $("#deposit_savebutton2").removeClass("disabled");
    $("#depositCampaignHasNotBeenSelected").hide();

    // List of possible payment sums
    if (method.amountList.length != 0) {
        $('#offline-payment-info').hide();
        $('#deposit2_amountForm').show();
		$("#amountOptions").show();
        $("#amountHeader").show();
		$("#promoHeader").hide();
		$("#deposit2_promoDesc").show();
        // Deal with campaigns
        $('#deposit2_campaignsFieldContainer').show();

        // Populate campaigns dropdown
        populateCampaignDropdown(methods.campaigns, 'deposit2_campaignsFieldContainer', 'deposit2_promoFieldContainer', { amount: getDepositAmount() }, currency);
    }
    // For Bonus method and Offline payment
    else {
        // Offline info
        if (methodId == '10000') {
            $('#offline-payment-info').show();
            $('#deposit2_amountForm').hide();
        }
        // Billfold bonus
        else {
            $('#offline-payment-info').hide();
            $('#deposit2_amountForm').show();
            $("#amountOptions").hide();
            $("#amountHeader").hide();
            $("#promoHeader").show();
            $("#deposit2_promoDesc").hide();
            // Deal with campaigns
            $('#deposit2_campaignsFieldContainer').hide();
            $('#deposit2_promoFieldContainer').removeClass('hidden');
            $('#deposit2_promocode').attr('disabled', false);
        }
	}

	// If this deposit method is limited
	if (method.limited && !method.limitReached) {
		var limitAmount = replaceParams(amountMode, [method.limitAmount, currency]),
			availableAmount = replaceParams(amountMode, [(method.limitAmount - method.limitUsed).toFixed(2), currency]),
			error = replaceParams(limitedMethodMessage, [getPaymentProv(provider, methodId), limitAmount, availableAmount]);
		showErrorObj($('#limitedMethodMessage'), error);
		$("#deposit_savebutton2").removeClass("disabled");
	}
	else if (method.limited && method.limitReached) {
		showErrorObj($('#limitedMethodMessage'), replaceParams(unverifiedLimitIsReached, [getPaymentProv(provider, methodId)]));
		$('#deposit_savebutton2')[0].className = "btn btn-primary btn-lg disabled";
	}
	else if (!method.limited) {
		$('#limitedMethodMessage').hide();
		$("#deposit_savebutton2").removeClass("disabled");
	}

    // Do not allow to input custom amounts in case of activated "fixed amount" property
    $('#deposit2_amountGroupWrapper').toggle(!fixedAmount);
    $('#deposit2_amountRadioButton').prop('disabled', fixedAmount);
    $('#deposit2_giveAmount').prop('disabled', fixedAmount);
    $('#deposit2_doublezeros').prop('disabled', fixedAmount);

    if (fixedAmount) {
        $('#deposit2_amountRadioButton').attr('checked', false).trigger('change');
    }

    // "Stored Payments" fields population on the deposit page
    populateStoredPayments(method);
    // Supported currencies population
    populateSupportedCurrencies(method);

    // Forms styling
    $(function() {
        if ( $("#deposit2_paymentForm_wrapper").height() > $("#deposit2_amountForm_wrapper").height() ) {
            $("#deposit2_paymentForm_wrapper").addClass("bordered");
            $("#deposit2_amountForm_wrapper").removeClass("bordered");
        }
        else {
            $("#deposit2_amountForm_wrapper").addClass("bordered");
            $("#deposit2_paymentForm_wrapper").removeClass("bordered");
        }
    });
}

// Confirm button on the 1nd Deposit page
function firstDepositConfirm() {

    var paymentAmount = getPaymentAmount(selectedPaymentMethod),
        isCampaignsSelectorValid = isDepositCampaignsSelectorValid();

    if (paymentAmount != null && isCampaignsSelectorValid) {

        // Processing the fee
        var fee = selectedPaymentMethod.fee,
            percentageFee = selectedPaymentMethod.percentageFee;

        if (fee != null || percentageFee != null) {

            var playerCurrency = methods.currency,
                playerCurrencySymbol = getCurrencySymbol(playerCurrency),
                supportedCurrency = $('#supportedCurrencies-select').val(),
                supportedCurrencySymbol = getCurrencySymbol(supportedCurrency),
                currency = supportedCurrencySymbol || playerCurrencySymbol,
                overallFee = 0,
                depositAmount = parseFloat(paymentAmount),
                overallDeposit = 0;

            var getExchangeMsg = function (amount) {
                var rate = parseFloat((selectedPaymentMethod.supportCurrencies || {})[supportedCurrency]) || 0;
                return supportedCurrency ? ' <small class="exchange">' + replaceParams(amountMode, [(amount * rate).toFixed(2), playerCurrencySymbol]) + '</small>' : '';
            };

            if (fee != null) {
                overallFee = parseFloat(fee);
            }

            if (percentageFee != null) {
                overallFee = overallFee + Math.round(depositAmount*percentageFee)/100;
            }

            // Fee is included in the selected amount
            if (selectedPaymentMethod.feeStrategy == 0 || selectedPaymentMethod.feeStrategy == null) {

                overallDeposit = depositAmount - overallFee;
                $("#totalDeducted").hide();
                $("#totalToAccount").show();
                $("#depositTableNote").html(replaceParams(depositTableNote, [overallFee.toFixed(2), currency]));
            }

            // Fee is added on top of the selected amount
            else {

                overallDeposit = depositAmount + overallFee;
                $("#totalDeducted").show();
                $("#totalToAccount").hide();
                $("#depositTableNote").html(replaceParams(depositTableAddedNote, [overallFee.toFixed(2), currency]));
            }

            $("#depositTableNote").show();
            $("#depositTableDeposit").html(replaceParams(amountMode, [paymentAmount, currency]) + getExchangeMsg(paymentAmount));
            $("#depositTableFee").html(replaceParams(amountMode, [overallFee.toFixed(2), currency]) + getExchangeMsg(overallFee));
            $("#depositTableNet").html(replaceParams(amountMode, [overallDeposit.toFixed(2), currency]) + getExchangeMsg(overallDeposit));

            document.getElementById('depositTableConfirmButtonClicked').checked = false;
            document.getElementById('depositTableContinue').className = "confirm btn btn-primary btn-lg";
            $("#depositError").html("");
            $("#depositError").hide();

            // Checking the deposit amount to be greater than 0
            if (selectedPaymentMethod.feeStrategy == 0 && overallDeposit <= 0) {
                document.getElementById('depositTableContinue').className = "confirm btn btn-primary btn-lg disabled";
                document.getElementById('depositTableConfirmButtonClicked').checked=true;
                $("#depositError").html(transactionError);
                $("#depositError").show();
            }
            $.fancybox([ {href : '#depositTable'} ]);
        }
        else {
            initDeposit();
        }
    }
}

function getPaymentAmount(method) {

    var amount = getAmount(method),
        availableDepositAmount = methods.available,
        currency = getCurrencySymbol(methods.currency);

    if (amount != false || amount === 0) {

        amount = parseFloat(amount);

        // Validate amount against available
        if ( amount > parseFloat(availableDepositAmount) ) {

            $("#paymentSum_error").html( replaceParams(depositAvailableAmount,[availableDepositAmount, currency]) );
            $("#paymentSum_errorWrapper").show();
            amount = null;

        } else {

            amount = amount.toFixed(2);

            $("#mainDeposit_error").hide();
            $("#mainDeposit_info").hide();
        }
    } else {
        amount = null;
    }

    return amount;
}

function isDepositCampaignsSelectorValid() {
    var selectedCampaign = $('#deposit2_campaigns').val();

    if ($('#deposit2_campaignsFieldContainer').css('display') === 'none' || selectedCampaign !== '') {
        $("#depositCampaignHasNotBeenSelected").hide();
        return true;
    }
    else {
        $("#depositCampaignHasNotBeenSelected").html(depositNoCampaignSelected).show();
        return false;
    }
}

function confirmDepositTable() {

    if (document.getElementById('depositTableConfirmButtonClicked').checked == false) {

        document.getElementById('depositTableContinue').className = "confirm btn btn-primary btn-lg disabled";
        document.getElementById('depositTableConfirmButtonClicked').checked=true;

        initDeposit();
    }
}

function initDeposit() {
    // If we need to ask additional data
    if (selectedPaymentMethod.fields.length != 0 && selectedPaymentMethod.detailsFilled == false) {

        $("#additionalInfo_notSend").empty();
        $("#additionalInfo_depositConfirmation").empty();
        $("#depositForm").show();

        generateDepositConfirmationList();

        // Displaying deposit confirmation popup
        $.fancybox([ {href : '#depositConfirmation'} ]);
    }
    else secondDepositConfirm(null);
    if (document.getElementById('depositConfirmationContinue') != null) document.getElementById('depositConfirmationContinue').className = "confirm btn btn-primary btn-lg";
    if (document.getElementById('depositTableContinue') != null) document.getElementById('depositTableContinue').className = "confirm btn btn-primary btn-lg";
    if (document.getElementById('confirmButtonClicked') != null) document.getElementById('confirmButtonClicked').checked=false;
}

// Return the money amount to be deposited
function getAmount(method) {

    $("#paymentSum_error").html("");
    $("#paymentSum_errorWrapper").hide();

	var amount = $("input[name='paymentSum']:checked").val(),
		result = false;

	// If the method is "bonus", then return zero
	if(method.method == 150001) {

		if(checkEmpty("deposit2_promocode")) return 0;
		else {
			$("#paymentSum_error").html(msgValidatorEmpty);
            $("#paymentSum_errorWrapper").show();
			return false;
		}
	}

	if(amount != null) {

		if(amount == '0') {
			var input_amount = $("#deposit2_giveAmount").val() + "." + $("#deposit2_doublezeros").val();
			if (validateDepositAmount(input_amount, method)) result = input_amount;
		}
		else {
			if (validateDepositAmount(amount, method)) result = amount;
		}
	}

	else {
		$("#paymentSum_error").html(depositSelectAmount);
        $("#paymentSum_errorWrapper").show();
		result = false;
	}

	return result;
}

function generateDepositConfirmationList() {

    var element, star,
        providerName = selectedPaymentMethod.provider,
        methodId = selectedPaymentMethod.method;

    $.each(selectedPaymentMethod.fields, function(index, value) {

        var fieldName = value.field,
            fieldType = value.fieldType,
            defaultValue = (value.defaultValue == null) ? '' : value.defaultValue,
            maxLength = parseInt(value.maxLength) || 256;

        if (value.optional) {
            var checked = 'checked="checked"';
            star = "";
        }
        else {
            star = " *";
        }

        if (value.defaultValue != null) defaultValue = value.defaultValue;

        if (fieldType == "text") {

            element =
                '<li id="' + fieldName + '_item" class="form-group has-feedback">' +
                    '<label id="' + fieldName + '_label" class="control-label" for="' + fieldName + '">' +
                        getProviderExtraInfo(providerName, methodId, fieldName) + star +
                    '</label>' +
                    '<input class="form-control" type="' + fieldType + '" id="' + fieldName + '" maxlength="' + maxLength + '" value="' + defaultValue + '">';

            // Adding hint
            var hintMessage = getProviderHintInfo(providerName, methodId, fieldName);
            if (hintMessage != null) {
                element = element +
                    '<div class="form-control-feedback">' +
                        '<i alt="?" title="' + hintMessage + '" class="fa fa-question fa-lg"></i>' +
                    '</div>';
            }

            // Adding picture for CVC
            if (fieldName.toString().indexOf('cvc') != -1) {
                element = element +
                    '<div class="form-control-feedback">' +
                        '<i alt="?" id="cvcImg" class="fa fa-question fa-lg"></i>' +
                    '</div>';
            }

            element = element +
                    '<span id="' + fieldName + '_error" class="depositFieldError help-block error-field">' +
                    '</span>' +
                    '<input type="checkbox" style="display: none;" id="' + fieldName + '_' + "optional" + '" ' + checked + '>' +
                '</li>';

        }

        else if (fieldType == "select") {

            element =
                    '<li id="' + fieldName + '_item" class="form-group">' +
                        '<label id="' + fieldName + '_label" class="control-label" for="' + fieldName + '">' +
                            getProviderExtraInfo(providerName, methodId, fieldName) + star +
                        '</label>' +
                        '<select id="' + fieldName + '" class="selector form-control">';

            // Adding options to the select element
            var options = value.dataList.split(',');

            for ( var i = 0; i < options.length; i++ ) {

                var value = options[i];
                if (value == 'MAST') options[i] = 'MASTERCARD';
                if (value == 'VISADEBIT') {
                    value = "VISA";
                    options[i] = 'VISA Debit';
                }

                if (defaultValue == value) element = element + '<option value="' + value + '" selected>' + options[i] + '</option>';
                else element = element + '<option value="' + value + '">' + options[i] + '</option>';
            }

            element = element +
                        '</select>' +
                        '<span id="' + fieldName + '_error" class="depositFieldError help-block error-field">' +
                        '</span>' +
                        '<input type="checkbox" style="display: none;" id="' + fieldName + '_' + "optional" + '" ' + checked + '>' +
                    '</li>';
        }
        $("#providerRegister").hide();
        /*
        if (selectedPaymentMethod.helpLink != null && selectedPaymentMethod.helpLink.url != null && !~[120001, 120002, 120003, 190001].indexOf(methodId)) {

            $("#registerForProviderStartText").html(replaceParams(registerForProviderStartText, [getPaymentProv(providerName, methodId)]));
            document.getElementById('providerRegisterLink').href = selectedPaymentMethod.helpLink.url;
            document.getElementById('providerRegisterLink').setAttribute('target', selectedPaymentMethod.helpLink.target);
            $("#providerRegister").show();
        }
        */

        $("#securePayment").show();
        $("#additionalInfo_depositConfirmation").append(element);

        // Showing CVC image
        $("#cvcImg").hover(function() {$("#cvc").show();}, function() {$("#cvc").hide();});
    });

    // Add descriptor for selected payment provider
    $("#depositDescriptor").html(getPaymentDescriptor(providerName));
}

function afterDepositConfirmation() {

    // If not a credit card
    if (!~[120001, 120002, 120003, 130016, 130017, 130018, 130019, 130020, 190001, 200001, 220001].indexOf(selectedPaymentMethod.method)) {
        validateBankDetails('depositForm');
    }
    else {
        secondDepositConfirm(null);
    }
}

// Deposit is confirmed
function secondDepositConfirm(pwd) {

    if (document.getElementById('confirmButtonClicked').checked == false) {

        var data = generateConfirmDepositData();

        if (data != null) {

            if (pwd != null) {
                data = data + '&password=' + encodeURIComponent(pwd);
                var ajaxCall;

                // TODO: Refactor this hard coding. It is now possible to fetch detailType from paymentDetails json.
                if (selectedPaymentMethod.method == depositProvidersIds.skrill || selectedPaymentMethod.method == depositProvidersIds.adyenSkrill) {
                    ajaxCall = 3;
                }
                else if (selectedPaymentMethod.method == depositProvidersIds.neteller || selectedPaymentMethod.method == depositProvidersIds.adyenNeteller) {
                    ajaxCall = 4;
                }
                else ajaxCall = null;

                progressIndicator(true);

                if (ajaxCall != null) {
                    $.when(updatePaymentDetails(ajaxCall, data)).done(function(map) {

                        progressIndicator(false);

                        if(hasErrors(map)) {
                            var error = getErrorMessage(map);
                            showErrorObj($("#mainDeposit_error"), error, $("#mainDeposit_info"));
                        }
                        else sendConfirmDeposit(data);
                    });
                }
                else sendConfirmDeposit(data);
            }
            else sendConfirmDeposit(data);
        }
    }
}

function sendConfirmDeposit(data) {

    document.getElementById('deposit_savebutton2').className = "btn btn-primary btn-lg disabled";
    document.getElementById('depositConfirmationContinue').className = "confirm btn btn-primary btn-lg disabled";
    document.getElementById('confirmButtonClicked').checked=true;

    progressIndicator(true);

    // Add small delay to the sending of info to make sure that progress indicator is visible in Chrome
    setTimeout(function() {

        $.fancybox.close();

        // Confirm deposit
        $.when(confirmDepositAjax(data)).done(function(map) {

            progressIndicator(false);

            if(!hasErrors(map)) {

                var redirectUrl = map.redirectUrl,
                    formTarget = map.formTarget || '_top',
                    formMethod = map.formMethod || 'POST';

                if(formMethod == "REDIRECT") {
                    redirect(redirectUrl);
                }
                else {
                    var form = document.createElement('form');
                    form.setAttribute('target', formTarget);
                    form.setAttribute('action', redirectUrl);
                    form.setAttribute('method', formMethod);

                    for (var i = 0, len = map.params.length; i < len; ++i) {
                        var param = map.params[i],
                            name = param.name,
                            value = param.value;
                        if (name == 'amount' && typeof value != 'string') value = value.toFixed(2);

                        var hiddenField = document.createElement('input');
                        hiddenField.setAttribute('type', 'hidden');
                        hiddenField.setAttribute('name', name);
                        hiddenField.setAttribute('value', value);

                        form.appendChild(hiddenField);
                    }

                    document.body.appendChild(form);
                    form.submit();
                }

            } else {
                var error = getErrorMessage(map);
                showErrorObj($("#mainDeposit_error"), error, $("#mainDeposit_info"));
            }
        });
    },50);
}

function confirmDepositAjax(data) {
    return $.ajax({
        url: "/billfold-api/payment/confirmDeposit",
        type: "POST",
        dataType: "json",
        async: false,
        data:data
    });
}

function getDepositAmount() {
    var amount = $('input[name="paymentSum"]:checked').val();

    if (amount == '0') {
        amount = $('#deposit2_giveAmount').val()  + '.' + $('#deposit2_doublezeros').val();
    }

    return parseFloat(amount) || 0;
}

function generateConfirmDepositData() {

    var returnData = true,
        bonusCode = $('#deposit2_promocode:visible').val(),
        campaignId = parseInt($('#deposit2_campaigns').val(), 10) || 0,
        supportedCurrency = $('#supportedCurrencies-select').val(),
        data = 'amount=' + getDepositAmount().toFixed(2) + (supportedCurrency ? '&currency=' + supportedCurrency : '') + '&method=' + selectedPaymentMethod.method;

    if (selectedPaymentMethod.fields.length != 0) {

        // Append additional fields from inputs
        $('ul#additionalInfo_depositConfirmation input[type!="checkbox"]').each(function() {

            var id = $(this).attr('id'),
                value = $(this).val().replace(/\+/g,'%2B');

            data = addToDataIfValidated(data, id, value);
            if (data == null) returnData = false;
        });

        // Append additional select fields
        $('ul#additionalInfo_depositConfirmation select').each(function() {

            var id = $(this).attr('id');
            var selectedObject = document.getElementById(id);
            var selectedOption = selectedObject.options[selectedObject.selectedIndex].value;

            data = addToDataIfValidated(data, id, selectedOption);
            if (data == null) returnData = false;
        });
    }

    // (order of conditions is important)
    if (bonusCode) {
        data += '&code=' + bonusCode;
    }
    else if (campaignId) {
        data += '&campaignId=' + campaignId;
    }

    // Collect data for the use of "Stored Payments" on the deposit page
    var storedDetailReference = $('#storedPayments-select').val();
    if (storedDetailReference) {
        data += '&storedDetailReference=' + storedDetailReference;
    }

    if (returnData) {
        return data;
    }
    else return null;
}

function addToDataIfValidated(data, id, value) {

    var returnData = true;

    // Remove spaces in number and cvc fields : customer_cc_cvc, customer_dc_cvc, customer_cc_number, customer_dc_number or any other number
    if (id.indexOf('cvc') != -1 || id.indexOf('number') != -1) {
        value = removeSpaces(id);
    }

    // Element is optional
    if (document.getElementById(id + '_optional').checked)
    {
        data = data + "&" + id + "=" + value;
    }

    // Element is required
    else {

        if (!checkEmpty(id) ) {
            $("#" + id + "_error").html( msgValidatorEmpty );
            returnData = false;
        }

        // If is an email
        else if ( id.indexOf('Email') != -1 && isEmail(id) == false ) {
            $("#" + id + "_error").html( msgValidatorEmail );
            returnData = false;
        }

        // Field is ok
        else {
            data = data + "&" + id + "=" + value;
        }
    }

    if (returnData) {
        return data;
    }
    else return null;
}

function loadPendingWithdrawTrans() {
    var lang = $.getCookie("_lang");
    if (lang == null) lang = defaultLanguage;
    redirect('/' + lang + '/postlogin/payment/transactions?withdrawal');
}
//---------------------------------------------------------------------------------------------------------------------
// Withdrawal-related Functions
//
function prefillWithdrawalOnLoad() {

    $(document).ready(function() {

        var requiredFieldsMap = {};
		progressIndicator(true);

		// Short timeout (delay) is needed that progress indicator can be visible also in Chrome
		setTimeout(function() {

        	$.when(initPrefillWithdrawalRequest(), ajaxCalls.playerDetailsRequest()).done(function (prefillResult, playerResult) {

                if (!hasErrors(prefillResult[0]) && !hasErrors(playerResult[0])) {

                    playersCountry = playerResult[0].country;

					progressIndicator(false);
                    document.getElementById("loadingContent").style.visibility = "visible";

                    prefillingPlayerInformation(prefillResult[0],playerResult[0],"prefillWithdrawal_savebutton");
                }
            });

		},50);
    });
}

function prefillWithdrawalContinue() {

        var emptyFields = false;

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			// Get the fields
			$.when(initPrefillWithdrawalRequest()).done(function(map) {

				progressIndicator(false);

				if(!hasErrors(map)) {

					$.each(map, function(index, value) {

						$("#" + index + "_error").html("");

						// If this field is mandatory
						if(value.mandatory && $("#"+index).length > 0) {

							if(index != "address" && index != "houseNumber") {

								if (!checkEmpty(index) ) {

									$("#" + index + "_error").html( msgValidatorEmpty );
									emptyFields = true;
								}

								// Validation of phonenumber
								else if( index == "phoneNumber") {

									if(!validatePhoneNumber("phoneNumber")) {

										$("#"+ index +"_error").html( msgPhoneValidationError );
										emptyFields = true;
									}
								}

							}

							// Validation of address and housenumber
							else if(index === "address") {

								if(!validateAddressField(index,"houseNumber","error")) {
									emptyFields = true;
								}
							}
						}

                        // Remove duplicated spaces from all fields
                        $('#' + index).val(removeDuplicatedSpaces(index));
					});

				}
				if( !emptyFields ) {

					var data = $("#withdrawalPrefill_form").serializeAndModify({
                        checkboxesAsBools: true,
                        modifier: afterSerialization
                    });

					$.when(checkNoDuplicates(data)).done(function (map) {

						if (map.success) {
							$.when(prefillWithrawalPost(data)).done(function(map) {

								if(!hasErrors(map)) {

									var lang = $.getCookie("_lang");
									if (lang == null) lang = defaultLanguage;

									redirect('/' + lang + '/postlogin/payment/withdrawal/withdrawal');
								}
								else {
                                    var error = getErrorMessage(map);
                                    showErrorObj($("#withdrawalPrefill_error"), error);
								}
							});
						}
						else {
                            showErrorObj($("#withdrawalPrefill_error"), msgDuplicatedData);
						}
					});
				}
			});

		},50);
}

var withdrawProviderMap = {};

function withdrawalOnLoad() {

	$(document).ready(function() {

		progressIndicator(true);

		// Short timeout (delay) is needed that progress indicator can be visible also in Chrome
		setTimeout(function() {

            $.when(initWithdrawAjax()).done(function(map) {
                if(map != null) {

                    $("#withdrawal_error").hide();
                    $("#withdrawal_info").hide();

                    if(typeof map.error == "undefined") {

                        withdrawProviderMap = map;

                        var promoBalance = map.promoBalance,
                            cashBalance = map.cashBalance,
                            remainingTurnover = map.currentTurnover,
                            currency = getCurrencySymbol(map.currency),
                            revokeBonus = map.revokeBonus,
                            methodListElement = "";

                        $("#withdrawalCash").html(replaceParams(amountMode, [parseFloat(cashBalance).toFixed(2), currency]));
                        $("#withdrawalBonus").html(replaceParams(amountMode, [parseFloat(promoBalance).toFixed(2), currency]));
                        $("#withdrawalRemainingTurnover").html(replaceParams(amountMode, [parseFloat(remainingTurnover).toFixed(2), currency]));
                        $("#withdrawalForm").show();

                        // Sort and populate the method menu if there are payment methods available
                        if(map.methods.length > 0 ) {

                            map.methods.sort(function (a, b) {
                                return a.order - b.order;
                            });

                            var defProviderSelected = '',
                                defProviderIsSelected = false;

                            $.each(map.methods, function(index, item) {
                                if (item.defaultMethod || map.methods.length == 1) {
                                    defProviderSelected = 'selected="selected"';
                                    defProviderIsSelected = true;
                                    selectWithdrawalMethod(item.method);
                                }
                                else {
                                    defProviderSelected = '';
                                }
                                methodListElement += "<option value='" + item.method + "' " + defProviderSelected + ">" + getWithdrawalMethodName(item.method) + "</option>";
                            });

                            // If none of the methods is selected, select the 1st one
                            if (!defProviderIsSelected && map.methods.length > 0) {
                                $('#withdrawalMethod option:first').attr('selected', true);
                                selectWithdrawalMethod(map.methods[0].method);
                            }

                            // Add options to selector and optionally stylize the dropdown
                            $("#withdrawalMethod").html(methodListElement);

                            // Drawing payment details popup
                            $.when(ajaxCalls.playerDetailsRequest()).done(function(playerMap) {
                                drawPaymentDetailsTab(map.methods, playerMap);
                            });
                        }

                        // No withdrawal payments available
                        else {
                            $("#withdrawalMethod").html("<option value='0'>" + withdrawalType_nothing + "</option>").attr("disabled","disabled");
                            $("#withdrawalAmountWrapper").hide();
                            disableWidhdrawalBtn();
                        }

                        if (revokeBonus) $("#revokeBonus").show();

                        if(styledInputs) {

                            setTimeout(function() {
                                stylizeDropdown("#withdrawalMethod",10);
                            }, 10);
                        }
                    }
                    else {
                        if (map.errorCode === 125) {
                            // Redirect to prefill withdrawal page
                            return window.location.href = '/postlogin/payment/withdrawal/prefillWithdrawal';
                        }
                        else {
                            displayWithdrawalError(map);
                        }
                    }

                    // Displaying the withdrawal results
                    var url = window.location.href.toString();
                    url = url.split("=")[1];
                    if (url == "ok") {
                        $("#withdrawal_error").hide();
                        $("#withdrawal_info").show();
                        $("#withdrawal_info").html( withdrawalSuccess );
                    }
                    else if (url == "failure") {
                        $("#withdrawal_info").hide();
                        $("#withdrawal_error").show();
                        $("#withdrawal_error").html( withdrawalFailure );
                    }
                    // Show the content
                    progressIndicator(false);
                    document.getElementById("loadingContent").style.visibility = "visible";
                }
            });
		},50);
    });
}

function initWithdrawAjax() {
    return $.ajax({
        url: "/billfold-api/payment/initWithdraw",
        type: "GET",
        dataType: "json",
        async: false
    });
}

// Get the name of the given withdrawal-method
function getWithdrawalMethodName(method) {

	var name = window["withdrawalType_default"]+" "+method;

	if(window["withdrawalType_"+method]) {
		name = window["withdrawalType_"+method];
	}

	return name;
}

// Send withdrawal-related data to server
function withdrawGet(data) {

	progressIndicator(true);

    // Collect data for the use of "Stored Payments" on the withdrawal page
    var storedDetailReference = $('#storedPayments-select').val();
    if (storedDetailReference) {
        data += '&storedPayoutReference=' + storedDetailReference;
    }

    return $.ajax({

		url: "/billfold-api/payment/withdraw",
		type: "POST",
		data: data,
		dataType: "json",
		async: false,
		success: function(map) {

			progressIndicator(false);

            if(!hasErrors(map)) {

                var redirectUrl = map.redirectUrl || '';

                if(redirectUrl) {
                    redirect(redirectUrl);
                }
                else {
                    if(typeof map.error == "undefined") {

                        if ( map.success ) {

                            $("#withdrawal_error").hide();
                            enableWithdrawalBtn();

                            var lang = $.getCookie("_lang");
                            if (lang == null) lang = defaultLanguage;
                            redirect('/' + lang + '/postlogin/payment/withdrawal/withdrawal?withdrawalStatus=ok');

                        }
                    } else {
                        $.fancybox.close();
                        displayWithdrawalError(map);
                    }
                }

			} else {
                var error = getErrorMessage(map);
                showErrorObj($("#withdrawal_error"), error, $("#withdrawal_info"));
            }
		}
	});
}

function displayWithdrawalError(map) {

    var error = getErrorMessage(map);
    showErrorObj($("#withdrawal_error"), error, $("#withdrawal_info"));

    if(map.errorCode == "127" || map.errorCode == "126"){
        $("#withdrawalForm").hide();
    }
}

// Execute provider-specific withdrawal operation
function withdraw() {

    if (document.getElementById('saveButtonClicked').checked == false) {

        $("#withdrawalAmount_error").html("");
        $("#withdrawalAmount").val("");

        var selector = document.getElementById("withdrawalMethod"),
            methodId = selector.options[selector.selectedIndex].value;

        var amount = getWithdrawalAmount();

        if (amount != false) {

            var data = "amount=" + amount + "&method=" + methodId,
                fee = selectedWithdrawalMethod.fee,
                percentageFee = selectedWithdrawalMethod.percentageFee,
                feeStrategy = selectedWithdrawalMethod.feeStrategy || 0,
                withdrawal,
                currency = getCurrencySymbol(withdrawProviderMap.currency);

            $("#withdrawalData").val(data);

            if (fee != null || percentageFee != null) {

                var overallFee = 0;
                if (fee != null) overallFee = parseFloat(fee);
                if (percentageFee != null) overallFee = overallFee + Math.round(amount*percentageFee)/100;

                // Fee is deducted from the withdrawal amount
                if (feeStrategy == 0) {
                    withdrawal = parseFloat(amount) - overallFee;
                    $("#withdrawalNet").show();
                    $("#withdrawalDeducted").hide();
                    $("#withdrawalNote1").html( replaceParams(withdrawalNote1, [overallFee.toFixed(2), currency]) );
                }

                // Fee is added on top of the withdrawal amount
                else {
                    withdrawal = parseFloat(amount) + overallFee;
                    $("#withdrawalDeducted").show();
                    $("#withdrawalNet").hide();
                    $("#withdrawalNote1").html( replaceParams(withdrawalNoteDeducted, [overallFee.toFixed(2), currency]) );
                }

                $("#withdrawalNote1").show();
                $("#withdrawalConfirmationWithdrawal").html(replaceParams(amountMode, [amount, currency]));
                $("#withdrawalConfirmationFee").html(replaceParams(amountMode, [overallFee.toFixed(2), currency]));
                $("#withdrawalConfirmationTotal").html(replaceParams(amountMode, [withdrawal.toFixed(2), currency]));

                document.getElementById('withdrawalConfirmContinue').className = "confirm btn btn-primary btn-lg";
                document.getElementById('withdrawalConfirmButtonClicked').checked = false;
                $("#withdrawalError").html("");
                $("#withdrawalError").hide();

                // Check withdrawal amount with the cash balance
                $.when(getBalance()).done(function(map) {
                    // If cash balance is not enough for withdrawal amount + fee
                    if ((feeStrategy == 1 && map.cashBalance < withdrawal) || (feeStrategy == 0 && map.cashBalance < amount)) {
                        document.getElementById('withdrawalConfirmContinue').className = "confirm btn btn-primary btn-lg disabled";
                        document.getElementById('withdrawalConfirmButtonClicked').checked=true;
                        $("#withdrawalError").html(notEnoughCashToWithdraw);
                        $("#withdrawalError").show();
                    }
                });
                $.fancybox([ {href : '#withdrawalConfirm'} ]);
            }
            else {
                afterFeeConfirmation(data);
            }
        }
    }
}

function afterFeeConfirmation(data) {

    // Displaying banking pop up
    if (!selectedWithdrawalMethod.detailsFilled && selectedWithdrawalMethod.fields.length != 0) {
        showBankInfoPopup(selectedWithdrawalMethod.provider, selectedWithdrawalMethod.method);
    }

    // All the other methods
    else {
        withdrawGet(data);
    }
}

function showBankInfoPopup(provider, methodId) {

    $("#selectedBankAccountType").val(provider + '_' + methodId);
    if (document.getElementById("selectedBankAccountType") != null) document.getElementById("selectedBankAccountType").disabled = true;
    var header = '<h4 class="h1" id="bankAccountInfo">' + bankInfoHeader + '</h4>';

	if (document.getElementById('bankAccountInfo') == null) {
		$("#bankInfo").prepend(header);
	}

	if(styledInputs) {

		setTimeout(function() {
			stylizeDropdown("#selectedBankAccountType",10);
		},50);
	}

    toggleBankInfo(provider + '_' + methodId);
    $.fancybox([ {href : '#bankInfo'} ]);
}

// Validate the size withdrawal of amount before submitting the form
function validateWithdrawalAmount(amount) {

    var result = true,
        minValue = getWithdrawalRange('min'),
        maxValue = getWithdrawalRange('max'),
        regexString = /^[0-9]+(\.[0-9]{0,2})?$/,
        currency = getCurrencySymbol(withdrawProviderMap.currency);
    minValue = parseFloat(minValue);
    maxValue = parseFloat(maxValue);

    if( !checkEmpty("withdrawalAmountNumber") ) {

        $("#withdrawalAmount_error").html(msgValidatorEmpty);
        result = false;
    }
	else if (isNaN(amount) || (!regexString.test($("#withdrawalAmountNumber").val())) || (!regexString.test($("#withdrawalAmountDecimal").val()))) {

		$("#withdrawalAmount_error").html(depositWrongAmount);
        result = false;
    }
    else if (!isNaN(minValue) && amount < minValue) {
        $("#withdrawalAmount_error").html(replaceParams(minWithdrawalAmount, [minValue, currency]));
        result = false;
    }
    else if (!isNaN(maxValue) && amount > maxValue) {
        $("#withdrawalAmount_error").html(replaceParams(maxWithdrawalAmount, [maxValue, currency]));
        result = false;
    }
    else if (amount > withdrawProviderMap.cashBalance) {
        $("#withdrawalAmount_error").html(insufficientFunds);
        result = false;
    }

    return result;
}

// Return the current withdrawal amount
function getWithdrawalAmount() {

    // Combine withdrawal amount -value by combining number- and decimal-fields (the fields "withdrawalAmountNumber" and "withdrawalAmountDecimal")
    var withdrawalAmount_numberPart = parseInt($("#withdrawalAmountNumber").val()),
        withdrawalAmount_decimalPart = $("#withdrawalAmountDecimal").val(),
        decimal1 = parseInt(withdrawalAmount_decimalPart.charAt(0)),
        decimal2 = parseInt(withdrawalAmount_decimalPart.charAt(1));

    if(isNaN(decimal2)) {
        decimal2 = 0;
    }

    var amount = parseFloat(withdrawalAmount_numberPart+(decimal1/10)+(decimal2/100)).toFixed(2);
    $("#withdrawalAmount").val(amount);

	if (validateWithdrawalAmount(amount) == false) amount = false;

	return amount;
}

// Close fancybox and send data after clicked "Continue" in Skrill-payment popup
function confirmWithdrawalDetails() {

    if (document.getElementById('confirmButtonClicked').checked == false) {

        if (isEmail('email')) {

            document.getElementById('withdrawalDetailsContinue').className = "confirm btn btn-primary btn-lg disabled";
            document.getElementById('confirmButtonClicked').checked = true;

            $.fancybox.close();

            var accountNumber = $("#email").val().replace(/\+/g,'%2B'),
                selector = document.getElementById("withdrawalMethod"),
                type = selector.options[selector.selectedIndex].value,
				amount = getWithdrawalAmount(),
                data = "amount=" + amount + "&method=" + type + "&accountNumber=" + accountNumber;

            withdrawGet(data);
        }
        else {
            $("#email_error").html( msgValidatorEmail );
        }
    }
}

var selectedWithdrawalMethod = {};

// Change the current withdrawal method
function selectWithdrawalMethod(value) {

	$.each(withdrawProviderMap.methods, function(index, item) {

		if(item.method == value) {

            selectedWithdrawalMethod = item;

            // "Stored Payments" fields population on the withdrawal page
            populateStoredPayments(item, false);
		}
	});

	// Updating message
    updateMessage();
}

// Get MIN or MAX amount of the withdrawal
function getWithdrawalRange(value) {

    var fee = parseFloat(selectedWithdrawalMethod.fee) || 0,
        percentageFee = parseFloat(selectedWithdrawalMethod.percentageFee) || 0,
        amount = 0;

    if (value == 'min') {
        amount = parseFloat(selectedWithdrawalMethod.minAmount) || 0;
        // Fee is included to the withdrawal amount
        if (selectedWithdrawalMethod.feeStrategy == 0) {
            amount += parseFloat(fee) + Math.round(amount*percentageFee)/100;
        }
    }
    else if (value == 'max') {
        amount = parseFloat(selectedWithdrawalMethod.maxAmount) || 0;
    }

    return amount;

}

// Submit withdrawal confirmation popup form
function confirmWithdrawal() {

    if (document.getElementById('withdrawalConfirmButtonClicked').checked == false) {

        document.getElementById('withdrawalConfirmContinue').className = "confirm btn btn-primary btn-lg disabled";
        document.getElementById('withdrawalConfirmButtonClicked').checked=true;

		var data = $('#withdrawalData').val();
        afterFeeConfirmation(data);
    }
}

// Update content of the message that shows minimum and maximum value; Update also the status of Confirm-button.
function updateMessage() {

    var currency = getCurrencySymbol(withdrawProviderMap.currency),
        cashBalance = parseFloat(withdrawProviderMap.cashBalance),
        minValue = getWithdrawalRange('min'),
        maxValue = getWithdrawalRange('max');

    cashBalance = Math.round(cashBalance*100)/100;
    minValue = Math.round(minValue*100)/100;
    maxValue = Math.round(maxValue*100)/100;

	if (minValue > 0) {

        enableWithdrawalBtn();

        if (maxValue > cashBalance) {
            maxValue = cashBalance;
        }
        if (maxValue > minValue) {
            $("#minWithdrawalValue").html( replaceParams(fromToWithdrawalValue,[parseFloat(minValue).toFixed(2), currency, parseFloat(maxValue).toFixed(2), currency]) );
        } else {
            $("#minWithdrawalValue").html( replaceParams(minWithdrawalValue,[parseFloat(minValue).toFixed(2), currency]) );

            if (maxValue < minValue) {
                disableWidhdrawalBtn();
            }
        }
    }
    else {
        $("#minWithdrawalValue").html(withdrawalAmount);
    }
}

function disableWidhdrawalBtn() {

    document.getElementById('withdrawal_savebutton').className = "btn btn-primary btn-lg disabled";
    document.getElementById('saveButtonClicked').checked = true;
}

function enableWithdrawalBtn() {

    document.getElementById('withdrawal_savebutton').className = "btn btn-primary btn-lg";
    document.getElementById('saveButtonClicked').checked = false;
}

// Get and show stored payment data for payment method (e.g. Adyen "Recurring Payments")
function populateStoredPayments(method, showNone) {
    showNone = typeof showNone === 'boolean' ? showNone : true;

    var selectBox = $('#storedPayments-select'),
        options = '';

    if (method.stored) {
        var storedData = method.storedData || [];

        for (var i = 0; i < storedData.length; i++) {
            options = '<option value="' + storedData[i].reference + '">' + storedData[i].description + '</option>' + options;
        }
        if (showNone) {
            options = '<option value="">' + storedPayments_none + '</option>' + options;
        }

        selectBox.removeAttr('disabled').html(options).find('option:eq(-' + i + ')').attr('selected', true);
        if (!i) {
            selectBox.attr('disabled', true);
        }
        $('#storedPayments').show();
    }
    else {
        selectBox.attr('disabled', true).html(options);
        $('#storedPayments').hide();
    }
}

// Populate list of the fixed amounts
function updateFixedAmountExchange(method, currency) {
    currency = currency || methods.currency;

    var amountList = method.amountList || [],
        currencySymbol = getCurrencySymbol(currency),
        playerCurrency = methods.currency,
        playerCurrencySymbol = getCurrencySymbol(playerCurrency),
        rate = parseFloat((method.supportCurrencies || {})[currency]) || 0,
        hasExchange = currency !== playerCurrency,
        minValue = parseFloat(method.minAmount) || 0,
        maxValue = parseFloat(method.maxAmount) || 0;

    for (var i = 0; i < 4 && i < amountList.length; i++) {
        var amount = parseFloat(amountList[i]) || 0,
            exchangeMsg = hasExchange ? ' <small class="exchange">' + replaceParams(amountMode, [(amount * rate).toFixed(2), playerCurrencySymbol]) + '</small>' : '';

        $('#paymentSum_option' + i).html(replaceParams(amountMode, [amount.toFixed(2), currencySymbol]) + exchangeMsg);
        $('#option' + i).val(amount);
    }

    $('#currency_for_input').html(currencySymbol);
    $('#amount_range').html(replaceParams(amountRangeFromTo, [minValue.toFixed(2), currencySymbol, maxValue.toFixed(2), currencySymbol]));
}

// Update exchange message for the input of the custom amount
function updateCustomAmountExchange(method, currency) {
    currency = currency || methods.currency;

    var amount = parseFloat($('#deposit2_giveAmount').val() + '.' + $('#deposit2_doublezeros').val()) || 0,
        playerCurrency = methods.currency,
        playerCurrencySymbol = getCurrencySymbol(playerCurrency),
        rate = parseFloat((method.supportCurrencies || {})[currency]) || 0,
        hasExchange = currency !== playerCurrency;

    $('#amount_exchange').html(hasExchange ? replaceParams(amountMode, [(amount * rate).toFixed(2), playerCurrencySymbol]) : '').toggleClass('hidden', !hasExchange);
}

// Populate selector of the supported currencies for the method
function populateSupportedCurrencies(method) {
    var $container = $('#supportedCurrencies'),
        $select = $('#supportedCurrencies-select'),
        currencies = method.supportCurrencies || {},
        hasCurrencies = false,
        playerCurrency = methods.currency,
        playerCurrencySymbol = getCurrencySymbol(playerCurrency),
        options = '';

    if (!currencies.hasOwnProperty(playerCurrency)) {
        for (var currency in currencies) {
            var rate = parseFloat(currencies[currency]) || 0,
                currencySymbol = getCurrencySymbol(currency),
                exchangeMsg = replaceParams(amountMode, [(1).toFixed(2), currencySymbol]) + ' = ' + replaceParams(amountMode, [rate.toFixed(2), playerCurrencySymbol]);

            options += '<option value="' + currency + '">' + currency + ' (' + exchangeMsg + ')' + '</option>';
            hasCurrencies = true;
        }
    }

    $container.toggleClass('hidden', !hasCurrencies);
    $select.html(options).trigger('change').prop('disabled', !hasCurrencies);
}