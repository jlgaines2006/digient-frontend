var playersCountry = "",
    pageIndex = 0,
    detailType = {},
    paymentDetailsFields = {};

function myaccOnLoad() {

	$(document).ready(function() {

		// Style upload type checkboxes (if needed)
		if(styledInputs) {

			setTimeout(function() {
				stylizeDropdown("#uploadTypeSelector",10);
			}, 10);
		}

		progressIndicator(true);

        // Loading player's data
        $.when(initDetailsGet(), ajaxCalls.playerDetailsRequest(), fetchBankInfoDetails()).done(function (initResult, playerResult, bankInfoDetails) {

            var initMap = initResult[0],
                playerDetails = playerResult[0],
                bankInfoMap = bankInfoDetails[0];

            if (!hasErrors(initMap) && !hasErrors(playerDetails)) {

                playersCountry = playerDetails.country;

                var identificationObj = {},
                    identificationFilledField = '';

                if (initMap.address != null) {
                    initMap.houseNumber = initMap.address;
                }
                if (initMap.residentialAddress != null) {
                    initMap.residentialHouseNumber = initMap.residentialAddress;
                }

                $.each(initMap, function(index, value) {

                    var maxLength = value.maxLength || 128;

                    // Displaying fields
                    $("#" + index + "_item").show();

                    // Displaying stars for obligatory fields
                    if (value.mandatory) $("#star_" + index).show();

                    // Assigning values to the fields except for password fields
                    if (index != "oldPassword" && index != "newPassword" && index != "confirmNewPassword") {
                        var content = (playerDetails[index] != null) ? playerDetails[index] : "";

                        // Add maxLength attribute
                        $('#' + index).attr('maxLength', maxLength);

                        if (value.readOnly && content != "") {
                            document.getElementById(index).className = "disabledInput form-control";
                            document.getElementById(index).readOnly = true;
                            document.getElementById(index).value = content;
                        } else {
                            $('#' + index).val("");
                        }
                        if (index == "phoneNumber") {

                            if (content) {
                                document.getElementById(index).value = content;
                            }
                            else document.getElementById(index).value = window["phoneCode_"+playerDetails.country];

                            // Adding example in the hint msg
                            addPhoneNumberExample("phoneNumberHint", playerDetails.country);
                        }
                        else if (index == "gender") {
                            // Adding gender information
                              if (content) document.getElementById('registerGender'+ content).checked = true;
                        }

                        if (index != "receiveEmail" && index != "campaignsEnabled" && index != "ssn" && index != "phoneNumber") {

                            // Assigning values to input objects
                            $('#' + index, '#updateMyAccountForm').val(content);
                        }

                        if ((index == "userName" || index == "nickName") && (content != "")) {

                            document.getElementById(index).className = "disabledInput form-control";
                            document.getElementById(index).readOnly = true;
                        }

                        // For the identification field
                        if (oFields.identification[index]) {
                            identificationObj[index] = !!value;
                            $('#identification_item').show();
                            if (value) {
                                $('#star_identification').show();
                            }
                            if (content) {
                                identificationFilledField = index;
                            }
                        }
                    }
                });

                // Identification field
                populateIdentificationDropdown(identificationObj, 'identification', null, '_item', identificationFilledField);

                // Dealing with receiveEmail separately. If it is true, we check the checkbox
                if (playerDetails.receiveEmail) {

                    $('#myAcc_receiveEmail').attr('checked', true);
                    $("#loggedInBanner2").show();
                }
                else $("#loggedInBanner2").hide();

                // Dealing with campaignsEnabled separately. If it is true, we check the checkbox
                if (playerDetails.campaignsEnabled) {
                    $('#campaignsEnabled').attr('checked', true);
                }

                // Dealing with documentType field separately.
                if (playerDetails.documentType) {
                    $('input[value="' + playerDetails.documentType + '"]', '#documentTypeItems').attr('checked', 'checked').change();
                }

                // Dealing with documentIssuedDate field separately.
                if (playerDetails.documentIssuedDate) {
                    var documentIssuedDateArray = playerDetails.documentIssuedDate.split('-');
                    $('#documentIssuedDateYear').val(documentIssuedDateArray[0]);
                    $('#documentIssuedDateMonth').val(documentIssuedDateArray[1]);
                    $('#documentIssuedDateDay').val(documentIssuedDateArray[2]);
                }

                // Dealing with passportExpiryDate field separately.
                if (playerDetails.passportExpiryDate) {
                    var passportExpiryDateArray = playerDetails.passportExpiryDate.split('-');
                    $('#passportExpiryDateYear').val(passportExpiryDateArray[0]);
                    $('#passportExpiryDateMonth').val(passportExpiryDateArray[1]);
                    $('#passportExpiryDateDay').val(passportExpiryDateArray[2]);
                }

                if ($(".jquery-checkbox-wrapper").length == 0 && styledInputs) {
                    $("#myAcc_receiveEmail").checkbox();
                    $("#myAcc_receiveEmail").hide();
                }

                // Dealing with ssn, passportNumber and BGC token fields, assuming that they all three come together
                if (typeof initMap.bgcPlayerToken != "undefined") {
                    if (playerDetails.bgcPlayerToken != null) {
                        $('#identification').val('').change().closest('#identification_item').hide();
                    } else {
                        $("#bgcPlayerToken_item").hide();
                        if (playerDetails.ssn != null) {
                            var ssn = playerDetails.ssn.asString
                            document.getElementById('ssn').value = ssn;
                            $("#ssnMyaccYear").val(ssn.substring(0,2));
                            $("#ssnMyaccMonth").val(ssn.substring(2,4));
                            $("#ssnMyaccDate").val(ssn.substring(4,6));
                            $("#ssnMyaccNumber").val(ssn.substring(6,11));
                        }
                        $("#save").hide();
                        $("#saveAndGetBGC").show();
                    }
                }

                // Show info message if BGC token has been acquired
                if (window.location.href.toString().indexOf('tokenOK') != -1) {
                    $("#profileSaved").show();
                    $("#profileSaved").html(bgcTokenAcquired);
                    $("#manageAccount_error").hide();
                }

                // Loading bank info tab
                if (!hasErrors(bankInfoMap)) drawPaymentDetailsTab(bankInfoMap, playerDetails);
                else $('fieldset#bank').hide();

                // Loading details of stored payments
                populateStoredPaymentsDetails();

                // Show the content
                progressIndicator(false);
                document.getElementById("loadingContent").style.visibility = "visible";
            }
        });

        // Set up calendar for closing account
        $("#closeAccountEndDate").datepicker({ "dateFormat": "dd-mm-yy", "minDate": "+1d" });
        $("#closeAccountEndDate").datepicker("setDate","+1d");

        //loadLoginHistory();
        updateLoginHistory(0,null);

        $("#manageAccount_error").hide();
        $("#updateBankAccount_error").hide();

        $("#updateMyAccountForm").find("input").focus(function() {
            $("#updateMyAccount_infosaved").fadeOut(300);
        });

        $("#updateMyBankDetails").find("input").focus(function() {
            $("#updateMyBankDetails_infosaved").fadeOut(300);
        });

        $("#updateMyPassword").find("input").focus(function() {
            $("#updateMyPassword_infosaved").fadeOut(300);
        });

        // Perform validations when operating fields
        $("#oldPassword").blur(function() {

            var pwdOld = "oldPassword";

            checkEmptyRegisterfield(pwdOld);
            checkRegisterMinFiledLength(pwdOld, 6);
        });

        // Variables should be defined here
        pwd1ID = "newPassword";
        pwd2ID = "confirmNewPassword";

        $("#newPassword").blur(function() {
            showRegisterErrorMessages = true;
            validatePassword(pwd1ID);
        });

        $("#confirmNewPassword").blur(function() {
            showRegisterErrorMessages = true;
            validatePassword(pwd2ID);
        });

        var url = window.location.href.toString(),
            index = -1;
        if (url.indexOf('documents') != -1) {
            index = $('#profile_documents_tab').index();
        }
        else if (url.indexOf('rtp') != -1) {
            index = $('#profile_rtp_tab').index();
        }
        $('#tabs').tabs({ active: (index != -1 ? index : 0) });
    });
}

function fetchBankInfoDetails() {
    return $.ajax({
        url: "/billfold-api/player/paymentDetails",
        type: "GET",
        dataType: "json",
        async: true
    });
}

function drawPaymentDetailsTab(bankInfoMap, playerDetailsMap) {

    // Clear the bank_ul
    $.each(bankInfoMap, function(index, value) {
        var provider = value.provider,
            methodId = value.method;

        // Appending select option
        var option = document.createElement("option");
        option.text = getPaymentDetailsSelection(provider, methodId);
        option.value = provider + '_' + methodId;
        document.getElementById("selectedBankAccountType").appendChild(option);

        detailType[option.value] = value.detailType;

        // Stylize bank account type dropdown (if needed)
        if(styledInputs) {
            setTimeout(function() {
                stylizeDropdown("#selectedBankAccountType",10);
            }, 10);
        }

        /* Structure of the div element
         * <div>
         *   <li id="offline_1001_bankAccount_li">
         *       <label>Bank Account *
         *          <i alt="?" title="What is ur bank acc?" class="fa fa-question fa-lg"></i>
         *          <span id="bankAccount_validOK"></span>
         *       </label>
         *       <input id="bankAccount" type="text">
         *       <div id="bankAccount_error" class="bankFieldError">
         *   </li>
         * </div>
         * */

        // Appending div element
        var selectionDiv = document.createElement('div');
        selectionDiv.id = provider + '_' + methodId;
        document.getElementById("bank_ul").appendChild(selectionDiv);
        paymentDetailsFields[provider + '_' + methodId] = selectionDiv;

        // Appending li elements to div
        $.each(value.fields, function(index, fieldsMap) {
            var mainFieldName = fieldsMap.field,
                maxLength = fieldsMap.maxLength;

            var fieldLi = document.createElement('li');
            fieldLi.id = provider + '_' + methodId + '_' + mainFieldName + '_li';
            fieldLi.className = 'form-group has-feedback';
            selectionDiv.appendChild(fieldLi);

            // Appending label for the input field
            var inputLabel = document.createElement('label');
            inputLabel.id = mainFieldName + '_label';
            inputLabel.className = 'control-label col-sm-3';
            inputLabel.htmlFor = mainFieldName;
            inputLabel.innerHTML = getProviderExtraInfo(provider, methodId, mainFieldName);
            if (!fieldsMap.optional) {
                inputLabel.innerHTML += (fieldsMap.dependent == null) ? ' *' : ' (*)';
            }
            fieldLi.appendChild(inputLabel);

            // Appending wrapper for the input field (bootstrap)
            var inputWrapper = document.createElement('div');
            inputWrapper.id = mainFieldName + '_wrapper';
            inputWrapper.className = 'col-sm-9';
            fieldLi.appendChild(inputWrapper);

            // Appending input element to wrapper
            if (fieldsMap.fieldType == "text") {
                var fieldInput = document.createElement('input');
                fieldInput.type = "text";
                fieldInput.id = mainFieldName;
                fieldInput.className = 'form-control';
                if (maxLength != null) fieldInput.maxLength = maxLength;
                // Uppercase for bic and account fields
                if (mainFieldName == "ibanAccount" || mainFieldName == "ibanBic" || mainFieldName == "bankBic") fieldInput.style.textTransform = "uppercase";
                inputWrapper.appendChild(fieldInput);

                // Adding value to the input element
                if(playerDetailsMap[mainFieldName] != null) $(fieldInput).val(playerDetailsMap[mainFieldName]);
                // Assigning default value
                if ($(fieldInput).val().length == 0 && fieldsMap.defaultValue != null) $(fieldInput).val(fieldsMap.defaultValue);
            }

            // Appending select element to wrapper
            if (fieldsMap.fieldType == 'select') {
                var fieldSelect = document.createElement('select');
                fieldSelect.id = mainFieldName;
                fieldSelect.className = 'form-control';

                var options = fieldsMap.dataList.split(','),
                    defaultValue = (fieldsMap.defaultValue == null) ? '' : fieldsMap.defaultValue,
                    element = '';

                // Adding options to the select element
                for (var i = 0; i < options.length; i++) {
                    var value = window[provider + '_' + methodId + '_' + options[i]];
                    if (defaultValue == options[i]) element =  '<option value="' + options[i] + '" selected>' + value + '</option>';
                    else element = '<option value="' + options[i] + '">' + value + '</option>';
                    fieldSelect.innerHTML += element;
                }
                inputWrapper.appendChild(fieldSelect);
            }

            // Appending wrapper for the feedback (bootstrap)
            var feedbackWrapper = document.createElement('div');
            feedbackWrapper.id = mainFieldName + '_feedback_wrapper';
            feedbackWrapper.className = 'form-control-feedback';
            inputWrapper.appendChild(feedbackWrapper);

            // Appending additional elements for feedback wrapper
            var hintImg = $('<i/>')
                .attr('alt', '?')
                .attr('title', getProviderHintInfo(provider, methodId, mainFieldName))
                .attr('class', "fa fa-question fa-lg");
            feedbackWrapper.appendChild(hintImg[0]);

            var spanOk = document.createElement('span');
            spanOk.id = mainFieldName + '_validOK';
            feedbackWrapper.appendChild(spanOk);

            // Appending error div element to wrapper
            var errorDiv = document.createElement('div');
            errorDiv.className = "bankFieldError help-block error-field";
            errorDiv.id = mainFieldName + "_error";
            inputWrapper.appendChild(errorDiv);
        });
    });

    // Binding onkeypress events, masked inputs
    //Masked input for IBAN account
    $.mask.definitions['b'] = "[A-Za-z0-9]";
    if ($("#ibanAccount").length >0) $("#ibanAccount").mask("?bbbb bbbb bbbb bbbb bbbb bbbb bbbb bbbb");

    // Validating IBAN, BANK BIC/SWIFT, Clearing Code
    var idsForBankInfoValidation = ["ibanBic", "bankBic", "bankClearingCode"];
    for (var i in idsForBankInfoValidation) {
        $("#"+idsForBankInfoValidation[i]).unbind();
        $("#"+idsForBankInfoValidation[i]).keypress(function(event) {validateInputs(event, 'bankInfo')});
    }

    // Validating number inputs for bank account and neteller account fields
    var idsForNumberValidation = ["bankAccount", "netAccount"];
    for (var i in idsForNumberValidation) {
        $("#"+idsForNumberValidation[i]).unbind();
        $("#"+idsForNumberValidation[i]).keypress(function(event) {validateInputs(event, 'number')});
    }

    // Select 1st option
    toggleBankInfo($("#selectedBankAccountType").val());
}

function prefillingPlayerInformation(requiredFields,playerMap,saveBtnId) {

    $.each(requiredFields, function(index, value) {

        var storedValue = playerMap[index],
            maxLength = value.maxLength || 128;

        // Add maxLength attribute
        $('#' + index).attr('maxLength', maxLength);

        if (value.readOnly && storedValue != null) {
            document.getElementById(index).className = "disabledInput form-control";
            document.getElementById(index).readOnly = true;
            document.getElementById(index).value = storedValue;
        } else {
            document.getElementById(index).value = "";
        }
        if (index == "phoneNumber") {

            if (storedValue == null) document.getElementById(index).value = window["phoneCode_"+playerMap.country];

            // Adding example in the hint msg
            addPhoneNumberExample("phoneNumberHint", playerMap.country);
        }
        else if (index == "gender") {
            if (storedValue) document.getElementById('registerGender'+ storedValue).checked = true;
        }

        // Showing and assigning values
        $("#" + index + "_item").show();
        if (storedValue != null) document.getElementById(index).value = storedValue;

        // Adding validation if this field is mandatory
        if(value.mandatory) {

            // Displaying stars
            $("#star_" + index).show();

            $("#" + index).blur(function() {

                $("#" + index + "_error").html("");

                setButtonStyle(saveBtnId, requiredFields);

                if(index === "address" || index === "houseNumber") {
                    validateAddressField("address","houseNumber","error");
                }
                else {

                    if (!checkEmpty(index)) {
                        $("#" + index + "_error").html( msgValidatorEmpty );
                    }
                    // Validation of phonenumber
                    else if( index == "phoneNumber") {

                        if(!validatePhoneNumber("phoneNumber")) {
                            $("#" + index + "_error").html( msgPhoneValidationError );
                        }
                    }
                }
            });
        }
    });
    setButtonStyle(saveBtnId, requiredFields);
}

function addPhoneNumberExample(id,country) {
    var phoneExample = window["phoneCode_"+country] + "xxxxx";
    document.getElementById(id).title = replaceParams(phoneNumberHint, [phoneExample]);
}

function toggleBankInfo(content) {
    $('#bank_ul').html(paymentDetailsFields[content] || '');
}

// Submit account details for MyAccount-page (form 1)
function submitProfile() {

    $("#manageAccount_error").hide();

    var validatedFields = true,
        arrayOfInputsIds = new Array(),
        arrayIndex = 0;

    // Creating an array of visible and editable inputs of type text for their further validation
    $('input[type=text], select', 'form#updateMyAccountForm').each(function() {

        var inputVisibility = $(this).is(":visible"),
            inputId = $(this).attr('id');

        if (inputVisibility) {
            arrayOfInputsIds[arrayIndex] = inputId;
            arrayIndex++;
        }
    });

    var year = '', month = '', day = '';
    // Dealing with ssn separate fields
    if ($("#ssnMyaccYear").is(":visible")) {
        $("#ssn").val($("#ssnMyaccYear").val() + $("#ssnMyaccMonth").val() + $("#ssnMyaccDate").val() + $("#ssnMyaccNumber").val());
        arrayOfInputsIds[arrayIndex] = "ssn";
    }
    // Dealing with "document type" field
    if ($('input[name="documentType"]').is(':visible')) {
        arrayOfInputsIds.push('documentType');
    }
    // Dealing with "document issued date" field
    if ($('#documentIssuedDateDay').is(':visible')) {
        year = $('#documentIssuedDateYear').val() || '';
        month = $('#documentIssuedDateMonth').val() || '';
        day = $('#documentIssuedDateDay').val() || '';

        if (year && month && day) {
            $('#documentIssuedDate').val(year + '-' + month + '-' + day);
        }

        arrayOfInputsIds.push('documentIssuedDate');
    }
    // Dealing with "passport expiry date" field
    if ($('#passportExpiryDateDay').is(':visible')) {

        year = $('#passportExpiryDateYear').val() || '';
        month = $('#passportExpiryDateMonth').val() || '';
        day = $('#passportExpiryDateDay').val() || '';

        if (year && month && day) {
            $('#passportExpiryDate').val(year + '-' + month + '-' + day);
        }

        arrayOfInputsIds.push('passportExpiryDate');
    }

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

        arrayOfInputsIds.forEach( function (visibleInputId) {
            showRegisterErrorMessages = true;

			// Validation of the required fields
			if ($('#star_' + visibleInputId).is(':visible')) {
				// Validation of address and housenumber
				if (visibleInputId === "address") {
                    if (!validateAddressField(visibleInputId,"houseNumber","error")) {
                        validatedFields = false;
                    }
				}
                // Validation of residential address and housenumber
                else if(visibleInputId === "residentialAddress") {
                    if (!validateAddressField(visibleInputId,"residentialHouseNumber","error")) {
                        validatedFields = false;
                    }
                }
                // Validation of citizenship number
                else if (visibleInputId === 'citizenshipNumber') {
                    if (!checkCitizenshipNumber(visibleInputId)) {
                        validatedFields = false;
                    }
                }
                // Validation of ssn
                else if (visibleInputId === 'ssn') {
                    if (!checkEmpty(visibleInputId)) {
                        $('#' + visibleInputId + '_error').html(msgValidatorEmpty);
                        validatedFields = false;
                    }
                    else if (($('#' + visibleInputId).val() || '').length !== 11) {
                        $('#' + visibleInputId + '_error').html(msgValidatorInvalidData);
                        validatedFields = false;
                    }
                    else {
                        $('#' + visibleInputId + '_error').html("");
                    }
                }
                // Validation of phonenumber
                else if (visibleInputId == "phoneNumber") {
                    if (!validatePhoneNumber(visibleInputId)) {
                        $("#" + visibleInputId + "_error").html(msgPhoneValidationError);
                        validatedFields = false;
                    }
                    else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
                // Validation of document type
                else if (visibleInputId == "documentType") {
                    if (!$('input[name="documentType"]:checked').val()) {
                        $("#" + visibleInputId + "_error").html(msgValidatorEmpty);
                        validatedFields = false;
                    }
                    else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
                // Validation of document issued date
                else if (visibleInputId == "documentIssuedDate") {
                    if (!checkDocumentIssuedDateField(visibleInputId)) {
                        validatedFields = false;
                    }
                }
                // Validation of document issued date
                else if (visibleInputId == "passportExpiryDate") {
                    if (!checkPassportExpiryDateField(visibleInputId)) {
                        validatedFields = false;
                    }
                }
                // Validation of skypeID, qqId, weChatId
                else if (visibleInputId == "skypeId" || visibleInputId == "qqId" || visibleInputId == "weChatId") {
                    if (checkEmpty(visibleInputId)) {
                        if (!validateUserAccounts(visibleInputId)) {
                            $("#" + visibleInputId + "_error").html(msgUserAccountsValidationError);
                            validatedFields = false;
                        }
                    } else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
                else {
                    if (!checkEmpty(visibleInputId)) {
                        $("#" + visibleInputId + "_error").html(msgValidatorEmpty);
                        validatedFields = false;
                    }
                    else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
            }
            // Fields that are not required to validate
            else {
				// Validation of address and housenumber
				if (visibleInputId === "address") {
					if (!validateAddressField(visibleInputId,"houseNumber","error")) {
                        validatedFields = false;
                    }
				}
                // Validation of residential address and housenumber
                else if (visibleInputId === "residentialAddress") {
                    if (!validateAddressField(visibleInputId,"residentialHouseNumber","error")) {
                        validatedFields = false;
                    }
                }
                // Validation of citizenship number
                else if (visibleInputId === 'citizenshipNumber') {
                    if (checkEmpty(visibleInputId) && !checkCitizenshipNumber(visibleInputId)) {
                        validatedFields = false;
                    }
                }
                // Validation of ssn
                else if (visibleInputId === 'ssn') {
                    if (checkEmpty(visibleInputId)) {
                        if (($('#' + visibleInputId).val() || '').length !== 11) {
                            $('#' + visibleInputId + '_error').html(msgValidatorInvalidData);
                            validatedFields = false;
                        }
                    }
                    else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
                // Validation of phoneNumber
                else if (visibleInputId == "phoneNumber") {
                    if (checkEmpty(visibleInputId)) {
                        if (!validatePhoneNumber(visibleInputId, false, true)) {
                            $("#" + visibleInputId + "_error").html(msgPhoneValidationError);
                            validatedFields = false;
                        }
                    }
                    else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
                // Validation of document issued date
                else if (visibleInputId == "documentIssuedDate") {
                    if (checkEmpty(visibleInputId) && !checkDocumentIssuedDateField(visibleInputId)) {
                        validatedFields = false;
                    }
                }
                // Validation of passport expiry date
                else if (visibleInputId == "passportExpiryDate") {
                    if (checkEmpty(visibleInputId) && !checkPassportExpiryDateField(visibleInputId)) {
                        validatedFields = false;
                    }
                }
                // Validation of skypeID, qqId, weChatId
                else if (visibleInputId == "skypeId" || visibleInputId == "qqId" || visibleInputId == "weChatId") {
                    if (checkEmpty(visibleInputId)) {
                        if (!validateUserAccounts(visibleInputId)) {
                            $("#" + visibleInputId + "_error").html(msgUserAccountsValidationError);
                            validatedFields = false;
                        }
                    } else {
                        $("#" + visibleInputId + "_error").html("");
                    }
                }
            }

            // Remove duplicated spaces from all fields
            $('#' + visibleInputId).val(removeDuplicatedSpaces(visibleInputId));
        });

        progressIndicator(false);

        if (validatedFields) {

            var data = $("#updateMyAccountForm").serializeAndModify({
                checkboxesAsBools: true,
                modifier: afterSerialization
            });

            progressIndicator(true);

            // Add small delay to the sending of info to make sure that progress indicator is displayed
            setTimeout(function() {

                $.when(checkNoDuplicates(data)).done(function(map) {

                    progressIndicator(false);

                    // Updating details
                    if (map.success) {

                        $.when(updateDetailsPost(data)).done(function(map) {

                            if(!hasErrors(map)) {

                                // If BGC token was not requested
                                if ($("#save").is(":visible")) {
                                    $("#profileSaved").show();
                                    $("#profileSaved").html(profileInfoSaved);
                                    $("#manageAccount_error").hide();
                                }
                                else {
                                    var lang = $.getCookie("_lang");
                                    if (lang == null) lang = defaultLanguage;
                                    redirect('/' + lang + '/postlogin/myacc?tokenOK');
                                }

                            } else {

                                $("#manageAccount_error").show();

                                if ( typeof map.errorCode == "109" && map.remaining != "undefined" ) {

                                    $("#manageAccount_error").html(replaceParams(msgPasswordNotMatchedException,[map.remaining]));
                                } else {

                                    var error = getErrorMessage(map);
                                    showErrorObj($("#manageAccount_error"), error);
                                }
                            }
                        });
                    }

                    // Showing error
                    else {
                        $("#profileSaved").hide();
                        $("#manageAccount_error").show();
                        $("#manageAccount_error").html(msgDuplicatedData);
                    }
                });

            },50);
        }

	},50);
}

// Process pressint Enter on the Confirm Password Form
function searchKeyPress(e) {

    // Look for window.event in case event isn't passed in
    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
        e.preventDefault();
        submitBankAccount();
    }

}

// Submit bank account details
function submitBankAccount() {

    if(validatePwdInPopup("myaccPasswordConfirm_pwd","myaccPasswordConfirm_error")) {

        $.fancybox.close();

        var password = $("#myaccPasswordConfirm_pwd").val(),
            data = "password=" + encodeURIComponent(password),
            selectedOption = $("#selectedBankAccountType").val(),
            ajaxCall = detailType[selectedOption];

        // If Deposit Page
        if (document.getElementById('deposit_savebutton2') != null) secondDepositConfirm(password);

        // My account and withdrawal Pages
        else {
            $("#bank_ul #" + selectedOption + " input[type=text]," +  "#bank_ul #" + selectedOption + " select").each(function() {
                var id = $(this).attr('id');
                data = data + '&' + id + '=' + removeSpaces(id).replace(/\+/g,'%2B');
            });

            progressIndicator(true);

            // Add small delay to the sending of info to make sure that progress indicator is displayed
            setTimeout(function() {

                $.when(updatePaymentDetails(ajaxCall, data)).done(function(map) {

                    progressIndicator(false);

                    if(!hasErrors(map)) {

                        // If it is myaccount page
                        if ( document.getElementById('withdrawal_savebutton') == null ) {
                            $("#updateBankAccount_error").hide();
                            $("#updateBankAccount_info").show();
                            $("#updateBankAccount_info").html( bankAccountInfoSaved );
                        }

                        // If it is withdrawal page
                        else {
                            var data = $('#withdrawalData').val();
                            withdrawGet(data);
                        }

                    } else {

                        var error = getErrorMessage(map);

                        // If it is myaccount page
                        if ( document.getElementById('withdrawal_savebutton') == null ) {
                            showErrorObj($("#updateBankAccount_error"), error, $("#updateBankAccount_info"));
                        }

                        // If it is withdrawal page
                        else {
                            showErrorObj($("#withdrawal_error"), error);
                        }
                    }
                });

            },50);
        }
    }
}

function updatePaymentDetails(ajaxCall, data) {
    return $.ajax({
        url: "/billfold-api/player/updatePaymentDetails",
        type: "POST",
        data: data + "&detailType=" + ajaxCall,
        dataType: "json"
    });
}

// Submit passwords of MyAccount-page (form 3)
function submitPassword() {

    $("#updatePassword_error").hide();

    var year = $('#manageAccountBirthyear option:selected').val(),
        month = $('#manageAccountBirthmonth option:selected').val(),
        date = $('#manageAccountBirthday option:selected').val(),

        oldPassword = $('#oldPassword').val(),
        password = $('#newPassword').val(),
        confirmPassword = $('#confirmNewPassword').val(),

        data = "year-birthDate=" + year + "&month-birthDate=" + month + "&day-birthDate=" + date + "&oldPassword=" + encodeURIComponent(oldPassword) + "&password=" + encodeURIComponent(password) + "&confirmPassword=" + encodeURIComponent(confirmPassword);

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			$.when(updatePasswordPost(data)).done(function(map) {

				progressIndicator(false);

				$.fancybox.close();
                $("#updateMyPassword")[0].reset();
                $("#newPassword_validOK").html("");
                $("#confirmNewPassword_validOK").html("");

				if(!hasErrors(map)) {
                    showInfoObj($("#passwordSaved"), passwordInfoSaved, $("#updatePassword_error"));
				}
				else {
                    var error = getErrorMessage(map);
					if (typeof map.remaining != "undefined") {
                        showErrorObj($("#updatePassword_error"), replaceParams(error,[map.remaining]), $("#passwordSaved"));
					}
					else {
						showErrorObj($("#updatePassword_error"), error, $("#passwordSaved"));
					}
				}
			});

		},50);
}

// ---------------------------------------------------------------------------------------------------------------------
// Upload-related functions and stuff
//
var documentNumber = 0;

// Validate file that the user is uploading
function validateFile(thisform) {

    $("#uploadDocuments_error").hide();

    if(!validateDocumentName("uploadNameField")) {

        $("#uploadDocuments_error").show();
        $("#uploadDocuments_error").html(msgFileNameFailed);
        return false;
    }

    if(documentNumber > 9) {

        $("#uploadDocuments_error").show();
        $("#uploadDocuments_error").html(msgFilesTooMany);
        return false;
    }

    with(thisform) {

        if(validateFileExtension(file, "uploadDocuments_error", msgFileTypeError,
            new Array("jpg","JPG","pdf","PDF","jpeg","JPEG","png","PNG")) == false) {
            return false;
        }
        if(validateFileSize(file, 2097152, "uploadDocuments_error", msgFileSizeError)==false) {
            return false;
        }
    }
}

// Validate file's extension
function validateFileExtension(component,msg_id,msg,extns) {

    var flag = 0;
    with(component) {

        var ext = value.substring(value.lastIndexOf('.')+1);
        for(i = 0; i<extns.length; i++) {

            if(ext == extns[i]) {

                flag = 0;
                break;
            }
            else {
                flag = 1;
            }
        }

        if(flag != 0) {

            $("#"+msg_id).show();
            $("#"+msg_id).html(msg);
            return false;
        }
        else {

            return true;
        }
    }
}

// Validate file's size
function validateFileSize(component,maxSize,msg_id,msg) {

    if(navigator.appName == "Microsoft Internet Explorer") {

        if(component.value) {

            var oas = new ActiveXObject("Scripting.FileSystemObject");
            var e = oas.getFile(component.value);
            var size = e.size;
        }
    }

    else {

        if(component.files[0] != undefined) {
            var size = component.files[0].size;
        }
    }

    if(size != undefined && size > maxSize) {

        $("#"+msg_id).show();
        $("#"+msg_id).html(msg);
        return false;
    }
    else {

        return true;
    }
}

// Populate the table of files
function populateFileTable() {
	progressIndicator(true);

    $.ajax({
        url: '/billfold-api/player/documents',
        success: function (map) {
            map = map || [];
            var html = '';

            for (var i = 0; i < map.length; i++) {
                var document = map[i];

                html +=
                    '<tr>' +
                        '<td>' + document.name + '</td>' +
                        '<td>' + getFileTypeText(document.documentType) + '</td>' +
                        '<td>' + getFileStatusText(document.documentStatus) + '</td>' +
                    '</tr>';
            }

            $('#fileTableBody').html(html);
            $('#fileTable').toggle(!!html);
            documentNumber = map.length;
            progressIndicator(false);
        }
    });
}

// Get the translation of file type (address/identity)
function getFileTypeText(param) {
    return window['msgFileType_' + param] ? window['msgFileType_' + param] : param;
}

// Get the translation of file status (uploaded/approved/rejected)
function getFileStatusText(param) {
    return window['documentIs_' + param] ? window['documentIs_' + param] : param;
}

function closeAccountPopup() {

    // Check the date if applicable
    if ($("#closeAccountForm input[type='radio']:checked").val() == "tillDate") {
        if (!validateEndDate('closeAccountEndDate')) {
            return false;
        }
    }

    $("#closeAccount_error").hide();
    $("#closeAccountPwdConfirm").val('');
    $.fancybox( { href: '#closeAccountConfirm' } );
}

function closeAccount() {

	if(validatePwdInPopup("closeAccountPwdConfirm","closeAccountPwdConfirm_error")) {

		$.fancybox.close();
        var data = generateDataForClosingAccount();

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			$.when(closeAccountAjax(data)).done(function (map) {

				progressIndicator(false);

				if(!hasErrors(map)) logout();
				else {
                    var error = getErrorMessage(map);
                    showErrorObj($("#closeAccount_error"), error);
				}
			});

		},50);
    }
}

function generateDataForClosingAccount() {

	var data = "password=" + encodeURIComponent($("#closeAccountPwdConfirm").val());

	if ($("#closeAccountForm input[type='radio']:checked").val() == "tillDate") {

		var endDateValue = $("#closeAccountEndDate").datepicker("getDate");
		endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();

		data = data + "&closeExpireDate=" + endDateValue;
	}
	return data;
}

function closeAccountAjax(data) {

	return $.ajax({

        url: "/billfold-api/player/close",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

// Get and show all stored payments (e.g. Adyen "Recurring Payments")
function populateStoredPaymentsDetails() {

    $.when(getListOfStoredPayments()).done(function (map) {
        var tableRows = '',
            isEmpty = false;

        if (!hasErrors(map)) {
            $.each(map.storedPaymentDetails, function(index, storedPayment) {
                tableRows =
                    '<tr id="storedPaymentsDetails-row-' + index + '" class="storedPaymentsDetails-row">' +
                        '<td>' + (window["providerTitle_" + storedPayment.paymentProviderName] || storedPayment.paymentProviderName ) + '</td>' +
                        '<td>' + storedPayment.description + '</td>' +
                        '<td>' + capitalizeString((storedPayment.tokenType || '').toString().toLowerCase()) + '</td>' +
                        '<td>' + getFormattedDate(storedPayment.createdTime) + '</td>' +
                        '<td>' + (typeof storedPayment.expired === 'boolean' ? (storedPayment.expired ? storedPaymentsDetails_yes : storedPaymentsDetails_no) : storedPaymentsDetails_na) + '</td>' +
                        '<td>' +
                            '<input class="btn btn-default btn-xs" type="button" value="' + storedPaymentsDetails_removeBtn + '" ' +
                                   'onclick="removeStoredPaymentDetails(\'' + storedPayment.paymentProviderName + '\', \'' + storedPayment.storedDetailReference + '\', this)">' +
                        '</td>' +
                    '</tr>' + tableRows;
            });
        }
        if (!tableRows) {
            isEmpty = true;
            tableRows =
                '<tr>' +
                    '<td>&mdash;</td>' +
                    '<td>&mdash;</td>' +
                    '<td>&mdash;</td>' +
                    '<td>&mdash;</td>' +
                    '<td>&mdash;</td>' +
                    '<td>&mdash;</td>' +
                '</tr>';
        }

        $('#storedPaymentsDetails-tableBody').html(tableRows);
        if (!isEmpty) {
            $('#storedPaymentsDetails').show();
        }
    });

}

function getListOfStoredPayments() {

    return $.ajax({
        url : '/billfold-api/payment/listStoredPaymentDetails',
        type : 'GET',
        dataType : 'json'
    });

}

function removeStoredPaymentDetails(providerName, storedPaymentReference, element) {

    progressIndicator(true);
    return $.ajax({
        url : '/billfold-api/payment/disableStoredPaymentDetail',
        type : 'POST',
        data : 'provider=' + providerName + '&storedDetailReference=' + storedPaymentReference,
        dataType : 'json'
    }).done(function (map) {
        if (map.success && element) {
            // Remove row from table
            $(element).closest('.storedPaymentsDetails-row').fadeOut(300, function () {
                $(this).remove();
                if ($('#storedPaymentsDetails-tableBody tr').length == 0) {
                    $('#storedPaymentsDetails-tableBody').html(
                        '<tr>' +
                            '<td>&mdash;</td>' +
                            '<td>&mdash;</td>' +
                            '<td>&mdash;</td>' +
                            '<td>&mdash;</td>' +
                            '<td>&mdash;</td>' +
                        '</tr>'
                    );
                }
            });
        }
    }).always(function () {
        progressIndicator(false);
    });

}

// Show login history
function updateLoginHistory(page_index, pagination_container) {

    $("#loginHistoryError").hide();
    $('#loginHistoryTableBody').empty();

    pageIndex = parseInt(page_index+1);
    var data = "page=" + pageIndex + getLoginHistoryPeriod();

    $.when(ajaxCalls.getLoginHistoryInfo(data)).done(function(map) {

        if (!hasErrors(map)) {

            if(typeof  map.activities != "undefined") {

                // Update info message on the page
                setHistoryInfoMessage(map);

                // Add the paginator; First Parameter: number of items; Second Parameter: options object
                if(map.count > map.size) {
                    $("#loginHistory").append('<div id="loginHistoryPaginator" class="pagination"></div>');
                    $("#loginHistoryPaginator").pagination(map.count, {
                        items_per_page: map.size,
                        current_page: pageIndex-1,
                        callback: updateLoginHistory
                    });
                    // Update the pagination icons to use current language
                    $(".prev").html(transactionPrevious);
                    $(".next").html(transactionNext);
                }
                // Filling in the data to the table
                populateHistoryTable(map);
            }

        }
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#loginHistoryError"), error);
        }

    });
}

// Set title message for login history table
function setHistoryInfoMessage(map) {

    // Set title of transactions
    var titleString = "";

    if(map.count > 0) {

        $("#loginHistoryTable").show();

        if(map.pageCount == 1) {
            titleString = replaceParams(activitiesFoundOne,[map.count]);
        }
        else {
            titleString = replaceParams(activitiesFoundMany,[map.count,map.size,map.pageCount]);
        }
    }
    else {
        $("#loginHistoryTable").hide();
        titleString = activitiesFoundZero;
    }
    $("#loginHistoryTitle").html(titleString);
}

// Get and show player login history
function getLoginHistoryPeriod() {

    var startDateId = "loginHistory_from",
        endDateId = "loginHistory_to",
        dateValue = "",
        startDateValue = $("#" + startDateId).datepicker("getDate"),
        endDateValue = $("#" + endDateId).datepicker("getDate");

    if (checkEmpty(startDateId) && checkEmpty(endDateId)) {

        validateDateRange(startDateId, endDateId);
        startDateValue = startDateValue.getFullYear()+"-"+(startDateValue.getMonth()+1)+"-"+startDateValue.getDate();
        endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();

        dateValue = "&startDate=" + startDateValue + "&endDate=" + endDateValue;
    }
    else if (checkEmpty(startDateId)) {
        startDateValue = startDateValue.getFullYear()+"-"+(startDateValue.getMonth()+1)+"-"+startDateValue.getDate();
        dateValue = "&startDate=" + startDateValue;
    }
    else if (checkEmpty(endDateId)) {
        endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();
        dateValue = "&endDate=" + endDateValue;
    }

    return dateValue;
}

// Populating table with login history
function populateHistoryTable(map) {

    $("#loginHistoryTableBody > tr").remove();

    var newRow = "";

    $.each(map.activities, function(index, item) {

        newRow =
            '<tr>' +
            '<td>' + getFormattedDate(item.createdTime) + '</td>' +
            '<td>' + item.ip + '</td>' +
            '<td>' + getUserActivityName(item.activityCode) + '</td>' +
            '</tr>';

        $('#loginHistoryTableBody').append(newRow);
        $("#loginHistoryTable").show();

    });
}
