var today = new Date();
var thisYearMinusLimit = (today.getFullYear())-18;
var cashValue = "init";
var promoValue = "init";
var jackPotsShown = false;
var jackPotsUpdaterTimer = null;
var currentSlidePageOpen = null;
var tokenValue = "";
var myAffiliateId = "";
var fullscreentimer = null;
var firstDepositDone = "init";
// NOTE: This variable should be taken from BO. It tells for the site if the player can open one or multiple game popup windows.
var multipleGameWindows = true;

var gameWindowElem = null;

// The values of these variables are later updated from frontend.properties!
var gameWindowOpening = "browser", liveGameWindowOpening = "browser", styledInputs = false, jackpotDisplayType = "scroller", passwordStrength = "strong", firstDepositLock = false,
    promoLockRuleEnabled = false, languageSelectorType = "dropdown";
var fancyboxReturnPopupClicked = false;

String.prototype.replaceWithParams= function () {
    var result = this.toString();
    for(var i=0; i<arguments.length; i++){
        result = result.replace("{" + i +"}", arguments[i]);
    }
    return result;
};

// Disable jQuery ajax cache for every page
$.ajaxSetup(
    { cache : false }
);

// Merging similar Ajax requests into the one, if they are used at the same time (extension of the standard jQuery Ajax function).
// (e.g. if you send several similar ajax requests in different parts of the code at the same time
// or if you click many times at once on an one button, which sends an ajax request).
// Two requests are considered similar if they have equal method (GET/POST) + url + query parameters (order is important).
// During the merging, ajax settings (async, cache, contentType, dataFilter, dataType... etc.) of the request
// will be overwritten by settings of the similar executed request.
// Author: Alex Kranz (please, let me know, if you want to change this function or you found a bug)
(function ($) {

    // Time in milliseconds between first and last similar requests, during which all of them will be merged into the one
    var delayBetweenSimilarRequests = 1000,
        logging = false;

    // Storage of the ajax requests
    $.ajaxPool = {};

    // Original jQuery Ajax function
    $.ajaxOriginal = $.ajax;

    // Modified jQuery Ajax function
    $.ajax = function (url, options) {

        // If url is an object, simulate pre-1.5 signature (taken from jQuery source)
        if (typeof url === 'object') {
            options = url;
            url = undefined;
        }

        var defaults = {
                processData: true,
                traditional: false,
                // Main property, that allows/forbids the merging of the similar Ajax requests
                // (if this property is set to "false", then it is analogous to original jQuery Ajax request or
                // "delayBetweenSimilarRequests=0")
                merge: true,
                // Additional property, that allows/forbids actualization of the response data
                // (e.g. set it to "false" for an your new call if you want to always use a data of the first ajax request only
                // and to prevent the sending of the new similar ones)
                actualize: true
            },
            settings = $.extend(defaults, options || {}),
            requestID = (url || settings.url) + '';

        // If merging is disabled, then invoke original jQuery Ajax function
        if (!settings.merge) {
            return $.ajaxOriginal(url, settings);
        }

        // Convert data if not already a string (taken from jQuery source)
        if (settings.data && settings.processData && typeof settings.data !== 'string') {
            settings.data = jQuery.param(settings.data, settings.traditional);
        }

        // Add method of the request
        requestID = (settings.type ? settings.type.toUpperCase() : 'GET') + ' ' + requestID;
        // Remove hash character
        requestID = requestID.replace(/#.*$/, '');
        // Remove a timestamp (e.g. "_=1403265925408", used as anti-cache)
        requestID = requestID.replace(/([?&]?)_=[0-9]+([&]?)/g, function (matchingSubstring, bracketNumber1, bracketNumber2, offset, originalString) {
            return bracketNumber2 == '' ? '' : bracketNumber1;
        });
        // Add url query params
        requestID += (settings.data ? (~requestID.indexOf('?') ? '&' : '?') + settings.data : '');

        if (typeof $.ajaxPool[requestID] === 'undefined' || (settings.actualize && $.ajaxPool[requestID].finished && (+new Date - $.ajaxPool[requestID].created) > delayBetweenSimilarRequests)) {
            cleanPool();
            // A new request, sent with parameter "actualize=true", should preserve parameter "actualize=false" of the similar request
            if (typeof $.ajaxPool[requestID] !== 'undefined' && !$.ajaxPool[requestID].options.actualize) {
                settings.actualize = false;
            }
            // Add new request into the pool
            $.ajaxPool[requestID] = {
                jqXHR: $.ajaxOriginal(url, settings),
                options: settings,
                created: +new Date,
                finished: 0
            };
            // Set status as finished after the request completion
            $.ajaxPool[requestID].jqXHR.done(function () {
                if (typeof $.ajaxPool[requestID] !== 'undefined') {
                    $.ajaxPool[requestID].finished = +new Date;
                }
            });
        }
        else {
            // Install callbacks (determined in the settings) on deferred, to preserve them for each request separately (taken from jQuery source)
            for (i in {success: 1, error: 1, complete: 1}) {
                $.ajaxPool[requestID].jqXHR[i](settings[i]);
            }
            if (logging) console.log('%c[merged] %c' + requestID, 'color:#978000', 'color:#777;font-style:italic');
        }
        return $.ajaxPool[requestID].jqXHR;

    };

    // Requests pool cleaning function
    var cleanPool = function () {
        for (var requestID in $.ajaxPool) {
            if ($.ajaxPool[requestID].options.actualize && $.ajaxPool[requestID].finished && (+new Date - $.ajaxPool[requestID].created) > delayBetweenSimilarRequests) {
                delete $.ajaxPool[requestID];
            }
        }
    };

})(jQuery);

// Handle placeholder if the browser doesn't support the attribute
function ElementSupportAttribute(elm, attr) {
    var test = document.createElement(elm);
    return attr in test;
}

$(document).ready(function() {

    function FauxPlaceholder() {

        if(!ElementSupportAttribute('input','placeholder')) {
            $("input[placeholder]").each(function() {

                var $input = $(this);
                $input.after('<input id="'+$input.attr('id')+'-faux" style="display:none;" type="text" value="' + $input.attr('placeholder') + '" />');
                var $faux = $('#'+$input.attr('id')+'-faux');

                if ($input.val() === '') {

                    $input.addClass("placeholderstyle");
                    $faux.show().attr('class', $input.attr('class')).attr('style', $input.attr('style'));
                    $input.hide();
                }

                $faux.focus(function() {
                    $faux.hide();
                    $input.removeClass("placeholderstyle");
                    $input.show().focus();
                });

                $input.blur(function() {
                    if($input.val() === '') {
                        $input.addClass("placeholderstyle");
                        $input.hide();
                        $faux.show();
                    }
                });
            });
        }
    }

    // Set placeholder for IE and other browsers that don't support placeholders
    FauxPlaceholder();

    // Detect the IE's version
    detectIEversion();

    // Slider on the main page
    if($('#slider').length > 0) {

        $('#slider').flexslider({
            animation: "slide",
            easing: "swing",
            direction: "horizontal",
            reverse: false,
            animationLoop: true,
            smoothHeight: false,
            startAt: 0,
            slideshow: true,
            slideshowSpeed: 5000,
            animationSpeed: 600,
            randomize: false,
            pauseOnAction: false,
            pauseOnHover: true,
            video: false,
            controlNav: true,
            directionNav: false
        });

    }

    // Truncate the visible postlogin name, if it's too long to fit to element
    var postLoginNickname = $("#postlogin_nickname").html();

    if(postLoginNickname) {

        if(postLoginNickname.length > 22) {
            $("#postlogin_nickname").html(postLoginNickname.substr(0,20)+"..");
        }
    }

    // Define fancybox for most of the popups (not register, forgot password, iframes etc.)
    $(".fancyboxPopup").fancybox({

        title : ' ',
        beforeLoad: function() {

            $("#loginRegisterPopup_error").hide();
            $("#loginRegisterPopup_email").val("");
            $("#loginRegisterPopup_password").val("");
            $(".faq p").hide();
        },
        afterClose: function() {

            // If playing games, make game window visible when popup is closed
            if($('#gamePageDropdown').length > 0 && popupButtonClickedOnGamePage == false) {

                $('#gameWindow').removeClass('gameWindowHidden');
                $('#gameWindowEmpty').hide();
            }
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Define fancybox for return to parent popup
    $(document).on('click', '.fancyboxReturnToParentPopup', function (event) {

        event.preventDefault();

        var $this = $(this);
        var currentPopup = '#' + $this.closest('.popup').attr('id');
        var nextPopup = $this.attr('href');

        if ($this.closest('.fancybox-inner').length > 0) {
            $(nextPopup).attr('data-fancybox-return',currentPopup);
            fancyboxReturnPopupClicked = true;
        }

        $.fancybox({
            title : '',
            href : '#' + nextPopup,
            beforeLoad : function () {
                fancyboxReturnPopupClicked = false;
            },
            afterClose : function () {
                if (!fancyboxReturnPopupClicked) {
                    returnToParentPopup(nextPopup);
                }
            },
            helpers : {
                overlay : { closeClick : false }
            }
        });

    });

    // Define fancybox for forgot password
    $(".fancyboxForgotPwd").fancybox({

        title : ' ',
        wrapCSS : 'forgotPwdFancyBox',
        beforeLoad: function() {

            $("#forgotpassword_email").val("");

            if($("#forgotpasswordBirthyear").length > 0) {

                setTimeout(function() {
                    if(styledInputs) {
                        stylizeDropdown("#forgotpasswordBirthyear",10);
                        stylizeDropdown("#forgotpasswordBirthmonth",10);
                        stylizeDropdown("#forgotpasswordBirthday",10);
                    }
                }, 10);
            }
        },
        afterClose: function() {

            // If playing games, make game window visible when popup is closed
            if($('#gamePageDropdown').length > 0) {
                $('#gameWindow').removeClass('gameWindowHidden');
                $('#gameWindowEmpty').hide();
                popupButtonClickedOnGamePage = false;
            }
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Define fancybox for register -form
    $(".fancyboxPopupRegister").fancybox({

        title : ' ',
        wrapCSS : 'registerFancyBox',
        beforeLoad: function() {

            // Prevent opening if user is logged
            if (userIsLogged) return false;

            setTimeout(function() {
                registerOnload();

                // Style dropdown selectors if defined to do that
                if(styledInputs) {
                    stylizeDropdown("#" + birthDateID,10);
                    stylizeDropdown("#" + birthMonthID,10);
                    stylizeDropdown("#" + countryID,10);
                }
            },10);
        },
        afterClose: function() {

            // If playing games, make game window visible when popup is closed
            if($('#gamePageDropdown').length > 0) {
                $('#gameWindow').removeClass('gameWindowHidden');
                $('#gameWindowEmpty').hide();
                popupButtonClickedOnGamePage = false;
            }
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Define fancybox to show "Terms & conditions" or "Policy" as popup from register form
    $(".fancyboxShowTermsOrPolicy").fancybox({

        title : ' ',
        beforeLoad: function() {

            // If playing games, make game window invisible when popup is opened
            if($('#gamePageDropdown').length > 0) {
                $('#gameWindow').addClass('gameWindowHidden');
                $('#gameWindowEmpty').show();
            }
        },
        afterClose: function() {

            $.fancybox({ href: '#registerPopup', title: '',
                afterClose: function() {

                    // If playing games, make game window visible when popup is closed
                    if($('#gamePageDropdown').length > 0) {
                        $('#gameWindow').removeClass('gameWindowHidden');
                        $('#gameWindowEmpty').hide();
                        popupButtonClickedOnGamePage = false;
                    }
                },
                helpers : {
                    overlay : {closeClick: false}
                }
            });
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Define fancybox to show "FAQ" as popup from contact us form
    $(".fancyboxShowFAQ").fancybox({

        title : ' ',
        afterClose: function() {

            $.fancybox({ href: '#contactPopup', title: '',
                beforeLoad: function() {

                    setTimeout(function() {
                        contactOnload();

                        if(styledInputs) {

                            if($("#li_sendMeCopy .jquery-checkbox-wrapper").length == 0) {

                                $("#contact_sendMeCopy").checkbox();
                            }

                            stylizeDropdown("#contact_subjectId",10);
                        }
                    },100);
                },
                helpers : {
                    overlay : {closeClick: false}
                }
            });
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Define fancybox for the birthday validation in MyAccount-page
    $(".fancyboxBirthdayPopup").fancybox({

        title : ' ',
        wrapCSS : 'birthDayFancyBox',
        beforeLoad: function() {

            if(savePassword() == false) {
                return false;
            }

            // Style checkboxes (if defined)
            if(styledInputs) {

                setTimeout(function() {
                    stylizeDropdown("#manageAccountBirthyear",10);
                    stylizeDropdown("#manageAccountBirthmonth",10);
                    stylizeDropdown("#manageAccountBirthday",10);
                }, 50);
            }
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Define fancybox for contact us -form
    $(".fancyboxPopupContact").fancybox({

        title : ' ',
        beforeLoad: function() {

            setTimeout(function() {
                contactOnload();

                if(styledInputs) {

                    if($("#li_sendMeCopy .jquery-checkbox-wrapper").length == 0) {

                        $("#contact_sendMeCopy").checkbox();
                    }

                    stylizeDropdown("#contact_subjectId",10);
                }
            },100);
        },
        helpers : {
            overlay : {closeClick: false}
        }
    });

    // Open games in iframes (if configured in frontend.properties)
    $(".fancyboxGameIframe").fancybox({
        title       : null,
        wrapCSS     : 'iFrameFancyBox',
        maxWidth    : '100%',
        maxHeight   : '100%',
        autoSize    : false,
        autoResize  : true,
        aspectRatio : true,
        fitToView   : true,
        closeClick  : false,
        scrolling   : 'no',
        openEffect  : 'none',
        closeEffect : 'none',
        type	    : 'iframe',
        iframe	    : { scrolling : 'no', preload : false },   // fixes iframe issue in the IE
        helpers     : { overlay : { closeClick: false } },
        beforeLoad  : function() {

            // FIXME: the ajax request (/billfold-api/game/...) cannot be merged with same request inside the iframe
            // Resize fancybox by provider aspect ratio (take a very high resolution and the aspectRatio property helps us)
            var name = getUrlParameter(this.href, 'name'),
                isDemo = getUrlParameter(this.href, 'thisIsDemo') == 1,
                providerID = getUrlParameter(this.href, 'provider'),
                gameParams = getGameParams(name, isDemo ? 'demo' : 'real') || {},
                ratio = gameParams.ratio && gameParams.ratio !== 1 ? gameParams.ratio : window['windowAspectRatio_provider_' + providerID];

            if (ratio) {
                this.width = 9999;
                this.height = Math.floor(this.width / ratio);
            }

        },
        afterClose  : function() {

            if($("#gameBackground").length > 0) {
                $("#gameBackground").slideUp(250);
            }

        }
    });

    // Submit the header login form if pressed enter
    $("#password").keyup(function(event) {

        if(event.keyCode == 13) {

            $("#password").blur();
            login();
        }
    });

    // Submit the popup login form if pressed enter
    $("#loginRegisterPopup_password").keyup(function(event) {

        if(event.keyCode == 13) {

            $("#loginRegisterPopup_password").blur();
            loginFromPopup();
        }
    });

    // Set up calendar selectors for limits-page
    if($('#pdl-endDate').length > 0) {

        $("#pdl-endDate, #rml-endDate").datepicker({ "dateFormat": "dd-mm-yy", "minDate": "+1d" });

        var end_of_month = new Date();
        $("#pdl-endDate").datepicker("setDate","+1d");
        end_of_month.setMonth(end_of_month.getMonth() + 1, 0);
        $("#rml-endDate").datepicker("setDate",end_of_month);
    }

    // Define slide-down animations in FAQ-page
    $(".faq p").hide();
    $(".faq h4").unbind().click(function () {
        if ($(this).next("p").is(":hidden")) {
            $(".faq p").slideUp();
            $(this).next("p").slideDown();
        }
        else {
            $(".faq p").slideUp();
        }
    });

    // Populate year selector for forgot password -form and change password -form
    for(var i = 0; i < 90; i++) {
        $("#forgotpasswordBirthyear").append('<option value="'+(thisYearMinusLimit-i)+'">'+(thisYearMinusLimit-i)+'</option>');
    }

    if($("#manageAccountBirthyear").length > 0) {

        for(var i = 0; i < 90; i++) {
            $("#manageAccountBirthyear").append('<option value="'+(thisYearMinusLimit-i)+'">'+(thisYearMinusLimit-i)+'</option>');
        }
    }

    // Style tooltips (Bootstrap)
    $(document).on("mouseenter", ".form-control-feedback > i", function () {
        var text = $(this).attr("title");
        if(text) {
            $(this).closest('.form-control-feedback').after('<div class="field-info-wrapper"><div class="alert alert-info field-info"><p>'+text+'</p></div></div>');
            $(this).removeAttr('title');

            $(this).mouseout(function(){
                $(".field-info-wrapper").remove();
                $(this).attr("title",text);
            });
        }
    });

    // Trace token in the url and write it to the cookies
    tokenValue = getParamValue("token");
    myAffiliateId = getParamValue("user_id");

    if(tokenValue) {

        var data = "myAffiliateToken=" + tokenValue + "&myAffiliateId=" + myAffiliateId;
        $.ajax({
            url: "/billfold-api/player/myAffiliate",
            type: "POST",
            dataType: 'json',
            async: false,
            data: data,
            success: function(map) {
            }
        });
    }

    if(infiniteScroll) {
        $(window).scroll(showMoreGames);
    }

});

function showMoreGames() {
    var element = $('#viewmorebutton span');
    if (!element.length) return;

    var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],
        windowHeight=w.innerHeight||e.clientHeight||g.clientHeight,
        elementBottom = element.offset().top + element.outerHeight(),
        windowBottom = windowHeight + (w.scrollY || w.pageYOffset),
        remaining = elementBottom - windowBottom;

    if(remaining - element.outerHeight() <= 0) {
        element.trigger('click');
    }
}

// Form serialization in object
$.fn.serializeObject = function () {

    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;

};

// Form serialization with possibility of modifying each field value
$.fn.serializeAndModify = function (options) {

    options = $.extend({
        allowEmpty: true,
        unmodifiable: [],   // List of fields, excluded from modification
        modifier: function (field) { return field; }   // A function to modify each field value
    }, options || {});

    var serializedForm = this.serializeArray(options);

    for (var i = 0; i < serializedForm.length; i++) {
        var field = serializedForm[i];
        if (!~$.inArray(field.name, options.unmodifiable)) {
            options.modifier(field);
        }
        if (field.value === '' && !options.allowEmpty) {
            serializedForm.splice(i, 1);
            i--;  // step back
        }
    }

    return $.param(serializedForm);

};

// This function is used for modification fields values after form serialization
function afterSerialization(field) {

    field.value = $.trim(field.value);
    if (field.name == 'zipCode') {
        field.value = field.value.replace(/[\s]/g, '');
    }
    else if (field.name == 'campaignId' && field.value === '0') {
        field.value = '';
    }
    return field;

}

// Populate language dropdown menu in the header of the site
function populateLanguageMenu(currentLang) {

    $.when(ajaxCalls.getLanguages()).done(function(map) {

        if(!hasErrors(map)) {

            if(languageSelectorType === "dropdown") {

                var languageOptions = "";

                // Loop languages got from JSON
                $.each(map.languages, function(index, item) {

                    languageOptions += "<option data-image='/resources/images/flags/"+item+".png' data-imagecss='flag' value='"+item+"'>"+window['languageOption_'+item]+"</option>";
                });

                // Add options to selector
                $("#changeLanguage").html(languageOptions);

                // Set the default dropdown option to current language
                $("#changeLanguage option").each(function() {

                    if ($(this).val() == currentLang) {
                        $(this).attr("selected", "selected");
                    }
                });

                setTimeout(function() {

                    stylizeDropdown("#changeLanguage",20);
                    $("#languageWrapper_login, #languageWrapper_postlogin").css("visibility","visible");

                    // Hide the "current" language in the dropdown menu
                    var activeLang = $("#changeLanguage_title .ddlabel").html();

                    $('#changeLanguage_child ul li.enabled').each(function(i, obj) {

                        var lang = $(this).find(".ddlabel").html();
                        if(lang === activeLang) {
                            $(this).addClass("hiddenLangOpt");
                        }
                    });

                    // Make language switcher to work
                    $("#changeLanguage").change(function() {

                        var language = $('#changeLanguage :selected').val();
                        chooseLanguage(language);
                    });

                }, 10);
            }
            else if(languageSelectorType === "buttons") {

                var languageElem, activeLangClass;

                $.each(map.languages, function(index, item) {

                    activeLangClass = "";

                    if(currentLang === item) {
                        activeLangClass = "langButton_current";
                    }

                    languageElem = "<div class='langButton "+activeLangClass+"' data='"+item+"' id='langButton_"+item+"'>" +
                        "<img class='langButtonFlag' src='/resources/images/flags/"+item+".png' alt='"+item+"'/>" +
                        "<span class='langButtonText'>"+window['languageOption_'+item]+"</span>" +
                        "</div>";

                    $("#changeLanguage").append(languageElem);
                });

                // Make language switcher to work
                $(".langButton").click(function() {

                    var language = $(this).attr("data");
                    chooseLanguage(language);
                });
            }
        }
    });
}

function homeLink() {
    var targetUrl = location.protocol + '//' + location.host + "/" + $.getCookie("_lang") + "/";
    window.location.href = targetUrl;
}

// Stylize the dropdown given as parameter
function stylizeDropdown(element, rows) {
    var dropDownMenu = $(element).msDropdown({ visibleRows: rows }).data('dd');

    if (!dropDownMenu) {
        return;
    }

    dropDownMenu.off('mouseout');
    dropDownMenu.on('mouseout', function() {
        dropDownMenu.close();
    });

    dropDownMenu.off('mouseover');
    dropDownMenu.on('mouseover', function() {
        dropDownMenu.open();
    });

    return dropDownMenu;
}

// Appending payment provider icon to the footer
function updatePaymentProvidersFooter() {

    $.when(getPaymentProvidersForFooter()).done(function (map) {
        if(map != null && typeof map.error == "undefined") {

            var providerName, methodId;

            for ( var i = 0; i < map.methods.length; i++ )
            {
                providerName = map.methods[i].provider;
                methodId = map.methods[i].method;

                var element = '<img src="/resources/images/icons/' + providerName + '_' + methodId + '.png" title="' + getPaymentProv(providerName, methodId) + '" alt="' + getPaymentProv(providerName, methodId) + '"/>';
                $("#payments").append(element);
            }
        }
    });
}

// Fetch payment providers' list
function getPaymentProviders() {

    return $.ajax({

        url: "/billfold-api/payment/methods",
        type: "GET",
        dataType: "json",
        async: false
    });
}

// Fetch payment providers' list for the footer images
function getPaymentProvidersForFooter() {

    return $.ajax({

        url: "/billfold-api/payment/providers",
        type: "GET",
        dataType: "json",
        async: false
    });
}

// Reload the Games List (including pagination)
// @param {array} gamesListArray  - An array of games
// @param {string} gamesGroupName - A name of the group, to which games belong
// @param {boolean} isPaginated   - Load the entire list of games or with a pagination
// @param {number} gamesListPage  - A page, from which the list will be loaded
// If any parameter has inappropriate value (undefined / null), the value of this parameter will be set in the previous state
function updateTabContent(gamesListArray, gamesGroupName, isPaginated, gamesListPage) {

    // Current loaded games array and group name. Used for the sorting.
    oGamesList.currentList.array = gamesListArray = $.isArray(gamesListArray) ? gamesListArray : oGamesList.currentList.array;
    oGamesList.currentList.name = gamesGroupName = typeof gamesGroupName === 'string' ? gamesGroupName : oGamesList.currentList.name;
    oGamesList.currentList.isPaginated = isPaginated = typeof isPaginated === 'boolean' ? isPaginated : oGamesList.currentList.isPaginated;
    gamesListPage = typeof gamesListPage !== 'undefined' ? parseInt(gamesListPage, 10) || 0 : oGamesList.currentList.page;

    // Filtering before loading of the content
    var searchParams = $('#gamesSearchForm').serializeObject();
    gamesListArray = filterGamesList(searchParams.keyword, gamesListArray, 'name', searchParams.sorting, searchParams.sortingIsReversed);

    // Load the content of the game list
    // If pageIndex == null, all pages of content will be loaded
    // If pageIndex >= 0, only specified page of content will be loaded
    var loadGamesListPage = function (pageIndex, paginationContainer) {

        oGamesList.currentList.page = pageIndex;

        $("#games").hide().empty();

        // Check if the game list is opened with param "home" (when going to home page) - or not
        if(gamesGroupName != "home") {
            // If tab name is not "home" (the main page), show title of the group (with a slide animation)
            $("#games").append('<h2 id="gameGroupTitle" class="col-xs-12 h1">' + getGameGroupDisplayName(gamesGroupName) + '</h2>');
        }

        if (!gamesListArray.length) {
            $("#games").append('<p class="h3 no-data-found">' + msgNoGamesFound + '</p>').fadeIn(500);
            return;
        }
    
        var gamesListCopy = gamesListArray.slice();
    
        // Restrict the number displayed games
        if(pageIndex != null) {

            var startIndex = oGamesList.itemsPerPage * pageIndex;
            var endIndex = oGamesList.itemsPerPage * pageIndex + oGamesList.itemsPerPage;

            if(viewMoreType == "pages") {
                if(endIndex != gamesListArray.length) {
                    if(endIndex > gamesListCopy.length - 1) {
                        endIndex = 0;
                    }
                    if(endIndex > 0) {
                        gamesListCopy.splice(endIndex);
                    }
                }
            }
            else {
                if (endIndex > gamesListCopy.length - 1) {
                    endIndex = 0;
                }
                if (endIndex > 0) {
                    gamesListCopy.splice(endIndex);
                }
                if (startIndex > 0) {
                    gamesListCopy.splice(0, startIndex);
                }
            }

        }

        // Main view of games list
        if (searchParams.view == 'text') {
            $("#games").addClass('view-text').append( $("#gameGroupTemplateViewText").tmpl({"games": gamesListCopy}) );
        }
        else {
            $("#games").removeClass('view-text').append( $("#gameGroupTemplate").tmpl({"games": gamesListCopy}) );
        }

        // Add possible promoted games to page
        $("#games").append('<div class="col-xs-12"><div id="promotedGames" class="row"></div></div>');
        $("#promotedGames").html( $("#promotedGameGroupTemplate").tmpl({"games": gamesListCopy}) );
    
        // Add lock-images to the real-game buttons if needed
        $('.gameElement_realButton, .promotedGame_realButton, .liveGameElement_realButton').each(function(i, obj) {
    
            if(($(this).hasClass('promoMoneyEnabled_false') && $(this).hasClass('gameElement_realButton') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false)) {
    
                if($(this).hasClass('liveGameElement_realButton') == false && $(this).hasClass('promotedGame_realButton') == false && !$(this).hasClass('freeSpinsEnabled_true')) {
                    $(this).addClass('lockBackground');
                }
            }
    
            if(($(this).hasClass('promoMoneyEnabled_false') && $(this).hasClass('promotedGame_realButton') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false)) {
    
                if($(this).hasClass('liveGameElement_realButton') == false && $(this).hasClass('gameElement_realButton') == false && !$(this).hasClass('freeSpinsEnabled_true')) {
                    $(this).addClass('promotedGamelockBackground');
                }
            }
    
            if(($(this).hasClass('promoMoneyEnabled_false') && $(this).hasClass('liveGameElement_realButton') && ((cashValue == 0 && promoLock) || (cashValue > 0 && promoValue > 0 && promoLockRuleEnabled == true))) || (firstDepositLock == true && firstDepositDone == false)) {
    
                if($(this).hasClass('gameElement_realButton') == false && $(this).hasClass('promotedGame_realButton') == false && !$(this).hasClass('freeSpinsEnabled_true')) {
                    $(this).addClass('liveGamelockBackground');
                }
            }
        });
    
        // Add jackpot values to game listing
        if(jackPots.length > 0) {
    
            $.each(jackPots, function(jackpotIndex,jackpotObj) {
                $('#gameListJackpot_'+jackpotObj.name).html(jackpotObj.currency+'<span class="jackpotgame_'+jackpotObj.name+'">'+numberWithCommas((jackpotObj.value).toFixed(2))+'</span>');
                $('#gameJackpot_' + jackpotObj.name).toggle(!!jackpotObj.value);
            });
        }
    
        // Define how the games are opened (live- and other games)
        defineGameOpening(".gameListLink",gameWindowOpening);
        defineGameOpening(".liveGameListLink",liveGameWindowOpening);

        // Highlight keywords, found in the games titles
        var keyword = $.trim(preg_quote(searchParams.keyword));
        if (keyword.length) {
            $('.gameTitleContent, .promotedGameTitleContent', '#games').each(function() {
                $(this).html( $(this).text().trim().replace(new RegExp('((' + keyword + ')+)', 'ig'), '<span class="highlight">$1</span>') );
            });
        }

        $("#games").fadeIn(500);
    
    };

    if (isPaginated && gamesListPage != null && oGamesList.itemsPerPage > 0) {
        // Numeric pagination
        if (oGamesList.hasPagination) {

            if(gamesListArray.length > oGamesList.itemsPerPage) {
                $('#gamelistpaginator').pagination(gamesListArray.length, {
                    items_per_page: oGamesList.itemsPerPage,
                    callback: loadGamesListPage,
                    current_page: gamesListPage,
                    link_to: '#games',
                    prev_text: transactionPrevious,
                    next_text: transactionNext,
                    prev_show_always: false,
                    next_show_always: false,
                    num_edge_entries: 1
                });
                loadGamesListPage(gamesListPage);
            }
            else {
                $('#gamelistpaginator').empty();
                loadGamesListPage(0);
            }

        }
        // View more
        else {

            if(gamesListArray.length > oGamesList.itemsPerPage) {
                $('#viewmorebutton').html('<span>' + viewMoreButtonText + '</span>');
                $('#viewmorebutton span').click(function () {

                    if(viewMoreType == "pages") {
                        loadGamesListPage(oGamesList.currentList.page+1);

                        if ((oGamesList.currentList.page * oGamesList.itemsPerPage + oGamesList.itemsPerPage) >= gamesListArray.length) {
                            $('#viewmorebutton').empty();
                        }
                    }
                    else {
                        loadGamesListPage(null);
                        $('#viewmorebutton').empty();
                    }

                });
                loadGamesListPage(0);
            }
            else {
                $('#viewmorebutton').empty();
                loadGamesListPage(0);
            }

        }
    }
    else {
        if (oGamesList.hasPagination) {
            $('#gamelistpaginator').empty();
        }
        else {
            $('#viewmorebutton').empty();
        }
        loadGamesListPage(null);
    }

    // Set the active group menu item to use different style
    $('li', '#menu').removeClass('active');
    $('#mainMenuGroupSpan_' + gamesGroupName).closest('li', '#menu').addClass('active');
    // Toggle accordion item
    if ($('#menu').hasClass('ui-accordion')) {
        var currentMenuItemIndex = $('#mainMenuGroupSpan_' + gamesGroupName).closest('li', '#menu').index();
        $('#menu').accordion('option', 'active', (~currentMenuItemIndex ? currentMenuItemIndex : null));
    }

}

// Fill the game list on the front- or games-page
function show_tab_content(groupName, isPaginated) {

    groupName = groupName || '';
    isPaginated = typeof isPaginated === 'boolean' ? isPaginated : true;

    // If user is not on homepage, redirect him to there; otherwise, open the selected group
    if (!$('#games').length || window.isLiveCasinoPage && groupName !== 'live') {
        var lang = $.getCookie('_lang') || defaultLanguage;
        return window.location.href = '/' + lang + '/gamegroup/' + groupName;
    }
    else if (!window.isLiveCasinoPage && groupName === 'live') {
        return window.location.href = '/liveCasinoPage';
    }

    $.getJSON('/billfold-api/games/' + groupName, null, function (map) {
        if (!hasErrors(map)) {

            map.games = extendGamesProperties(map.games, groupName);

            oGamesList.activeGroup.array = map.games;
            oGamesList.activeGroup.name = groupName;
            oGamesList.activeGroup.isPaginated = isPaginated;

            // Сlear the search string
            $('#gamesSearchKeyword').val('');

            updateTabContent(map.games, groupName, isPaginated, 0);

        }
    });

}

// Add additional properties for each game
function extendGamesProperties(games, groupName) {
    games = games || [];
    groupName = groupName || '';
    var lang = $.getCookie('_lang') || defaultLanguage;

    // Exclude games from the list if game does not support language. F.e. exclude=zh, game will not be shown in Chinese language.
    for (var i = 0; i < games.length; i++) {
        var game = games[i],
            metadata = game.metadata || {},
            languages = metadata.exclude ? ('' + metadata.exclude).split(',') : [];

        if (languages.length && ~languages.indexOf(lang)) {
            games.splice(i--, 1);
        }
        else {
            game.title = getGameDisplayName(game.name);
            game.providerName = getGameProviderName(game.provider);
            game.tabName = groupName;
            game.freeSpinsEnabled = !!game.freeSpins;
            game.hideDemoUrl = groupName === 'live' ? true : game.hideDemoUrl;
        }
    }

    return games;
}

// Filtering the list of games
// @param {string} keyword - search word (e.g. used for the filtering by name)
// @param {array} gamesListIn - games list, which should be filtered
// @param {string} filterBy - filtering method (or list of methods, separated by commas)
// @param {string} orderBy - sorting
// @param {boolean} orderIsReversed - invert sorting
// @return {array} - filtered games list (it is NOT a fully new array, objects within the array refer to the initial array)
function filterGamesList(keyword, gamesListIn, filterBy, orderBy, orderIsReversed) {

    keyword = typeof keyword !== 'undefined' ? $.trim(preg_quote(keyword)) : '';
    gamesListIn = $.isArray(gamesListIn) ? gamesListIn : [];
    filterBy = typeof filterBy === 'string' ? filterBy.split(',') : [];
    orderBy = typeof orderBy === 'string' ? orderBy : '';
    orderIsReversed = !!orderIsReversed;

    var gamesListOut = [],
        regex = new RegExp(keyword, 'i');

    // Populating of filtered array
    for (var i = 0; i < gamesListIn.length; i++) {
        if (gamesListIn[i].gameDisabled) continue;
        if (~$.inArray('name', filterBy) && !regex.test(gamesListIn[i].title)) continue;
        // if (~$.inArray('new', filterBy) && !gamesListIn[i].newGame) continue;   // Example of adding a new filter method ("new" - leaves only new games)
        gamesListOut.push(gamesListIn[i]);
    }

    // Sorting of filtered array
    var compare = function (a, b) {
        if (orderBy == 'name') {
            if (a.title > b.title) return 1;
            else return -1;
        }
        else if (orderBy == 'new') {
            if (!a.newGame && b.newGame) return 1;
            else if (a.newGame && !b.newGame) return -1;
            else return 0;
        }
    };

    if (orderBy) gamesListOut.sort(compare);
    if (orderIsReversed) gamesListOut.reverse();

    return gamesListOut;

}

// Populate main menu's game dropdown lists (like live, table games etc.)
function populate_gameGroupItems(dropDown,dropDownType,excludeGroup,numberOfGames,maxNumberOfDropDownItems,pagination) {

    // Get groups
    $.getJSON("/billfold-api/groups", null, function(map) {

        if (map != null && typeof map.error == "undefined") {

            // Get games and create dropdown menus related to each group
            $.each(map.groups, function(index, tabName) {

                // Don't show the group excluded in frontend.properties
                if (excludeGroup == false || tabName != excludeGroup) {

                    // Create main menu item and specify its name and click-event
                    var groupName = getGameGroupDisplayName(tabName);
                    var menuElement = $('<li/>'),
                        gameGroupLink = $('<a/>')
                            .append('<span id="mainMenuGroupSpan_'+tabName+'">'+groupName+'</span>')
                            .click(function(){show_tab_content(tabName)});

                    if (dropDown == false) {
                        menuElement
                            .append(gameGroupLink)
                    }
                    else {
                        var gamesList = $('<ul id="mainMenuGroup_'+tabName+'">');

                        if (dropDownType == 'click' || dropDownType == 'hover') {

                            var clickAttr = ( dropDownType == 'click' ? 'data-toggle="dropdown" data-target="#"' : '' );

                            gameGroupLink = $('<a' + clickAttr + '/>')
                                .click(function() {show_tab_content(tabName)})
                                .append('<span id="mainMenuGroupSpan_'+tabName+'">'+groupName+'</span> <span class="caret"></span>');

                            gamesList.addClass('dropdown-menu')
                            menuElement.addClass('dropdown')
                                .append(gameGroupLink)
                                .append(gamesList);
                        }
                        else if (dropDownType == 'accordion') {

                            gamesList.addClass('accordion-menu')
                            menuElement.addClass("accordion")
                                .append(gameGroupLink)
                                .append(gamesList);
                        }
                    }

                    $("#menu").append(menuElement);

                    if (dropDown == false || dropDownType == 'hover' || dropDownType == 'accordion') {

                        setTimeout(function() {
                            $('#mainMenuGroupSpan_'+tabName).closest('li', '#menu').unbind('click.gamesGroupMenu').bind('click.gamesGroupMenu', function (e) {
                                e.preventDefault();
                                if (!$(this).hasClass('active') && $('#games').length) {
                                    show_tab_content(tabName);
                                }
                                // Toggle accordion item on click
                                if ($('#menu').hasClass('ui-accordion')) {
                                    var currentMenuItemIndex = !$(this).find('> .ui-accordion-header-active').length ? $(this).index() : -1;
                                    $('#menu').accordion('option', 'active', (~currentMenuItemIndex ? currentMenuItemIndex : null));
                                }
                            });
                        }, 5);

                    }

                    // Populate dropdown menu if needed
                    if (dropDown == true) {

                        $.getJSON("/billfold-api/games/"+tabName, null, function(map) {

                            if (map != null && typeof map.error == "undefined") {

                                var games = (map.games || []).slice(0);
                                map = { games: games };

                                // Add new properties for each game
                                $.each(map.games, function(index, game) {
                                    game['title'] = getGameDisplayName(game.name);
                                    game['tabName'] = tabName;
                                });

                                // Maximum number of dropdown items on any page
                                if (maxNumberOfDropDownItems != "false") {
                                    map.games.splice(parseInt(maxNumberOfDropDownItems), map.games.length-1);
                                }

                                // Optionally limit the number of menu's sub-items (on homepage)
                                if ($("#slider").length > 0 && numberOfGames != "false") {

                                    map.games.splice(parseInt(numberOfGames), map.games.length-1);
                                }

                                $("#mainMenuGroup_"+tabName).append( $("#mainMenuTemplate").tmpl(map) );

                                // Define how the games are opened (live- and other games)
                                defineGameOpening(".gameLink",gameWindowOpening);
                                defineGameOpening(".liveGameLink",liveGameWindowOpening);

                            }

                        });
                    }

                }
            });
        }

        // Define menu dropdown animations if needed
        if (dropDown == true) {

            if (dropDownType == 'hover') {

                $("#menu > li").hover(function() {
                    $(this).find(".dropdown-menu").stop(true,true).slideDown(200);
                }, function() {
                    $(this).find(".dropdown-menu").hide();
                });

            }
            else if (dropDownType == 'accordion') {

                $("#menu").accordion({
                    active : false,
                    heightStyle : "content",
                    collapsible : true,
                    // Disable native click event. We will emulate this feature by yourself
                    event: false
                });

                // Prevent scroll "bubbling" for Game Group Accordion Menu.
                $('.ui-accordion-content').bind('mousewheel', function(e, d) {
                    if((this.scrollTop === ( $(this).get(0).scrollHeight - $(this).height() ) && d < 0) || (this.scrollTop === 0 && d > 0)) {
                        e.preventDefault();
                    }
                });

            }

        }

    });
}

// Populate winner list
function populate_winnerList(winVelocity, winDirection, winStartFrom) {

    $.getJSON("/billfold-api/winners", null, function(map) {

        if(map != null) {

            // Add new properties for each element
            $.each(map, function(index, element) {
                element['title'] = getGameDisplayName(element.name);
            });

            $("#winnerSliderTextWrapper").append( $("#winnerListTemplate").tmpl(map) );

            var currencyInUse = "";

            $('.winnerListCurrency').each(function() {
                currencyInUse = $(this).html();
                $(this).html(getCurrencySymbol(currencyInUse));
            });

            // Format the win values
            $('span.winnerListAmount').html(function() { return parseFloat($(this).html()).toFixed(2); });

            // Start the scroller
            setTimeout(function() {
                $('#winnerSliderTextWrapper').css('visibility','visible');
                $('#winnerSlider').SetScroller({
                    velocity: 	 winVelocity,
                    direction: 	 winDirection,
                    startfrom: 	 winStartFrom,
                    loop:		 'infinite',
                    movetype: 	 'linear',
                    onmouseover: 'pause',
                    onmouseout:  'play',
                    onstartup: 	 'play',
                    cursor:      'default'
                });
            }, 500);

            // Define how the games are opened (live and other games)
            defineGameOpening(".gameLink",gameWindowOpening);
            defineGameOpening(".liveGameLink",liveGameWindowOpening);

        }
    });
}

var balanceErrorsNumber = 0;

// Fetches player balances
function showBalance() {

    $.when(getBalance()).done(function(map) {
        if(typeof map.cash != "undefined" && typeof map.promo != "undefined" && typeof map.balance != "undefined" && typeof map.currency != "undefined") {

            $("#cash_balance_value").html(replaceParams(amountMode, [map.cash.toFixed(2), getCurrencySymbol(map.currency)]));
            $("#promo_balance_value, #bonunsesPromo").html(replaceParams(amountMode, [map.promo.toFixed(2), getCurrencySymbol(map.currency)]));
            $("#scash_balance_value").html(replaceParams(amountMode, [map.savingCash.toFixed(2), getCurrencySymbol(map.currency)]));
            $("#total_balance_value, #total_balance_value_dropdown").html(replaceParams(amountMode, [map.balance.toFixed(2), getCurrencySymbol(map.currency)]));
            $("#award_balance_value").html(map.awardPoints.toFixed(2));

            var wageringRequirement = calculateWageringRequirement(map.currentTurnover, map.totalTurnover);
            $('#wagering_requirement_value, #bonunses_wagering_requirement_value').width((wageringRequirement || 0) + '%').html((wageringRequirement || 0) + '%');
            $('#wagering_requirement_wrapper, #bonusTurnoverWrapper').toggle(wageringRequirement != null).find('.progress-hint span').html(wageringRequirementHint);

            $.when(getBIABonus()).done(function (mapBIABonus) {
                var rolloverRemain = mapBIABonus.rolloverRemain || 0,
                    rolloverRequirement = calculateWageringRequirement(rolloverRemain, mapBIABonus.rolloverRequired),
                    bonusAmount = mapBIABonus.bonusAmount || 0,
                    currency = getCurrencySymbol(map.currency);
                $('#rollover_requirement_value').width((rolloverRequirement || 0) + '%').html((rolloverRequirement || 0) + '%');
                $('#rollover_requirement_wrapper').toggle(rolloverRequirement != null).find('.progress-hint span').html(replaceParams(biaRolloverRequirementHint, [bonusAmount, currency, rolloverRemain, currency]));
                $('#biaBonusAmount').html(bonusAmount + currency);
                $('#biaBonusDropdown').toggleClass('hidden', !bonusAmount).toggleClass('is-shown', !!bonusAmount).toggleClass('dropdown-off', rolloverRequirement == null);
            });

            // Define according to the lock rules, what the games are locked
            var cashLockRule = (((map.cash == 0 && cashValue > 0) || (map.cash > 0 && cashValue == 0)) && cashValue != "init");
            var promoLockRule = (((map.promo == 0 && promoValue > 0) || (map.promo > 0 && promoValue == 0)) && promoValue != "init" && promoLockRuleEnabled == true);
            var firstDepositLockRules = ((firstDepositLock == true && map.firstDepositDone == false && firstDepositDone === "init")
                || (firstDepositLock == true && map.firstDepositDone == true && (firstDepositDone == false || firstDepositDone === "init")));

            if(cashLockRule || promoLockRule || firstDepositLockRules) {

                // Remove all the locks as default
                $('.gameElement_realButton, .promotedGame_realButton, .liveGameElement_realButton').each(function(i, obj) {

                    $(this).removeClass("lockBackground");
                    $(this).removeClass("promotedGamelockBackground");
                    $(this).removeClass("liveGamelockBackground");
                });

                // Add lock-images for the games which has promo money disabled depending on the values of cash- and promo money
                $('.gameElement_realButton, .promotedGame_realButton, .liveGameElement_realButton').each(function(i, obj) {

                    if($(this).hasClass('promoMoneyEnabled_false') && $(this).hasClass('gameElement_realButton')) {

                        if((map.cash == 0 && promoLock) || (map.cash > 0 && map.promo > 0 && promoLockRuleEnabled == true)) {
                            $(this).addClass('lockBackground');
                        }
                    }

                    else if($(this).hasClass('promoMoneyEnabled_false') && $(this).hasClass('promotedGame_realButton')) {

                        if((map.cash == 0 && promoLock) || (map.cash > 0 && map.promo > 0 && promoLockRuleEnabled == true)) {
                            $(this).addClass('promotedGamelockBackground');
                        }
                    }

                    else if($(this).hasClass('promoMoneyEnabled_false') && $(this).hasClass('liveGameElement_realButton')) {

                        if((map.cash == 0 && promoLock) || (map.cash > 0 && map.promo > 0 && promoLockRuleEnabled == true)) {
                            $(this).addClass('liveGamelockBackground');
                        }
                    }
                });

                // Add lock images to ALL the games if firstDepositLock-parameter in frontend.properties is switched on, and the player hasn't done his first deposit.
                if(firstDepositLock == true && map.firstDepositDone == false) {

                    $('.gameElement_realButton, .promotedGame_realButton, .liveGameElement_realButton').each(function(i, obj) {

                        if($(this).hasClass('gameElement_realButton')) {

                            $(this).addClass('lockBackground');
                        }

                        else if($(this).hasClass('promotedGame_realButton')) {

                            $(this).addClass('promotedGamelockBackground');
                        }

                        else if($(this).hasClass('liveGameElement_realButton')) {

                            $(this).addClass('liveGamelockBackground');
                        }
                    });

                    firstDepositDone = false;
                }
                else if(firstDepositLock == true && map.firstDepositDone == true) {

                    firstDepositDone = true;
                }

                // Remove locks from Freespins games if the first deposit is not done and the player has free spins left
                $('.gameElement_realButton, .promotedGame_realButton, .liveGameElement_realButton').each(function(i, obj) {

                    if($(this).hasClass('freeSpinsEnabled_true') && $(this).hasClass('gameElement_realButton')) {

                        $(this).removeClass("lockBackground");
                    }

                    else if($(this).hasClass('freeSpinsEnabled_true') && $(this).hasClass('promotedGame_realButton')) {

                        $(this).removeClass("promotedGamelockBackground");
                    }

                    else if($(this).hasClass('freeSpinsEnabled_true') && $(this).hasClass('liveGameElement_realButton')) {

                        $(this).removeClass("liveGamelockBackground");
                    }
                });

                cashValue = map.cash;
                promoValue = map.promo;

                defineGameOpening(".gameListLink",gameWindowOpening);
                defineGameOpening(".gameLink",gameWindowOpening);
                defineGameOpening(".liveGameListLink",liveGameWindowOpening);
                defineGameOpening(".liveGameLink",liveGameWindowOpening);
            }

            cashValue = map.cash;
            promoValue = map.promo;

            checkTimeReachedLimit(map.playerTimeExpired);
            if (map.exceedNoActivityTimeLimit) { logout(); }
        }
        else {
            logout();
        }

        balanceErrorsNumber = 0;
    }).fail(function () {
        balanceErrorsNumber++;
        if (balanceErrorsNumber === 2) {
            logout();
        }
    });
}

function getBalance(forcibly) {
    return $.ajax({
        url: "/billfold-api/player/balance",
        type: "GET",
        dataType: "json",
        merge: !forcibly
    });
}

// Checking time reached limit and displaying popup if it has been reached
function checkTimeReachedLimit(reached) {

    if (reached) {
        $.fancybox([ {href : '#timeReachedPopup', closeBtn: false, closeClick: false, modal: true} ]);
    }
}

// Define balance updater
function balanceTimer() {

    window.setInterval("showBalance()", 5000);
}

// Redirect user to given url and checks user session
function redirect(targetUrl) {

    $.ajax({

        url: "/billfold-api/player/refreshSession",
        async: false,
        type: "POST",
        dataType: 'json',
        success: function(map) {

            if(map != null) {

                // logged-in and not logged-in user
                if(map.success || map.errorCode == 115) {

                    window.top.location = targetUrl;
                }
                else window.location.href = '/';
            }
        }
    });
}

function loginPost(data) {

    return $.ajax({

        url: "/billfold-api/login",
        type: "POST",
        dataType: "json",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        async: true,
        data:data
    });
}

// Login to site from header
function login(params) {

    if(checkEmpty("loginEmail") && checkEmpty("password")) {

        var emailField = $("#loginEmail").val().replace(/\+/g,'%2B'),
            passwordField = $("#password").val(),
            data = "login=" + emailField + "&password=" + encodeURIComponent(passwordField);

        for (var key in params) {
            data += "&" + key + "=" + params[key];
        }

        // Hide errors as default
        $("#login_error").hide();

        // Try to launch the "normal" submit operation to make browser save email-field's value to cache
        $('#loginSubmitHidden').click();

        // Send data to server and refresh the page if everything is ok
        $.when(loginPost(data)).done(function(map) {

            if(!hasErrors(map)) {

                var lang = map.language;
                if (lang != "") changeLanguage(lang);
                else lang = defaultLanguage;

                redirect("/" + lang + "/");
            } else {

                if (map.errorCode == "155") {

                    $.fancybox({
                        href : '#termsAcceptancePopup',
                        title : '',
                        beforeLoad : function() {
                            $('#acceptTermsButton').unbind('click.acceptance').bind('click.acceptance', function () {
                                login({ policyChecked: true });
                                $.fancybox.close();
                            });
                        },
                        helpers : {
                            overlay : { closeClick: false }
                        }
                    });

                } else {

                    var error = getErrorMessage(map);
                    showErrorObj($('#login_error'), error);
                }

            }

        });
    }
}

// Login to site from popup box
function loginFromPopup(params) {

    var emailField = $("#loginRegisterPopup_email").val().replace(/\+/g,'%2B');
    var passwordField = $("#loginRegisterPopup_password").val();
    var data = "login=" + emailField + "&password=" + encodeURIComponent(passwordField);

    for (var key in params) {
        data += "&" + key + "=" + params[key];
    }

    $("#loginRegisterPopup_error").hide();

    $.when(loginPost(data)).done(function(map) {

        if(!hasErrors(map)) {

            if(map.success == true) {

                $.fancybox.close();

                // Reloading normal game
                if($("#demoGameWindow_gamename").length > 0) {

                    var name = $('#demoGameWindow_gamename').val();
                    var provider = $('#demoGameWindow_provider').val();

                    window.location.href = "/realGame?name="+name+"&provider="+provider;
                    window.opener.location.reload(false);
                }
                // Reloading the live lobby
                else if ($('#gameWindow').length > 0) {
                    var href = window.location.href;
                    href = href.replace('&thisIsDemo=1', '');
                    window.location.href = href;
                    window.opener.location.reload(false);
                }
                else {

                    var lang = map.language;
                    if (lang != "") changeLanguage(lang)
                    else lang = defaultLanguage;

                    createCookie("gamestart","?game="+gameToBeStarted+"&mode="+gameModeToBeStarted,0);

                    redirect("/" + lang + "/");
                }
            }
        } else {

            if (map.errorCode == "155") {

                $.fancybox({
                    href : '#termsAcceptancePopup',
                    title : '',
                    beforeLoad : function() {
                        $('#acceptTermsButton').unbind('click.acceptance').bind('click.acceptance', function () {
                            loginFromPopup({ policyChecked: true });
                            $.fancybox.close();
                        });
                        $("#termsAcceptancePopup").attr('data-fancybox-return','#loginregisterPopup');
                    },
                    afterClose : function() {
						openLoginRegisterPopup();
                    },
                    helpers : {
                        overlay : { closeClick: false }
                    }
                });

            } else {

                var error = getErrorMessage(map);
                showErrorObj($("#loginRegisterPopup_error"), error);
            }
        }
    });
}

// Login user automatically after a successful registration if 'ACTIVATION_PROCESS' is disabled
function autoLoginFromRegisterPopup() {

    var data = "&password=" + encodeURIComponent($("#registerPassword").val());

    if (loginMode === 3) { // if user can login only with nickName
        data = "login=" + $("#" + nnameID).val() + data;
    }
    else if (loginMode === 5) { // if user can login only with userName
        data = "login=" + $("#" + unameID).val() + data;
    }
    else {
        data = "login=" + ($("#" + emailID).val().replace(/\+/g,'%2B') || $("#" + nnameID).val() || $("#" + unameID).val()) + data;
    }

    $("#registerLoginButton").addClass("disabled");

    // Send data to server and refresh the page if everything is ok
    $.when(loginPost(data)).done(function(map) {
        if(!hasErrors(map)) {
            var lang = map.language;
            if (lang != "") {
                changeLanguage(lang);
            }
            else {
                lang = defaultLanguage;
            }
            redirect("/" + lang + "/");
        }
        else {
            var error = getErrorMessage(map);
            showErrorObj($('#register_error'), error);
        }
    });

}

function openLoginRegisterPopup() {
	$.fancybox({
		href : '#loginregisterPopup',
		title : '',
		helpers : {
			overlay : { closeClick: false }
		}
	});
}

function openRegisterPopup() {
	$.fancybox([ {href : '#registerPopup',

		title : ' ',
		wrapCSS : 'registerFancyBox',
		beforeLoad: function() {

			setTimeout(function() {
				registerOnload();

				// Style dropdown selectors if defined to do that
				if(styledInputs) {
					stylizeDropdown("#" + birthDateID,10);
					stylizeDropdown("#" + birthMonthID,10);
					stylizeDropdown("#" + countryID,10);
				}
			},10);
		},
		helpers : {
			overlay : {closeClick: false}
		}
	}]);
}

function depositDetailFilledRequest() {

    return $.ajax({

        url: "/billfold-api/payment/detailFilled",
        type: "GET",
        dataType: "json"
    });
}

function withdrawalDetailFilledRequest() {

    return $.ajax({

        url: "/billfold-api/withdrawal/detailFilled",
        type: "GET",
        dataType: "json"
    });
}

// Check the player data and redirect either to the prefill deposit page or to the deposit page with the service providers
function payment_redirect(preTargetUrl, targetUrl, pageType) {

    var ajaxFunctionName;

    if ( pageType == "deposit" ) {
        ajaxFunctionName = "depositDetailFilledRequest";
    } else if ( pageType == "withdrawal" ) {
        ajaxFunctionName = "withdrawalDetailFilledRequest";
    }

    // Check whether all the prefilled data are filled
    $.when(window[ajaxFunctionName]()).done(function(map){

        var lang = $.getCookie("_lang");
        if (lang == null) lang = defaultLanguage;

        if(!hasErrors(map)) {

            targetUrl = "/" + lang + targetUrl;
            redirect(targetUrl);
        } else if (map.errorCode == 125) {

            preTargetUrl = "/" + lang + preTargetUrl;
            redirect(preTargetUrl);
        }
        else {
            var error = getErrorMessage(map);
            showErrorObj($("#header_error"), error);
        }
    });
}

// Logout to site
function logout() {
    var lang = $.getCookie('_lang') || defaultLanguage;

    $.ajax({
        url: '/billfold-api/logout',
        type: 'POST',
        dataType: 'json',
        async: false,
        success: function (map) {
            if (map.success === true) {
                window.location.href = '/' + lang + '/';
            }
        },
        error: function () {
            window.location.href = '/' + lang + '/';
        }
    });
}

// Return error message from JSON data
function getErrorMessage(map) {

    var errorString = "",
        params = map.params;

    if (map.errorCode == "100") {

        errorString = msgGeneralException;
    }
    else if (map.errorCode == "101") {

        errorString = msgNoSuchElementException;
    }
    else if (map.errorCode == "102") {

        errorString = msgIllegalArgumentException;
    }
    else if (map.errorCode == "103") {

        errorString = msgValidationException;
    }
    else if (map.errorCode == "104") {

        errorString = msgValidationException;
    }
    else if (map.errorCode == "105") {

        errorString = msgDuplicatedEmailException;
    }
    else if (map.errorCode == "106") {

        errorString = msgUserRegistrationLimitException;
    }
    else if (map.errorCode == "107") {

        errorString = msgBadCredentialsException.replaceWithParams(params.hackLimit - params.hackCount);
    }
    else if (map.errorCode == "108") {

        errorString = msgBlacklistedException;
    }
    else if (map.errorCode == "109") {

        if(map.params.field == "birthDate") {

            errorString = replaceParams(msgBirthdayNotMatchedException,[params.remaining]);
        }
        else if(map.params.field == "password") {

            errorString = replaceParams(msgPasswordNotMatchedException,[params.remaining]);
        }
        else if(map.params.field == "oldPassword") {

            errorString = replaceParams(msgOldPasswordNotMatchedException,[params.remaining]);
        }
        else {
            errorString = msgIllegalArgumentException;
        }
    }
    else if (map.errorCode == "110") {

        errorString = msgBadCredentialsNoUserException;
    }
    else if (map.errorCode == "111") {

        errorString = msgUserFrozenException;

        setTimeout(function() {
            logout();
        }, 3000);
    }
    else if (map.errorCode == "112") {

        errorString = msgUserClosedException;
    }
    else if (map.errorCode == "113") {

        errorString = msgAlreadyActivatedException;
    }
    else if (map.errorCode == "114") {

        errorString = msgNotActivationException;
    }
    else if (map.errorCode == "115") {

        errorString = msgNoAvailableSessionException;
    }
    else if (map.errorCode == "116") {

        errorString = msgActivationCodeExpiredException;
    }
    else if (map.errorCode == "117") {

        if(params.fields != null) {
            var amount;
            if (params.fields.limit == "minAmount") {

                amount = parseFloat(params.fields.amount).toFixed(2);
                errorString = replaceParams(msgPaymentMinLimitException,[amount]);
            }
            else {

                amount = parseFloat(params.fields.amount).toFixed(2);
                errorString = replaceParams(msgPaymentReachedLimitException,[amount, getCurrencySymbol(methods.currency)]);
            }
        }
        else if (params.paymentLimit == "unverifiedDepositLimit") {
            errorString = msgUnverifiedDepositLimit;
        }
        else {

            errorString = msgPaymentLimitException;
        }
    }
    else if (map.errorCode == "118") {

        errorString = msgPaymentException;
    }
    else if (map.errorCode == "119") {

        errorString = msgPaymentGatewayException;
    }
    else if (map.errorCode == "120") {

        errorString = msgRealMoneyNotEnabledException;
    }
    else if (map.errorCode == "121") {

        errorString = msgNegativeBalanceException;
    }
    else if (map.errorCode == "122") {

        errorString = msgGamingLimitException;
    }
    else if (map.errorCode == "123") {

        errorString = msgBGCTokenDuplicatedException;
    }
    else if (map.errorCode == "124") {

        errorString = msgDepositRequiredException;
    }
    else if (map.errorCode == "125") {

        errorString = msgNoPaymentDetailsFilledException;
    }
    else if (map.errorCode == "126") {

        errorString = replaceParams(msgPaymentDetailsChangedException, [params.changeTime, params.blockEndsTime]);
    }
    else if(map.errorCode == "127"){

        errorString = replaceParams(msgWithdrawalBlockedException, [params.currentTurnover, getCurrencySymbol(params.currency)]);
    }
    else if(map.errorCode == "128"){

        errorString = msgInvalidPointsAmount;
    }
    else if (map.errorCode == "129") {

        errorString = PaymentNotAllowedException;
    }
    else if (map.errorCode == "130") {

        errorString = msgNotActiveUserException;
    }
    else if (map.errorCode == "131") {

        errorString = msgGameNotFoundException;
    }
    else if (map.errorCode == "132") {

        errorString = msgGameDisabledException;
    }
    else if (map.errorCode == "133") {

        errorString = NoBgcTokenException;
    }
    else if (map.errorCode == "134") {

        errorString = BgcLossCreditLimitException;
    }
    else if (map.errorCode == "140") {

        errorString = msgExternalConnectionException;
    }
    else if (map.errorCode == "170") {

        errorString = msgDuplicatedAddressException;
    }
	else if (map.errorCode == "181") {

		errorString = msgEmailNotFoundException;
	}
	else if (map.errorCode == "182") {

        errorString = msgNewAndOldPasswordSameException;
    }
    else if (map.errorCode == "183") {

        errorString = msgActivationCodeNotFoundException;
    }
    else if (map.errorCode == "184") {

        errorString = msgBGCEpisException;
    }
    else if (map.errorCode == "186") {

        errorString = paymentFailedException;
    }

    return errorString;
}

// Show error object and hide if needed info object
function showErrorObj(errorObj, error, infoObj) {
    errorObj.show();
    errorObj.html(error);
    // If there is an info obj to hide
    if (typeof infoObj != "undefined") {
        infoObj.hide();
    }
}

// Show info object and hide if needed error object
function showInfoObj(infoObj, info, errorObj) {
    infoObj.show();
    infoObj.html(info);
    // If there is an error obj to hide
    if (typeof errorObj != "undefined") {
        errorObj.hide();
    }
}

// Return value of single param in URL
function getParamValue(paramName) {

    return decodeURIComponent((new RegExp('[?|&]'+paramName+'=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

//---------------------------------------------------------------------------------------------------------------------
// Registration related codes (for register.jsp)
//

// Define variables to point to ids of the register form's fields
var emailID = "registerEmail",
    emailConfID = "registerConfirmEmail",
    pwd1ID  = "registerPassword",
    pwd2ID  = "registerConfirmPassword",
    nnameID = "registerNickname",
    unameID = "registerUserName",
    promoID = "registrationPromoCode",
    ageCheckedID = "ageChecked",
    termsID = "accept_user_terms",
    firstNameID = "registerFirstName",
    lastNameID = "registerLastName",
    middleNameID = "registerMiddleName",
    birthYearID = "registerBirthyear",
    birthMonthID = "registerBirthmonth",
    birthDateID = "registerBirthday",
    countryID = "registerCountrySelector",
    currencyID = "registerCurrencySelector",
    campaignID = "registerCampaignSelector",
    birthPlaceID = "registerBirthPlace",
    addressID = "registerAddress",
    houseNumberID = "registerHouseNumber",
    cityID = "registerCity",
    zipCodeID = "registerZipCode",
    passportNumberID = "registerPassportNumber",
    genderMaleID = "registerGenderMale",
    genderFemaleID = "registerGenderFemale",
    ssnID = "registerSsn",
    ssnYearID = "ssnYear",
    ssnMonthID = "ssnMonth",
    ssnDateID = "ssnDate",
    ssnNumberID = "ssnNumber",
    phoneNumberID="registerPhoneNumber",
    skypeIdID="registerSkypeId",
    weChatIdID="registerWeChatId",
    qqIdID="registerQqId",
    identificationID="registerIdentificationSelector",
    citizenshipNumberID="registerCitizenshipNumber",
    foreignerNumberID = "registerForeignerNumber",
    documentTypeIdCardID = "registerDocumentTypeIdCard",
    documentTypePassportID = "registerDocumentTypePassport",
    documentTypeOtherID = "registerDocumentTypeOther",
    documentNumberID = "registerDocumentNumber",
    documentIssuingCountryID = "registerDocumentIssuingCountry",
    documentIssuedDateID = "registerDocumentIssuedDate",
    documentIssuedDateDayID = "registerDocumentIssuedDateDay",
    documentIssuedDateMonthID = "registerDocumentIssuedDateMonth",
    documentIssuedDateYearID = "registerDocumentIssuedDateYear",
    documentIssuedByID = "registerDocumentIssuedBy",
    residentialAddressID="registerResidentialAddress",
    residentialHouseNumberID="registerResidentialHouseNumber",
    residentialCityID="registerResidentialCity",
    residentialZipCodeID="registerResidentialZipCode",
    promocode = "",
    currentLanguage = "en",
    currentAgeLimit = null,
    termsText = null,
    registerYearSelector = null,
    initRegisterMap = {},
    registrationCampaigns = [],
    termsCheckBoxChangeBound = false,
    showRegisterErrorMessages = true;

// Set button style to "disabled" if the validation in any field fails
function setRegisterSaveButtonStyle() {

    showRegisterErrorMessages = false;

    if(checkAllTheRequiredRegisterFields()) $("#signUpButton").removeClass("disabled");
    else $("#signUpButton").addClass("disabled");

    showRegisterErrorMessages = true;
}

// Set button style to "disabled" if the validation in any field fails for continue button (2-steps registration)
function setRegisterContinueButtonStyle() {

    showRegisterErrorMessages = false;

    if(checkAllTheRequiredRegisterFields()) $("#continueSignUpButton").removeClass("disabled");
    else $("#continueSignUpButton").addClass("disabled");

    showRegisterErrorMessages = true;
}

// Set promocode for register form
function setPromoCode(param) {

    promocode = param;

    if(promocode == "" || promocode == null) {
        $("#"+promoID).val("");
    }
    else {
        $("#"+promoID).val(promocode);
        $("#"+promoID).attr("readonly","true");
    }
}

// Replace parameters
function replaceParams(source, param) {
    source = source || '';
    param = param || [];

    for(var i = 0; i < param.length; i++) {
        source = source.replace("{"+i+"}", param[i]);
    }

    return source;
}

// Launched when register form is loaded
function registerOnload(currentLang) {

    currentLanguage = currentLang || $.getCookie('_lang') || defaultLanguage;

    var bindCallback = setRegisterSaveButtonStyle;
    if (registerType == '2steps') {
        bindCallback = function () {
            if ($('#continueSignUpButton').is(':visible')) {
                setRegisterContinueButtonStyle();
            }
            else if ($('#signUpButton').is(':visible')) {
                setRegisterSaveButtonStyle();
            }
        };
    }

    var isRegistrationBlocked = false;

    // Show the fields and mark with the star the required ones
    $.when(initRegister()).done(function (map) {

        var identificationObj = {};
        initRegisterMap = {};
        registrationCampaigns = map.campaigns || [];

        if (map.errorCode == 108) {
            $('#registerWrapper').html($('#register_error').html(getErrorMessage(map)));
            isRegistrationBlocked = true;
            return;
        }

        if (map.address != null) {
            map.houseNumber = map.address;
        }
        if (map.residentialAddress != null) {
            map.residentialHouseNumber = map.residentialAddress;
        }

        for (var key in map) {
            var mandatory = map[key].mandatory || false,
                readOnly = map[key].readOnly || false,
                minLength = map[key].minLength || 0,
                maxLength = map[key].maxLength || 128;

            showField(key, mandatory, readOnly, minLength, maxLength);

            // For the identification field
            if (oFields.identification[key]) {
                showField('identification', identificationObj[key] = !!mandatory);
            }

            // Temporary set to "null" all mandatory fields, related to the second step of registration,
            // thereby validation will be working on the first step
            if (registerType == '2steps' && map[key].mandatory && $('#register-step-2').find('[data-register-field="' + key + '"]').length > 0) {
                map[key].mandatory = null;
            }
        }

        // Identification field
        populateIdentificationDropdown(identificationObj, identificationID, 'li_');

        function showField(name, isRequired, isReadOnly, minLength, maxLength) {
            name = name || '';
            isRequired = !!isRequired;
            isReadOnly = !!isReadOnly;
            minLength = minLength || 0;
            maxLength = maxLength || 128;

            // Prevent double initialization (but allow to enable ability of being mandatory)
            if (initRegisterMap[name] === false && !isRequired || initRegisterMap[name] === true) {
                return;
            }

            $('#li_' + name).
                find(':text, :password').
                prop('readonly', isReadOnly).
                prop('minLength', minLength).
                prop('maxLength', maxLength);

            $('#li_' + name).find(':checkbox, :radio, select').each(function() {
                if(isReadOnly) {
                    var selectId = $(this).prop('id'),
                        selectName = $(this).prop('name'),
                        selectValue = $(this).prop('value');

                    if ($('#' + selectId + 'Copy').length == 0) {
                        $('<input>').attr({
                            id: selectId + 'Copy',
                            type: 'hidden',
                            name: selectName,
                            value: selectValue
                        }).appendTo('#li_' + name);
                    }
                    $(this).prop('disabled', isReadOnly);
                }
            });

            $('#li_' + name).show();
            initRegisterMap[name] = isRequired;

            // If it is a required field, we show the star
            if (isRequired) {
                $('#star_' + name).show();
                // Perform validations when operating fields (blur function)
                bindBlurFunction(name, bindCallback);
            }
        }

    });

    if (isRegistrationBlocked) {
        return;
    }

    // Reset initial values of the fields
    $("#register_form")[0].reset();
    $('input[type="hidden"]:not([id$="Copy"])', '#register_form').val('');  // The standard reset() function doesn't reset hidden inputs

    setPromoCode(getParamValue("usePromo"));

    // Reset error- and promo elements
    $(".registerValidOK").html("");
    $(".registerFieldError").html("");
    $(".registerFieldError_checkbox").html("");

    $("#registerSuccess").hide();
    $("#registerWrapper").show();
    $("#register_error").hide();

    // Hide curtain
    $("#registerWrapper_curtain").hide();
    $("#registerWrapper").css("opacity",1);

    $("#signUpButton").addClass("disabled");

    if (registerType == '2steps') {
        $("#register-step-1").show();
        $("#register-step-2").hide();
        $("#continueSignUpButton").addClass("disabled");
    }

    if(promocode == "" || promocode == null) {
        $("#" + promoID).removeAttr("readonly");
    }

    // The original text in register form's terms-part
    if(!termsText) {
        termsText = $("#registerTerms1Text").html();
    }

    // Style checkboxes (if defined)
    if($(".jquery-checkbox-wrapper").length == 0 && styledInputs)   {
        $("#"+termsID+", #receiveEmail").checkbox();
    }
    else if(styledInputs) {
        $("a.jquery-checkbox[name=accept_user_terms]").removeClass("jquery-checkbox-on");
    }

    $("#" + pwd1ID).keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
            event.preventDefault();
        }
    });

    // Paste option disable from right click (mouse button)
    $("#" + pwd2ID).bind('paste', null, function(e) {
        if(!e.keyCode) return false;
    });

    // Ctrl+v paste option disable from keyboard
    $("#" + pwd2ID).keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
            event.preventDefault();
        }
    });

    // Perform validations when operating fields (change function)
    $.each([birthYearID, birthMonthID, birthDateID, countryID, termsID, currencyID, campaignID, identificationID, documentIssuingCountryID, documentIssuedDateYearID, documentIssuedDateMonthID, documentIssuedDateDayID], function(index, entry) {
        bindChangeFunction(entry, bindCallback);
    });

    // Populate country dropdown
    populateRegisterCountry();

    // Populate currency dropdown
    populateCurrencyDropdown();

    // Assigning tab indexes - from left to right in the columns
    defineTabIndexes();

}

function populateIdentificationDropdown(fieldsObj, dropdownId, fieldPrefix, fieldPostfix, preSelectedField) {
    fieldsObj = fieldsObj || {};
    dropdownId = dropdownId || '';
    fieldPrefix = fieldPrefix || '';
    fieldPostfix = fieldPostfix || '';
    preSelectedField = preSelectedField || '';

    if ($.isEmptyObject(fieldsObj)) return;

    var $dropdown = $('#' + dropdownId),
        selectorOptions = ['<option value="" selected="selected" disabled="disabled">' + registerSelectIdentification + '</option>'];

    for (var field in fieldsObj) {
        var fieldGroup = oFields.identification[field],
            selectorOption = '<option value="' + fieldGroup + '">' + window['registerIdentification_' + fieldGroup] + '</option>';

        if (!~selectorOptions.indexOf(selectorOption)) {
            selectorOptions.push(selectorOption);
        }
        $('#' + fieldPrefix + field + fieldPostfix).addClass('identificationItem hidden');
    }

    $dropdown.html(selectorOptions.join(''));
    $dropdown.unbind('change.fields').bind('change.fields', function () {
        var value = $(this).val() || '';

        for (var field in fieldsObj) {
            var isFieldEnabled = value === oFields.identification[field];
            $('#' + fieldPrefix + field + fieldPostfix).toggleClass('hidden', !isFieldEnabled).find(':input').attr('disabled', !isFieldEnabled);
        }
    });

    if (preSelectedField) {
        $dropdown.val(oFields.identification[preSelectedField]).change();
    }
}

function populateRegisterCountry() {
    playersCountry = "AFG";

    $.when(getRegisterCountry()).done(function(map) {
        var $country = $('#' + countryID);

        if (!hasErrors(map)) {
            playersCountry = map.country;
        }

        // Set the current value of country selector
        $country.val(playersCountry).change();

        populateYearsInRegister(playersCountry,false);
        populateRegisterPhoneNumber(playersCountry);
    });
}

function populateCurrencyDropdown() {

    $.when(getCurrencies()).done(function(map) {
        if(map != null) {

            var currencyOptions = "<option value='' selected='true' disabled='disabled'>" + registerSelectCurrency + "</option>";

            // Loop currencies from JSON
            $.each(map.currencies, function(index, item){
                currencyOptions += "<option value='"+item+"'>"+item+"</option>";
            });

            $('#' + currencyID)
                // Add options to selector
                .html(currencyOptions)
                // Populate campaigns dropdown according to selected currency
                .unbind('change.campaigns')
                .bind('change.campaigns', function () {
                    var currency = $(this).val();
                    populateCampaignDropdown( registrationCampaigns, 'li_campaigns', 'li_promoCode', { currency: currency }, getCurrencySymbol(currency));
                })
                .trigger('change.campaigns');

            if(styledInputs) {
                stylizeDropdown("#" + currencyID,5);
            }
        }
    });
}

function populateCampaignDropdown(campaignsArr, dropdownContainerId, promoCodeContainerId, filterOptions, currencySymbol) {
    campaignsArr = campaignsArr || [];
    filterOptions = filterOptions || {};
    currencySymbol = currencySymbol || '';

    var $dropdownContainerId = $('#' + dropdownContainerId),
        $dropdown = $dropdownContainerId.find('select'),
        $dropdownHint = $dropdownContainerId.find('.control-hint'),
        dropdownOptions = '',
        dropdownObject = {},
        hasPredefinedPromoCode = !!getParamValue('usePromo'),
        hasPromoCodeCampaign = false,
        activeCampaign = $dropdown.val() || {};

    // Adding empty option to campaigns dropdown
    dropdownOptions += '<option value="" disabled>' + campaignsSelectBonus + '</option>';

    for (var i = 0; i < campaignsArr.length; i++) {
        var campaign = campaignsArr[i];

        if (filterOptions.currency && !~campaign.currency.indexOf(filterOptions.currency)) {
            continue;
        }
        if (filterOptions.amount && (campaign.minDepositAmount != null && filterOptions.amount < campaign.minDepositAmount || campaign.maxDepositAmount != null && filterOptions.amount > campaign.maxDepositAmount && !campaign.maxAmountCapEnabled)) {
            continue;
        }
        if (campaign.promoCode === true) {
            hasPromoCodeCampaign = true;
        }
        else {
            dropdownOptions += '<option value="' + campaign.campaignId + '">' + campaign.title + '</option>';
            dropdownObject[campaign.campaignId] = campaign;
        }
    }

    if (hasPromoCodeCampaign) {
        dropdownOptions += '<option value="0"' + (hasPredefinedPromoCode ? ' selected="selected"' : '') + '>' + campaignsSelectPromoCode + '</option>';
        dropdownObject[0] = {};
    }
    dropdownOptions += '<option value="-1">' + campaignsSelectNone + '</option>';
    $dropdown.html(dropdownOptions);
    $dropdownContainerId.toggleClass('hidden', !campaignsArr.length);

    // Try to preserve selected campaign when list is re-filtered ("no bonus" isn't preserved)
    if (dropdownObject[activeCampaign]) {
        $dropdown.val(activeCampaign);
    }
    else {
        $dropdown.val("");
    }

    // Populate hint and handle promo code
    $dropdown
        .unbind('change.campaigns')
        .bind('change.campaigns', function () {
            var campaignId = $(this).val(),
                campaign = dropdownObject[campaignId] || {},
                amount = retrieveCampaignBonusAmount(campaign, currencySymbol),
                bonusAmount = amount.bonusAmount,
                maxBonusAmount = amount.maxBonusAmount,
                turnoverFactor = parseFloat(campaign.turnoverFactor) || 0,
                balanceType = campaign.balanceType || 'deposit',  // promo, deposit, freeSpins, betInAction
                originalHint = $dropdownHint.attr('data-title'),
                newHint = '',
                $promoCodeContainerId = $('#' + promoCodeContainerId),
                $promoCode = $promoCodeContainerId.find('input');

            // Show / Hide promo code container and enable / disable promo code field
            $promoCodeContainerId.toggleClass('hidden', campaignId !== '0');
            $promoCode.attr('disabled', campaignId !== '0');

            // Preserve original hint
            if (!originalHint) {
                originalHint = $dropdownHint.attr('title');
                $dropdownHint.attr('data-title', originalHint);
            }

            // Set new hint
            if (bonusAmount && turnoverFactor) {
                newHint = replaceParams((balanceType === 'betInAction' ? campaignsBIAHintWithWagering : campaignsHintWithWagering), [bonusAmount, turnoverFactor, maxBonusAmount]);
            }
            else if (bonusAmount) {
                newHint = replaceParams(campaignsHint, [bonusAmount, maxBonusAmount]);
            }
            else {
                newHint = originalHint;
            }

            $dropdownHint.attr('title', newHint);
        })
        .trigger('change.campaigns');
}

function retrieveCampaignBonusAmount(campaign, currency) {
    var bonusAmount = parseFloat(campaign.bonusAmount) || 0,
        amountType = campaign.amountType || 'fixed',  // fixed, percentage
        balanceType = campaign.balanceType || 'deposit',  // promo, deposit, freeSpins, betInAction
        gameProviderName = getGameProviderName(campaign.campaignGameProvider || 0),
        maxBonusAmount = '';

    if (bonusAmount) {
        if (balanceType === 'deposit' || balanceType === 'promo' || balanceType === 'betInAction') {
            if (amountType === 'fixed') {
                bonusAmount = (currency || '') + bonusAmount.toFixed(2);
            }
            else if (amountType === 'percentage') {
                bonusAmount += '%';
                maxBonusAmount = campaign.maxAmountCapEnabled ? ' ' + upTo + ' ' + (currency || '') + (campaign.maxDepositAmount * campaign.bonusAmount / 100).toFixed(2) : '';
            }
            if (balanceType === 'promo') {
                bonusAmount += ' ' + transactionPromo;
            }
            else {
                bonusAmount += ' ' + transactionCash;
            }
        }
        else if (balanceType === 'freeSpins') {
            if (amountType === 'percentage') {
                bonusAmount += '%';
                maxBonusAmount = campaign.maxAmountCapEnabled ? ' ' + upTo + ' ' + Math.floor(campaign.maxDepositAmount * campaign.bonusAmount / 100) : '';
            }
            bonusAmount += ' ' + transactionFreeSpins + (gameProviderName ? ' (' + gameProviderName + ')' : '');
        }
    }

    return {
        bonusAmount: bonusAmount,
        maxBonusAmount: maxBonusAmount
    };
}

function moveToNextField(id) {

    // Delay for input field population (mostly for IE)
    setTimeout(function () {
        if (($("#" + id).val().length) == $("#" + id)[0].maxLength) {
            var nextInput = $(':input').get($(':input').index($("#" + id)) + 1);
            if (nextInput) {
                nextInput.focus();
            }
        }
    }, 1);

}

function getRegisterCountry() {
    return $.ajax({
        url: "/billfold-api/config/country",
        type: "GET",
        dataType: "json",
        async: false
    });
}

function getCurrencies() {
    return $.ajax({
        url: "/billfold-api/config/currencies",
        type: "GET",
        dataType: "json",
        async: false
    });
}

function bindBlurFunction(elementID, callbackFunction) {

    callbackFunction = callbackFunction || function () {};

    showRegisterErrorMessages = true;

    if (elementID == "password") {
        $("#" + pwd1ID).unbind("blur").blur(function() {
            validatePassword(pwd1ID);
            callbackFunction();
        });
    }
    else if (elementID == "confirmPassword") {
        $("#" + pwd2ID).unbind("blur").blur(function() {
            validatePassword(pwd2ID);
            callbackFunction();
        });
    }
    else if (elementID == "nickName") {
        $("#" + nnameID).unbind("blur").blur(function() {
            validateNickName();
            callbackFunction();
        });
    }
    else if (elementID == "userName") {
        $("#" + unameID).unbind("blur").blur(function() {
            validateUserName();
            callbackFunction();
        });
    }
    else if(elementID === "email") {
        $("#" + emailID).unbind("blur").blur(function() {
            validateEmail(emailID);
            callbackFunction();
        });
    }
    else if (elementID == "confirmEmail") {
        $("#" + emailConfID).unbind("blur").blur(function() {
            validateEmail(emailConfID);
            callbackFunction();
        });
    }
    else if (elementID == "promoCode") {
        $("#" + promoID).unbind("blur").blur(function() {
            promocode = $("#" + promoID).val();
            checkEmptyRegisterfield(promoID);
            callbackFunction();
        });
    }
    else if (elementID == "firstName") {
        $("#" + firstNameID).unbind("blur").blur(function() {
            validatePersonalName(firstNameID);
            callbackFunction();
        });
    }
    else if (elementID == "lastName") {
        $("#" + lastNameID).unbind("blur").blur(function() {
            validatePersonalName(lastNameID);
            callbackFunction();
        });
    }
    else if (elementID == "middleName") {
        $("#" + middleNameID).unbind("blur").blur(function() {
            validatePersonalName(middleNameID);
            callbackFunction();
        });
    }
    else if (elementID == "birthPlace") {
        $("#" + birthPlaceID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(birthPlaceID);
            callbackFunction();
        });
    }
    else if (elementID == "address") {
        $("#" + addressID).unbind("blur").blur(function() {
            validateAddressField(addressID,houseNumberID,"error")
            callbackFunction();
        });
    }
    else if (elementID == "houseNumber") {
        $("#" + houseNumberID).unbind("blur").blur(function() {
            validateAddressField(addressID,houseNumberID,"error")
            callbackFunction();
        });
    }
    else if (elementID == "city") {
        $("#" + cityID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(cityID);
            callbackFunction();
        });
    }
    else if (elementID == "zipCode") {
        $("#" + zipCodeID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(zipCodeID);
            callbackFunction();
        });
    }
    else if (elementID == "ssn") {
        $.each([ssnYearID, ssnMonthID, ssnDateID, ssnNumberID], function () {
            $("#" + this).unbind("blur").blur(function() {
                checkSSN();
                callbackFunction();
            });
        });
    }
    else if (elementID == "passportNumber") {
        $("#" + passportNumberID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(passportNumberID);
            callbackFunction();
        });
    }
    else if (elementID == "foreignerNumber") {
        $("#" + foreignerNumberID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(foreignerNumberID);
            callbackFunction();
        });
    }
    else if (elementID == "documentNumber") {
        $("#" + documentNumberID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(documentNumberID);
            callbackFunction();
        });
    }
    else if (elementID == "documentIssuedBy") {
        $("#" + documentIssuedByID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(documentIssuedByID);
            callbackFunction();
        });
    }
    else if (elementID == "gender") {
        $.each([genderMaleID, genderFemaleID], function() {
            $("#" + this).unbind("change").change(function() {
                checkRadioSelector("gender", "registerGender_error");
                callbackFunction();
            });
        });
    }
    else if (elementID == "phoneNumber") {
        $("#" + phoneNumberID).unbind("blur").blur(function() {
            validateRegistrationPhoneNumber(phoneNumberID, true);
            callbackFunction();
        });
    }
    else if (elementID == "skypeId") {
        $("#" + skypeIdID).unbind("blur").blur(function() {
            validateUserAccounts(skypeIdID);
            callbackFunction();
        });
    }
    else if (elementID == "weChatId") {
        $("#" + weChatIdID).unbind("blur").blur(function() {
            validateUserAccounts(weChatIdID);
            callbackFunction();
        });
    }
    else if (elementID == "qqId") {
        $("#" + qqIdID).unbind("blur").blur(function() {
            validateUserAccounts(qqIdID);
            callbackFunction();
        });
    }
    else if (elementID == "citizenshipNumber") {
        $("#" + citizenshipNumberID).unbind("blur").blur(function() {
            checkCitizenshipNumber(citizenshipNumberID);
            callbackFunction();
        });
    }
    else if (elementID == "documentType") {
        $.each([documentTypeIdCardID, documentTypePassportID, documentTypeOtherID], function() {
            $("#" + this).unbind("change").change(function() {
                checkRadioSelector('documentType', 'registerDocumentType_error');
                callbackFunction();
            });
        });
    }
    else if (elementID == "residentialAddress") {
        $("#" + residentialAddressID).unbind("blur").blur(function() {
            validateAddressField(residentialAddressID, residentialHouseNumberID, "error");
            callbackFunction();
        });
    }
    else if (elementID == "residentialHouseNumber") {
        $("#" + residentialHouseNumberID).unbind("blur").blur(function() {
            validateAddressField(residentialAddressID, residentialHouseNumberID, "error");
            callbackFunction();
        });
    }
    else if (elementID == "residentialCity") {
        $("#" + residentialCityID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(residentialCityID);
            callbackFunction();
        });
    }
    else if (elementID == "residentialZipCode") {
        $("#" + residentialZipCodeID).unbind("blur").blur(function() {
            checkEmptyRegisterfield(residentialZipCodeID);
            callbackFunction();
        });
    }
}

function bindChangeFunction(elementID, callbackFunction) {
    callbackFunction = callbackFunction || function () {};

    if(elementID != termsID || (elementID == termsID && !styledInputs)) {
        $("#" + elementID).unbind('change.binding');
    }

    // Bind change-event to Terms & Conditions -checkbox just one time
    if(!(elementID == termsID && termsCheckBoxChangeBound == true)) {
        if(elementID == termsID && styledInputs) {
            termsCheckBoxChangeBound = true;
        }

        $("#" + elementID).bind('change.binding', function() {
            if (elementID == birthYearID || elementID == birthMonthID || elementID == birthDateID) {
                checkBirthdayField();
                callbackFunction();
            }
            else if (elementID == countryID) {
                playersCountry = $('#' + countryID).val();
                populateYearsInRegister(playersCountry,false);
                populateRegisterPhoneNumber(playersCountry);
                callbackFunction();
            }
            else if (elementID == termsID) {
                if($("#" + termsID).attr("checked") == "checked") {
                    $("#" + termsID).val("1");
                    $("#" + ageCheckedID).val("1");
                }
                else {
                    $("#" + termsID).val("0");
                    $("#" + ageCheckedID).val("0");
                }
                checkTerms(termsID);
                callbackFunction();
            }
            else if (elementID == currencyID) {
                checkEmptyRegisterfield(currencyID);
                callbackFunction();
            }
            else if (elementID == campaignID) {
                checkEmptyRegisterfield(campaignID);
                callbackFunction();
            }
            else if (elementID == identificationID) {
                checkEmptyRegisterfield(identificationID);
                callbackFunction();
            }
            else if (elementID == documentIssuingCountryID) {
                checkEmptyRegisterfield(documentIssuingCountryID);
                callbackFunction();
            }
            else if (elementID == documentIssuedDateYearID || elementID == documentIssuedDateMonthID || elementID == documentIssuedDateDayID) {
                if (checkdocumentIssuedDateField(documentIssuedDateID)) {
                    var year = $('#' + documentIssuedDateYearID).val() || '',
                        month = $('#' + documentIssuedDateMonthID).val() || '',
                        day = $('#' + documentIssuedDateDayID).val() || '';
                    $('#' + documentIssuedDateID).val(year + '-' + month + '-' + day);
                }
                callbackFunction();
            }
        });
    }
}

// Assigning tab indexes for register fields
function defineTabIndexes() {

    var numberOfRows = 0,
        numberOfCols = 0,
        tabIndex = 1;

    // Remove all tabindex
    $('#registerWrapper [tabindex]').removeAttr('tabindex');

    // Count rows and cols
    $('#registerWrapper .register-col:visible').each(function () {
        var rows = $(this).find('> ul > li:visible').length;
        if (rows > numberOfRows) {
            numberOfRows = rows;
        }
        numberOfCols++;
    });

    // Assigning tab indexes from left to right
    for (var i = 0; i < numberOfRows; i++) {
        for (var j = 0; j < numberOfCols; j++) {
            $('#registerWrapper .register-col:visible:eq('+j+') > ul > li:visible:eq('+i+')').find('input:visible, select:visible').each(function () {
                $(this).attr("tabindex", tabIndex);
                tabIndex++;
            });
        }
    }

    // Assigning tab indexes from left to right in the footer
    $("#register_footer:visible input:visible").each(function() {
        $(this).attr("tabindex", tabIndex);
        tabIndex++;
    });

    // Assigning tab index for Continue and Register btn
    $("#continueSignUpButton:visible").attr("tabindex", tabIndex);
    $("#signUpButton:visible").attr("tabindex", tabIndex);

    // Focus on the 1st element
    $('#registerWrapper [tabindex="1"]:first').focus();

}

function initRegister() {
    return $.ajax({

        url: "/billfold-api/player/initRegister",
        type: "GET",
        dataType: "json",
        async: false
    });
}

// Populate the birthyear dropdown in register form
function populateYearsInRegister(requestCountry, showNote) {
    var yearLimit = 18;

    // Populate year dropdown (different age limit for Estonia, Belgium and Mexico)
    if (requestCountry == "EST" || requestCountry == "BEL" || requestCountry == "MEX") {
        yearLimit = 21;
    }

    if (yearLimit === currentAgeLimit) return;

    currentAgeLimit = yearLimit;

    var $birthYear = $('#' + birthYearID),
        $birthDateError = $('#birthdayField_error'),
        birthYearOptions = '',
        yearTopValue = today.getFullYear() - yearLimit;

    for(var i = 0; i < 90; i++) {
        birthYearOptions += '<option value="' + (yearTopValue - i) + '">' + (yearTopValue - i) + '</option>';
    }

    $('option:not(:first)', $birthYear).remove();
    $birthYear.append(birthYearOptions);

    if ($birthYear.val() || $birthDateError.html()) {
        $('option:first', $birthYear).attr('selected', 'selected').change();
    }
    if (styledInputs) {
        if (registerYearSelector) {
            registerYearSelector.destroy();
        }
        registerYearSelector = stylizeDropdown('#' + birthYearID, 10);
    }
    if (showNote) {
        $birthDateError.html(replaceParams(msgBirthyearNote, [currentAgeLimit]));
    }

    // Replace the age number in the register form's terms-text
    $('#registerTerms1Text').html(replaceParams(termsText, [currentAgeLimit]));
}

function populateRegisterPhoneNumber(country) {

    country = country || 'AFG';

    $("#" + phoneNumberID + "_error").empty();
    $("#" + phoneNumberID + "_validOK").empty();
    $('#' + phoneNumberID).val(window['phoneCode_' + country]);

    // Adding example in the hint msg
    addPhoneNumberExample("registerPhoneNumberHint", country);
}

// Validate fields and submit registration form to server
function register() {

    if(checkAllTheRequiredRegisterFields()) {

        progressIndicator(true);
        $("#registerWrapper_curtain").show();
        $("#registerWrapper").css("opacity",0.3);

        // Add small delay to the sending of info to make sure that progress indicator is displayed
        setTimeout(function() {

            var data = $("#register_form").serializeAndModify({
                checkboxesAsBools: true,
                allowEmpty: false,
                unmodifiable: ['password', 'confirmPassword'],
                modifier: afterSerialization
            }) + "&device=" + deviceFingerprint;

            $.when(registerAjax(data)).done(function (map) {
                if(!hasErrors(map)) {

                    if(map.success) {

                        progressIndicator(false);
                        $("#registerWrapper_curtain").hide();
                        $("#registerWrapper").css("opacity",1);
                        $("#registerWrapper").hide();

                        // if 'ACTIVATION_PROCESS' property enabled in BO
                        if (activationProcess) {
                            $("#registerSuccess").show();
                            $.fancybox.update();
                        }
                        else {
                            $(".registerFancyBox").hide();
                            $.fancybox.close();
                            autoLoginFromRegisterPopup();
                        }

                        // Reset values of the fields
                        $("#register_form")[0].reset();
                    }
                }
                else {

                    progressIndicator(false);
                    $("#registerWrapper_curtain").hide();
                    $("#registerWrapper").css("opacity",1);
                    var error = getErrorMessage(map);
                    showErrorObj($("#register_error"), error);
                }
            });
        }, 50);
    }
}

// Validate fields on the first step of registration (2-steps registration)
function continueRegister() {

    if (checkAllTheRequiredRegisterFields()) {
        $("#register-step-1").hide();
        $("#register-step-2").show();
        defineTabIndexes();

        // Restore validation of mandatory fields for the second step of validation
        // and set to "null" for mandatory fields, related to the first step
        for (var key in initRegisterMap) {
            if (initRegisterMap[key]) {
                initRegisterMap[key] = null;
            }
            else if (initRegisterMap[key] === null) {
                initRegisterMap[key] = true;
            }
        }
    }

}

function registerAjax(data) {

    // Timezone offset in mins
    var currentDate = new Date(),
        offset = (-1)*currentDate.getTimezoneOffset();

    return $.ajax({
        url: "/billfold-api/player/registerPlayer",
        type: "POST",
        data: data + '&timezone=' + offset,
        dataType: "json"
    });
}

function checkAllTheRequiredRegisterFields() {

    showRegisterErrorMessages = true;
    var isOK = true,
        identificationVal = $('#' + identificationID).val() || '';

    for (var key in initRegisterMap) {
        var identificationFieldGroup = oFields.identification[key];

        if (initRegisterMap[key] && (!identificationFieldGroup || identificationFieldGroup === identificationVal)) {
            if (key == "email" && !validateEmail(emailID)) {
                isOK = false;
            }
            else if (key == "confirmEmail" && !validateEmail(emailConfID)) {
                isOK = false;
            }
            else if (key == "password" && !validatePassword(pwd1ID)) {
                isOK = false;
            }
            else if (key == "confirmPassword" && !validatePassword(pwd2ID)) {
                isOK = false;
            }
            else if (key == "firstName" && !validatePersonalName(firstNameID)) {
                isOK = false;
            }
            else if (key == "lastName" && !validatePersonalName(lastNameID)) {
                isOK = false;
            }
            else if (key == "middleName" && !validatePersonalName(middleNameID)) {
                isOK = false;
            }
            else if (key == "nickName" && !validateNickName()) {
                isOK = false;
            }
            else if (key == "userName" && !validateUserName()) {
                isOK = false;
            }
            else if (key == "birthPlace" && !checkEmptyRegisterfield(birthPlaceID)) {
                isOK = false;
            }
            else if (key == "birthDate" && !checkBirthdayField()) {
                isOK = false;
            }
            else if (key == "address" && !validateAddressField(addressID,houseNumberID,"error")) {
                isOK = false;
            }
            else if (key == "houseNumber" && !validateAddressField(addressID,houseNumberID,"error")) {
                isOK = false;
            }
            else if (key == "city" && !checkEmptyRegisterfield(cityID)) {
                isOK = false;
            }
            else if (key == "zipCode" && !checkEmptyRegisterfield(zipCodeID)) {
                isOK = false;
            }
            else if (key == "gender" && !checkRadioSelector("gender", "registerGender_error")) {
                isOK = false;
            }
            else if (key == "policyChecked" && !checkTerms(termsID)) {
                isOK = false;
            }
            else if (key == "promoCode" && !checkEmptyRegisterfield(promoID)) {
                isOK = false;
            }
            else if (key == "phoneNumber" && !validateRegistrationPhoneNumber(phoneNumberID, true)) {
                isOK = false;
            }
            else if (key == "skypeId" && !validateUserAccounts(skypeIdID)) {
                isOK = false;
            }
            else if (key == "weChatId" && !validateUserAccounts(weChatIdID)) {
                isOK = false;
            }
            else if (key == "qqId" && !validateUserAccounts(qqIdID)) {
                isOK = false;
            }
            else if (key == "identification" && !checkEmptyRegisterfield(identificationID)) {
                isOK = false;
            }
            else if (key == "passportNumber" && !checkEmptyRegisterfield(passportNumberID)) {
                isOK = false;
            }
            else if (key == "ssn" && !checkSSN()) {
                isOK = false;
            }
            else if (key == "citizenshipNumber" && !checkCitizenshipNumber(citizenshipNumberID, true)) {
                isOK = false;
            }
            else if (key == "foreignerNumber" && !checkEmptyRegisterfield(foreignerNumberID)) {
                isOK = false;
            }
            else if (key == "documentType" && !checkRadioSelector('documentType', 'registerDocumentType_error')) {
                isOK = false;
            }
            else if (key == "documentNumber" && !checkEmptyRegisterfield(documentNumberID)) {
                isOK = false;
            }
            else if (key == "documentIssuingCountry" && !checkEmptyRegisterfield(documentIssuingCountryID)) {
                isOK = false;
            }
            else if (key == "documentIssuedDate" && !checkdocumentIssuedDateField(documentIssuedDateID)) {
                isOK = false;
            }
            else if (key == "documentIssuedBy" && !checkEmptyRegisterfield(documentIssuedByID)) {
                isOK = false;
            }
            else if (key == "residentialAddress" && !validateAddressField(residentialAddressID, residentialHouseNumberID, "error")) {
                isOK = false;
            }
            else if (key == "residentialHouseNumber" && !validateAddressField(residentialAddressID, residentialHouseNumberID, "error")) {
                isOK = false;
            }
            else if (key == "residentialCity" && !checkEmptyRegisterfield(residentialCityID)) {
                isOK = false;
            }
            else if (key == "residentialZipCode" && !checkEmptyRegisterfield(residentialZipCodeID)) {
                isOK = false;
            }
            // Check that currency is selected
            else if (key == "currency" && !checkEmptyRegisterfield(currencyID)) {
                isOK = false;
            }
        }
        if (showRegisterErrorMessages == false) break;

        // Check that campaign is selected
        if (key == "campaigns" && !checkEmptyRegisterfield(campaignID)) {
            isOK = false;
        }
    }

    return isOK;
}

// DEPRECATED
function selectSsnOrPassportNumber() {
    if ($('input[name=passportNumberRadio]:checked').val() == "ssn") {
        $("#ssnDiv").show();
        $("#passportNumberDiv").hide();
    }
    else {
        $("#ssnDiv").hide();
        $("#passportNumberDiv").show();
    }
}

// Activate player after clicked link in register e-mail
function activatePlayer() {

    var code = getParamValue("activationCode");

    $.ajax({

        cache: false,
        url: '/billfold-api/player/activate',
        type: 'post',
        async: false,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: "activationCode="+code,
        dataType: 'json',
        success: function(map) {

            if(!hasErrors(map)) {

                if(map.success == true) {
                    loadUserTrackingScript('register', {'%ACTIVATIONID%': code});

                    var activationMsg = "<h4 class='h1'>"+msgActivationConfirmationHeader+"</h4><p>"+msgActivationConfirmationWelcome+"</p>";
                    $("#activationMessage").show();
                    $("#activationMessage").html(activationMsg);
                    $("#loginEmail").val(map.email);

                    // If the browser is IE, handle the loginEmail field separately so that the email is prefilled correctly
                    var isMSIE = /*@cc_on!@*/false;

                    if(isMSIE) {
                        $("#loginEmail-faux").hide();
                        $("#loginEmail").removeClass("placeholderstyle");
                        $("#loginEmail").show().focus();
                    }
                }
            }
            else {

                var error = "<h4 class='h1'>" + getErrorMessage(map) + "</h4>";
                showErrorObj($("#activationMessage"), error);
            }
        }
    });
}

// Verificate email after clicked link in verification e-mail
function verifyPlayerEmail() {

    var code = getParamValue("activationCode");

    $.ajax({

        cache: false,
        url: '/billfold-api/player/verify',
        type: 'post',
        async: false,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: "activationCode="+code,
        dataType: 'json',
        success: function(map) {

            if(!hasErrors(map)) {

                if(map.success == true) {
                    loadUserTrackingScript('verify', {'%VERIFICATIONID%': code});

                    var verificationMsg = "<h4 class='h1'>"+msgVerificationConfirmationHeader+"</h4><p>"+msgVerificationConfirmationWelcome+"</p>";
                    $("#verificationMessage").show();
                    $("#verificationMessage").html(verificationMsg);
                    $("#loginEmail").val(map.email);

                    // If the browser is IE, handle the loginEmail field separately so that the email is prefilled correctly
                    var isMSIE = /*@cc_on!@*/false;

                    if(isMSIE) {
                        $("#loginEmail-faux").hide();
                        $("#loginEmail").removeClass("placeholderstyle");
                        $("#loginEmail").show().focus();
                    }
                }
            }
            else {

                var error = "<h4 class='h1'>" + getErrorMessage(map) + "</h4>";
                showErrorObj($("#verificationMessage"), error);
            }
        }
    });
}

//---------------------------------------------------------------------------------------------------------------------
// Forgot password form (forgotpassword.jsp)
//

// Show field-specific error
function showValidateError(show, msg) {

    var p = $('#forgotPwdEmail_error');

    if (p) {
        p.text(msg);

        var s = (show == true) ? 'block' : 'none';
        p.css('display', s);
    }
}

// Submit the forgot password -form
function submitForgotPassword(){

    if (validateForgotpwdEmail()) {

        // Show progress indicator and curtain
        progressIndicator(true);
        $("#forgotPwdWrapper_curtain").show();
        $("#forgotpasswordWrapper").css("opacity",0.3);

        // Add small delay to the sending of info to make sure that progress indicator is displayed
        setTimeout(function() {

            $.ajax({
                url: '/billfold-api/player/triggerForgotPassword',
                type: 'post',
                async: false,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                data: $("#forgetpasswordForm").serialize(),
                dataType: 'json',
                success: function(map) {

                    // Hide progress indicator and curtain
                    progressIndicator(false);
                    $("#forgotPwdWrapper_curtain").hide();
                    $("#forgotpasswordWrapper").css("opacity",1);

                    if(!hasErrors(map)) {

                        $("#forgotpasswordWrapper").hide();
                        $("#forgotpasswordSuccess").show();

                        $.fancybox.update();
                    }
                    else {

                        var errorString = getErrorMessage(map);

                        if( typeof map.remaining != "undefined" ) {
                            showErrorObj($("#forgotPwdHead_error"), replaceParams(errorString,[map.remaining]));
                        } else {
                            showErrorObj($("#forgotPwdHead_error"), errorString);
                        }
                    }
                }
            });

        },50);
    }
}

//---------------------------------------------------------------------------------------------------------------------
// Reset password -related functions
//
var resetPasswordActivationCode = "";

function initResetPassword() {

    pwd1ID = "resetPassword_password";
    pwd2ID = "resetPassword_confirmPassword";

    resetPasswordActivationCode = getParamValue("activationCode");
    $("#resetPassword_activationCode").val(resetPasswordActivationCode);

    $("#"+pwd1ID).on('keyup blur input', function() {
        validatePassword(pwd1ID);
        showRegisterErrorMessages = true;
    });

    $("#"+pwd2ID).on('keyup blur input', function() {
        validatePassword(pwd2ID);
        showRegisterErrorMessages = true;
    });
}

// Check validity of passwords before reseting them
function resetPassword() {

    pwd1ID = "resetPassword_password";
    pwd2ID = "resetPassword_confirmPassword";

    var isOK = true;

    if(!validatePassword(pwd1ID)) {
        isOK = false;
    }

    if(!validatePassword(pwd2ID)) {
        isOK = false;
    }

    if (isOK) {

        progressIndicator(true);

        // Short timeout (delay) is needed that progress indicator can be visible also in Chrome
        setTimeout(function() {

            $.ajax({
                url: '/billfold-api/player/modifyForgotPassword',
                type: 'post',
                async: false,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                dataType: 'json',
                data: $("#resetPassword_form").serialize(),
                success: function(map) {

                    progressIndicator(false);

                    if(!hasErrors(map)) {

                        $("#resetPassword_successful").show();
                        $("#resetPassword_form").hide();
                        $("#resetPassword_error").html('');
                        $("#resetPassword_error").hide();
                    }
                    else {
                        var error = getErrorMessage(map);
                        showErrorObj($("#resetPassword_error"), error);
                    }
                }
            });

        },50);
    }
}

//---------------------------------------------------------------------------------------------------------------------
// Newsletter ordering functions
//

// Submit newsletter form
function submitNewsForm() {

    $.ajax({
        url: '/billfold-api/newsletter',
        type: 'post',
        async: false,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: $("#newsletter_form").serialize(),
        dataType: 'json',
        success: function(map) {

            if(!hasErrors(map)) {

                $("#newsMsg").html(msgNewsletterOk);

                $("#newComfirmNo").hide();
                $("#newComfirmyes").hide();
                $("#newCancel").hide();
                $("#newsletterOk").show();
            }
            else {
                var error = getErrorMessage(map);
                showErrorObj($("#newsMsg"), error);

                $("#newComfirmNo").hide();
                $("#newComfirmyes").hide();
                $("#newCancel").show();
                $("#newsletterOk").hide();
            }
        }
    });
}

//---------------------------------------------------------------------------------------------------------------------
// Language switcher -related functions
//

// Get the date in format yyyy-MM-dd hh:mm
function getFormattedDate(param) {

    var dateObj = new Date(parseInt(param));
    var formattedDate = addDateZero(dateObj.getDate())+"-"+addDateZero(((dateObj.getMonth())+1))+"-"+dateObj.getFullYear()+" "+addDateZero(dateObj.getHours())+":"+addDateZero(dateObj.getMinutes());

    return formattedDate;
}

// Adds extra zero to date fields that are < 10
function addDateZero(param) {

    if(param < 10) {
        param = "0"+param;
    }
    return param;
}

// Get the symbol of currency
function getCurrencySymbol(param) {

    if(window['transactionCurrencySymbol_'+param]) {
        return window['transactionCurrencySymbol_'+param];
    } else {
        return param;
    }
}

// Get the name based on product id
function getProductName(param) {

    if(window['transactionProductId_'+param]) {
        return window['transactionProductId_'+param];
    } else {
        return param;
    }
}

function getGameDisplayName(name) {
    if (window[name]) return window[name];
    return name;
}

function getGameGroupDisplayName(name) {
    if (window["msgGameGroup_"+name]) return window["msgGameGroup_"+name];
    return name;
}

// Get the game providers names by their ID
function getGameProviderName(id) {
    if(window["gameProv_"+id]) return window["gameProv_"+id];
    return '';
}

// Get the transaction type translation string
function getType(param) {

    if(window['transactionType_'+param]) {
        return window['transactionType_'+param];
    } else {
        return param;
    }
}

// Get the transaction status translation string
function getStatus(param) {

    if(window['transactionStatus_'+param]) {
        return window['transactionStatus_'+param];
    } else {
        return param;
    }
}

// Get the transaction direction translation string
function getCommissionType(param) {
    return window['transactionCommissionType_' + param] || param;
}

// Get the name of payment provider
function getPaymentProv(provider, methodId) {

    if(window['paymentProv_' + methodId]) {
        return window['paymentProv_' + methodId];
    } else {
        return provider;
    }
}

// Get the title for the additional info for the payment provider
function getProviderExtraInfo(providerName, methodId, field) {

    if(window[providerName + '_' + methodId + '_' + field]) {
        return window[providerName + '_' + methodId + '_' + field];
    } else {
        return null;
    }
}

// Get the title for the selection of the Payment Details
function getPaymentDetailsSelection(providerName, methodId) {

    if(window['paymentDetails_'+ providerName + '_' + methodId]) {
        return window['paymentDetails_'+ providerName + '_' + methodId];
    } else {
        return null;
    }
}

// Get hint for the banking details
function getProviderHintInfo(providerName, methodId, field) {

    if(window[providerName + '_' + methodId + '_' + field + '_hint']) {
        return window[providerName + '_' + methodId + '_' + field + '_hint'];
    } else {
        return null;
    }
}

// Get descriptor for payment provider
function getPaymentDescriptor(providerName) {

    if(window['depositDescriptor_'+providerName]) {
        return window['depositDescriptor_'+providerName];
    } else {
        return '';
    }
}

// Get user activity name
function getUserActivityName(name) {
    if (window["userActivity"+name]) return window["userActivity"+name];
    return name;
}

//---------------------------------------------------------------------------------------------------------------------
// User loyalty tier
function getUserLoyaltyTier(loyaltyTier) {
    if(window['userLoyaltyTier_'+ loyaltyTier]) {
        return window['userLoyaltyTier_' + loyaltyTier];
    } else {
        return loyaltyTier;
    }
}

//---------------------------------------------------------------------------------------------------------------------
// Contact us -popup
//

// Reset fields before opening contact form
function contactOnload() {

    if($("#contactWrapper").length > 0) {

        // Reset initial values of the fields
        var contactSubj = document.getElementById("contact_subjectId");

        if(contactSubj) {
            contactSubj.options[0].selected = true;
        }

        //$("#contact_specifySubject").val("");
        $("#contact_message").val("");
        $("#contact_sendMeCopy").attr("checked", true);

        $("#contact_firstName_validOK").html("");
        $("#contact_lastName_validOK").html("");
        $("#contact_email_validOK").html("");
        $("#contact_subjectId_validOK").html("");
        $("#contact_specifySubject_validOK").html("");
        $("#contact_message_validOK").html("");

        // Perform validations when operating fields
        $("#contact_firstName").blur(function() {
            checkEmptyContactUs("contact_firstName");
            setContactSendButtonStyle();
        });

        $("#contact_lastName").blur(function() {
            checkEmptyContactUs("contact_lastName");
            setContactSendButtonStyle();
        });

        $("#contact_email").blur(function() {
            checkContactUsEmail("contact_email");
            setContactSendButtonStyle();
        });

        $("#contact_subjectId").change(function() {
            checkEmptyContactUs("contact_subjectId");
            setContactSendButtonStyle();
        });

        $("#contact_specifySubject").blur(function() {
            checkEmptyContactUs("contact_specifySubject");
            setContactSendButtonStyle();
        });

        $("#contact_message").blur(function() {
            checkEmptyContactUs("contact_message");
            setContactSendButtonStyle();
        });

        // Hide curtain
        $("#contactWrapper_curtain").hide();
        $("#contactWrapper").css("opacity",1);

        // Empty error fields
        $(".contactFieldError").html("");

        showErrors = true;

        if(userIsLogged == false) {

            $("#contact_firstName").val("");
            $("#contact_lastName").val("");
            $("#contact_email").val("");

        } else {

            if($("#contact_firstName").length > 0) {
                if($("#contact_firstName").val().length > 0) $("#contact_firstName_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
            }
            if($("#contact_lastName").length > 0) {
                if($("#contact_lastName").val().length > 0) $("#contact_lastName_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
            }
            if($("#contact_email").length > 0) {
                if($("#contact_email").val().length > 0) $("#contact_email_validOK").html('<i title="OK" class="fa fa-check fa-lg"></i>');
            }
        }

        // Make the submit button to show "disabled" (grey) when initializing the form
        setContactSendButtonStyle();

        // Make sure that correct elements are shown or hidden
        $("#contactWrapper").show();
        $("#contactSuccess").hide();
        $("#contact_error").hide();

        $("#contactus_toplink").addClass("staticLinkActive");
    }

}

// Set button style to "disabled" if the validation fails
function setContactSendButtonStyle() {

    showErrors = false;
    var isOK = true;

    if(!checkEmptyContactUs("contact_firstName")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_lastName")){
        isOK = false;
    }

    if(!checkContactUsEmail("contact_email")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_subjectId")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_specifySubject")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_message")){
        isOK = false;
    }

    if(isOK) {
        $("#contact_sendMessage").removeClass("disabled");
    }
    else {
        $("#contact_sendMessage").addClass("disabled");
    }

    showErrors = true;
}

function submitContactUsForm() {

    var isOK = true;

    if(!checkEmptyContactUs("contact_firstName")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_lastName")){
        isOK = false;
    }

    if(!checkContactUsEmail("contact_email")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_subjectId")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_specifySubject")){
        isOK = false;
    }

    if(!checkEmptyContactUs("contact_message")){
        isOK = false;
    }

    if(isOK){

        $("#contact_error").hide();

        // Show the progress indicator and curtain
        progressIndicator(true);
        $("#contactWrapper_curtain").show();
        $("#contactWrapper").css("opacity",0.3);

        // Add small delay to the sending of info to make sure that progress indicator is displayed
        setTimeout(function() {

            $.ajax({
                url: '/billfold-api/contactus',
                type: 'post',
                async: false,
                data: $("#contactForm").serialize({ checkboxesAsBools: true }),
                dataType: 'json',
                success: function(map) {

                    progressIndicator(false);
                    $("#contactWrapper_curtain").hide();
                    $("#contactWrapper").css("opacity",1);

                    if(!hasErrors(map)) {

                        $("#contactWrapper").hide();
                        $("#contactSuccess").show();

                        $.fancybox.update();

                        // Reset values of the fields
                        if(userIsLogged == false) {

                            $("#contact_firstName").val("");
                            $("#contact_lastName").val("");
                            $("#contact_email").val("");
                        }

                        document.getElementById("contact_subjectId").options[0].selected = true;
                        $("#contact_message").val("");
                        $("#contact_sendMeCopy").attr("checked", true);

                    } else {

                        var error = getErrorMessage(map);
                        showErrorObj($("#contact_error"), error);
                    }
                }
            });

        }, 50);
    }
}

//---------------------------------------------------------------------------------------------------------------------
// Refer a friend -page
//

// Reset fields before opening the form
function referAFriendOnload() {

    // Hide progress indicator and curtain
    progressIndicator(false);
    $("#referAFriendWrapper_curtain").hide();
    $("#referAFriendWrapper").css("opacity",1);

    // Reset fields
    $("#referAFriend_yourName").val("");
    $("#referAFriend_friendsName").val("");
    $("#referAFriend_friendsEmail").val("");
    $("#referAFriend_yourMessage").val("");

    // Make sure that correct elements are shown or hidden
    $("#referAFriendWrapper").show();
    $("#referAFriendSuccess").hide();
    $("#referAFriend_error").hide();
}

function submitReferAFriendForm() {

    var isOK = true;

    if(!checkEmptyReferAFriend("referAFriend_yourName")){
        isOK = false;
    }

    if(!checkEmptyReferAFriend("referAFriend_friendsName")){
        isOK = false;
    }

    if(!checkReferAFriendEmail("referAFriend_friendsEmail")){
        isOK = false;
    }

    if(!checkEmptyReferAFriend("referAFriend_yourMessage")){
        isOK = false;
    }

    if(isOK){

        $("#referAFriend_error").hide();

        // Show the progress indicator and curtain

        progressIndicator(true);
        $("#referAFriendWrapper_curtain").show();
        $("#referAFriendWrapper").css("opacity",0.3);

        // Add small delay to the sending of info to make sure that progress indicator is displayed
        setTimeout(function() {

            $.ajax({
                url: '/billfold-api/contactus',
                type: 'post',
                async: false,
                data: $("#referAFriendForm").serialize(),
                dataType: 'json',
                success: function(map) {

                    if(!hasErrors(map)) {

                        progressIndicator(false);
                        $("#referAFriendWrapper_curtain").hide();
                        $("#referAFriendWrapper").css("opacity",1);

                        $("#referAFriendWrapper").hide();
                        $("#referAFriendSuccess").show();

                        // Reset fields
                        $("#referAFriend_yourName").val("");
                        $("#referAFriend_friendsName").val("");
                        $("#referAFriend_friendsEmail").val("");
                        $("#referAFriend_yourMessage").val("");

                    }
                    else {

                        var error = getErrorMessage(map);
                        showErrorObj($("#referAFriend_error"), error);

                        progressIndicator(false);
                        $("#referAFriendtWrapper_curtain").hide();
                        $("#referAFriendWrapper").css("opacity",1);
                    }
                }
            });

        }, 50);
    }
}

function hasErrors(result) {

    return (typeof result.error !== 'undefined');
}

function initDetailsGet() {

    return $.ajax({

        url: "/billfold-api/player/initDetails",
        type: "GET",
        dataType: "json"
    });
}

function updateDetailsPost(data) {

    return $.ajax({

        url: "/billfold-api/player/updateDetails",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function checkNoDuplicates(data) {

    if (data.indexOf('city') != -1 && data.indexOf('zipCode') != -1 && data.indexOf('address') != -1  && data.indexOf('lastName') != -1 && data.indexOf('firstName') != -1) {
        return $.ajax({

            url: "/billfold-api/player/validateAddress",
            type: "POST",
            data: data,
            dataType: "json"
        });
    }
    else return { success: true };
}

function getLoyaltyInfo() {

    return $.ajax({

        url: "/billfold-api/player/loyalty",
        type: "GET",
        dataType: "json"
    });
}

function updatePasswordPost(data) {

    return $.ajax({

        url: "/billfold-api/player/updatePassword",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function limitsRequest() {

    return $.ajax({

        url: "/billfold-api/player/limits",
        type: "GET",
        dataType: "json"
    });
}

function totalLimitsPost(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/deposit",
        type: "POST",
        data: data,
        dataType: "json"
    });
}
function totalLossLimitsPost(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/loss",
        type: "POST",
        data: data,
        dataType: "json"
    });
}
function totalWagerLimitsPost(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/wager",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function oneTimeLimitsPost(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/oneTime",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function realMoneyLimitPost(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/exclude",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function initPrefillRequest() {

    return $.ajax({

        url: "/billfold-api/payment/initPrefillDeposit",
        type: "GET",
        dataType: "json"
    });
}

function prefillUpdatePost(data) {

    return $.ajax({

        url: "/billfold-api/payment/prefillDeposit",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function initPrefillWithdrawalRequest() {

    return $.ajax({

        url: "/billfold-api/payment/initPrefillWithdrawal",
        type: "GET",
        dataType: "json"
    });
}

function prefillWithrawalPost(data) {

    return $.ajax({

        url: "/billfold-api/payment/prefillWithdrawal",
        type: "POST",
        data: data,
        dataType: "json"
    });
}

function getGamesListAll(forcibly) {

    return $.ajax({
        url: '/billfold-api/games/all',
        dataType: 'json',
        actualize: !!forcibly
    }).done(function (map) {
        // Add new properties for each game
        $.each(map.games, function (index, game) {
            game['title'] = getGameDisplayName(game.name);
            game['providerName'] = getGameProviderName(game.provider);
        });
    });

}

function getLoyaltyRate(forcibly) {

    return $.ajax({
        url: '/billfold-api/player/loyaltyRate',
        dataType: 'json',
        actualize: !!forcibly
    });

}

function getBIABonus(forcibly) {

    return $.ajax({
        url: '/billfold-api/player/biaBonus',
        dataType: 'json',
        actualize: !!forcibly
    });

}

function convertLoyaltyPoints(amount) {

    return $.ajax({
        url: '/billfold-api/player/convertLoyaltyPoints',
        type: 'POST',
        dataType: 'json',
        data: {
            pointAmount: amount
        }
    });

}

function detectIEversion() {

    // IE's version
    var isMSIE = /*@cc_on!@*/false;
    var ieVersion = (function(reg) { return isMSIE && navigator.userAgent.match(reg) ? RegExp.$1 * 1 : null; })(/MSIE\s([0-9]+[\.0-9]*)/);

    var oldIE = false;
    if (isMSIE && ieVersion < 8) {
        oldIE = true;
    }

    if (oldIE) {
        // Here's JS for IE.
        alert(msgBrowserVersionError);
    } else {
        // ..And here's the full-fat code for everyone else
    }

    return ieVersion;
}

// Show or hide the progress indicator
function progressIndicator(value) {

    if(value) {
        $("#progressIndicatorBackground").show();
    }
    else {
        $("#progressIndicatorBackground").hide();
    }
}

// Authentication for the site
function indexOnLoad() {

    $(document).ready(function() {

        if (document.getElementById('authenticationCheckbox').checked == false) {
            $("#loading-overlay").show();
        }

    });

}

// Recursive function for return to parent popup
function returnToParentPopup(currentPopup) {
    var parentPopup = $(currentPopup).attr('data-fancybox-return');
    if (parentPopup) {
        $.fancybox({
            href : '#' + parentPopup,
            title : '',
            beforeLoad : function() {
                $(currentPopup).removeAttr('data-fancybox-return');
                fancyboxReturnPopupClicked = false;
            },
            afterClose : function() {
                if (!fancyboxReturnPopupClicked) {
                    returnToParentPopup(parentPopup);
                }
            },
            helpers : {
                overlay : { closeClick: false }
            }
        });
    }
}

function getUrlParameter(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url ? url.substring(url.indexOf('?')) : location.search);
    return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Load a user tracking script to a specific page
function loadUserTrackingScript(page, params) {

    page = page || 'default';
    params = $.isPlainObject(params) ? params : {};

    return $.ajax({
        url: '/cms/pages/user-tracking/' + page + '.html',
        dataType: 'html'
    }).done(function (data) {
        data = data.replace(/%\w+%/g, function (placeholder) {
            return params[placeholder] || placeholder;
        });
        $('head:not(.tracked)').append(data).addClass('tracked');
    });

}

// Remember me check
$(function() {

	/* Using local storage instead of cookie for better security */

	if (localStorage.checkBoxValidation && localStorage.checkBoxValidation != '') {
		$('#rememberMe').attr('checked', 'checked');
		$('#loginEmail').val(localStorage.userName);
	} else {
		$('#rememberMe').removeAttr('checked');
		$('#loginEmail').val('');
	}

	$('#login_form').on('submit', function() {

		if ($('#rememberMe').is(':checked')) {
			// save username and password
			localStorage.userName = $('#loginEmail').val();
			localStorage.checkBoxValidation = $('#rememberMe').val();
		} else {
			localStorage.userName = '';
			localStorage.checkBoxValidation = '';
		}
	});
});

// T&C, Privacy Policy. Add content to fancybox
function populatePopupsViaAjax(popupIdentifiers, popupsPath) {
    for (var i = 0; i < popupIdentifiers.length; i++) {
        var popupIdentifier = popupIdentifiers[i];
        $("#" + popupIdentifier + "Popup").load(popupsPath + popupIdentifier + ".html");
    }
}

// Add/remove game to/from favourites
function toggleFavouriteGame(element, gameId) {
    if ($(element).hasClass('active')) {
        $(element).addClass('pending');
        ajaxCalls.removeFromFavourites(gameId).done(function (response) {
            if (response.success === true) {
                updateGamesListCache(false, gameId);
                $(element).removeClass('active');
            }
        }).always(function () {
            $(element).removeClass('pending');
        });
    }
    else {
        $(element).addClass('pending');
        ajaxCalls.addToFavourites(gameId).done(function (response) {
            if (response.success === true) {
                updateGamesListCache(true, gameId);
                $(element).addClass('active');
            }
        }).always(function () {
            $(element).removeClass('pending');
        });
    }
}

function updateGamesListCache(isFavourite, gameId) {
    var gamesListCache = oGamesList.activeGroup.array;
    for (var i = 0; i < gamesListCache.length; i++) {
      var game = gamesListCache[i];
      if (game.gameId == gameId) {
          game.favourite = isFavourite;
          break;
      }
    }
}

// Returns percentage of current turnover limit.
// (How much wagering is required before you can withdraw your bonus funds. 100% means you can withdraw and 0% means you have a long way to go)
function calculateWageringRequirement(remaining, total) {
    remaining = parseFloat(remaining) || 0;
    total = parseFloat(total) || 0;

    if (!total) {
        return null;
    }
    return Math.floor(100 - remaining / total * 100);
}