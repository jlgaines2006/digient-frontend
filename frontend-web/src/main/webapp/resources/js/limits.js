function limitsOnLoad() {

	$(document).ready(function() {

		var currencyInUse = "";

		$('.limitCurrencySymbol').each(function(i, obj) {
			currencyInUse = $(this).html();
			$(this).html(getCurrencySymbol(currencyInUse));
		});

		if(styledInputs) {

			setTimeout(function() {

				stylizeDropdown("#periodSelector_deposit",10);
				stylizeDropdown("#periodSelector_loss",10);
				stylizeDropdown("#periodSelector_wager",10);
			}, 10);
		}

		progressIndicator(true);

		// Short timeout (delay) is needed that progress indicator can be visible also in Chrome
		setTimeout(function() {

			$.when(limitsRequest()).done(function (limitsMap) {

				if(!hasErrors(limitsMap)) {

					for (var i = 0; i < limitsMap.length; i++) {

						// TotalDepositLimit form
						if (limitsMap[i].deposit != null) {

							storeMinAndMaxAmounts(limitsMap[i], 'deposit');

							if (limitsMap[i].deposit) {
								displayDepositWagerLossLimits(limitsMap[i], 'deposit');
							}
							else {
								displayDepositWagerLossLimitNotSaved('deposit');
							}
						}

						// Total Wager form
						else if (limitsMap[i].wager != null) {

							storeMinAndMaxAmounts(limitsMap[i], 'wager');

							if (limitsMap[i].wager) {
								displayDepositWagerLossLimits(limitsMap[i],'wager');
							}
							else {
								displayDepositWagerLossLimitNotSaved('wager');
							}
						}

						// Total Loss form
						else if (limitsMap[i].loss != null) {

							storeMinAndMaxAmounts(limitsMap[i], 'loss');

							if (limitsMap[i].loss) {
								displayDepositWagerLossLimits(limitsMap[i], 'loss');
							}
							else {
								displayDepositWagerLossLimitNotSaved('loss');
							}
						}

						// OneTimeLimit form
						else if (limitsMap[i].oneTime != null) {

							storeMinAndMaxAmounts(limitsMap[i], 'oneTime');

							if (limitsMap[i].oneTime) {
								$("#limitCurrencySymbol").hide();
								displayOneTimeLimit(limitsMap[i]);
							}
							else {
								$("#limitNotSaved_oneTime").show();
								$("#limits_saveButton2").show();
							}
						}

						// RealMoney form
						else if (limitsMap[i].exclude != null) {

							if (limitsMap[i].exclude) {
								displayExcludeLimit(limitsMap[i]);
							} else {
								$("#limitNotSaved_exclude").show();
								$("#limits_saveButton3").show();
							}
						}

						else if (limitsMap[i].timeLimit != null) {

							if (limitsMap[i].timeLimit) {
								displayTimeLimitSaved(limitsMap[i], 'timeLimit');
							}
							else {
								displayTimeLimitNotSaved('timeLimit');
							}
						}

						else if (limitsMap[i].gamingTimeLimit != null) {
							if (limitsMap[i].gamingTimeLimit) {
								$('#realityCheck').attr('checked', 'checked');
							}
						}

						// Auto Flushing
						else if (limitsMap[i].autoFlush != null) {
							populateAutoFlushLimit(limitsMap[i]);
						}

					}

					// Show all forms
					progressIndicator(false);
					document.getElementById("loadingContent").style.visibility = "visible";

					if(styledInputs) {

						setTimeout(function() {

							stylizeDropdown("#periodSelector_deposit",10);
							stylizeDropdown("#periodSelector_loss",10);
							stylizeDropdown("#periodSelector_wager",10);

							$("#autoRenew_deposit").checkbox();
							$("#autoRenew_deposit").hide();
							$("#autoRenew_loss").checkbox();
							$("#autoRenew_loss").hide();
							$("#autoRenew_wager").checkbox();
							$("#autoRenew_wager").hide();
							$("#realityCheck").checkbox();
							$("#realityCheck").hide();
						}, 10);
					}
				}
			});

		},50);

		$( "#tabs_limits" ).tabs({
			active: 0,
			activate: function( event, ui ) {
				$("#limitsWrapper .limitsFieldError").empty();

				$("#newAmount_loss, #newAmount_wager, #newAmount_deposit, #pdl_limit_amount").val("");
			}
		});
    });
}

function displayDepositWagerLossLimits(map, limitType) {

    var amount = parseFloat(map.amount).toFixed(2);
    $("#savedLimit_" + limitType).val(amount);
    $("#period_" + limitType).val(map.period);

    $("#limitSaved_" + limitType).show();
    $("#limitNotSaved_" + limitType).hide();
    $("#newLimitAmount_" + limitType).show();
    $("#limitAmount_" + limitType).hide();
    $("#newAmount_" + limitType).val('');
    $("#decreaseExplanation_" + limitType).show();

    if (map.autoRenew) {
        $('#autoRenew_' + limitType).attr('checked', 'checked');
        var dateString = getDateString(parseDateString(map.startTime), map.period);
    } else {
        var dateString = map.endTime;
    }
    $("#limitSavedInfo_" + limitType).html(getInfoString(amount, getCurrencySymbol(map.currency), dateString, map.autoRenew, map.period));
}

function displayDepositWagerLossLimitNotSaved(limitType) {

    $("#limitNotSaved_" + limitType).show();
    $("#limitAmount_" + limitType).show();
}

function displayExcludeLimit(map) {

    var endDate = map.endDate;
    $("#limitSaved_exclude").show();
    $("#limitNotSaved_exclude").hide();
    $("#excludeLimit_true_endDate").html(endDate);
}

function displayOneTimeLimit(map) {

	var amount = parseFloat(map.amount).toFixed(2),
		endDate = map.endDate;
	$("#limitSaved_oneTime").show();
	$("#limitNotSaved_oneTime").hide();
	$("#limits_saveButton2").css("display","none");
	$("#oneTimeLimit_true_amount").html(amount);
	$("#oneTimeLimit_true_endDate").html(endDate);
}

function displayTimeLimitSaved(map, limitType) {

    $("#limitNotSaved_" + limitType).hide();
    $("#limitSaved_" + limitType).show();
    if (limitType == 'timeLimit') {
        $("#limitSavedInfo_" + limitType).html(replaceParams(timeLimitSaved, [map.timeLimitAmount]));
    }
    else if (limitType == 'gamingTimeLimit') {
        $("#limitSavedInfo_" + limitType).html(replaceParams(gamingTimeLimitSaved, [map.gamingTimeLimitAmount]));
    }

}

function displayTimeLimitNotSaved(limitType) {

	$.when(getTimeLimits()).done(function(map) {

		var select = document.getElementById('periodSelector_' + limitType);
		select.options.length = 0;

		$.each(map.timeLimitAmounts, function (key, value) {
			select.options.add(new Option(value + ' ' + limitHourPeriod, value));
		});

		if(styledInputs) {

			setTimeout(function() {
				stylizeDropdown("#periodSelector_timeLimit",15);
			}, 10);
		}
	});
	$("#limitSaved_" + limitType).hide();
	$("#limitNotSaved_" + limitType).show();
}

function getDateString(startTime, period) {

    switch (period) {
        case "day":
            startTime.setDate(startTime.getDate() + 1);
            break;
        case "week":
            startTime.setDate(startTime.getDate() + 7);
            break;
        case "month":
            startTime.setMonth(startTime.getMonth() + 1);
            break;
    }

    var zero_month = "", zero_date = "";
    if ((startTime.getMonth()+1) < 10) {
        zero_month = "0";
    }
    if (startTime.getDate() < 10) {
        zero_date = "0";
    }

    var dateString = zero_date + startTime.getDate() + "-" + zero_month + (startTime.getMonth()+1) + "-" + startTime.getFullYear() + " " + startTime.getHours() + ":" + startTime.getMinutes() + ":" + startTime.getSeconds();
    return dateString;
}

function getPeriodMessage(period) {

	var periodMessage = "";

	switch (period) {
		case "day":
			periodMessage = msgDaily;
			break;
		case "week":
			periodMessage = msgWeekly;
			break;
		case "month":
			periodMessage = msgMonthly;
			break;
	}

	return periodMessage;
}

function getInfoString(amount, currency, dateString, isAutoRenew, period) {

    var infoString = "";
    var period = getPeriodMessage(period);

    if (isAutoRenew) {
        infoString = replaceParams(limitActiveRenewed,[amount, currency, dateString, period]);
    } else {
        infoString = replaceParams(limitActive,[amount, currency, dateString]);
    }

    return infoString;
}

// Storing min and max amounts
function storeMinAndMaxAmounts(limitsMap, limitType) {

    $("#minLimit_" + limitType).val(limitsMap.minAmount);
    $("#maxLimit_" + limitType).val(limitsMap.maxAmount);
}

function totalLimitsSave() {

    var minLimitAmount=$("#minLimit_deposit").val(),
        maxLimitAmount=$("#maxLimit_deposit").val();

    // Original form
    if ($("#savedLimit_deposit").val() == "") {

        if (validateAmount("newAmount_deposit", minLimitAmount, maxLimitAmount)) {

            var amount = parseFloat($("#newAmount_deposit").val()).toFixed(2),
                period = $("#periodSelector_deposit").val();
            updateTotalLimit(amount, period);
        }
    }

    // Limits are already set up
    else {
        var currentLimit = $("#savedLimit_deposit").val(),
            newLimit = $("#newAmount_deposit").val();

        // There is a new limit amount
        if (newLimit != "") {

            if (validateAmount("newAmount_deposit", minLimitAmount, maxLimitAmount)) {

                var amount = parseFloat($("#newAmount_deposit").val()).toFixed(2),
                    period = $("#period_deposit").val();
                updateTotalLimit(amount, period);
            }
        }

        // Check renew checkbox was changed
        else {

            var period = $("#period_deposit").val();
            updateTotalLimit(currentLimit, period);
        }
    }
}

function updateTotalLimit(amount, period) {

    // Obtaining autoRenew field
    var autoRenew = false;

    if ($('#autoRenew_deposit').is(':checked')) {
        autoRenew = true;
    }

    var data = "amount=" + amount + "&period=" + period + "&autoRenew=" + autoRenew;

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

		$.when(totalLimitsPost(data)).done(function(map) {

			progressIndicator(false);

			if(!hasErrors(map)) {

				for (var i = 0; i < map.length; i++) {
					if (map[i].deposit != null)  {
						displayDepositWagerLossLimits(map[i], 'deposit');
						storeMinAndMaxAmounts(map[i],'deposit');
						$("#maxLimit_oneTime").val(map[i].maxAmount);
					}
				}

			} else {
                var error = getErrorMessage(map);
                showErrorObj($("#totalDepositLimit_error"), error);
			}
		});

	},50);
}

// save Loss limit information
function totalLossLimitsSave() {

    var minLimitAmount=$("#minLimit_loss").val(),
        maxLimitAmount=$("#maxLimit_loss").val();

    // Original form
    if ($("#savedLimit_loss").val() == "") {

        if (validateAmount("newAmount_loss", minLimitAmount, maxLimitAmount)) {

            var amount = parseFloat($("#newAmount_loss").val()).toFixed(2),
                period = $("#periodSelector_loss").val();
            updateLossLimit(amount, period);
        }
    }

    // Limits are already set up
    else {

        var currentLimit = $("#savedLimit_loss").val(),
            newLimit = $("#newAmount_loss").val();

        // There is a new limit amount
        if (newLimit != "") {

            if (validateAmount("newAmount_loss", minLimitAmount, maxLimitAmount)) {

                var amount = parseFloat($("#newAmount_loss").val()).toFixed(2),
                    period = $("#period_loss").val();
                updateLossLimit(amount, period);
            }
        }
        // Check renew checkbox was changed
        else {

            var period = $("#period_loss").val();
            updateLossLimit(currentLimit, period);
        }
    }
}

function updateLossLimit(amount, period) {

    // Obtaining autoRenew field
    var autoRenew = false;

    if ($('#autoRenew_loss').is(':checked')) {
        autoRenew = true;
    }

    var data = "amount=" + amount + "&period=" + period + "&autoRenew=" + autoRenew;

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

		$.when(totalLossLimitsPost(data)).done(function(map) {

			progressIndicator(false);

			if(!hasErrors(map)) {

				for (var i = 0; i < map.length; i++) {
					if (map[i].loss != null) {
						displayDepositWagerLossLimits(map[i], 'loss');
						storeMinAndMaxAmounts(map[i],'loss');
					}
				}
			} else {
                var error = getErrorMessage(map);
                showErrorObj($("#totalLossLimit_error"), error);
			}
		});

	},50);
}

// save Wager limit information
function totalWagerLimitsSave() {

    var minLimitAmount=$("#minLimit_wager").val(),
        maxLimitAmount=$("#maxLimit_wager").val();

    // Original form
    if ($("#savedLimit_wager").val() == "") {

        if (validateAmount("newAmount_wager", minLimitAmount, maxLimitAmount)) {

            var amount = parseFloat($("#newAmount_wager").val()).toFixed(2),
                period = $("#periodSelector_wager").val();
            updateWagerLimit(amount, period);
        }
    }

    // Limits are already set up
    else {

        var currentLimit = $("#savedLimit_wager").val(),
            newLimit = $("#newAmount_wager").val();

        // There is a new limit amount
        if (newLimit != "") {

            if (validateAmount("newAmount_wager", minLimitAmount, maxLimitAmount)) {

                var amount = parseFloat($("#newAmount_wager").val()).toFixed(2),
                    period = $("#period_wager").val();
                updateWagerLimit(amount, period);
            }
        }

        // Check renew checkbox was changed
        else {

            var period = $("#period_wager").val();
            updateWagerLimit(currentLimit, period);
        }
    }
}

function updateWagerLimit(amount, period) {

    var autoRenew = false;

    if ($('#autoRenew_wager').is(':checked')) {
        autoRenew = true;
    }

    var data = "amount=" + amount + "&period=" + period + "&autoRenew=" + autoRenew;

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

		$.when(totalWagerLimitsPost(data)).done(function(map) {

			progressIndicator(false);

			if(!hasErrors(map)) {

				for (var i = 0; i < map.length; i++) {
					if (map[i].wager != null)  {
						displayDepositWagerLossLimits(map[i], 'wager');
						storeMinAndMaxAmounts(map[i],'wager');
					}
				}
			} else {
                var error = getErrorMessage(map);
                showErrorObj($("#totalWagerLimit_error"), error);
			}
		});

	},50);
}

//per deposit limit button
function limits_oneTime() {

	var minAmount = $("#minLimit_oneTime").val(),
		maxAmount = $("#maxLimit_oneTime").val(),
		amount_validate = validateAmount("pdl_limit_amount", minAmount, maxAmount),
		date_validate = validateEndDate("pdl-endDate");

	if(amount_validate && date_validate) {

		var amount = $("#pdl_limit_amount").val();
		amount = parseFloat(amount).toFixed(2);

		var endDateValue = $("#pdl-endDate").datepicker("getDate");
		endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();

		var data = "amount=" + amount + "&endDate=" + endDateValue;

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			$.when(oneTimeLimitsPost(data)).done(function(map) {

				progressIndicator(false);

				if(!hasErrors(map)) {

					for (var i = 0; i < map.length; i++) {
						if (map[i].oneTime != null)  displayOneTimeLimit(map[i]);
					}
				}
				else {
                    var error = getErrorMessage(map);
                    showErrorObj($("#oneTimeDepositLimit_error"), error);
				}
			});

		},50);
	}
}

//real money limit button
function limits_realMoney() {

	if(validateEndDate("rml-endDate")) {

		var endDateValue = $("#rml-endDate").datepicker("getDate");
		endDateValue = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();

		var data = "endDate=" + endDateValue;

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			$.when(realMoneyLimitPost(data)).done(function(map) {

				progressIndicator(false);

				if(!hasErrors(map)) {

					for (var i = 0; i < map.length; i++) {
						if (map[i].exclude != null)  displayExcludeLimit(map[i]);
					}
				} else {
                    var error = getErrorMessage(map);
                    showErrorObj($("#realMoneyLimit_error"), error);
				}
			});

		},50);
	}
}

// Get time limit values
function getTimeLimits() {

    return $.ajax({

        url: "/billfold-api/config/timeLimit/amounts",
        type: "GET",
        dataType: "json"
    });
}

function timeLimitSave() {

    var selector = document.getElementById("periodSelector_timeLimit"),
        value = selector.options[selector.selectedIndex].value,
        data = "timeLimitAmount=" + value;

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

		$.when(updateTimeLimit(data)).done(function(map) {

			progressIndicator(false);

			if(!hasErrors(map)) {

				for (var i = 0; i < map.length; i++) {
					if (map[i].timeLimit != null) displayTimeLimitSaved(map[i], 'timeLimit');
				}
			} else {
                var error = getErrorMessage(map);
                showErrorObj($("#timeLimit_error"), error);
			}
		});

	},50);
}

function removeTimeLimit(limitType) {

	progressIndicator(true);

	// Add small delay to the sending of info to make sure that progress indicator is displayed
	setTimeout(function() {

		$.when(removeTimeLimitAjax()).done(function(map) {

			progressIndicator(false);

			if (!hasErrors(map)) {

				displayTimeLimitNotSaved(limitType);
			} else {
                var error = getErrorMessage(map);
                showErrorObj($("#timeLimit_error"), error);
			}
		});

	},50);
}

function updateTimeLimit(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/time",
        type: "POST",
        dataType: "json",
        async: false,
        data: data
    });
}

function removeTimeLimitAjax() {

    return $.ajax({

        url: "/billfold-api/player/limit/time/remove",
        type: "POST",
        dataType: "json",
        async: false
    });
}

function parseDateString(dateString) {

    dateString = dateString.replace(/-|:|\s/g,",");
    dateString = dateString.split(",");

    var integers = [];

    for (var i = 0; i < dateString.length; i++) {
        integers[i] = parseInt(dateString[i]);
    }

    return new Date(integers[0],integers[1],integers[2],integers[3],integers[4],integers[5]);
}

function timeReachedContinue(answer) {

    $.when(resetTimer(answer)).done(function (map) {
        if (map.logout) logout();
        $.fancybox.close();
    });
}

// answer can be Y or N
function resetTimer(answer) {

    return $.ajax({

        url: "/billfold-api/player/limit/resetTime/" + answer,
        type: "GET",
        dataType: "json",
        async: false
    });
}

function realityCheckSave(limitType) {

    if ($('#realityCheck').is(':checked')) {

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			$.when(updateGamingTimeLimit('value=60')).done(function(map) {

				progressIndicator(false);

				if(hasErrors(map)) {
                    var error = getErrorMessage(map);
                    showErrorObj($("#" + limitType + "_error"), error);
				}
			});

		},50);

    } else {

		progressIndicator(true);

		// Add small delay to the sending of info to make sure that progress indicator is displayed
		setTimeout(function() {

			$.when(removeGamingTimeLimitAjax()).done(function(map) {

				progressIndicator(false);

				if (hasErrors(map)) {
                    var error = getErrorMessage(map);
                    showErrorObj($("#" + limitType + "_error"), error);
				}
			});

		},50);
    }
}

function updateGamingTimeLimit(data) {

    return $.ajax({

        url: "/billfold-api/player/limit/gamingTime",
        type: "POST",
        dataType: "json",
        async: false,
        data: data
    });
}

function removeGamingTimeLimitAjax() {

    return $.ajax({

        url: "/billfold-api/player/limit/gamingTime/remove",
        type: "POST",
        dataType: "json",
        async: false
    });
}

function populateAutoFlushLimit(data) {
	var isActive = data.autoFlush,
		period = data.autoFlushPeriod,
		$selector = $('#autoFlushLimitPeriodSelector');

	$('#autoFlushLimitInfo').html( isActive ? replaceParams(limitActiveUntil, ['<strong>' + data.endTime + '</strong>']) : '' ).toggle(isActive);

	if (period === 'month' || period === 'week' || period === 'day') {
		$selector.find('option[value="day"]').remove();
	}
	if (period === 'month' || period === 'week') {
		$selector.find('option[value="week"]').remove();
	}
	if (period === 'month') {
		$selector.find('option[value="month"]').remove();
		$('#autoFlushLimitFields').hide();
	}
}

function updateAutoFlushLimit(period) {
	var $error = $('#autoFlushLimitError').hide();
	progressIndicator(true);

	setTimeout(function () {
		$.when(sendAutoFlushLimit(period)).done(function (data) {

			if (!hasErrors(data)) {
				for (var i = 0; i < data.length; i++) {
					if (data[i].autoFlush != null) {
						populateAutoFlushLimit(data[i]);
					}
				}
			}
			else {
				$error.html(getErrorMessage(data)).show();
			}

		}).always(function () {
			progressIndicator(false);
		});
	}, 50);
}

function sendAutoFlushLimit(period) {
	return $.ajax({
		url: '/billfold-api/player/limit/autoFlush',
		type: 'POST',
		data: { period: period },
		dataType: 'json'
	});
}
