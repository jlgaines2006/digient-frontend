jQuery.preloadCssImages = function(){

	var allImgs = [];//new array for all the image urls  
	var k = 0; //iterator for adding images
	var sheets = document.styleSheets;//array of stylesheets

	for(var i = 1; i<sheets.length; i++){//loop through each stylesheet

		var cssPile = '';//create large string of all css rules in sheet
		/*var csshref = (sheets[i].href) ? sheets[i].href : 'window.location.href';
		 var baseURLarr = csshref.split('/');//split href at / to make array

		 baseURLarr.pop();//remove file path from baseURL array

		 var baseURL = baseURLarr.join('/');//create base url for the images in this sheet (css file's dir)

		 if(baseURL!="") baseURL+='/'; //tack on a / if needed*/

		var baseURL = "";

		try {
			if(document.styleSheets[i].cssRules) {//w3

				var thisSheetRules = document.styleSheets[i].cssRules; //w3

				for(var j = 0; j<thisSheetRules.length; j++){

					if(thisSheetRules[j].cssText.indexOf("@import") == -1) {
						cssPile+= thisSheetRules[j].cssText;
					}
				}
			}
			else {
				cssPile+= document.styleSheets[i].cssText;
			}
		}
		catch(err) {

			return true;
		}

		//parse cssPile for image urls and load them into the DOM
		var imgUrls = cssPile.match(/[^\(]+\.(gif|jpg|jpeg|png)/g);//reg ex to get a string of between a "(" and a ".filename"

		if(imgUrls != null && imgUrls.length>0 && imgUrls != ''){//loop array

			var arr = jQuery.makeArray(imgUrls);//create array from regex obj        
			var urlString;

			jQuery(arr).each(function() {

				urlString = this;
				if(this.charAt(0) === '"') {
					urlString = this.substring(1);
				}

				allImgs[k] = new Image(); //new img obj
				allImgs[k].src = (this[0] == '/' || this.match('http://')) ? this : baseURL + urlString;     //set src either absolute or rel to css dir
				k++;
			});
		}
	}//loop
	return allImgs;
}