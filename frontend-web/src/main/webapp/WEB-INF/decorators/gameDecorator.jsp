<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://granule.com/tags" prefix="g" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">

<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<head>
    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Language" content="${_lang == null ? features.defaultLanguage : _lang}" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta name="revisit-after" content="2days">
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="<spring:message code='site.identifierUrl'/>/cms/images/logo.png"/>
    <meta property="og:site_name" content="<spring:message code='site.officialName'/>"/>
    <meta name="Author" content="<spring:message code='site.officialName'/>">
    <meta name="reply-to" content="<spring:message code='EMAIL_contactus_FROM_ADDRESS'/>">
    <meta name="Identifier-URL" content="<spring:message code='site.identifierUrl'/>"/>

    <%
        response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
        response.addHeader("Cache-Control", "no-store"); //Firefox
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0
        response.setDateHeader("Expires", -1);
        response.setDateHeader("max-age", 0);
    %>

    <link rel="shortcut icon" href="${cmsRoot}/images/icons/general-icons/favicon.ico" />
    <link rel="bookmark" href="${cmsRoot}/images/icons/general-icons/favicon.ico" />

    <%@ include file="/WEB-INF/pages/inc/css-includes.jsp" %>
    <%@ include file="/WEB-INF/pages/inc/js-includes.jsp" %>
    <script type="text/javascript" src="/resources/js/languages/${_lang == null ? features.defaultLanguage : _lang}/jstranslations.js?ts=<%=System.currentTimeMillis()%>"></script>
    <script type="text/javascript" src="/resources/js/languages/gameNames.js?ts=<%=System.currentTimeMillis()%>"></script>
    <%@ include file="/WEB-INF/pages/inc/frontendPropertiesVars.jsp" %>

    <c:if test="${loginUser == null}">
        <script type="text/javascript">

            // Erase game starting -related cookies always if the user hasn't logged in
            eraseCookie("gamestart");

            // Show the content after everything is loaded - or after a second
            setTimeout(function() {

                $("#preLoaderBackground").hide();
                $("#wrapper").show();
                $(window).trigger("resize");
            }, 1000);


            var readyStateCheckInterval = setInterval(function() {

                if (document.readyState === "complete") {

                    $("#preLoaderBackground").hide();
                    $("#wrapper").show();
                    $(window).trigger("resize");

                    clearInterval(readyStateCheckInterval);
                }
            }, 10);

            // Preload all the css-images
            $(document).ready(function() {

              //  $.preloadCssImages();
            });
        </script>
    </c:if>

    <script type="text/javascript">
        recordGameActivity();
    </script>
</head>

<c:choose>
    <c:when test="${features.gameWindowOpening == 'gameInPage'}">

        <body class="${loginUser != null ? 'is-logged-in' : 'is-logged-out'}">

            <c:if test="${loginUser == null}">
                <div id="preLoaderBackground" style="display: block">
                    <div id="preLoader">
                        <img src="/cms/images/icons/general-icons/preLoader.gif" alt="Loading...">
                    </div>
                </div>
            </c:if>

            <c:choose>
                <c:when test="${loginUser == null}">
                    <div id="wrapper" class="container-fluid" style="display: none;">
                </c:when>
                <c:otherwise>
                    <div id="wrapper" class="container-fluid">
                </c:otherwise>
            </c:choose>

                <%@ include file="/WEB-INF/pages/inc/header.jsp" %>

                <c:if test="${features.subHeader != 'false'}">

                    <div id="subheader-fluid" class="row">
                        <div class="container">
                            <div id="subheader" class="row">
                                <div class="col-xs-12">

                                    <c:set var="subHeaderArray" value="${features.subHeader}" />

                                    <c:if test="${!empty subHeaderArray}">
                                        <c:forEach var="subHeaderArray" items="${fn:split(subHeaderArray, ',')}">
                                            <c:import url="/WEB-INF/pages/inc/${subHeaderArray}.jsp" />
                                        </c:forEach>
                                    </c:if>

                                </div>
                            </div><%-- #subheader --%>
                        </div>
                    </div><%-- #subheader-fluid --%>

                </c:if>

                <div id="gamePage-fluid" class="row">
                    <div class="container">
                        <div id="gamePage">
                            <sitemesh:write property='body'/>
                        </div>
                    </div>
                </div>

                <c:if test="${features.preFooter == 'true'}">

                    <div id="prefooter-fluid" class="row">
                        <div class="container">
                            <div id="prefooter" class="row">
                                <div class="col-xs-12">

                                    <%@ include file="/WEB-INF/pages/inc/preFooter.jsp" %>

                                </div>
                            </div><%-- #prefooter --%>
                        </div>
                    </div><%-- #prefooter-fluid --%>

                </c:if>

                <c:if test="${features.socialMediaFeeds != 'false'}">

                    <div id="socialMediaFeedsWrapper-fluid" class="row">
                        <div class="container">
                            <div id="socialMediaFeedsWrapper" class="row">
                                <div class="col-xs-12">

                                    <c:set var="socialMediaFeedsArray">${features.socialMediaFeeds}</c:set>

                                    <c:if test="${!empty socialMediaFeedsArray}">
                                        <c:forEach var="socialMediaFeedsArray" items="${fn:split(socialMediaFeedsArray, ',')}">
                                            <c:import url="/WEB-INF/pages/inc/${socialMediaFeedsArray}.jsp" />
                                        </c:forEach>
                                    </c:if>

                                </div>
                            </div><%-- #socialMediaFeedsWrapper --%>
                        </div>
                    </div><%-- #socialMediaFeedsWrapper-fluid --%>

                </c:if>

                <c:choose>

                    <c:when test="${features.footerType == 'Static'}">
                        <%@ include file="/WEB-INF/pages/inc/footerStatic.jsp" %>
                    </c:when>

                    <c:otherwise>
                        <%@ include file="/WEB-INF/pages/inc/footerPopup.jsp" %>
                    </c:otherwise>

                </c:choose>

            </div><%-- #wrapper --%>

            <div id="progressIndicatorBackground">
                <div id="progressIndicator">
                    <img src="/cms/images/icons/general-icons/progressIndicator.gif" alt="Loading...">
                </div>
            </div>

            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/pages/zenDesk.html" />

            <%@ include file="/WEB-INF/pages/inc/js-latest-includes.jsp" %>

        </body>

    </c:when>
    <c:otherwise>

        <body id="gamePage" class="${loginUser != null ? 'is-logged-in' : 'is-logged-out'} lang-${_lang == null ? features.defaultLanguage : _lang}">
            <sitemesh:write property='body'/>

            <div id="progressIndicatorBackground">
                <div id="progressIndicator">
                    <img src="/cms/images/icons/general-icons/progressIndicator.gif" alt="Loading...">
                </div>
            </div>

            <%@ include file="/WEB-INF/pages/inc/js-latest-includes.jsp" %>
        </body>

    </c:otherwise>
</c:choose>


</html>