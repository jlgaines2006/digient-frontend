<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="resetPasswordWrapper">

                        <h4 class="h1"><spring:message code="resetpassword.h1"/></h4>
                        <p id="resetPassword_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>
                        <p id="resetPassword_successful" class="alert alert-success alert-dismissable" style="display: none"><spring:message code="resetpassword.successful"/></p>

                        <form id="resetPassword_form" class="resetPassword_form form-horizontal">

                            <fieldset>

                                <ul class="list-unstyled">

                                    <li class="form-group has-feedback">
                                        <label class="control-label col-sm-3" for="resetPassword_password"><spring:message code="myaccount.newPassword"/></label>
                                        <div class="col-sm-9">
                                            <input id="resetPassword_password" class="form-control" type="password" name="password" value="">
                                            <div class="form-control-feedback">
                                                <i alt="?" title="<spring:message code="myaccount.newPassword.hint"/>" class="fa fa-question fa-lg"></i>
                                                <span id="resetPassword_password_validOK"></span>
                                            </div>
                                            <div id="resetPassword_password_error" class="registerFieldError resetPwdError help-block error-field"></div>
                                        </div>
                                    </li>


                                    <li class="form-group has-feedback">
                                        <label class="control-label col-sm-3" for="resetPassword_confirmPassword"><spring:message code="myaccount.confirmNewPassword"/></label>
                                        <div class="col-sm-9">
                                            <input id="resetPassword_confirmPassword" class="form-control" type="password" name="confirmPassword" value="">
                                            <div class="form-control-feedback">
                                                <i alt="?" title="<spring:message code="myaccount.confirmNewPassword.hint"/>" class="fa fa-question fa-lg"></i>
                                                <span id="resetPassword_confirmPassword_validOK"></span>
                                            </div>
                                            <div id="resetPassword_confirmPassword_error" class="registerFieldError resetPwdError help-block error-field"></div>
                                        </div>
                                    </li>

                                    <input type="hidden" name="activationCode" id="resetPassword_activationCode" value="1" />

                                    <li class="row">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div id="resetPassword_form_saveButton" class="btn btn-primary btn-lg" onclick="resetPassword();"><spring:message code="save"/></div>
                                        </div>
                                    </li>

                                </ul>

                            </fieldset>

                        </form>

                    </div>

                </div>

            </div>

            <c:if test="${features.mainBannersOthers != 'false'}">

                <div id="main_rightpart" class="col-md-3">

                    <div id="mainbanners">

                        <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                            <%-- JSTL foreach tag example to loop an array in jsp --%>
                        <c:if test="${!empty mainBannersOthersArray}">
                            <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                            </c:forEach>
                        </c:if>

                    </div>

                </div>

            </c:if>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

<script type="text/javascript">

    $(document).ready(function(){
        initResetPassword();
    });

</script>