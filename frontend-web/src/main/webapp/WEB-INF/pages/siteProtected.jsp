<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>To continue provide a username and password</title>
    <style type="text/css">
        * {
            font-family: Tahoma,Arial;
            font-size: 12px;
        }
    </style>
</head>
<body>
<form method="POST" action="/siteProtectionCheck">
    To continue, please provide a username and password. <br /><br />
    <table border="0">
        <tr>
            <td>Username:&nbsp;</td>
            <td><input type="text" name="siteProtectionUsername" /><br /></td>
        </tr>
        <tr>
            <td>Password:&nbsp;</td>
            <td><input type="password" name="siteProtectionPassword" /><br /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" value="Login" title="Login" /></td>
        </tr>
    </table>
</form>
</body>
</html>