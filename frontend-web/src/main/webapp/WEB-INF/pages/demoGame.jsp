<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="gamePageWrapper">

    <div id="msg" class="alert alert-danger alert-dismissable" style="display: none"></div>

    <iframe id="gameWindow">
    </iframe>

    <div id="gameWindowEmpty" style="display: none;">
    </div>

    <div id="gamePageButtons">

        <c:if test="${features.gameWindowOpening == 'fullscreen'}">

            <select id="gamePageDropdown" class="form-control">
            </select>

            <a id="gamePageButton_rules" class="fancyboxPopup" href="#rulesPopup">
                <spring:message code="game.button.rules" />
            </a>
            <a id="gamePageButton_customerService" href="#">
                <spring:message code="game.button.service" />
            </a>
            <div id="gamePageButton_fullscreen">
            </div>

        </c:if>

        <c:choose>
            <c:when test="${loginUser != null}">
                <a id="gamePageButton2_demoLoggedIn" class="btn btn-primary btn-lg" href="#">
                    <spring:message code="game.play.real"/>
                </a>
            </c:when>
            <c:otherwise>
                <a id="gamePageButton2" class="fancyboxPopup btn btn-primary btn-lg" href="#loginregisterPopup" onclick="$('#gameWindow').addClass('gameWindowHidden'); $('#gameWindowEmpty').show();">
                    <spring:message code="game.play.real"/>
                </a>
            </c:otherwise>
        </c:choose>

        <c:if test="${features.gameWindowOpening != 'fullscreen'}">
            <select id="gamePageDropdown" class="form-control">
            </select>
        </c:if>

    </div>

    <input type="hidden" id="demoGameWindow_gamename" value="0" />
    <input type="hidden" id="demoGameWindow_provider" value="0" />

</div>

<div id="rulesPopup" class="popup" style="display: none;">
</div>

<c:if test="${features.gameWindowOpening != 'gameInPage'}">

    <div id="registerPopup" class="popup" style="display: none;">
    <c:choose>
        <c:when test="${features.registerType == '2steps'}">
            <c:import url="/WEB-INF/pages/inc/registerSteps.jsp" />
        </c:when>
        <c:otherwise>
            <c:import url="/WEB-INF/pages/inc/register.jsp" />
        </c:otherwise>
    </c:choose>
    </div>

    <div id="termsPopup" class="popup" style="display: none;">
    </div>

    <div id="termsAcceptancePopup" class="popup" style="display: none;">
    </div>

    <div id="policyPopup" class="popup" style="display: none;">
    </div>

    <div id="loginregisterPopup" class="popup" style="display: none;">
        <c:import url="/WEB-INF/pages/inc/loginregisterpopup.jsp" />
    </div>

    <div id="forgotpasswordPopup" class="popup" style="display: none;">
        <c:import url="/WEB-INF/pages/inc/forgotpassword.jsp" />
    </div>

</c:if>

<script type="text/javascript">

    $(document).ready(function() {

        currentLanguage = "${_lang == null ? features.defaultLanguage : _lang}";
        gameOnload();

        // If we use fullscreen, add the support for receiving messages from gameroom iframe to change the ongoing game
        <c:if test="${features.gameWindowOpening == 'fullscreen'}">
            initFullScreenToggle();
        </c:if>

        // T&C, Privacy Policy. Add content to fancybox
        populatePopupsViaAjax(["terms", "policy", "termsAcceptance"], "${cmsRoot}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/");

    });

    // Open Play2Go -real games when clicked "Play for real" -button
    var PlayForReal = function(username,gameid) {

        <c:choose>

        <c:when test="${loginUser != null}">
            var realGameLink = $("#gamePageButton2_demoLoggedIn").attr('href');
            window.location.href = realGameLink;
        </c:when>

        <c:otherwise>
            $("#gamePageButton2").trigger("click");
        </c:otherwise>

        </c:choose>

    }

</script>