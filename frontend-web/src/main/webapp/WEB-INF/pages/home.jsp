<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<div id="authentication">
    <input id="authenticationCheckbox" type="checkbox" style="display: none">
</div>

<script type="text/javascript">
    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");
    //var isIndexPage=true;
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/gamegroup/${param.gamegroup}.html">
        <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>
    </t:import>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/<c:if test='${not empty param.gamegroup}'>gamegroup/${param.gamegroup}</c:if>" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row index-page">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersIndex != 'false' ? '9' : '12'}">

                <div id="content">

                    <c:choose>

                        <c:when test="${loginUser == null}">
                            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/sliders/sliderPreLogin.html" />
                        </c:when>

                        <c:otherwise>
                            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/sliders/sliderPostLogin.html" />
                        </c:otherwise>

                    </c:choose>

                    <c:if test="${features.sliderBanners != 'false'}">

                        <div id="sliderbanners">

                            <c:set var="sliderBannersArray" value="${features.sliderBanners}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty sliderBannersArray}">
                                <c:forEach var="sliderBannersArray" items="${fn:split(sliderBannersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${sliderBannersArray}.jsp" />
                                </c:forEach>
                            </c:if>
                        </div>

                    </c:if>

                </div>

                <div id="content_rightPlace"></div>

            </div>

            <%--
            <c:if test="${features.mainBannersIndex != 'false'}">

                <div id="main_rightpart" class="col-md-3">
                    <div id="mainbanners">

                        <c:set var="mainBannersIndexArray" value="${features.mainBannersIndex}"/>

                        <c:if test="${!empty mainBannersIndexArray}">
                            <c:forEach var="mainBannersIndexArray" items="${fn:split(mainBannersIndexArray, ',')}">
                                <c:import url="/WEB-INF/pages/inc/${mainBannersIndexArray}.jsp" />
                            </c:forEach>
                        </c:if>

                    </div>
                </div>

            </c:if>
            --%>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

<%--
<c:if test="${features.subSlider != 'false'}">

    <div id="subslider-fluid" class="row">
        <div class="container">
            <div id="subslider" class="row">
                <div class="col-xs-12">

                    <c:set var="subSliderArray" value="${features.subSlider}"/>

                    <c:if test="${!empty subSliderArray}">
                        <c:forEach var="subSliderArray" items="${fn:split(subSliderArray, ',')}">
                            <c:import url="/WEB-INF/pages/inc/${subSliderArray}.jsp" />
                        </c:forEach>
                    </c:if>

                </div>
            </div>&lt;%&ndash; #subslider &ndash;%&gt;
        </div>
    </div>&lt;%&ndash; #subslider-fluid &ndash;%&gt;

</c:if>
--%>

<c:if test="${features.contentMenu != 'false'}">

    <div id="contentmenu-fluid" class="row">
        <div class="container">
            <div id="contentmenu" class="row">
                <div class="col-xs-12">

                    <c:set var="contentMenuArray" value="${features.contentMenu}"/>

                    <c:if test="${!empty contentMenuArray}">
                        <c:forEach var="contentMenuArray" items="${fn:split(contentMenuArray, ',')}">
                            <c:import url="/WEB-INF/pages/inc/${contentMenuArray}.jsp" />
                        </c:forEach>
                    </c:if>

                </div>
            </div><%-- #contentmenu --%>
        </div>
    </div><%-- #contentmenu-fluid --%>

</c:if>

<div id="homecontent-fluid" class="row">
    <div class="container">
        <div id="homecontent" class="row">

            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/home.html" />

        </div><%-- #homecontent --%>
    </div>
</div><%-- #homecontent-fluid --%>

<c:if test="${features.subHeader == 'staticMenuButtons' || features.subHeader == 'slideMenuButtons'}">

    <script type="text/javascript">

        // Make sure that "Home"-menu button is showed with different style when opening index-page (when using static menu buttons in subheader)
        $(document).ready(function() {

            var slideUrlParam = getParamValue("currentSlide");

            if(!slideUrlParam || slideUrlParam == "null")
                $("#home_toplink").addClass("staticLinkActive");
        });

    </script>

</c:if>

<c:if test="${loginUser == null}">

    <script type="text/javascript">

        // Open register form popup automatically - if the site is opened with correct parameter
        $(document).ready(function() {

            var promoCodeParam = getParamValue("usePromo");

            // Open form without promocode
            if(promoCodeParam && promoCodeParam != "0") {

                setPromoCode(promoCodeParam);
            }
            // Open form without promocode
            else if(promoCodeParam === "0") {

                setPromoCode("");
            }

            if(promoCodeParam || promoCodeParam === "0") {

                openRegisterPopup();
            }

        });

    </script>

</c:if>
