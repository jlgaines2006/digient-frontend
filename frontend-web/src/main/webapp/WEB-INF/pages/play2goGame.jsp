<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title></title>
    <c:import url="/WEB-INF/pages/inc/js-includes.jsp"/>
    <script type="text/javascript" src="/resources/js/languages/${_lang == null ? features.defaultLanguage : _lang}/jstranslations.js?ts=<%=System.currentTimeMillis()%>"></script>
    <c:import url="/WEB-INF/pages/inc/frontendPropertiesVars.jsp"/>

    <style type="text/css">
        html, body {
            width: 100%;
            height: 100%;
            margin: 0;
            position: relative;
        }
        #gamecontent {
            width: 100%;
            height: 100%;
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
        }
    </style>

    <script type="text/javascript">

        gameWindowOpening = "${features.gameWindowOpening}";

        $(document).ready(function() {

            var provider = getUrlParameter(null, 'provider');

            // Play'n GO - games. Note: Play2Go -games can't be live-games
            if (provider == '3') {
                onGamePlay2GoLoad();
            }
            else {
                generateSWFObject();
            }

            // Make sure (especially in IE) that the game is resized correctly
            setTimeout(function() {
                $(window).trigger("resize");
            },300);
            setTimeout(function() {
                $(window).trigger("resize");
            },3000);

        });

        $(window).resize(function() {

            var provider = getUrlParameter(null, 'provider');

            if (gameWindowOpening === "gameInPage") {

                var windowWidth = $("body").width(),
                    windowHeight = Math.floor(windowWidth / window['windowAspectRatio_provider_' + provider]) * 0.95;

                document.getElementById("gamecontent").width = (windowWidth - 25) + "px";
                document.getElementById("gamecontent").height = (windowHeight - 25) + "px";

                document.getElementById("gamecontent").style.width = (windowWidth - 25) + "px";
                document.getElementById("gamecontent").style.height = (windowHeight - 25) + "px";

            }
            else if (gameWindowOpening === "browser") {

                var boxDimensions = calculateBoxDimensions({ boxRatio: window['windowAspectRatio_provider_' + provider] });

                document.getElementById("gamecontent").style.width = boxDimensions.width + "px";
                document.getElementById("gamecontent").style.height = boxDimensions.height + "px";

            }

        });

        function Logout() {

            if(gameWindowOpening === "fancybox" || gameWindowOpening === "fancyboxBkg") {
                parent.$.fancybox.close();
            }
            else if(gameWindowOpening === "gameInPage") {
                parent.location.href = "/";
            }
            else {
                parent.window.close();
            }
        }

        function PlayForReal(username,gameid) {

            if(gameWindowOpening === "browser") {
                parent.window.PlayForReal(username,gameid);
            }
        }

        function ShowCashier() {

            if(gameWindowOpening === "browser") {
                parent.window.ShowCashier();
            }
        }

    </script>

</head>
<body>

    <div id="gamecontent"></div>

</body>
</html>