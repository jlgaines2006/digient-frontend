<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">
    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/${param.pageName}.html">
        <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>
    </t:import>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/${param.pageName}" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-sm-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">
                    <c:choose>
                        <c:when test="${param.pageName == 'contact' || param.pageName == 'contactus' || param.pageName == 'contact-us'}">
                            <c:import url="/WEB-INF/pages/inc/contact.jsp" />
                        </c:when>
                        <c:when test="${fn:startsWith(param.pageName, 'news_')}">
                            <t:import url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/news/${param.pageName}.html">
                                <c:redirect url="/${_lang == null ? features.defaultLanguage : _lang}/"/>
                            </t:import>
                        </c:when>
                        <c:otherwise>
                            <t:import url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/${param.pageName}.html">
                                <c:redirect url="/${_lang == null ? features.defaultLanguage : _lang}/"/>
                            </t:import>
                        </c:otherwise>
                    </c:choose>
                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-sm-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

