<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html>
<head>
    <title></title>
    <script>
        (function () {
            'use strict';

            var mode = getUrlParameter(null, 'mode') || 'redirect';  // "redirect", "fancybox", "window"

            if (mode === 'redirect') {  // Redirect to home page
                window.parent.location.href = '/';
            }
            else if (mode === 'fancybox') {  // Close fancybox
                window.parent.$.fancybox.close();
            }
            else if (mode === 'window') {  // Close window
                window.parent.open('', '_self').close();
            }

            function getUrlParameter(url, name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(url ? url.substring(url.indexOf('?')) : location.search);
                return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
        })();
    </script>
</head>
<body></body>
</html>
