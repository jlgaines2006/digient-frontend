<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="https://netent-static.casinomodule.com/gameinclusion/library/gameinclusion.js"></script>
    <style>
        html, body {
            width: 100%;
            height: 100%;
            display: block;
            position: relative;
            margin: 0;
            overflow: hidden;
        }
        #game {
            width: 100%;
            height: 100%;
            display: block;
            position: relative;
            top: 0;
            left: 0;
        }
        #error {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            padding: 15px;
            margin: 10px;
            border: 1px solid #ebccd1;
            border-radius: 4px;
            color: #a94442;
            font: 14px/1.5 "Helvetica Neue", Helvetica, Arial, sans-serif;
            background-color: #f2dede;
            z-index: 500;
        }
    </style>
</head>
<body>
    <div id="game"></div>
    <div id="error"></div>

    <script type="text/javascript">
        (function () {
            'use strict';

            var game = document.getElementById('game'),
                gameAspectRatio = parseFloat(getUrlParameter(null, 'ratio')) || 1.333;

            // Init resize
            (window.onresize = function () {
                resizeGame(game, gameAspectRatio);
            })();

            // Init game
            netent.launch({
                gameId: getUrlParameter(null, 'gameId'),
                staticServer: getUrlParameter(null, 'staticServer'),
                gameServer: getUrlParameter(null, 'gameServer'),
                sessionId: getUrlParameter(null, 'sessionId'),
                language: getUrlParameter(null, 'language'),
                targetElement: 'game'
            }, function (netEntExtend) {
                // Update game container
                game = document.getElementById('game');
                window.onresize();
            }, function (e) {
                var error = document.getElementById('error');
                error.innerHTML = '' + (e || {}).message;
                error.style.display = 'block';
            });

            function resizeGame(game, aspectRatio) {
                aspectRatio = aspectRatio || 1;

                var body = document.body,
                    bodyWidth = body.clientWidth || 0,
                    bodyHeight = body.clientHeight || 0,
                    bodyAspectRatio = (bodyWidth / bodyHeight) || 1,
                    gameWidth, gameHeight, gameTop, gameLeft;

                if (bodyAspectRatio > aspectRatio) {
                    gameWidth = Math.floor(bodyHeight * aspectRatio);
                    gameHeight = bodyHeight;
                    gameTop = 0;
                    gameLeft = Math.floor((bodyWidth - gameWidth) / 2);
                }
                else {
                    gameWidth = bodyWidth;
                    gameHeight = Math.floor(bodyWidth / aspectRatio);
                    gameTop = Math.floor((bodyHeight - gameHeight) / 2);
                    gameLeft = 0;
                }

                game.style.width = gameWidth + 'px';
                game.style.height = gameHeight + 'px';
                game.style.top = gameTop + 'px';
                game.style.left = gameLeft + 'px';
            }

            function getUrlParameter(url, name) {
                name = name.replace(/[\[\]]/g, '\\$&');
                var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                    results = regex.exec(url || window.location.href);
                return !results ? null : (!results[2] ? '' : decodeURIComponent(results[2].replace(/\+/g, ' ')));
            }
        })();
    </script>
</body>
</html>
