<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<script type="text/javascript">
    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");
    var isLiveCasinoPage = true;
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/liveCasino.html">
        <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>
    </t:import>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/liveCasinoPage" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="gamecontent-fluid" class="live-games row">
    <div class="container">
        <div id="gamecontent" class="row">

            <c:if test="${features.gamesPageLeft != 'false'}">

                <div id="gamecontentleft" class="col-xs-12">

                    <c:set var="gamesPageLeftArray" value="${features.gamesPageLeft}"/>

                    <c:if test="${!empty gamesPageLeftArray}">
                        <c:forEach var="gamesPageLeftArray" items="${fn:split(gamesPageLeftArray, ',')}">
                            <c:import url="/WEB-INF/pages/inc/${gamesPageLeftArray}.jsp" />
                        </c:forEach>
                    </c:if>

                </div>

            </c:if>

            <c:if test="${features.liveCasinoPageCenter != 'false'}">

                <div id="gamecontentcenter" class="col-xs-12">

                    <c:set var="liveCasinoPageCenterArray" value="${features.liveCasinoPageCenter}"/>

                    <c:if test="${!empty liveCasinoPageCenterArray}">
                        <c:forEach var="liveCasinoPageCenterArray" items="${fn:split(liveCasinoPageCenterArray, ',')}">
                            <c:import url="/WEB-INF/pages/inc/${liveCasinoPageCenterArray}.jsp" />
                        </c:forEach>
                    </c:if>

                </div>

            </c:if>

            <c:if test="${features.gamesPageRight != 'false'}">

                <div id="gamecontentright" class="col-xs-12">

                    <c:set var="gamesPageRightArray">${features.gamesPageRight}</c:set>

                    <c:if test="${!empty gamesPageRightArray}">
                        <c:forEach var="gamesPageRightArray" items="${fn:split(gamesPageRightArray, ',')}">
                            <c:import url="/WEB-INF/pages/inc/${gamesPageRightArray}.jsp" />
                        </c:forEach>
                    </c:if>

                </div>

            </c:if>

        </div><%-- #gamecontent --%>
    </div>
</div><%-- #gamecontent-fluid --%>