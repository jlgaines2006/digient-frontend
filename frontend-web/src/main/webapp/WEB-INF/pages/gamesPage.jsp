<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");
</script>

<div id="gamecontent">

    <c:choose>

        <c:when test="${features.gamesPageLeft != 'false'}">

            <div id="gamecontentleft">

                <c:set var="gamesPageLeftArray" value="${features.gamesPageLeft}"/>

                <%-- JSTL foreach tag example to loop an array in jsp --%>
                <c:if test="${!empty gamesPageLeftArray}">
                    <c:forEach var="gamesPageLeftArray" items="${fn:split(gamesPageLeftArray, ',')}">
                        <c:import url="/WEB-INF/pages/inc/${gamesPageLeftArray}.jsp" />
                    </c:forEach>
                </c:if>

            </div>

        </c:when>

    </c:choose>

    <c:choose>

        <c:when test="${features.gamesPageCenter != 'false'}">

            <div id="gamecontentcenter">

                <c:set var="gamesPageCenterArray" value="${features.gamesPageCenter}"/>

                <%-- JSTL foreach tag example to loop an array in jsp --%>
                <c:if test="${!empty gamesPageCenterArray}">
                    <c:forEach var="gamesPageCenterArray" items="${fn:split(gamesPageCenterArray, ',')}">
                        <c:import url="/WEB-INF/pages/inc/${gamesPageCenterArray}.jsp" />
                    </c:forEach>
                </c:if>

            </div>

        </c:when>

    </c:choose>

    <c:choose>

        <c:when test="${features.gamesPageRight != 'false'}">

            <div id="gamecontentright">

                <c:set var="gamesPageRightArray">${features.gamesPageRight}</c:set>

                <%-- JSTL foreach tag example to loop an array in jsp --%>
                <c:if test="${!empty gamesPageRightArray}">
                    <c:forEach var="gamesPageRightArray" items="${fn:split(gamesPageRightArray, ',')}">
                        <c:import url="/WEB-INF/pages/inc/${gamesPageRightArray}.jsp" />
                    </c:forEach>
                </c:if>

            </div>

        </c:when>

    </c:choose>

</div>

