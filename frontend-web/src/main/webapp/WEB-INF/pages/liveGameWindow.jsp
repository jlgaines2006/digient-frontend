<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<head>
    <c:set var="path">
        <c:choose>
            <c:when test='${param.name == "BIAPreLive"}'>betting/sports</c:when>
            <c:when test='${fn:startsWith(param.name, "BIA")}'>betting/live</c:when>
            <c:otherwise>liveCasino</c:otherwise>
        </c:choose>
    </c:set>

    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/${path}.html">
        <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>
    </t:import>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/<c:if test='${not empty param.name}'>${path}</c:if>" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="gamePageWrapper">

    <div id="msg" class="alert alert-danger alert-dismissable" style="display: none"></div>

    <iframe id="gameWindow">
    </iframe>

</div>

<script type="text/javascript">

    $(document).ready(function() {

        // Betinaction
        var provider = getUrlParameter(window.href, 'provider') || gameProvider;
        if (provider === '28') {
            $('#gameWindow').attr('scrolling', 'no');
            $('#gameWindow').css('background-color', 'transparent');
            $('#gameWindowEmpty').css('background-color', 'transparent');
            if (window.addEventListener) window.addEventListener("message", listener, false);
            else window.attachEvent("onmessage", listener);
        }

        liveGameWindowOnLoad();
    });

</script>
