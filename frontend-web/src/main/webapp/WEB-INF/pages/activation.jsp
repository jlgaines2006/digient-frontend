<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="activationMessage" style="display: none"></div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

<script type="text/javascript">

    $(document).ready(function(){
        activatePlayer();
    });

</script>