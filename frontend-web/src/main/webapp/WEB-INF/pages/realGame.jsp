<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="gamePageWrapper">

    <div id="msg" class="alert alert-danger alert-dismissable" style="display: none"></div>

    <iframe id="gameWindow">
    </iframe>

    <div id="gamePageButtons">

        <c:choose>

            <c:when test="${features.gameWindowOpening == 'fullscreen'}">

                <select id="gamePageDropdown" class="form-control">
                </select>

                <a id="gamePageButton_rules" class="fancyboxPopup" href="#rulesPopup">
                    <spring:message code="game.button.rules" />
                </a>
                <a id="gamePageButton_customerService" href="#">
                    <spring:message code="game.button.service" />
                </a>
                <a id="gamePageButton_cashier">
                    <spring:message code="game.depositbutton"/>
                </a>
                <div id="gamePageButton_fullscreen">
                </div>

            </c:when>

            <c:otherwise>

                <jsp:include page="inc/one-click-deposit-widget.jsp"/>

                <a id="gamePageButton3" class="btn btn-primary btn-lg">
                    <spring:message code="game.depositbutton"/>
                </a>

                <jsp:include page="inc/funds-widget.jsp"/>

                <select id="gamePageDropdown" class="form-control">
                </select>

            </c:otherwise>

        </c:choose>

    </div>

</div>

<div id="rulesPopup" class="popup" style="display: none;">
</div>

<script type="text/javascript">

    $(document).ready(function() {

        currentLanguage = "${_lang == null ? features.defaultLanguage : _lang}";
        gameOnload();

        // Create link for deposit-button
        $.when(window["depositDetailFilledRequest"]()).done(function(map){

            if(!hasErrors(map)) {
                $("#gamePageButton3").attr("href","/${_lang == null ? features.defaultLanguage : _lang}/postlogin/payment/deposit");
            }
            else if (map.errorCode == 125) {
                $("#gamePageButton3").attr("href","/${_lang == null ? features.defaultLanguage : _lang}/postlogin/payment/prefill?deposit");
            }
            else {
                var error = getErrorMessage(map);
                showErrorObj($("#header_error"), error);
            }
        });

        // Open a new window when clicked deposit-button
        $("#gamePageButton3").click(function() {

            var NWin = window.open($(this).prop('href'), '', 'directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=1');

            if (window.focus) {
                NWin.focus();
            }

            return false;

        });

        // If we use fullscreen, add the support for receiving messages from gameroom iframe to change the ongoing game
        <c:if test="${features.gameWindowOpening == 'fullscreen'}">
            initFullScreenToggle();
        </c:if>

    });

    // Open Play2Go -real games when clicked "Play for real" -button
    var ShowCashier = function() {

        $("#gamePageButton3, #gamePageButton_cashier").trigger("click");
    }

</script>