<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="gamePageWrapper">

    <div id="msg" class="alert alert-danger alert-dismissable" style="display: none"></div>

    <div class="container">
        <div id="betting_div" style="height: 3400px;">
        </div>
    </div>

</div>

<script id="bcsportsbook"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $("#gamePage").css("background","black");

        var currencies = {'eur':'€', 'usd':'$', 'gbp':'£'};

        window.bettingCB = function bettingCB(name, value) {
            if (name == 'balance') {
                var cur = currencies[value.currency_name.toLowerCase()];
                $('#global-balance-input').val(cur + value.balance);
            }
            // Actions performed from the betslip
            if (value == 'betslip') {
                // Sign in btn
                if (name == 'login') {
                    openLoginRegisterPopup();
                }
                // Register btn
                if (name == 'register') {
                    openRegisterPopup();
                }
            }
        }

        var gameParams = null;
        if (userIsLogged) gameParams = getGameParams(220001, 'real')
        else gameParams = getGameParams(220001, 'demo');
        if (gameParams != null) $('#bcsportsbook').attr('src', gameParams.url);
    });

</script>