<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form id="autoFlushLimitForm" class="myAccForm form-horizontal" name="autoFlushLimitForm" action="">

    <fieldset>

        <legend><spring:message code="limits.autoFlush.header"/></legend>

        <p id="autoFlushLimitInfo" class="alert alert-warning" style="display: none;"></p>
        <p id="autoFlushLimitError" class="error alert alert-danger alert-dismissable" style="display: none;"></p>

        <ul id="autoFlushLimitFields" class="list-unstyled">

            <li id="autoFlushLimitPeriod" class="form-group has-feedback">
                <label class="control-label col-sm-3" for="autoFlushLimitPeriodSelector">
                    <spring:message code="limits.autoFlush.period"/>
                </label>
                <div class="col-sm-9">
                    <div class="select">
                        <select id="autoFlushLimitPeriodSelector" class="form-control" name="autoFlushLimitPeriod">
                            <option value="day"><spring:message code="limits.forDay"/></option>
                            <option value="week"><spring:message code="limits.forWeek"/></option>
                            <option value="month"><spring:message code="limits.forMonth"/></option>
                        </select>
                    </div>
                    <div class="form-control-feedback">
                        <i class="fa fa-question fa-lg" title="<spring:message code="limits.autoFlush.period.hint"/>"></i>
                    </div>
                </div>
            </li>

            <li id="autoFlushLimitButtons" class="form-buttons row">
                <div class="col-sm-offset-3 col-sm-9">
                    <button id="autoFlushLimitSubmit" class="submit btn btn-primary btn-lg" type="button">
                        <spring:message code="save"/>
                    </button>
                </div>
            </li>

        </ul>

    </fieldset>

</form>

<script>
    $(function () {
        $('#autoFlushLimitForm').submit(function (e) {
            e.preventDefault();
        });
        // Set limit
        $('#autoFlushLimitSubmit').click(function () {
            updateAutoFlushLimit($('#autoFlushLimitPeriodSelector').val());
        });
    });
</script>