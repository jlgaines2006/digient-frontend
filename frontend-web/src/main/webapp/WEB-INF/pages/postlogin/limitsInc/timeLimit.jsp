<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form id="timeLimitForm" class="myAccForm form-horizontal" action="">

    <fieldset>

        <legend><spring:message code="limits.timeLimit"/></legend>

        <p id="timeLimit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>
        <p id="limitSaved_timeLimit" class="alert alert-warning alert-dismissable" style="display: none;"><strong><span id="limitSavedInfo_timeLimit"></span></strong></p>

        <ul class="list-unstyled">

            <li id="limitNotSaved_timeLimit" class="form-group has-feedback" style="display: none;">
                <label class="control-label col-sm-3" for="limitTypeSelector">
                    <spring:message code="limits.limitPeriod"/>
                </label>
                <div class="col-sm-9">
                    <div id="limitTypeSelector" class="select">
                        <select id="periodSelector_timeLimit" class="form-control" name="type">
                            <!-- Dynamically populated in limitsOnLoad() -->
                        </select>
                    </div>
                    <div class="form-control-feedback">
                        <i alt="?" title="<spring:message code="limits.limitPeriod.hint"/>" class="fa fa-question fa-lg"></i>
                    </div>
                </div>
            </li>

            <li class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="submit saveButton saveTimeLimit btn btn-primary btn-lg" onclick="timeLimitSave();"><spring:message code="save"/></div>
                    <div class="submit saveButton removeTimeLimitBtn btn btn-default btn-lg" onclick="removeTimeLimit('timeLimit');"><spring:message code="limits.remove"/></div>
                </div>
            </li>

        </ul>

    </fieldset>

</form>