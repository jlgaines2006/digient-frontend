<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="limitTotalAmountForm" class="myAccForm form-horizontal" action="">

    <fieldset id="accountDetails">

        <legend><spring:message code="limits.limitTotalAmountDeposit"/></legend>

        <p id="totalDepositLimit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>
        <p id="decreaseExplanation_deposit" class="alert alert-info alert-dismissable" style="display: none;"><spring:message code="limits.decreaseExplanation"/></p>
        <p id="limitSaved_deposit" class="alert alert-warning alert-dismissable" style="display: none;"><strong><span id="limitSavedInfo_deposit"></span></strong></p>

        <ul class="list-unstyled">

            <li class="form-group has-feedback">
                <label class="control-label col-sm-3" for="newAmount_deposit">
                    <span id="limitAmount_deposit" style="display: none;"><spring:message code="limits.amount"/> (<span class="limitCurrencySymbol">${loginUser.registrationCurrency}</span>)</span>
                    <span id="newLimitAmount_deposit" style="display: none;"><spring:message code="limits.new.amount"/> (<span class="limitCurrencySymbol">${loginUser.registrationCurrency}</span>)</span>
                </label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <input id="newAmount_deposit" class="form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'number');">
                        <span id="tdl_limit_amount_doublezeros" class="doublezeros input-group-addon">.00</span>
                    </div>
                    <div class="form-control-feedback">
                        <i alt="?" id="amount_question" title="<spring:message code="limits.amount.hint"/>" class="fa fa-question fa-lg"></i>
                        <span id="tdl_limit_amount_validOK"></span>
                    </div>
                    <span id="newAmount_deposit_error" class="limitsFieldError help-block error-field"></span>

                    <input type="hidden" id="savedLimit_deposit" value=""/>
                    <input type="hidden" id="minLimit_deposit" value=""/>
                    <input type="hidden" id="maxLimit_deposit" value=""/>
                    <input type="hidden" id="period_deposit" value=""/>
                </div>
            </li>

            <li id="limitNotSaved_deposit" class="form-group has-feedback" style="display: none;">
                <label class="control-label col-sm-3" for="limitTypeSelector">
                    <spring:message code="limits.limitPeriod"/>
                </label>
                <div class="col-sm-9">
                    <div id="limitTypeSelector" class="select">
                        <select id="periodSelector_deposit" class="form-control" name="type">
                            <option value="day"><spring:message code="limits.perDay"/></option>
                            <option value="week"><spring:message code="limits.perWeek"/></option>
                            <option value="month"><spring:message code="limits.perMonth"/></option>
                        </select>
                    </div>
                    <div class="form-control-feedback">
                        <i alt="?" title="<spring:message code="limits.limitPeriod.hint"/>" class="fa fa-question fa-lg"></i>
                    </div>
                </div>
            </li>

            <li id="renew_checkbox" class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="checkbox">
                        <label for="autoRenew_deposit">
                            <input id="autoRenew_deposit" type="checkbox" value="" ${isChecked} />
                            <spring:message code="limits.renew"/>
                        </label>
                    </div>
                </div>
            </li>

            <li class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="limits_saveButton1" class="submit btn btn-primary btn-lg" onclick="totalLimitsSave()"><spring:message code="save"/></div>
                </div>
            </li>

        </ul>

    </fieldset>

</form>