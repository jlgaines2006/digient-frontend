<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="timeReachedConfirm">

    <div id="timeReachedConfirm_content">

        <h4 class="h1"><spring:message code="timeReached.title"/></h4>
        <p><spring:message code="timeReached.explanation"/></p>

        <div class="confirm btn btn-primary btn-lg" title="" onclick="javascript: timeReachedContinue('Y');" ><spring:message code="timeReached.continue"/></div>
        <div class="confirm btn btn-default btn-lg" title="" onclick="javascript: timeReachedContinue('N');" ><spring:message code="timeReached.logOut"/></div>

    </div>

</div>