<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="excludeMeGamesForm" class="myAccForm form-horizontal" action="" name="excludeMeGamesForm">
    <fieldset>

        <legend><spring:message code="limits.excludeRealMoney"/></legend>

        <p id="realMoneyLimit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>
        <div id="limitSaved_exclude" class="alert alert-warning alert-dismissable" style="display: none;">
            <p><strong><spring:message code="limits.real.description"/></strong></p>
            <p><spring:message code="limits.real.endDate"/>:&nbsp;<strong><span id="excludeLimit_true_endDate"></span></strong></p>
        </div>

        <ul id="limitNotSaved_exclude" class="list-unstyled" style="display: none;">

            <li class="form-group has-feedback has-datepicker">
                <label class="control-label col-sm-3" for="rml-endDate">
                    <spring:message code="limits.real.endDate"/>
                    <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                </label>
                <div class="col-sm-9">
                    <div class="form-control-datepicker">
                        <label for="rml-endDate">
                            <i class="fa fa-calendar fa-lg"></i>
                        </label>
                        <input id="rml-endDate" class="form-control" type="text" name="rml-endDate" value="" maxlength="256">
                        <div class="form-control-feedback">
                            <span id="rml-endDate_validOK"></span>
                        </div>
                    </div>
                    <span id="rml-endDate_error" class="limitsFieldError help-block error-field"></span>
                </div>
            </li>

            <li class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="limits_saveButton3" class="submit btn btn-primary btn-lg" onclick="limits_realMoney()" style="display: none;"><spring:message code="save"/></div>
                </div>
            </li>

        </ul>

    </fieldset>
</form>