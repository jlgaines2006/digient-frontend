<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="limitTotalWagerAmountForm" class="myAccForm form-horizontal" action="" name="limitTotalWagerAmountForm">

    <fieldset id="wagerAccountDetails">

        <legend><spring:message code="limits.wager.limitTotalAmount"/></legend>

        <p id="totalWagerLimit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>
        <p id="decreaseExplanation_wager" class="alert alert-info alert-dismissable" style="display: none;"><spring:message code="limits.wager.decreaseExplanation"/></p>
        <p id="limitSaved_wager" class="alert alert-warning alert-dismissable" style="display: none;"><strong><span id="limitSavedInfo_wager"></span></strong></p>

        <ul class="list-unstyled">

            <li class="form-group has-feedback">
                <label class="control-label col-sm-3" for="newAmount_wager">
                    <span id="limitAmount_wager" style="display: none;"><spring:message code="limits.wager.amount"/> (<span class="limitCurrencySymbol">${loginUser.registrationCurrency}</span>)</span>
                    <span id="newLimitAmount_wager" style="display: none;"><spring:message code="limits.wager.new.amount"/> (<span class="limitCurrencySymbol">${loginUser.registrationCurrency}</span>)</span>
                </label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <input id="newAmount_wager" class="form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'number');">
                        <span id="limit_wager_amount_doublezeros" class="doublezeros input-group-addon">.00</span>
                    </div>
                    <div class="form-control-feedback">
                        <i alt="?" id="amount_question" title="<spring:message code="limits.wager.amount.hint"/>" class="fa fa-question fa-lg"></i>
                        <span id="limit_wager_amount_validOK"></span>
                    </div>
                    <span id="newAmount_wager_error" class="limitsFieldError help-block error-field"></span>

                    <input type="hidden" id="savedLimit_wager" value=""/>
                    <input type="hidden" id="minLimit_wager" value=""/>
                    <input type="hidden" id="maxLimit_wager" value=""/>
                    <input type="hidden" id="period_wager" value=""/>
                </div>
            </li>

            <li id="limitNotSaved_wager" class="form-group has-feedback" style="display: none;">
                <label class="control-label col-sm-3" for="limitTypeSelector">
                    <spring:message code="limits.wager.limitPeriod"/>
                </label>
                <div class="col-sm-9">
                    <div id="limitTypeSelector" class="select">
                        <select id="periodSelector_wager" class="form-control" name="type">
                            <option value="day"><spring:message code="limits.perDay"/></option>
                            <option value="week"><spring:message code="limits.perWeek"/></option>
                            <option value="month"><spring:message code="limits.perMonth"/></option>
                        </select>
                    </div>
                    <div class="form-control-feedback">
                        <i alt="?" title="<spring:message code="limits.wager.limitPeriod.hint"/>" class="fa fa-question fa-lg"></i>
                    </div>
                </div>
            </li>

            <li id="renew_checkbox" class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="checkbox">
                        <label for="autoRenew_wager">
                            <input id="autoRenew_wager" type="checkbox" value="" ${isChecked} />
                            <spring:message code="limits.renew"/>
                        </label>
                    </div>
                </div>
            </li>

            <li class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="limits_saveButton1" class="submit btn btn-primary btn-lg" onclick="totalWagerLimitsSave()"><spring:message code="save"/></div>
                </div>
            </li>

        </ul>

    </fieldset>

</form>