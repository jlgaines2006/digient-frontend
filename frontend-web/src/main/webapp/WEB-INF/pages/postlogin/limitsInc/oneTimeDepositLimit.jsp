<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="limitAmountPerDepositForm" class="myAccForm form-horizontal" action="" name="limitAmountPerDepositForm">

    <fieldset id="bank">

        <legend><spring:message code="limits.limitAmountPerDeposit"/></legend>

                <p id="oneTimeDepositLimit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>
                <div id="limitSaved_oneTime" class="alert alert-warning alert-dismissable" style="display: none;">
                    <p>
                        <spring:message code="limits.oneTime.amount"/>:&nbsp;(<span class="limitCurrencySymbol">${loginUser.registrationCurrency}</span>)
                        <strong><span id="oneTimeLimit_true_amount"></span></strong>
                    </p>
                    <p><spring:message code="limits.oneTime.endDate"/>:&nbsp;<strong><span id="oneTimeLimit_true_endDate"></span></strong></p>
                </div>

                <ul id="limitNotSaved_oneTime" class="list-unstyled" style="display: none;">

                    <li class="form-group has-feedback">
                        <label class="control-label col-sm-3" for="pdl_limit_amount">
                            <spring:message code="limits.oneTime.amount"/> (<span class="limitCurrencySymbol">${loginUser.registrationCurrency}</span>)
                        </label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input id="pdl_limit_amount" class="form-control" type="text" name="amount" value="" maxlength="256" onkeypress="validateInputs(event, 'number');">
                                <span id="pdl_limit_amount_doublezeros" class="doublezeros input-group-addon">.00</span>
                            </div>
                            <div class="form-control-feedback">
                                <i alt="?" title="<spring:message code="limits.oneTime.amount.hint"/>" class="fa fa-question fa-lg"></i>
                                <span id="pdl_limit_amount_validOK"></span>
                            </div>
                            <span id="pdl_limit_amount_error" class="limitsFieldError help-block error-field"></span>

                            <input type="hidden" id="minLimit_oneTime" value=""/>
                            <input type="hidden" id="maxLimit_oneTime" value=""/>
                        </div>
                    </li>

                    <li class="form-group has-feedback has-datepicker">
                        <label class="control-label col-sm-3" for="pdl-endDate">
                            <spring:message code="limits.oneTime.endDate"/> <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="form-control-datepicker">
                                <label for="pdl-endDate">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                                <input id="pdl-endDate" class="form-control" type="text" name="pdl-endDate" value="" maxlength="256">
                                <div class="form-control-feedback">
                                    <span id="pdl-endDate_validOK"></span>
                                </div>
                            </div>
                            <span id="pdl-endDate_error" class="limitsFieldError help-block error-field"></span>
                        </div>
                    </li>

                    <li class="row">
                        <div class="col-sm-offset-3 col-sm-9">
                            <div id="limits_saveButton2" class="submit btn btn-primary btn-lg" onclick="limits_oneTime()" style="display: none;"><spring:message code="save"/></div>
                        </div>
                    </li>

                </ul>

    </fieldset>

</form>