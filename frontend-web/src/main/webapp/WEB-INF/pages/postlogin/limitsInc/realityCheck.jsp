<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form class="myAccForm" action="" id="realityCheckForm">

    <fieldset>

        <legend><spring:message code="realityCheck.header"/></legend>

        <p id="gamingTimeLimit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>

        <ul class="list-unstyled">

            <li id="realitycheck_checkbox" class="checkbox">
                <label for="realityCheck">
                    <input type="checkbox" value="" id="realityCheck" ${isChecked} />
                    <spring:message code="realityCheck.agree"/>
                </label>
            </li>

        </ul>

        <div class="submit saveButton saveTimeLimit btn btn-primary btn-lg" onclick="realityCheckSave('gamingTimeLimit')"><spring:message code="save"/></div>

    </fieldset>

</form>