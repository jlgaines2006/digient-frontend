<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">
    limitsOnLoad();

    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");

    $(function () {
        var url = window.location.href.toString(),
            index = -1;
        if (url.indexOf('deposit') != -1) {
            index = $('#limits_deposit_tab').index();
        }
        else if (url.indexOf('withdrawal') != -1) {
            index = $('#limits_withdrawal_tab').index();
        }
        $('#tabs_limits').tabs({ active: (index != -1 ? index : 0) });
    });
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/limits" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="limitsWrapper">

                        <div id="myAccountView_top">

                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/transactions" id="transactionsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.transactions"/></a>
                            <a href="#" id="limitsButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.limits"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns" id="campaignsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.campaigns"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/myacc" id="profileButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.myaccount"/></a>

                            <h1 id="accountviewTitle"><spring:message code="limits.header"/></h1>

                        </div>

                        <div id="loadingContent" style="visibility: hidden;">

                            <div id="tabs_limits">

                                <ul>
                                    <li id="limits_loss_tab"><a href="#limits_loss"><spring:message code="limits.loss"/></a></li>
                                    <li id="limits_wager_tab"><a href="#limits_wager"><spring:message code="limits.wager"/></a></li>
                                    <li id="limits_time_tab"><a href="#limits_time"><spring:message code="limits.time"/></a></li>
                                    <li id="limits_gaming_tab"><a href="#limits_gaming"><spring:message code="limits.gaming"/></a></li>
                                    <li id="limits_deposit_tab"><a href="#limits_deposit"><spring:message code="limits.deposit"/></a></li>
                                    <li id="limits_withdrawal_tab"><a href="#limits_withdrawal"><spring:message code="limits.withdrawal"/></a></li>
                                </ul>

                                <!-- Loss Limit -->
                                <div id="limits_loss">
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/totalLossLimit.jsp" %>
                                </div>

                                <!-- Wager Limit -->
                                <div id="limits_wager">
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/totalWagerLimit.jsp" %>
                                </div>

                                <!-- Time Limit -->
                                <div id="limits_time">
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/timeLimit.jsp" %>
                                    <c:choose>
                                        <c:when test="${features.realityCheck == true}">
                                            <%@ include file="/WEB-INF/pages/postlogin/limitsInc/realityCheck.jsp" %>
                                        </c:when>
                                    </c:choose>
                                </div>

                                <!-- Gaming Limit -->
                                <div id="limits_gaming">
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/realMoneyLimit.jsp" %>
                                </div>

                                <!-- Deposit Limits -->
                                <div id="limits_deposit">
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/totalDepositLimit.jsp" %>
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/oneTimeDepositLimit.jsp" %>
                                </div>

                                <!-- Withdrawal Limits -->
                                <div id="limits_withdrawal">
                                    <%@ include file="/WEB-INF/pages/postlogin/limitsInc/autoFlushLimit.jsp" %>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>