<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/payment/withdrawal/withdrawal" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="depositWrapper">

                        <div id="depositView_top">

                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/payment/transactions" id="transactionsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit.transactions"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/limits" id="limitsButton" class="limitsButtonWithdrawal accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.withdrawal.limits"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/withdrawal/prefillWithdrawal', '/postlogin/payment/withdrawal/withdrawal', 'withdrawal');" id="withdrawalButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.withdrawal"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');" id="depositPageButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit"/></a>

                            <h1 id="depositviewTitle"><spring:message code="withdrawal.header"/></h1>

                        </div>

                        <p id="withdrawal_info" class="informationSaved alert alert-success alert-dismissable" style="display: none"></p>
                        <p id="withdrawal_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

                        <div id="loadingContent" style="visibility: hidden;">

                            <form id="withdrawalForm" class="withdrawalForm form-horizontal" action="" name="withdrawalForm" style="display: none;">

                            <fieldset>

                                <div class="depositDesc">
                                    <h4 class="h3"><spring:message code="withdrawal.help"/></h4>
                                </div>

                                <div id="withdrawalInfoWrapper" class="alert alert-info alert-dismissable">
                                    <p><spring:message code="withdrawal.note.header"/></p>
                                    <ul id="withdrawalNote">
                                        <li><spring:message code="withdrawal.note2"/></li>
                                        <li><spring:message code="withdrawal.note3"/></li>
                                    </ul>
                                </div>

                                <ul id="withdrawalList" class="list-unstyled">

                                    <li class="form-group">
                                        <label class="control-label col-sm-3">
                                            <spring:message code="withdrawal.bonus"/>
                                        </label>
                                        <div class="col-sm-3">
                                            <span id="withdrawalBonus" class="form-control"></span>
                                            <span id="revokeBonus" class="withdrawalNote depositFieldError help-block error-field" style="display: none;"><spring:message code="withdrawal.revokeBonus"/></span>
                                        </div>
                                        <label class="control-label col-sm-3">
                                            <spring:message code="withdrawal.remainingTurnover"/>
                                        </label>
                                        <div class="col-sm-3">
                                            <span id="withdrawalRemainingTurnover" class="form-control"></span>
                                        </div>
                                    </li>

                                    <li class="form-group">
                                        <label class="control-label col-sm-3">
                                            <spring:message code="withdrawal.available"/>
                                        </label>
                                        <div class="col-sm-9">
                                            <span id="withdrawalCash" class="form-control"></span>
                                        </div>
                                    </li>

                                    <hr class="hrWithdraw">

                                    <!--<li class="form-group">
                                        <label class="control-label col-sm-3">
                                            <spring:message code="withdrawal.remainingTurnover"/>
                                        </label>
                                        <div class="col-sm-9">
                                            <span id="withdrawalRemainingTurnover" class="form-control"></span>
                                        </div>
                                    </li>-->

                                    <li class="form-group has-feedback">
                                        <label class="control-label col-sm-3" for="withdrawalMethod">
                                            <spring:message code="withdrawal.method.type"/>
                                        </label>
                                        <div class="col-sm-9">
                                            <select id="withdrawalMethod" class="selector form-control" name="withdrawalMethod" onchange="selectWithdrawalMethod(this.options[this.selectedIndex].value)">
                                            </select>
                                            <div class="form-control-feedback">
                                                <i alt="?" title="<spring:message code="withdrawal.type.hint"/>" class="fa fa-question fa-lg"></i>
                                            </div>
                                        </div>
                                    </li>

                                    <li id="storedPayments" class="form-group" style="display: none;">
                                        <label class="control-label col-sm-3" for="storedPayments-select">
                                            <spring:message code="storedPayments.header"/>
                                        </label>
                                        <div class="col-sm-9">
                                            <select id="storedPayments-select" class="form-control"></select>
                                        </div>
                                    </li>

                                    <li id="withdrawalAmountWrapper" class="form-group has-feedback">
                                        <label class="control-label col-sm-3" for="withdrawalAmountNumber">
                                            <span id="minWithdrawalValue"></span>
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <input id="withdrawalAmountNumber" class="form-control" type="text" maxlength="256" onkeypress="validateInputs(event, 'number')" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <span id="withdrawalAmountDecimalDotText">.</span>
                                                    <input id="withdrawalAmountDecimal" class="form-control" type="text" value="00" maxlength="2" onkeypress="validateInputs(event, 'number')" />
                                                </div>
                                            </div>
                                            <div class="form-control-feedback">
                                                <i alt="?" title="<spring:message code="withdrawal.amount.hint"/>" class="fa fa-question fa-lg"></i>
                                                <span id="withdrawalAmount_validOK"></span>
                                            </div>
                                            <span id="withdrawalAmount_error" class="depositFieldError help-block error-field"></span>
                                        </div>
                                    </li>

                                    <input type="hidden" name="withdrawalAmount" id="withdrawalAmount" />
                                    <input type="hidden" id="withdrawalData" value="">

                                    <li id="li_withdrawal_savebutton" class="row">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div id="withdrawal_savebutton" class="btn btn-primary btn-lg" onclick="withdraw();"><spring:message code="withdrawal.withdraw"/></div>
                                            <input id='saveButtonClicked' type="checkbox" style="display: none">
                                        </div>
                                    </li>
                                </ul>

                            </fieldset>

                        </form>

                            <c:import url="withdrawalDetails.jsp" />
                            <c:import url="withdrawalConfirm.jsp" />
                            <div id="bankInfo" class="popup" style="display: none;">
                                <c:import url="../../myaccInc/updateMyBankDetails.jsp" />
                            </div>
                            <div id="withdrawalPasswordConfirm" class="popup" style="display: none;">
                                <c:import url="../../myaccInc/passwordConfirm.jsp" />
                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

<script type="text/javascript">
    withdrawalOnLoad();
</script>