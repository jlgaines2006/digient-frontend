<%--
 Confirmation before the withdrawal
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="withdrawalConfirm" class="popup" style="display: none;">

    <div id="withdrawalConfirm_content">

        <h4 id="withdrawalConfirm_header" class="h1"><spring:message code="withdrawalConfirmation.header" /></h4>
        <div id="withdrawalNote1" class="alert alert-info alert-dismissable" style="display: none;"></div>
        <div id="withdrawalError" class="infoError alert alert-danger alert-dismissable" style="display: none;"></div>

        <div class="confirmationDiv panel panel-primary">
            <table class="confirmationTable table">
                <tr>
                    <td><spring:message code="withdrawalConfirmation.withdraw" /></td>
                    <td><span id="withdrawalConfirmationWithdrawal"></span></td>
                </tr>
                <tr>
                    <td><spring:message code="withdrawalConfirmation.fee" /></td>
                    <td><span id="withdrawalConfirmationFee"></span></td>
                </tr>
                <tr class="totalAmount">
                    <td id="withdrawalNet"><spring:message code="withdrawalConfirmation.total" /></td>
                    <td id="withdrawalDeducted" style="display: none;"><spring:message code="withdrawalConfirmation.totalDeducted" /></td>
                    <td><span id="withdrawalConfirmationTotal"></span></td>
                </tr>
            </table>
        </div>

        <div id="withdrawalConfirmContinue" class="confirm btn btn-primary btn-lg" title="" onclick="confirmWithdrawal();" ><spring:message code="deposit3.confirm"/></div>
        <div id="withdrawalConfirmCancel" class="confirm btn btn-default btn-lg" title="" onclick="$.fancybox.close();" ><spring:message code="deposit3.back"/></div>
        <input type="checkbox" id='withdrawalConfirmButtonClicked' style="display: none;">

    </div>

</div>