<%--
 Confirmation before the deposit if it has a fee
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="depositTable" class="popup" style="display: none;">

    <div id="depositTable_content">

        <h4 id="depositTable_header" class="h1"><spring:message code="depositTable.header" /></h4>
        <div id="depositTableNote" class="alert alert-info alert-dismissable" style="display: none;"></div>
        <div id="depositError" class="infoError alert alert-danger alert-dismissable" style="display: none;"></div>

        <div class="confirmationDiv panel panel-primary">
            <table class="confirmationTable table">
                <tr>
                    <td><spring:message code="depositTable.deposit" /></td>
                    <td><span id="depositTableDeposit"></span></td>
                </tr>
                <tr>
                    <td><spring:message code="depositTable.fee" /></td>
                    <td><span id="depositTableFee"></span></td>
                </tr>
                <tr class="totalAmount">
                    <td id="totalToAccount"><spring:message code="depositTable.total" /></td>
                    <td id="totalDeducted" style="display: none;"><spring:message code="depositTable.totalDeducted" /></td>
                    <td><span id="depositTableNet"></span></td>
                </tr>
            </table>
        </div>

        <div id="depositTableContinue" class="confirm btn btn-primary btn-lg" title="" onclick="confirmDepositTable();" ><spring:message code="deposit3.confirm"/></div>
        <div id="depositTableCancel" class="confirm btn btn-default btn-lg" title="" onclick="$.fancybox.close();" ><spring:message code="deposit3.back"/></div>
        <input type="checkbox" id='depositTableConfirmButtonClicked' style="display: none;">

    </div>

</div>