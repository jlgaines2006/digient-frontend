<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<script type="text/javascript">

    $(document).ready(function() {

        var url = window.location.href.toString();
        if (url.indexOf('withdrawal') == -1) {
            $( "#tabs" ).tabs({ active: ${features.betinactionBetHistory ? 3 : 2} });
            getTransactionsData("deposit");
        }
        else {
            $( "#tabs" ).tabs({ active: ${features.betinactionBetHistory ? 4 : 3} });
            getTransactionsData('withdrawal');

            $("#status_withdrawal").val('pending');
            $("#status_withdrawal").change();
        }
    });

</script>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="transactionsWrapper">

                        <div class="depositTransactionsTopBar" id="myAccountView_top">

                            <a href="#" id="transactionsButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.deposit.transactions"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/withdrawal/prefillWithdrawal', '/postlogin/payment/withdrawal/withdrawal', 'withdrawal');" id="withdrawalButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.withdrawal"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');" id="depositPageButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit"/></a>

                            <h1 id="accountviewTitle"><spring:message code="deposit.transaction.header"/></h1>

                        </div>

                        <c:import url="../transactions/allTransactions.jsp" />

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>
