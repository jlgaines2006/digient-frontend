<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="depositWrapper">

                        <div id="depositView_top">

                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/payment/transactions" id="transactionsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit.transactions"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/limits?deposit" id="limitsButton" class="limitsButtonDeposit accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit.limits"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/withdrawal/prefillWithdrawal', '/postlogin/payment/withdrawal/withdrawal', 'withdrawal');" id="withdrawalButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.withdrawal"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');" id="depositPageButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit"/></a>

                            <span id="depositHeader" style="display: none;">
                                <h1 id="depositviewTitle"><spring:message code="deposit.header"/></h1>
                            </span>

                        </div>

                        <div id="loadingContent" style="visibility: hidden;">

                            <form id="depositForm" class="depositForm form-horizontal" action="">

                                <fieldset>

                                    <div class="depositDesc alert alert-warning alert-dismissable">
                                        <p><strong><span id="helpMessage"></span></strong></p>
                                        <p><spring:message code="deposit.info"/></p>
                                    </div>

                                    <p id="deposit_error" class="error alert alert-danger alert-dismissable" style="display: none;"></p>

                                    <ul class="list-unstyled">

                                        <%@ include file="/WEB-INF/pages/postlogin/userFields/firstName.jsp" %>
                                        <%@ include file="/WEB-INF/pages/postlogin/userFields/lastName.jsp" %>
                                        <%@ include file="/WEB-INF/pages/postlogin/userFields/address.jsp" %>
                                        <%@ include file="/WEB-INF/pages/postlogin/userFields/city.jsp" %>
                                        <%@ include file="/WEB-INF/pages/postlogin/userFields/zipCode.jsp" %>
                                        <%@ include file="/WEB-INF/pages/postlogin/userFields/phoneNumber.jsp" %>

                                        <li class="row">

                                            <div class="col-sm-offset-3 col-sm-9">
                                                <div id="deposit_savebutton1" class="btn btn-primary btn-lg" onclick="beforeLoadProviders();"><spring:message code="deposit.confirm"/></div>
                                            </div>

                                        </li>

                                    </ul>

                                </fieldset>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

<script type="text/javascript">
    prefillOnLoad();
</script>