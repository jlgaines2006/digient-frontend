<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="withdrawalDetails" class="popup" style="display: none;">

    <div id="withdrawalDetails_content">

    <h4 id="withdrawalDetails_confirm" class="h1"><spring:message code="withdrawalDetails.header" /></h4>

        <form class="depositForm" action="" id="withdrawalDetailsForm">

            <fieldset class="withdrawalDetailsFieldset">

                <p><spring:message code="withdrawalDetails.additionalInfoDescription" /></p>

                <ul id="additionalInfo_withdrawalDetails" class="list-unstyled">

                    <li id="email_item">

                        <label id="email_label" for="email"><spring:message code="email"/></label>
                        <input id="email" class="form-control" type="text" name="email" value="" maxlength="256">
                        <span id="email_error" class="depositFieldError help-block error-field"></span>

                    </li>

                </ul>

            </fieldset>

        </form>

        <div class="secureWithdrawal">
            <p><img id="serurityLock" src="/resources/images/icons/secure.png" /><spring:message code="withdrawalDetails.securityInfo"/></p>
        </div>

        <div id="withdrawalDetailsContinue" class="confirm btn btn-primary btn-lg" title="" onclick="confirmWithdrawalDetails();" ><spring:message code="deposit3.confirm"/></div>
        <div id="withdrawalDetailsCancel" class="confirm btn btn-default btn-lg" title="" onclick="$.fancybox.close();" ><spring:message code="deposit3.back"/></div>
        <input type="checkbox" id='confirmButtonClicked' style="display: none;">

    </div>

</div>