<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="depositConfirmation" class="popup" style="display: none;">

    <div id="depositConfirmation_content">

        <h4 id="depositviewTitle_confirm" class="h1"><spring:message code="deposit3.depositConfirmationHeader" /></h4>

            <form id="depositForm" class="depositForm" action="" style="display: none;" onsubmit="javascript: event.preventDefault(); validateBankDetails(this.id);">

            <fieldset>

                    <p id="additionalInfoDescription"><spring:message code="deposit3.additionalInfoDescription" /></p>
                    <p><spring:message code="deposit3.requiredField"/></p>

                    <ul id="additionalInfo_depositConfirmation" class="list-unstyled">
                        <%-- Dynamically populated in deposit_savebutton2();--%>
                    </ul>

                    <div id="cvc" style="display: none;">
                        <img src="${cmsRoot}/images/icons/bank-page/cvc.png" />
                    </div>

                    <div id="providerRegister" class="paymentRegister" style="display: none !important;">
                        <p>
                            <span id="registerForProviderStartText"></span>
                            <a id="providerRegisterLink" href="" target="_blank"><spring:message code="registerForProviderTextLink"/></a>
                            <spring:message code="registerForProviderEndText"/>
                        </p>
                    </div>

                    <div id="depositDescriptor"></div>

            </fieldset>

        </form>

        <div class="depositDesc">
            <p><spring:message code='deposit3.help' /></p>
        </div>

        <div id="securePayment" class="securePayment" style="display: none;">
            <p><img id="serurityLock" src="/resources/images/icons/secure.png" /><spring:message code="deposit3.securityInfo"/></p>
        </div>

        <div id="depositConfirmationContinue" class="confirm btn btn-primary btn-lg" title="" onclick="afterDepositConfirmation()" ><spring:message code="deposit3.confirm"/></div>
        <div id="depositConfirmationCancel" class="confirm btn btn-default btn-lg" title="" onclick="$.fancybox.close();" ><spring:message code="deposit3.back"/></div>
        <input type="checkbox" id='confirmButtonClicked' style="display: none;">
    </div>

</div>