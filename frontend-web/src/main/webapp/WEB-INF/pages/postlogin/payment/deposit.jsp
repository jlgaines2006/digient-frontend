<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">
    if (window.top != window.self) {
        window.top.location.href = window.self.location.href;
    }
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/payment/deposit" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_onepart" class="col-md-12">

                <div id="content">

                    <div id="depositWrapper">

                        <div id="depositView_top">

                            <a href="javascript: redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/payment/transactions');" id="transactionsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit.transactions"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/limits?deposit" id="limitsButton" class="limitsButtonDeposit accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.deposit.limits"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/withdrawal/prefillWithdrawal', '/postlogin/payment/withdrawal/withdrawal', 'withdrawal');" id="withdrawalButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.withdrawal"/></a>
                            <a href="javascript: payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');" id="depositPageButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.deposit"/></a>

                            <h1 id="depositviewTitle"><spring:message code="deposit.header"/></h1>

                        </div>

                        <div id="loadingContent" style="visibility: hidden;">

                            <p id="mainDeposit_info" class="informationSaved alert alert-success alert-dismissable" style="display: none"></p>
                            <p id="mainDeposit_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>
                            <p id="pendingWithdrawals_info" class="info alert alert-info alert-dismissable" style="display: none">
                                <spring:message code="infoPendingWithdrawals"/>
                                <a href="javascript: loadPendingWithdrawTrans()" class="link alert-link"><spring:message code="cancelWithdrawal"/></a>
                            </p>

                            <div id="deposit2_forms" style="display: none;">

                                <div class="row">

                                    <div id="deposit2_paymentForm_wrapper" class="col-sm-5">

                                        <form id="deposit2_paymentForm" class="deposit2_paymentForm" name="deposit2_paymentForm" action="" method="" target="" >

                                            <div class="depositDesc depositDesc2">
                                                <h4 class="h3"><spring:message code="deposit2.header"/></h4>
                                            </div>

                                            <ul id="deposit2_providers" class="list-unstyled">
                                                <!-- Dynamically populated in depositOnLoad() -->
                                            </ul>

                                        </form>

                                    </div>

                                    <div id="deposit2_amountForm_wrapper" class="col-sm-7">

                                        <form id="deposit2_amountForm" class="deposit2_amountForm" action="">

                                            <div class="depositDesc depositDesc2">
                                                <h4 id="amountHeader" class="h3"><spring:message code="deposit2.amount.header"/></h4>
                                                <h4 id="promoHeader" class="h3" style="display: none;"><spring:message code="deposit2.promo.header"/></h4>
                                            </div>

                                            <div id="depositLimitMessage" class="alert alert-warning alert-dismissable" style="display: none;"></div>
                                            <div id="limitedMethodMessage" class="alert alert-warning alert-dismissable" style="display: none"></div>
                                            <div id="paymentSum_errorWrapper" class="alert alert-danger alert-dismissable" style="display: none;">
                                                <span id="paymentSum_error" class="limitsFieldError"></span>
                                            </div>
                                            <div id="depositCampaignHasNotBeenSelected" class="alert alert-danger alert-dismissable" style="display: none;"></div>

                                            <div id="supportedCurrencies"  class="form-group has-feedback hidden">
                                                <label class="control-label" for="supportedCurrencies-select"><spring:message code="deposit.form.currency"/></label>
                                                <select id="supportedCurrencies-select" class="form-control" name="currency"></select>

                                                <div class="form-control-feedback">
                                                    <i id="supportedCurrencies-hint" alt="?" title="<spring:message code='deposit.hint.currency'/>" class="fa fa-question fa-lg control-hint"></i>
                                                </div>
                                            </div>

                                            <ul id="amountOptions" class="list-unstyled">

                                                <li class="radio">
                                                    <label>
                                                        <input id="option0" type="radio" name="paymentSum" value="" /><span id="paymentSum_option0"></span>
                                                    </label>
                                                </li>

                                                <li class="radio">
                                                    <label>
                                                        <input id="option1" type="radio" name="paymentSum" value="" /><span id="paymentSum_option1"></span>
                                                    </label>
                                                </li>

                                                <li class="radio">
                                                    <label>
                                                        <input id="option2" type="radio" name="paymentSum" value="" /><span id="paymentSum_option2"></span>
                                                    </label>
                                                </li>

                                                <li class="radio">
                                                    <label>
                                                        <input id="option3" type="radio" name="paymentSum" value="" /><span id="paymentSum_option3"></span>
                                                    </label>
                                                </li>

                                                <li id="deposit2_amountGroupWrapper">
                                                    <label class="radio-inline">
                                                        <input id="deposit2_amountRadioButton"  type="radio" name="paymentSum" value="0" /><span id="currency_for_input"></span>
                                                    </label>
                                                    <div id="deposit2_amountGroup" class="form-group inline-block-xxs">
                                                        <input id="deposit2_giveAmount" class="form-control" type="text" name="amount" value="" onkeypress="validateInputs(event, 'number');"/>
                                                        <span id="depositAmountDecimalDotText">.</span>
                                                        <input id="deposit2_doublezeros" class="doublezeros form-control" type="text" value="00" onkeypress="validateInputs(event, 'number');" maxlength="2"/>
                                                        <div id="amount_range" class="help-block error-field"></div>
                                                        <div id="amount_exchange" class="exchange hidden"></div>
                                                    </div>
                                                </li>

                                            </ul>

                                            <div id="storedPayments" style="display: none;">
                                                <h4 class="h3"><spring:message code="storedPayments.header"/></h4>
                                                <select id="storedPayments-select" class="form-control"></select>
                                            </div>

                                            <div id="deposit2_promoFields" class="row">
                                                <div id="deposit2_campaignsFieldContainer" class="col-sm-7 hidden">
                                                    <div class="form-group has-feedback">
                                                        <label class="control-label" for="deposit2_campaigns"><spring:message code="deposit.form.campaigns"/></label>
                                                        <select id="deposit2_campaigns" class="form-control" name="campaignId"></select>

                                                        <div class="form-control-feedback">
                                                            <i id="deposit2_campaigns_hint" alt="?" title="<spring:message code='deposit.hint.campaigns'/>" class="fa fa-question fa-lg control-hint"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="deposit2_promoFieldContainer" class="col-sm-5 hidden">
                                                    <div class="form-group">
                                                        <label id="deposit2_promoDesc" for="deposit2_promocode">
                                                            <spring:message code="deposit2.promo"/>
                                                        </label>
                                                        <input id="deposit2_promocode" class="form-control" type="text" name="registrationPromoCode" value=""/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="deposit_savebutton2" class="btn btn-primary btn-lg" onclick="firstDepositConfirm();"><spring:message code="deposit2.confirm"/></div>

                                        </form>

                                        <div id="offline-payment-info">
                                            <div class="depositDesc depositDesc2">
                                                <h4 class="h3"><spring:message code="offline.bank.info.header"/></h4>
                                                <div><spring:message code="offline.bank.info.description"/></div>
                                            </div>
                                            <div class="alert alert-info">
                                                <span id="offline-bank-info-span"><spring:message code="offline.bank.info"/></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <c:import url="depositConfirmation.jsp" />
                                <c:import url="depositTable.jsp" />
                                <c:import url="../myaccInc/passwordConfirm.jsp" />

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>

<script type="text/javascript">
    depositOnLoad();
</script>
