<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">
    campaignsOnLoad();

    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/campaigns" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="myAccountWrapper">

                        <div id="myAccountView_top">

                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/transactions" id="transactionsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.transactions"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/limits" id="limitsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.limits"/></a>
                            <a href="#" id="campaignsButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.campaigns"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/myacc" id="profileButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.myaccount"/></a>

                            <h1 id="accountviewTitle"><spring:message code="bonuses.header"/></h1>

                        </div>

                        <div id="loadingContent" style="visibility: hidden;">

                            <c:set var="pageType" value="myaccount" scope="request"/>

                            <div id="tabs">

                                <ul>
                                    <li id="loyalty_details_tab"><a href="#loyalty_details"><spring:message code="loyalty.header"/></a></li>
                                    <li id="profile_bonuses_tab"><a href="#profile_bonuses"><spring:message code="myprofile.bonuses"/></a></li>
                                    <li id="profile_promotions_tab"><a href="#profile_promotions"><spring:message code="myprofile.promotions"/></a></li>
                                </ul>

                                <!-- Loyalty Details -->
                                <div id="loyalty_details">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/userLoyalty.jsp" %>
                                </div>

                                <!-- Bonuses Info -->
                                <div id="profile_bonuses">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/bonusesInfo.jsp" %>
                                </div>

                                <!-- Campaigns Info -->
                                <div id="profile_promotions">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/promotionsInfo.jsp" %>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>
