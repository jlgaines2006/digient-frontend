<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">
    myaccOnLoad();

    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");
</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/myacc" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="myAccountWrapper">

                        <div id="myAccountView_top">

                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/transactions" id="transactionsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.transactions"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/limits" id="limitsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.limits"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns" id="campaignsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.campaigns"/></a>
                            <a href="#" id="profileButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.myaccount"/></a>

                            <h1 id="accountviewTitle"><spring:message code="myaccount.header"/></h1>

                        </div>

                        <div id="loadingContent" style="visibility: hidden;">

                            <c:set var="pageType" value="myaccount" scope="request"/>

                            <div id="tabs">

                                <ul>
                                    <li id="profile_details_tab"><a href="#profile_details"><spring:message code="myprofile.profile"/></a></li>
                                    <li id="profile_account_tab"><a href="#profile_account"><spring:message code="myprofile.bankAccount"/></a></li>
                                    <li id="profile_password_tab"><a href="#profile_password"><spring:message code="password"/></a></li>
                                    <li id="profile_documents_tab"><a href="#profile_documents"><spring:message code="myprofile.documents"/></a></li>
                                    <c:if test="${features.showRTPInfo}">
                                        <li id="profile_rtp_tab"><a href="#profile_rtp"><spring:message code="myprofile.rtp"/></a></li>
                                    </c:if>
                                    <li id="profile_login_history_tab"><a href="#profile_login_history"><spring:message code="myprofile.login.history"/></a></li>
                                </ul>

                                <!-- Profile Details -->
                                <div id="profile_details">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/updateMyAccount.jsp" %>
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/closeAccount.jsp" %>
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/closeAccountConfirm.jsp" %>
                                </div>

                                <!-- Bank Account Details -->
                                <div id="profile_account">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/updateMyBankDetails.jsp" %>
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/passwordConfirm.jsp" %>
                                </div>

                                <!-- Password Details -->
                                <div id="profile_password">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/updateMyPassword.jsp" %>
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/birthdayConfirm.jsp" %>
                                </div>

                                <!-- Documents Details -->
                                <div id="profile_documents">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/uploadDocuments.jsp" %>
                                </div>

                                <!-- RTP Info -->
                                <c:if test="${features.showRTPInfo}">
                                    <div id="profile_rtp">
                                        <%@ include file="/WEB-INF/pages/postlogin/myaccInc/rtpInfo.jsp" %>
                                    </div>
                                </c:if>

                                <!-- User Login History Info -->
                                <div id="profile_login_history">
                                    <%@ include file="/WEB-INF/pages/postlogin/myaccInc/loginHistory.jsp" %>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>
