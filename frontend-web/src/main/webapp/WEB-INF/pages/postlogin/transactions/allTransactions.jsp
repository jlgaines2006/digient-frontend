<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">

    $(document).ready(function() {

        // Set up calendar selectors for all the tabs transactions-page
        var tabs = ["game", "promo", "deposit", "withdrawal", "commission"];

        for (var i = 0; i < tabs.length; i++) {

            if($('#transactions_' + tabs[i] + '_from').length > 0) {
                $("#transactions_" + tabs[i] + "_from").datepicker({ "dateFormat": "dd-mm-yy" });
            }
            if($('#transactions_' + tabs[i] + '_to').length > 0) {
                $("#transactions_" + tabs[i] + "_to").datepicker({ "dateFormat": "dd-mm-yy" });
            }

            // Set default value of "game" period selector to be the current day
            if($("#transactions_game_from").length > 0 && $("#transactions_game_to").length > 0) {

                $("#transactions_game_from").datepicker("setDate", new Date());
                $("#transactions_game_to").datepicker("setDate", new Date());
            }
        }
    });

</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/payment/transactions" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="loadingContent" style="visibility: hidden;">

    <div id="tabs">

        <ul>

            <li id="transactions_game_tab"><a href="#transactions_game" onclick="getTransactionsData('game')"><spring:message code="transactionTab.game"/></a></li>
            <c:if test="${features.betinactionBetHistory}">
                <li id="transactions_betting_tab"><a href="#transactions_betting" onclick="getBettingTransactions()"><spring:message code="transactionTab.betting"/></a></li>
            </c:if>
            <li id="transactions_promo_tab"><a href="#transactions_promo" onclick="getTransactionsData('promo')"><spring:message code="transactionTab.promo"/></a></li>
            <li id="transactions_deposit_tab"><a href="#transactions_deposit" onclick="getTransactionsData('deposit')"><spring:message code="transactionTab.deposit"/></a></li>
            <li id="transactions_withdrawal_tab"><a href="#transactions_withdrawal" onclick="getTransactionsData('withdrawal')"><spring:message code="transactionTab.withdrawal"/></a></li>
            <li id="transactions_loyalty_tab"><a href="#transactions_loyalty" onclick="getTransactionsData('loyalty')"><spring:message code="loyalty.header"/></a></li>
            <%--<li id="transactions_commission_tab"><a href="#transactions_commission" onclick="getTransactionsData('commission')"><spring:message code="transactionTab.commission"/></a></li>--%>

        </ul>

        <!-- Game transactions -->
        <div id="transactions_game" class="transactionsElement">

            <div id="transactionError_game" class="error transError alert alert-danger alert-dismissable" style="display: none"></div>

            <ul class="list-unstyled form-horizontal">

                <li class="form-group">
                    <label class="transactionSelectorLabel control-label col-sm-3" for="gameGroup">
                        <spring:message code="transaction.chooseGroup"/>
                    </label>
                    <div class="col-sm-9">
                        <select id="gameGroup" class="form-control" onchange="changeGameGroupDynamicSelector()"></select>
                    </div>
                </li>

                <li id="gameNameRow" class="form-group" style="display: none;">
                    <label class="control-label col-sm-3" for="gameGroup">
                        <spring:message code="transaction.chooseName"/>
                    </label>
                    <div id="gameNameColumn" class="col-sm-9">
                        <select id="gameName" class="selectClass form-control" onchange="changeGameSelector()"></select>
                    </div>
                </li>

                <li class="form-group has-datepicker">
                    <label class="transactionLabel control-label col-sm-3" for="transactions_game_from">
                        <spring:message code="transaction.date"/>
                        <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                    </label>
                    <div class="col-sm-9">
                        <div class="form-control-datepicker">
                            <input id="transactions_game_from" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onchange="periodForTransactions();">
                            <label for="transactions_game_from">
                                <i class="fa fa-calendar fa-lg"></i>
                            </label>
                        </div>
                        <div class="form-control-datepicker" style="display: none">
                            <input id="transactions_game_to" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onchange="periodForTransactions();">
                        </div>
                        <span id="transactions_game_to_error" class="transactionFieldError help-block error-field"></span>
                    </div>
                </li>

            </ul>

            <p id="transactionsTitle_game" class="h3 transactionsTitle"></p>

            <table id="transactionsTable_game" class="transactionsTable table table-striped" style="display: none;">

                <thead>
                    <tr>
                        <th><spring:message code="transaction.timestamp"/></th>
                        <th><spring:message code="transaction.gameName"/></th>
                        <th><spring:message code="transaction.betAmount"/></th>
                        <th><spring:message code="transaction.winAmount"/></th>
                        <th><spring:message code="transaction.status"/></th>
                        <th><spring:message code="transaction.transactionId"/></th>
                    </tr>
                </thead>

                <tbody id="transactionsTableBody_game">
                </tbody>

            </table>

        </div>

        <!-- Betting transactions -->
        <c:if test="${features.betinactionBetHistory}">
        <div class="row">
            <div id="transactions_betting" class="transactionsElement">
                <iframe class="col-sm-12">
                    <!-- Populated in getBettingTransactions();-->
                </iframe>
            </div>
        </div>
        </c:if>

        <!-- Promo transactions -->
        <div id="transactions_promo" class="transactionsElement">

            <div id="transactionError_promo" class="error transError alert alert-danger alert-dismissable" style="display: none"></div>

            <ul class="list-unstyled form-horizontal">

                <li class="form-group has-datepicker">
                    <label class="transactionLabel control-label col-sm-3" for="transactions_promo_from">
                        <spring:message code="transaction.period"/> <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                    </label>
                    <div class="col-sm-9">
                        <div class="form-control-datepicker">
                            <span><spring:message code="transaction.from"/></span>
                            <input id="transactions_promo_from" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                            <label for="transactions_promo_from">
                                <i class="fa fa-calendar fa-lg"></i>
                            </label>
                        </div>
                        <div class="form-control-datepicker">
                            <span><spring:message code="transaction.to"/></span>
                            <input id="transactions_promo_to" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                            <label for="transactions_promo_to">
                                <i class="fa fa-calendar fa-lg"></i>
                            </label>
                        </div>
                        <span id="transactions_promo_to_error" class="transactionFieldError help-block error-field"></span>
                    </div>
                </li>

            </ul>

            <p id="transactionsTitle_promo" class="h3 transactionsTitle"></p>

            <table id="transactionsTable_promo" class="transactionsTable table table-striped" style="display: none;">

                <thead>
                    <tr>
                        <th><spring:message code="transaction.timestamp"/></th>
                        <th><spring:message code="transaction.name"/></th>
                        <th><spring:message code="transaction.amount"/></th>
                        <th><spring:message code="transaction.type"/></th>
                        <th><spring:message code="transaction.reference"/></th>
                    </tr>
                </thead>

                <tbody id="transactionsTableBody_promo">
                </tbody>

            </table>

        </div>

        <!-- Deposit transactions -->
        <div id="transactions_deposit" class="transactionsElement">

            <div id="transactionError_deposit" class="error transError alert alert-danger alert-dismissable" style="display: none"></div>

            <div id="depositTransTable">

                <ul class="list-unstyled form-horizontal">

                    <li class="form-group has-datepicker">
                        <label class="transactionLabel control-label col-sm-3" for="transactions_deposit_from">
                            <spring:message code="transaction.period"/> <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="form-control-datepicker">
                                <span><spring:message code="transaction.from"/></span>
                                <input id="transactions_deposit_from" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                                <label for="transactions_deposit_from">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                            </div>
                            <div class="form-control-datepicker">
                                <span><spring:message code="transaction.to"/></span>
                                <input id="transactions_deposit_to" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                                <label for="transactions_deposit_to">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                            </div>
                            <span id="transactions_deposit_to_error" class="transactionFieldError help-block error-field"></span>
                        </div>
                    </li>

                    <li class="form-group">
                        <label class="transactionSelectorLabel control-label col-sm-3" for="status_deposit">
                            <spring:message code="transaction.status"/>
                        </label>
                        <div class="col-sm-9">
                            <select id="status_deposit" class="form-control" onchange="changeDepositStatusSelector()"></select>
                        </div>
                    </li>

                </ul>

                <p id="transactionsTitle_deposit" class="h3 transactionsTitle"></p>

                <table id="transactionsTable_deposit" class="transactionsTable table table-striped" style="display: none;">

                    <thead>
                    <tr>
                        <th><spring:message code="transaction.timestamp"/></th>
                        <th><spring:message code="transaction.provider"/></th>
                        <th><spring:message code="transaction.amount"/></th>
                        <th><spring:message code="transaction.bonusAmount"/></th>
                        <th><spring:message code="transaction.status"/></th>
                        <th><spring:message code="transaction.reference"/></th>
                    </tr>
                    </thead>

                    <tbody id="transactionsTableBody_deposit">
                    </tbody>

                </table>

            </div>

            <div id="depositTransDetail" style="display: none;">

                <p class="transactionsTitle"><spring:message code="transaction.details"/></p>

                <table id="depositDetailsTable" class="transactionsTable table table-striped">
                    <tbody id="depositDetailsBody">
                    <tr>
                        <td><spring:message code="transaction.timestamp"/></td>
                        <td id="depositDetailTimestamp" colspan="2"></td>
                    </tr>
                    <tr class="transactionTableOddRow">
                        <td><spring:message code="transaction.provider"/></td>
                        <td id="depositDetailProvider" colspan="2"></td>
                    </tr>
                    <tr>
                        <td><spring:message code="transaction.toPlayerAccount"/></td>
                        <td id="depositDetailAmount" colspan="2"></td>
                    </tr>
                    <tr class="transactionTableOddRow">
                        <td><spring:message code="transaction.fromBankAccount"/></td>
                        <td id="depositDetailBankAmount" colspan="2"></td>
                    </tr>
                    <tr>
                        <td><spring:message code="depositTable.fee"/></td>
                        <td id="depositDetailFee" colspan="2"></td>
                    </tr>
                    <tr class="transactionTableOddRow">
                        <td><spring:message code="transaction.status"/></td>
                        <td id="depositDetailStatus"></td>
                    </tr>
                    <tr>
                        <td><spring:message code="transaction.reference"/></td>
                        <td id="depositDetailRef" colspan="2"></td>
                    </tr>
                    </tbody>
                </table>

                <a class="transDetailLink btn btn-default" href="javascript: closeDepositTransDetail();"><spring:message code="back"/></a>

            </div>

        </div>

        <!-- Withdrawal transactions -->
        <div id="transactions_withdrawal" class="transactionsElement">

            <div id="transactionError_withdrawal" class="error transError alert alert-danger alert-dismissable" style="display: none"></div>

            <div id="withdrawalTransTable">

                <ul class="list-unstyled form-horizontal">

                    <li class="form-group has-datepicker">
                        <label class="transactionLabel control-label col-sm-3" for="transactions_withdrawal_from">
                            <spring:message code="transaction.period"/> <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="form-control-datepicker">
                                <span><spring:message code="transaction.from"/></span>
                                <input id="transactions_withdrawal_from" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                                <label for="transactions_withdrawal_from">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                            </div>
                            <div class="form-control-datepicker">
                                <span><spring:message code="transaction.to"/></span>
                                <input id="transactions_withdrawal_to" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                                <label for="transactions_withdrawal_to">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                            </div>
                            <span id="transactions_withdrawal_to_error" class="transactionFieldError help-block error-field"></span>
                        </div>
                    </li>

                    <li class="form-group">
                        <label class="transactionSelectorLabel control-label col-sm-3" for="status_withdrawal">
                            <spring:message code="transaction.status"/>
                        </label>
                        <div class="col-sm-9">
                            <select id="status_withdrawal" class="form-control" onchange="changeWithdrawalStatusSelector()"></select>
                        </div>
                    </li>

                </ul>

                <p id="transactionsTitle_withdrawal" class="h3 transactionsTitle"></p>

                <table id="transactionsTable_withdrawal" class="transactionsTable table table-striped" style="display: none;">

                    <thead>
                        <tr>
                            <th><spring:message code="transaction.timestamp"/></th>
                            <th><spring:message code="transaction.provider"/></th>
                            <th><spring:message code="transaction.amount"/></th>
                            <th><spring:message code="transaction.status"/></th>
                            <th><spring:message code="transaction.action"/></th>
                        </tr>
                    </thead>

                    <tbody id="transactionsTableBody_withdrawal">
                    </tbody>

                </table>

            </div>

            <div id="withdrawalTransDetail" style="display: none;">

                <p class="transactionsTitle"><spring:message code="transaction.details"/></p>

                <table id="withdrawalDetailsTable" class="transactionsTable table table-striped">
                    <tbody id="withdrawalDetailsBody">
                        <tr>
                            <td><spring:message code="transaction.timestamp"/></td>
                            <td id="detailTimestamp" colspan="2"></td>
                        </tr>
                        <tr class="transactionTableOddRow">
                            <td><spring:message code="transaction.provider"/></td>
                            <td id="detailProvider" colspan="2"></td>
                        </tr>
                        <tr>
                            <td><spring:message code="transaction.toBankAccount"/></td>
                            <td id="detailBankAmount" colspan="2"></td>
                        </tr>
                        <tr class="transactionTableOddRow">
                            <td><spring:message code="depositTable.fee"/></td>
                            <td id="detailFee" colspan="2"></td>
                        </tr>
                        <tr>
                            <td><spring:message code="transaction.fromPlayerAccount"/></td>
                            <td id="detailAmount" colspan="2"></td>
                        </tr>
                        <tr class="transactionTableOddRow">
                            <td><spring:message code="transaction.status"/></td>
                            <td id="detailStatus"></td>
                            <td class="transactionBtn" id="detailTd"></td>
                        </tr>
                        <tr>
                            <td><spring:message code="transaction.reference"/></td>
                            <td id="detailRef" colspan="2"></td>
                        </tr>
                    </tbody>
                </table>

                <a class="transDetailLink btn btn-default" href="javascript: closeWithdrawalTransDetail();"><spring:message code="back"/></a>

            </div>

        </div>

        <!-- Loyalty transactions -->
        <div id="transactions_loyalty" class="transactionsElement">

            <div id="transactionError_loyalty" class="error transError alert alert-danger alert-dismissable" style="display: none"></div>

            <p id="transactionsTitle_loyalty" class="h3 transactionsTitle"></p>

            <table id="transactionsTable_loyalty" class="transactionsTable table table-striped" style="display: none;">

                <thead>
                    <tr>
                        <th><spring:message code="transaction.timestamp"/></th>
                        <th><spring:message code="transaction.description"/></th>
                        <th><spring:message code="transaction.type"/></th>
                        <th><spring:message code="transaction.statusPoints"/></th>
                        <th><spring:message code="transaction.awardPoints"/></th>
                    </tr>
                </thead>

                <tbody id="transactionsTableBody_loyalty">
                </tbody>

            </table>

        </div>

        <!-- Commission transactions -->
        <%--<div id="transactions_commission" class="transactionsElement">

            <div id="transactionError_commission" class="error transError alert alert-danger alert-dismissable" style="display: none"></div>

            <div id="commissionTransTable">

                <ul class="list-unstyled form-horizontal">

                    <li class="form-group has-datepicker">
                        <label class="transactionLabel control-label col-sm-3" for="transactions_commission_from">
                            <spring:message code="transaction.period"/> <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="form-control-datepicker">
                                <span><spring:message code="transaction.from"/></span>
                                <input id="transactions_commission_from" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                                <label for="transactions_commission_from">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                            </div>
                            <div class="form-control-datepicker">
                                <span><spring:message code="transaction.to"/></span>
                                <input id="transactions_commission_to" class="transactionsDateInput form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="periodForTransactions();">
                                <label for="transactions_commission_to">
                                    <i class="fa fa-calendar fa-lg"></i>
                                </label>
                            </div>
                            <span id="transactions_commission_to_error" class="transactionFieldError help-block error-field"></span>
                        </div>
                    </li>

                    <li class="form-group">
                        <label class="transactionSelectorLabel control-label col-sm-3" for="type_commission">
                            <spring:message code="transaction.commission.type"/>
                        </label>
                        <div class="col-sm-9">
                            <select id="type_commission" class="form-control" onchange="changeCommissionTypeSelector()"></select>
                        </div>
                    </li>

                </ul>

                <p id="transactionsTitle_commission" class="h3 transactionsTitle"></p>

                <table id="transactionsTable_commission" class="transactionsTable table table-striped" style="display: none;">

                    <thead>
                    <tr>
                        <th><spring:message code="transaction.timestamp"/></th>
                        <th><spring:message code="transaction.commission.type"/></th>
                        <th><spring:message code="transaction.commission.promo"/></th>
                        <th><spring:message code="transaction.commission.cash"/></th>
                        <th><spring:message code="transaction.commission.total"/></th>
                        <th><spring:message code="transaction.commission.payout"/></th>
                        <th><spring:message code="transaction.commission.rate"/></th>
                    </tr>
                    </thead>

                    <tbody id="transactionsTableBody_commission">
                    </tbody>

                </table>

            </div>

        </div>--%>

    </div>

</div>
