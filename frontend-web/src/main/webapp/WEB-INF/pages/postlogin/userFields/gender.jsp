<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="gender_item" class="form-group" style="display: none">
    <label class="control-label col-sm-3">
        <spring:message code="register.form.gender"/><span id="star_gender" style="display: none"> *</span>
    </label>
    <div id="gender" class="col-sm-9">
        <label id="genderMaleLabel" class="radio-inline">
            <input id="registerGenderMale" type="radio" name="gender" value="Male"><spring:message code="register.form.gender.male"/>
        </label>
        <label id="genderFemaleLabel" class="radio-inline">
            <input id="registerGenderFemale" type="radio" name="gender" value="Female"><spring:message code="register.form.gender.female"/>
        </label>
        <div  id="gender_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>