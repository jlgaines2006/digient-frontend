<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="birthPlace_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="birthPlace">
        <spring:message code="birthPlace"/><span id="star_birthPlace" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="birthPlace" class="form-control" type="text" name="birthPlace" value="" maxlength="256">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="birthPlace.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="birthPlace_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>