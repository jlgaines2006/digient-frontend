<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="residentialZipCode_item" class="form-group has-feedback" style="display: none">
    <label class="control-label col-sm-3" for="residentialZipCode">
        <spring:message code="myaccount.residentialZipCode"/><span id="star_residentialZipCode" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="residentialZipCode" class="form-control" type="text" name="residentialZipCode" value="" maxlength="16" onkeypress="validateInputs(event, 'zipCode');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.residentialZipCode.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="residentialZipCode_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>