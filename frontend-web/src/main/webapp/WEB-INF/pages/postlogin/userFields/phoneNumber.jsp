<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="phoneNumber_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="phoneNumber">
        <spring:message code="myaccount.phoneNumber"/><span id="star_phoneNumber" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="phoneNumber" class="form-control" type="text" name="phoneNumber" value="" onkeypress="validateInputs(event, 'phone');">
        <div class="form-control-feedback">
            <i alt="?" id="phoneNumberHint" class="fa fa-question fa-lg"></i>
        </div>
        <div id="phoneNumber_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>