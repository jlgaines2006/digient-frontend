<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="middleName_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="middleName">
        <spring:message code="middleName"/><span id="star_middleName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="middleName" class="form-control" type="text" name="middleName" value="">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="middleName.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="middleName_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>