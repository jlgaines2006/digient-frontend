<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<li id="passportCountry_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="passportCountry">
        <spring:message code="passportCountry"/><span id="star_passportCountry" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <select id="passportCountry" class="form-control" name="passportCountry">
            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-country.html" />
        </select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="passportCountry.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
    </div>

</li>

<!-- Making the default value same as the country of registration -->
<c:set var="registerCountry" value="${loginUser.country}" />
<script>
    var value = "${registerCountry}";
    $('#passportCountry').val(value).prop('selected', true);
</script>