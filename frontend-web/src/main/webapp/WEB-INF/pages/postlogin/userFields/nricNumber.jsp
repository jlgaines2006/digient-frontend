<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="nricNumber_item" class="form-group has-feedback" style="display: none">
    <label class="control-label col-sm-3">
        <spring:message code="nricNumber"/><span id="star_nricNumber" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">

        <input id="nricNumber" name="nricNumber" class="form-control" type="text" value="" maxlength="9"  style="text-transform: uppercase">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="nricNumber.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="nricNumber_error" class="bankFieldError help-block error-field"></div>

    </div>

</li>

<script style="text/javascript">
    //Masked input for NRIC number
    $("#nricNumber").mask("a9999999a");
</script>