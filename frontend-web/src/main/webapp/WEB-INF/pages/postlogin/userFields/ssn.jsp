<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="ssn_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="ssnMyaccYear">
        <spring:message code="myaccount.ssn"/><span id="star_ssn" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <div class="row">
            <div class="ssnBlock col-xs-3">
                <input id="ssnMyaccYear" class="form-control" type="text" maxlength="2" onkeypress="validateInputs(event, 'number'); moveToNextField('ssnMyaccYear');" placeholder="<spring:message code="year"/>"/>
                <span class="ssnMyaccFirstDot">.</span>
            </div>
            <div class="ssnBlock col-xs-3">
                <input id="ssnMyaccMonth" class="form-control" type="text" maxlength="2" onkeypress="validateInputs(event, 'number'); moveToNextField('ssnMyaccMonth');" placeholder="<spring:message code="month"/>"/>
                <span class="ssnMyaccSecondDot">.</span>
            </div>
            <div class="ssnBlock col-xs-3">
                <input id="ssnMyaccDate" class="form-control" type="text" maxlength="2" onkeypress="validateInputs(event, 'number'); moveToNextField('ssnMyaccDate');" placeholder="<spring:message code="date"/>"/>
                <span class="ssnMyaccDash">-</span>
            </div>
            <div class="ssnBlock col-xs-3">
                <input id="ssnMyaccNumber" class="form-control" type="text" maxlength="5" onkeypress="validateInputs(event, 'number');" placeholder="<spring:message code="number"/>"/>
            </div>
        </div>
        <input id="ssn" class="form-control" type="text" name="ssn" value="" style="display: none;">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.ssn.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="ssn_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>