<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="citizenshipNumber_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="citizenshipNumber">
        <spring:message code="myaccount.citizenshipNumber"/><span id="star_citizenshipNumber" style="display: none;"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="citizenshipNumber" class="form-control" type="text" name="citizenshipNumber" value="" onkeypress="validateInputs(event, 'number');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.citizenshipNumber.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="citizenshipNumber_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>