<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="identification_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="identification">
        <spring:message code="myaccount.identification"/><span id="star_identification" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <select id="identification" class="form-control"></select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.identification.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="identification_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>