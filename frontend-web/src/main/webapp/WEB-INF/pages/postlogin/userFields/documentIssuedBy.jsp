<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="documentIssuedBy_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="documentIssuedBy">
        <spring:message code="myaccount.documentIssuedBy"/><span id="star_documentIssuedBy" style="display: none;"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="documentIssuedBy" class="form-control" type="text" name="documentIssuedBy" value="" maxlength="256">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.documentIssuedBy.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="documentIssuedBy_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>