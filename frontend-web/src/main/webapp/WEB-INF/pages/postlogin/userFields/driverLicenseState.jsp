<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<li id="driverLicenceState_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="driverLicenceState">
        <spring:message code="driverLicenseState"/><span id="star_driverLicenceState" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <select id="driverLicenceState" class="form-control" name="driverLicenceState">
            <c:choose>
                <c:when test="${loginUser.country == 'AUS'}">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/country-states/australian-states.html" />
                </c:when>
            </c:choose>
        </select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="driverLicenseState.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
    </div>

</li>