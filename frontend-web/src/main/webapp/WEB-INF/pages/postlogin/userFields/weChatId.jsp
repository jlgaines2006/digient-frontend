<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="weChatId_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="weChatId">
        <spring:message code="weChatId"/><span id="star_weChatId" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="weChatId" class="form-control" type="text" name="weChatId" value="" onkeypress="validateInputs(event, 'userAccounts');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="weChatId.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="weChatId_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>