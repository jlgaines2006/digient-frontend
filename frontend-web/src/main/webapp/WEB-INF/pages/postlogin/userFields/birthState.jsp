<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<li id="birthState_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="birthState">
        <spring:message code="birthState"/><span id="star_birthState" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <select id="birthState" class="form-control" name="birthState">
            <c:if test="${loginUser.country == 'SGP'}">
                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/country-states/singapore-states.html" />
            </c:if>
        </select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="birthState.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
    </div>

</li>