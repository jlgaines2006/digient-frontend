<!-- Passport Machine Readable Zone (MRZ) is printed on most passports and includes two lines of characters, each line being 44 characters long -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="passportMRZLine1_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="passportMRZLine1">
        <spring:message code="passportMRZLine1"/><span id="star_lastName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9" style="position: relative">
        <input id="passportMRZLine1" class="form-control" type="text" name="passportMRZLine1" value="" maxlength="44" onkeypress="validateInputs(event, 'passportMRZ'); " style="text-transform: uppercase" >
        <div class="form-control-feedback">
            <i alt="?" onmouseover="javascript: $('#cvc').show();" onmouseout="javascript: $('#cvc').hide();" class="fa fa-question fa-lg"></i>
        </div>
        <div id="cvc" style="display: none;" class="hintPic">
            <img src="${cmsRoot}/images/icons/passportMRZ.jpg" />
        </div>
        <div id="passportMRZLine1_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>