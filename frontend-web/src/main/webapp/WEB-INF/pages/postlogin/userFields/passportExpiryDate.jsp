<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="passwordExpiryDate" class="java.util.Date" />
<fmt:formatDate value="${passwordExpiryDate}" pattern="yyyy" var="currentYear" />

<li id="passportExpiryDate_item" class="form-group" style="display: none">
    <label class="control-label col-sm-3" for="passportExpiryDateDay">
        <spring:message code="myaccount.passportExpiryDate"/><span id="star_passportExpiryDate" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="passportExpiryDate" type="hidden" value="" name="passportExpiryDate">
        <div id="passportExpiryDateWrapper">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <select id="passportExpiryDateDay" class="form-control">
                                <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.day'/></option>
                                <c:forEach var="day" begin="1" end="31" step="1">
                                    <c:set var="dayWithZero" value="0${day}"/>
                                    <option value="${day < 10 ? dayWithZero : day}">${day < 10 ? dayWithZero : day}</option>
                                </c:forEach>
                            </select>
                        </td>

                        <td style="text-align:center;">
                            <select id="passportExpiryDateMonth" class="form-control">
                                <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.month'/></option>
                                <c:forEach var="month" begin="1" end="12" step="1">
                                    <c:set var="monthWithZero" value="0${month}"/>
                                    <option value="${month < 10 ? monthWithZero : month}"><spring:message code='month.${month < 10 ? monthWithZero : month}'/></option>
                                </c:forEach>
                            </select>
                        </td>

                        <td style="text-align:right;">
                            <select id="passportExpiryDateYear" class="form-control">
                                <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.year'/></option>
                                <c:forEach var="counter" begin="0" end="25" step="1">
                                    <option value="${currentYear + counter}">${currentYear + counter}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="passportExpiryDate_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>