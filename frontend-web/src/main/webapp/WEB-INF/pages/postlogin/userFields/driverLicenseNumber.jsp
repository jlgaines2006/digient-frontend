<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="driverLicenceNumber_item" class="form-group has-feedback" style="display: none">
    <label class="control-label col-sm-3">
        <spring:message code="driverLicenseNumber"/><span id="star_driverLicenceNumber" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">

        <input id="driverLicenceNumber" name="driverLicenceNumber" class="form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'bankInfo');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="driverLicenseNumber.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="driverLicenceNumber_error" class="bankFieldError help-block error-field"></div>

    </div>

</li>