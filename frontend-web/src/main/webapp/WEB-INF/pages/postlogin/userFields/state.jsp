<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<li id="state_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="state">
        <spring:message code="state"/><span id="star_state" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <select id="state" class="form-control" name="state">
            <c:if test="${loginUser.country == 'AUS'}">
                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/country-states/australian-states.html" />
            </c:if>
        </select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="state.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
    </div>

</li>