<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="documentIssuingCountry_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="documentIssuingCountry">
        <spring:message code="myaccount.documentIssuingCountry"/><span id="star_documentIssuingCountry" style="display: none;"> *</span>
    </label>

    <div class="col-sm-9">
        <select id="documentIssuingCountry" class="form-control" name="documentIssuingCountry"></select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.documentIssuingCountry.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="documentIssuingCountry_error" class="bankFieldError help-block error-field"></div>

        <script type="text/javascript">
            (function () {
                $('#documentIssuingCountry').load('${cmsRoot}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-country.html', function () {
                    $(this).prepend('<option value="" selected="selected" disabled="disabled">' + registerSelectDocumentIssuingCountry + '</option>');
                });
            })();
        </script>
    </div>
</li>