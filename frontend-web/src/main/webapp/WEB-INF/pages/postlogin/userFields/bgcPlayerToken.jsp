<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="bgcPlayerToken_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="bgcPlayerToken">
        <spring:message code="myaccount.bgc.filled"/><span id="star_bgcPlayerToken" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="bgcPlayerToken" class="disabledInput form-control" type="text" name="bgcPlayerToken" value="" disabled="true">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.bgc.filled.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div class="bankFieldError help-block error-field"></div>
    </div>

</li>