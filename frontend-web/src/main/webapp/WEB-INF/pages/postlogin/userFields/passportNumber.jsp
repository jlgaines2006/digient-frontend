<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="passportNumber_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="passportNumber">
        <spring:message code="myaccount.passportNumber"/><span id="star_passportNumber" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="passportNumber" class="form-control" type="text" name="passportNumber" value="" maxlength="256" onkeypress="validateInputs(event, 'bankInfo');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.passportNumber.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="passportNumber_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>