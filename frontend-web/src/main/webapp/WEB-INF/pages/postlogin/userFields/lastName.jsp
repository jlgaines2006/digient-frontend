<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="lastName_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="lastName">
        <spring:message code="lastName"/><span id="star_lastName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="lastName" class="form-control" type="text" name="lastName" value="">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="lastName.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="lastName_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>