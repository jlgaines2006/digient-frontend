<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="firstName_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="firstName">
        <spring:message code="firstName"/><span id="star_firstName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="firstName" class="form-control" type="text" name="firstName" value="">
        <div class="form-control-feedback">
           <i alt="?" title="<spring:message code="firstName.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="firstName_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>