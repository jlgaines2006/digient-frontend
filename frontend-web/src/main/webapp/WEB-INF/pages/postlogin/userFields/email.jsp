<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="email_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="email">
        <spring:message code="email"/><span id="star_email" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="email" class="disabledInput form-control" type="text" disabled="true">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.email.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div class="bankFieldError help-block error-field"></div>
    </div>

</li>