<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="zipCode_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="zipCode">
        <spring:message code="myaccount.zipCode"/><span id="star_zipCode" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="zipCode" class="form-control" type="text" name="zipCode" value="" onkeypress="validateInputs(event, 'zipCode');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.zipCode.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="zipCode_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>