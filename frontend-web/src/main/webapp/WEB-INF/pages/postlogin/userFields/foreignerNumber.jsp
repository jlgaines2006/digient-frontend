<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="foreignerNumber_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="foreignerNumber">
        <spring:message code="myaccount.foreignerNumber"/><span id="star_foreignerNumber" style="display: none;"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="foreignerNumber" class="form-control" type="text" name="foreignerNumber" value="" maxlength="256" onkeypress="validateInputs(event, 'bankInfo');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.foreignerNumber.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="foreignerNumber_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>