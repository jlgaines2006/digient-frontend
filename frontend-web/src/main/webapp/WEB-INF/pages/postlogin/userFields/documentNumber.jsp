<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="documentNumber_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="documentNumber">
        <spring:message code="myaccount.documentNumber"/><span id="star_documentNumber" style="display: none;"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="documentNumber" class="form-control" type="text" name="documentNumber" value="" maxlength="256" onkeypress="validateInputs(event, 'bankInfo');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.documentNumber.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="documentNumber_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>