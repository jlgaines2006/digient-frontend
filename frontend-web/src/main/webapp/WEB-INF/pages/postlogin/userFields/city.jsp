<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="city_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="city">
        <spring:message code="myaccount.city"/><span id="star_city" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="city" class="form-control" type="text" name="city" value="">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.city.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="city_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>