<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="userName_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="userName">
        <spring:message code="userName"/><span id="star_userName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="userName" class="disabledInput form-control" name="userName" type="text" onkeypress="validateInputs(event, 'userAccounts');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.userName.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="userName_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>