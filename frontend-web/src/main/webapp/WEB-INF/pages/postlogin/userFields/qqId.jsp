<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="qqId_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="qqId">
        <spring:message code="qqId"/><span id="star_qqId" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="qqId" class="form-control" type="text" name="qqId" value="" onkeypress="validateInputs(event, 'userAccounts');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="qqId.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="qqId_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>