<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="birthCountry_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="birthCountry">
        <spring:message code="birthCountry"/><span id="star_birthCountry" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <select id="birthCountry" class="form-control" name="birthCountry">
            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-country.html" />
        </select>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="birthCountry.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div  id="birthCountry_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>

<!-- Making the default value same as the country of registration -->
<c:set var="registerCountry" value="${loginUser.country}" />
<script>
    var value = "${registerCountry}";
    $('#birthCountry').val(value).prop('selected', true);
</script>