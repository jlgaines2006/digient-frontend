<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="skypeId_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="skypeId">
        <spring:message code="skypeId"/><span id="star_skypeId" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="skypeId" class="form-control" type="text" name="skypeId" value="" onkeypress="validateInputs(event, 'userAccounts');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="skypeId.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="skypeId_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>