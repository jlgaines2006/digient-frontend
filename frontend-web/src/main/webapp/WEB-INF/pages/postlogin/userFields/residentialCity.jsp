<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="residentialCity_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="residentialCity">
        <spring:message code="myaccount.residentialCity"/><span id="star_residentialCity" style="display: none;"> *</span>
    </label>

    <div class="col-sm-9">
        <input id="residentialCity" class="form-control" type="text" name="residentialCity" value="" maxlength="256">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.residentialCity.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="residentialCity_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>