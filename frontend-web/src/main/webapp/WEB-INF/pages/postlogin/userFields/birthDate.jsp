<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<li id="birthDate_item" class="form-group has-feedback" style="display: none">
    <label class="control-label col-sm-3" for="birthDate">
        <spring:message code="register.form.birthDate"/>
        <span class="birthdatetitle">
            <spring:message code='register.form.birthDateFormat'/>
        </span>
        <span id="star_birthDate" style="display: none"> *</span>
    </label>

    <div name="birthDate" class="col-sm-9">
        <table>
            <tbody>
                <tr>
                    <td>
                        <select id="birthday" class="form-control col-sm-1" name="day-birthDate" disabled>

                            <option selected="selected" value="1">01</option>
                            <c:forEach var="day" begin="2" end="31" step="1">
                                <c:set var="dayWithZero" value="0${day}"/>
                                <option value="${day}">${day < 10 ? dayWithZero : day}</option>
                            </c:forEach>
                        </select>
                    </td>

                    <td style="text-align:center;">

                        <select id="birthMonth" class="form-control" name="month-birthDate" disabled>
                            <option value="1" selected="selected"><spring:message code='month.01'/></option>
                            <c:forEach var="month" begin="2" end="12" step="1">
                                <c:set var="monthWithZero" value="0${month}"/>
                                <option value="${month}"><spring:message code='month.${month < 10 ? monthWithZero : month}'/></option>
                            </c:forEach>
                        </select>
                    </td>

                    <td style="text-align:right;">
                        <select id="birthYear" class="form-control" name="year-birthDate" disabled>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    <div id="birthDate_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>
