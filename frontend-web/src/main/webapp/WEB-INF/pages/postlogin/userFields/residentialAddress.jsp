<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="residentialAddress_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3" for="residentialAddress">
        <spring:message code="myaccount.residentialAddress"/><span id="star_residentialAddress" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <div class="row">
            <div class="col-xs-8">
                <input id="residentialAddress" class="form-control" type="text" name="residentialAddress" value="" maxlength="256">
            </div>
            <div class="col-xs-4">
                <input id="residentialHouseNumber" class="form-control" type="text" name="residentialHouseNumber" value="" maxlength="10">
            </div>
        </div>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.residentialAddress.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="residentialAddress_error" class="bankFieldError help-block error-field"></div>
        <div id="residentialHouseNumber_error" class="bankFieldError help-block error-field" style="display: none"></div>
    </div>
</li>