<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="address_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="address">
        <spring:message code="address"/><span id="star_address" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <div class="row">
            <div class="col-xs-8">
                <input id="address" class="form-control" type="text" name="address" value="">
            </div>
            <div class="col-xs-4">
                <input id="houseNumber" class="form-control" type="text" name="houseNumber" value="">
            </div>
        </div>
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="address.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="address_error" class="bankFieldError help-block error-field"></div>
        <div id="houseNumber_error" class="bankFieldError help-block error-field" style="display: none"></div>
    </div>

</li>