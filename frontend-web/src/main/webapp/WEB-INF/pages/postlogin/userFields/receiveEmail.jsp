<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="receiveEmail_item" class="row" style="display: none;">

    <div class="col-sm-offset-3 col-sm-9">
        <div class="checkbox">
            <label for="myAcc_receiveEmail">
                <input id="myAcc_receiveEmail" type="checkbox" name="receiveEmail">
                <spring:message code="myaccount.receiveSms"/>
            </label>
        </div>
    </div>

</li>