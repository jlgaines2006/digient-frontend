<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="nickName_item" class="form-group has-feedback" style="display: none;">

    <label class="control-label col-sm-3" for="nickName">
        <spring:message code="nickname"/><span id="star_nickName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="nickName" class="disabledInput form-control" name="nickName" type="text" onkeypress="validateInputs(event, 'userAccounts');">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.nickname.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="nickName_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>