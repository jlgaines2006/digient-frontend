<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="maidenName_item" class="form-group has-feedback" style="display: none">

    <label class="control-label col-sm-3" for="maidenName">
        <spring:message code="maidenName"/><span id="star_maidenName" style="display: none"> *</span>
    </label>
    <div class="col-sm-9">
        <input id="maidenName" class="form-control" type="text" name="maidenName" value="" maxlength="256">
        <div class="form-control-feedback">
            <i alt="?" title="<spring:message code="maidenName.hint"/>" class="fa fa-question fa-lg"></i>
        </div>
        <div id="maidenName_error" class="bankFieldError help-block error-field"></div>
    </div>

</li>