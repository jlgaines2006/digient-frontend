<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="documentType_item" class="form-group has-feedback" style="display: none;">
    <label class="control-label col-sm-3">
        <spring:message code="myaccount.documentType"/><span id="star_documentType" style="display: none"> *</span>
    </label>

    <div class="col-sm-9">
        <div id="documentTypeItems" class="labels">
            <label id="documentTypeIdCardLabel" class="radio-inline">
                <input id="documentTypeIdCard" type="radio" name="documentType" value="idCard"><spring:message code="myaccount.documentType.idCard"/>
            </label>
            <label id="documentTypePassportLabel" class="radio-inline">
                <input id="documentTypePassport" type="radio" name="documentType" value="passport"><spring:message code="myaccount.documentType.passport"/>
            </label>
            <label id="documentTypeOtherLabel" class="radio-inline">
                <input id="documentTypeOther" type="radio" name="documentType" value="other"><spring:message code="myaccount.documentType.other"/>
            </label>
        </div>
        <div id="documentType_error" class="bankFieldError help-block error-field"></div>
    </div>
</li>