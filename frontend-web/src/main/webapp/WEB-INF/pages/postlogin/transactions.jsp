<%@ page buffer="none" session="true" language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">

    $(document).ready(function() {
        var url = window.location.href.toString(),
            index = -1;
        if (url.indexOf('promo') != -1) {
            index = $('#transactions_promo_tab').index();
            getTransactionsData('promo');
        }
        else if (url.indexOf('deposit') != -1) {
            index = $('#transactions_deposit_tab').index();
            getTransactionsData('deposit');
        }
        else if (url.indexOf('withdrawal') != -1) {
            index = $('#transactions_withdrawal_tab').index();
            getTransactionsData('withdrawal');
        }
        else if (url.indexOf('commission') != -1) {
            index = $('#transactions_commission_tab').index();
            getTransactionsData('commission');
        }
        else {
            index = $('#transactions_game_tab').index();
            getTransactionsData('game');
        }
        $('#tabs').tabs({ active: (index != -1 ? index : 0) });
    });

</script>

<head>
    <t:import url="${cmsRootLocal}/metadata/${_lang == null ? features.defaultLanguage : _lang}/index.html"/>

    <c:forEach var="language" items="${features.languages}">
        <c:if test="${_lang != language}">
            <link rel="alternate" href="<spring:message code='site.identifierUrl'/>/${language}/postlogin/transactions" hreflang="${language}"/>
        </c:if>
    </c:forEach>
</head>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <div id="transactionsWrapper">

                        <div id="myAccountView_top">

                            <a href="#" id="transactionsButton" class="accountviewButton accountviewbutton_active btn btn-default active"><spring:message code="navigation.transactions"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/limits" id="limitsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.limits"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns" id="campaignsButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.campaigns"/></a>
                            <a href="/${_lang == null ? features.defaultLanguage : _lang}/postlogin/myacc" id="profileButton" class="accountviewButton accountviewbutton_notActive btn btn-default"><spring:message code="navigation.myaccount"/></a>

                            <h1 id="accountviewTitle"><spring:message code="transaction.header"/></h1>

                        </div>

                        <c:import url="transactions/allTransactions.jsp" />

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>