<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form class="myAccForm" id="loyaltyDetailsForm">

    <p id="loyaltyDetails_error" class="error" style="display: none"></p>

    <table id="loyaltyDesc" class="loyaltyDescTable">
        <tbody>
        <tr>
            <td></td>
            <td id="silverPoints"></td>
            <td></td>
            <td id="goldPoints"></td>
            <td></td>
            <td id="vipPoints"></td>
            <td></td>
        </tr>
        <tr>
            <td><a class="aBorderBronze" id="aBorderBronze"><img src="/cms/images/icons/profile-loyalty/player-icon-bronze.png" alt=""></a></td>
            <td><img src="/resources/images/icons/blackArrow.png" alt=""></td>
            <td><a class="aBorderSilver" id="aBorderSilver"><img src="/cms/images/icons/profile-loyalty/player-icon-silver.png" alt=""></a></td>
            <td><img src="/resources/images/icons/blackArrow.png" alt=""></td>
            <td><a class="aBorderGold" id="aBorderGold"><img src="/cms/images/icons/profile-loyalty/player-icon-gold.png" alt=""></a></td>
            <td><img src="/resources/images/icons/blackArrow.png" alt=""></td>
            <td><a class="aBorderVip" id="aBorderVip"><img src="/cms/images/icons/profile-loyalty/player-icon-platinum.png" alt=""></a></td>
        </tr>
        <tr>
            <td id="bronze"></td>
            <td></td>
            <td id="silver"></td>
            <td></td>
            <td id="gold"></td>
            <td></td>
            <td id="platinum"></td>
        </tr>
        </tbody>
    </table>


    <table class="loyaltyTable table table-striped" id="notHighestLevel" style="display: none">
        <tbody>
            <tr class="loyaltyRowColor">
                <td><spring:message code="loyalty.userCategory"/></td>
                <td id="currentTier"></td>
            </tr>
            <tr>
                <td><spring:message code="loyalty.nextCategory"/></td>
                <td id="nextTier"></td>
            </tr>
            <tr class="loyaltyRowColor">
                <td><spring:message code="loyalty.currentPoints"/></td>
                <td id="currentPoints"></td>
            </tr>
            <tr>
                <td><spring:message code="loyalty.neededPoints"/></td>
                <td id="requiredPoints"></td>
            </tr>
            <tr class="loyaltyRowColor">
                <td><spring:message code="loyalty.nextPoints"/></td>
                <td id="nextPoints"></td>
            </tr>
            <tr>
                <td><spring:message code="loyalty.awardPoints"/></td>
                <td id="awardPoints"></td>
            </tr>
            <tr class="loyaltyRowColor" id="periodExpiryTr" style="display: none">
                <td><spring:message code="loyalty.statusExitDate"/></td>
                <td id="periodExpiry"></td>
            </tr>
        </tbody>
    </table>

    <table class="loyaltyTable table table-striped" id="highestLevel" style="display: none">
        <tbody>
            <tr class="loyaltyRowColor">
                <td><spring:message code="loyalty.userCategory"/></td>
                <td id="currentTierPlatinum"></td>
            </tr>
            <tr>
                <td><spring:message code="loyalty.currentPoints"/></td>
                <td id="currentPointsPlatinum"></td>
            </tr>
            <tr class="loyaltyRowColor">
                <td><spring:message code="loyalty.qualifyPlatinum"/></td>
                <td id="qualifyPointsPlatinum"></td>
            </tr>
            <tr>
                <td><spring:message code="loyalty.awardPoints"/></td>
                <td id="awardPointsPlatinum"></td>
            </tr>
            <tr class="loyaltyRowColor">
                <td><spring:message code="loyalty.statusExitDate"/></td>
                <td id="periodExpiryPlatinum"></td>
            </tr>
        </tbody>
    </table>

</form>