<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="closeAccountConfirm" class="popup" style="display: none">

    <div id="closeAccountConfirm_content">

        <h4 class="h1"><spring:message code="closeAccount.title"/></h4>
        <p><spring:message code="closeAccount.explanation"/></p>

        <label for="closeAccountPwdConfirm"><spring:message code="myaccount.bank.confirmPassword"/></label>
        <input id="closeAccountPwdConfirm" class="registerField form-control" type="password"  value="" />
        <p id="closeAccountPwdConfirm_error" class="help-block error-field">&nbsp;</p>

        <div id="closeAccountConfirmButton" class="confirm btn btn-primary btn-lg" title="" onclick="javascript: closeAccount();" ><spring:message code="closeAccount.closeBtn"/></div>
        <div id="closeAccountBackButton" class="confirm btn btn-default btn-lg" title="" onclick="javascript: $.fancybox.close();" ><spring:message code="back"/></div>

    </div>

</div>