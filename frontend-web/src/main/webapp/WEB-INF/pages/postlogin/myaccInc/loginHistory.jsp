<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">

    $(document).ready(function() {
        // Set up calendar selectors
        $("#loginHistory_from").datepicker({ "dateFormat": "dd-mm-yy" });
        $("#loginHistory_to").datepicker({ "dateFormat": "dd-mm-yy" });
    });

</script>

<div id="loginHistory">

    <div id="loginHistoryError" class="error alert alert-danger alert-dismissable" style="display: none"></div>

    <ul class="list-unstyled form-horizontal">

        <li class="form-group has-datepicker">
            <label class="control-label col-sm-3" for="loginHistory_from">
                <spring:message code="transaction.period"/> <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
            </label>
            <div class="col-sm-9">
                <div class="form-control-datepicker">
                    <span><spring:message code="transaction.from"/></span>
                    <input id="loginHistory_from" class="form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="updateLoginHistory(0,null);">
                    <label for="loginHistory_from">
                        <i class="fa fa-calendar fa-lg"></i>
                    </label>
                </div>
                <div class="form-control-datepicker">
                    <span><spring:message code="transaction.to"/></span>
                    <input id="loginHistory_to" class="form-control" type="text" value="" maxlength="256" onkeypress="validateInputs(event, 'date');" onchange="updateLoginHistory(0,null);">
                    <label for="loginHistory_to">
                        <i class="fa fa-calendar fa-lg"></i>
                    </label>
                </div>
                <span id="loginHistory_to_error" class="help-block error-field"></span>
            </div>
        </li>

    </ul>

    <p id="loginHistoryTitle" class="h3"></p>

    <table id="loginHistoryTable" class="table table-striped" style="display: none;">

        <thead>
            <tr>
                <th><spring:message code="login.history.createdTime"/></th>
                <th><spring:message code="login.history.ip"/></th>
                <th><spring:message code="login.history.activity"/></th>
            </tr>
        </thead>

        <tbody id="loginHistoryTableBody">
        </tbody>

    </table>

</div>