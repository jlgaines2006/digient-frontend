<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="myaccPasswordConfirm" class="popup" style="display: none;">

    <div id="myaccPasswordConfirm_content">

        <form id="myaccPasswordConfirmForm" action="">

            <div class="form-group has-feedback">
                <label for="myaccPasswordConfirm_pwd"><spring:message code="myaccount.bank.confirmPassword"/></label>
                <input id="myaccPasswordConfirm_pwd" class="registerField form-control" type="password" name="password" value="" onkeypress="searchKeyPress(event);" autofocus="autofocus" />
                <p id="myaccPasswordConfirm_error" class="help-block error-field">&nbsp;</p>
            </div>

        </form>

        <div id="myaccPasswordComfirmContinue" class="confirm btn btn-primary btn-lg" title="" onclick="javascript: submitBankAccount();" onkeypress="searchKeyPress(event);"><spring:message code="birthdate.question.continue"/></div>
        <div id="myaccPasswordComfirmCancel" class="confirm btn btn-default btn-lg" title="" onclick="javascript: $.fancybox.close();" ><spring:message code="birthdate.question.cancel"/></div>
    </div>

</div>
