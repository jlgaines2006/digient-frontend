<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form id="updateMyBankDetails" class="myAccForm form-horizontal">

    <p id="updateBankAccount_info" class="informationSaved alert alert-success alert-dismissable" style="display: none"></p>

    <fieldset id="bank">

        <p id="updateBankAccount_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

        <!-- Selector -->
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <select id="selectedBankAccountType" class="form-control" onchange="toggleBankInfo($(this).val())">
                    <!-- Dynamically populated in myaccOnLoad(); -->
                </select>
            </div>
        </div>

        <ul id="bank_ul" class="list-unstyled">
            <!-- Dynamically populated in myaccOnLoad(); -->
        </ul>

        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <div id="manageAccount_saveButton2" class="btn btn-primary btn-lg" onclick="validateBankDetails();"><spring:message code="save"/></div>
            </div>
        </div>

    </fieldset>

    <fieldset id="storedPaymentsDetails" style="display:none">

        <legend><spring:message code="storedPaymentsDetails.title"/></legend>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><spring:message code="storedPaymentsDetails.provider"/></th>
                    <th><spring:message code="storedPaymentsDetails.description"/></th>
                    <th><spring:message code="storedPaymentsDetails.tokenType"/></th>
                    <th><spring:message code="storedPaymentsDetails.created"/></th>
                    <th><spring:message code="storedPaymentsDetails.expired"/></th>
                    <th><spring:message code="storedPaymentsDetails.remove"/></th>
                </tr>
            </thead>
            <tbody id="storedPaymentsDetails-tableBody"></tbody>
        </table>

    </fieldset>

</form>