<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="rtpInfoWrapper">

    <h4 class="rtpHeader"><spring:message code="myprofile.rtp.header"/></h4>

    <div class="rtpOptions">
        <p id="rtpDayInfo" class="rtpInfo">
            <span id="rtpDayValue" class="rtpValue">&mdash;</span> <span class="rtpPeriod"><spring:message code="myprofile.rtp.day"/></span>
        </p>

        <p id="rtpMonthInfo" class="rtpInfo">
            <span id="rtpMonthValue" class="rtpValue">&mdash;</span> <span class="rtpPeriod"><spring:message code="myprofile.rtp.month"/></span>
        </p>

        <p id="rtpLifetimeInfo" class="rtpInfo">
            <span id="rtpLifetimeValue" class="rtpValue">&mdash;</span> <span class="rtpPeriod"><spring:message code="myprofile.rtp.lifetime"/></span>
        </p>
    </div>

</div><%-- #rtpInfoWrapper --%>

<script>
    $(function () {
        $.when(ajaxCalls.getRTPInfo()).done(function (data) {
            if (!hasErrors(data)) {
                $('#rtpDayValue').html(parseFloat(data.today.total).toFixed(2) + '%');
                $('#rtpMonthValue').html(parseFloat(data.lastMonth.total).toFixed(2) + '%');
                $('#rtpLifetimeValue').html(parseFloat(data.lifeTime.total).toFixed(2) + '%');
            }
        });
    });
</script>