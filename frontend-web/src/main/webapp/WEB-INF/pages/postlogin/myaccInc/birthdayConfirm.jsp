<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="birthdayConfirm" class="popup" style="display: none;">

    <div id="birthdayConfirm_content">

        <h4 class="h1"><spring:message code="birthdate.question.title"/></h4>
        <p id="birthdayValidate_error" class="alert alert-danger alert-dismissable" style="display: none"></p>
        <label for="manageAccountBirthday">
            <spring:message code="birthdate.question.help"/>
            <span class="birthdatetitle"><spring:message code='register.form.birthDateFormat'/></span>
        </label>

        <div id="birthdayConfirmTable" class="row">

            <div class="col-xs-4">
                <select id="manageAccountBirthday" class="form-control" name="day-birthDate">
                    <option selected="selected" value="1">01</option>
                    <c:forEach var="day" begin="2" end="31" step="1">
                        <c:set var="dayWithZero" value="0${day}"/>
                        <option value="${day}">${day < 10 ? dayWithZero : day}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="col-xs-4">
                <select id="manageAccountBirthmonth" class="form-control" name="month-birthDate">
                    <option value="1" selected="selected"><spring:message code='month.01'/></option>
                    <c:forEach var="month" begin="2" end="12" step="1">
                        <c:set var="monthWithZero" value="0${month}"/>
                        <option value="${month}"><spring:message code='month.${month < 10 ? monthWithZero : month}'/></option>
                    </c:forEach>
                </select>
            </div>

            <div class="col-xs-4">
                <select id="manageAccountBirthyear" class="form-control" name="year-birthDate">
                </select>
            </div>

        </div>

        <div id="birthdayComfirmContinue" class="confirm btn btn-primary btn-lg" title="" onclick="javascript: submitPassword();" ><spring:message code="birthdate.question.continue"/></div>
        <div id="birthdayComfirmCancel" class="confirm btn btn-default btn-lg" title="" onclick="javascript: $.fancybox.close();" ><spring:message code="birthdate.question.cancel"/></div>

    </div>

</div>