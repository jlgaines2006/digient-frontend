<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="updateMyAccountForm" class="myAccForm form-horizontal" action="" name="updateMyAccountForm">

    <p id="profileSaved" class="informationSaved alert alert-success alert-dismissable" style="display: none"></p>

    <fieldset id="accountDetails">

        <p id="manageAccount_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

        <ul id="accountDetails_ul" class="list-unstyled">

            <%@ include file="/WEB-INF/pages/postlogin/userFields/firstName.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/middleName.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/lastName.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/maidenName.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/nickName.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/userName.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/gender.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/address.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/city.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/zipCode.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/country.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/email.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/phoneNumber.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/identification.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/passportNumber.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/passportExpiryDate.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/ssn.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/citizenshipNumber.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/foreignerNumber.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/documentType.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/documentNumber.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/documentIssuingCountry.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/documentIssuedDate.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/documentIssuedBy.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/residentialAddress.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/residentialCity.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/residentialZipCode.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/bgcPlayerToken.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/skypeId.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/weChatId.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/qqId.jsp" %>
            <%@ include file="/WEB-INF/pages/postlogin/userFields/receiveEmail.jsp" %>

            <li id="campaignsEnabled_item" class="row" style="display: none;">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="checkbox">
                        <label for="campaignsEnabled">
                            <input id="campaignsEnabled" type="checkbox" value="1" name="campaignsEnabled">
                            <spring:message code="myaccount.form.campaignsEnabled"/><span id="star_campaignsEnabled" style="display: none"> *</span>
                        </label>
                    </div>
                </div>
            </li>

            <li class="row">

                <div class="col-sm-offset-3 col-sm-9">
                    <p class="myaccountNote form-control-static"><spring:message code="register.form.note"/></p>
                </div>

            </li>

            <li class="row">

                <div class="col-sm-offset-3 col-sm-9">
                    <div id="manageAccount_saveButton1" class="submit btn btn-primary btn-lg" onclick="submitProfile();">
                        <span id="save"><spring:message code="save"/></span>
                        <span id="saveAndGetBGC" style="display: none"><spring:message code="saveAndGetBGC"/></span>
                    </div>
                </div>

            </li>

        </ul>

    </fieldset>

</form>
