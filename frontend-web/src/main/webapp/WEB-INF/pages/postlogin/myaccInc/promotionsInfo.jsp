<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="campaignsInfoWrapper">

    <h4 id="campaignsInfoHeader"><spring:message code="myprofile.campaigns.header"/></h4>
    <p id="campaignsInfoError" class="alert alert-danger alert-dismissable" style="display: none"></p>

    <div id="campaignsInfo"></div>

</div><%-- #campaignsInfoWrapper --%>

<script id="campaignsInfoBlockTemplate" type="text/x-jquery-tmpl">
    <div class="campaignBlock">
        <h5 class="campaignTitle">{{= title}}</h5>
        <p class="campaignConditions">{{= conditions}}</p>
        <p class="campaignTurnover">{{= turnover}}</p>
        <a class="campaignDepositLink btn btn-primary btn-sm" href="/postlogin/payment/deposit">{{= button}}</a>
    </div>
</script>

<script>
    $(function () {
        var $tab = $('#profile_promotions_tab'),
            $error = $('#campaignsInfoError'),
            $content = $('#campaignsInfo'),
            $blockTemplate = $('#campaignsInfoBlockTemplate'),
            campaignsNumber = 0;

        $.when(getPaymentProviders()).done(function (map) {
            var campaigns = map.campaigns || [],
                currency = getCurrencySymbol(map.currency) || '';

            for (var i = 0; i < campaigns.length; i++) {
                var campaign = campaigns[i];

                if (campaign.campaignId && !campaign.promoCode) {
                    var campaignTitle = campaign.title,
                        amount = retrieveCampaignBonusAmount(campaign, currency),
                        campaignAmount = amount.bonusAmount,
                        maxBonusAmount = amount.maxBonusAmount,
                        campaignMinDeposit = currency + (campaign.minDepositAmount || 0).toFixed(2),
                        campaignMaxDeposit = currency + (campaign.maxDepositAmount || 0).toFixed(2),
                        campaignTurnover = campaign.turnoverFactor || 0,
                        campaignCapEnabled = !!campaign.maxAmountCapEnabled,
                        templateVariables = {
                            title: campaignTitle,
                            conditions: replaceParams((campaignCapEnabled ?
                                    (campaign.amountType == "percentage" ? availableCampaignConditions3 : availableCampaignConditions1) :
                                    availableCampaignConditions2), [campaignAmount, campaignMinDeposit, campaignMaxDeposit, maxBonusAmount]),
                            turnover: replaceParams(availableCampaignTurnover, [campaignTurnover]),
                            button: availableCampaignButton
                        };

                    $blockTemplate.tmpl(templateVariables).appendTo($content);
                    campaignsNumber++;
                }
            }

            if (!campaignsNumber) {
                $error.show().html(noAvailableCampaigns);
            }
        });
    });
</script>