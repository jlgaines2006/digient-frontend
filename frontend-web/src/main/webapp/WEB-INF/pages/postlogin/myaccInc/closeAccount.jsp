<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form id="closeAccountForm" class="myAccForm" action="">

    <fieldset>

        <legend><spring:message code="myaccount.status.header"/></legend>

        <p id="closeAccount_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

        <p><spring:message code="myaccount.status"/><strong><spring:message code="myaccount.status.active"/></strong></p>
        <p><spring:message code="closeAccount.desc"/></p>

        <ul class="list-unstyled">

            <li class="row has-feedback has-datepicker">
                <label class="control-label col-sm-3">
                    <label class="radio-inline">
                        <input type="radio" name="closeAccountRadio" value="tillDate" checked="checked" /> <spring:message code="closeAccount.tillDate"/>
                        <span class="birthdatetitle"><spring:message code="register.form.birthDateFormat"/></span>
                    </label>
                </label>
                <div class="col-sm-9">
                    <div class="form-control-datepicker">
                        <label for="closeAccountEndDate">
                            <i class="fa fa-calendar fa-lg"></i>
                        </label>
                        <input id="closeAccountEndDate" class="form-control" type="text" value="" maxlength="256">
                        <div class="form-control-feedback">
                            <span id="closeAccountEndDate_validOK"></span>
                        </div>
                    </div>
                    <span id="closeAccountEndDate_error" class="limitsFieldError help-block error-field"></span>
                </div>
            </li>

            <li class="row">
                <label class="control-label col-sm-12">
                    <label class="radio-inline">
                        <input type="radio" name="closeAccountRadio" value="forever" /> <spring:message code="closeAccount.permanently"/>
                    </label>
                </label>
            </li>

            <li class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="closeAccountBtn" class="submit saveButton btn btn-primary btn-lg" onclick="javascript: closeAccountPopup();"><spring:message code="myaccount.close"/></div>
                </div>
            </li>

        </ul>

    </fieldset>

</form>