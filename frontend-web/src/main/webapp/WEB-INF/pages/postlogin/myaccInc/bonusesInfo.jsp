<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="bonusesBalanceInfo" class="row">
    <div class="form-group">
        <div id="bonusPromoWrapper" class="col-sm-4">
            <label class="control-label">
                <spring:message code="bonuses.balance.promo"/>
            </label>
            <span id="bonunsesPromo" class="form-control"></span>
        </div>
        <div id="bonusTurnoverWrapper" class="col-sm-4">
            <label class="control-label">
                <spring:message code="bonuses.balance.turnover"/>
                <span class="progress-hint"><i class="fa fa-question fa-lg"></i><span class="alert alert-info"></span></span>
            </label>
            <div class="">
                <div id="bonunses_wagering_requirement" class="progress">
                    <div id="bonunses_wagering_requirement_value" class="progress-bar"></div>
                </div>
            </div>
        </div>
        <div id="bonusStatusWrapper" class="col-sm-4">
            <label class="control-label">
                <spring:message code="bonus.status"/>
            </label>
            <div class="radio-group">
                <div class="radio">
                    <label for="status-active">
                        <input id="status-active" name="bonusStatus" type="radio" value="active" onclick="updateBonusesList(0, null);">
                        <span><spring:message code="myprofile.bonuses.active"/></span>
                    </label>
                </div>
                <div class="radio">
                    <label for="status-all">
                        <input id="status-all" name="bonusStatus" type="radio" value="all" onclick="updateBonusesList(0, null);">
                        <span><spring:message code="myprofile.bonuses.all"/></span>
                    </label>
                </div>
            </div>
            <a id="promotionsHistoryLink" class="btn btn-default" href="/postlogin/transactions?promo"><spring:message code="myprofile.bonuses.history"/></a>
        </div>
    </div>
</div>

<!-- Bonus transactions -->
<div id="bonusesInfo">

    <div id="bonusesError" class="error alert alert-danger alert-dismissable" style="display: none"></div>

    <div class="h3 bonusesTitle"><spring:message code="bonuses.table.title"/></div>

    <p id="bonusesTitle"></p>

    <table id="bonusesTable" class="table table-striped" style="display: none;">

        <thead>
            <tr>
                <th><spring:message code="myprofile.bonuses.name"/></th>
                <th><spring:message code="myprofile.bonuses.dateReceived"/></th>
                <th><spring:message code="myprofile.bonuses.freespins"/></th>
                <th><spring:message code="myprofile.bonuses.total"/></th>
                <th><spring:message code="myprofile.bonuses.winnings"/></th>
                <th><spring:message code="myprofile.bonuses.turnover"/></th>
                <th><spring:message code="myprofile.bonuses.validUntil"/></th>
                <th><spring:message code="myprofile.bonuses.status"/></th>
                <th><spring:message code="myprofile.bonuses.games"/></th>
                <th><spring:message code="myprofile.bonuses.action"/></th>
            </tr>
        </thead>

        <tbody id="bonusesTableBody">
        </tbody>

    </table>

</div>