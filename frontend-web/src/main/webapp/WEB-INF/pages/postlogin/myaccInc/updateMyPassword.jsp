<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="updateMyPassword" class="myAccForm form-horizontal" method="POST" action="" name="updateMyPassword">

    <p id="passwordSaved" class="informationSaved alert alert-success alert-dismissable" style="display: none"></p>

    <fieldset>

        <p id="updatePassword_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

        <ul id="updatePassword_ul" class="list-unstyled">

            <li id="oldPassword_item" class="form-group has-feedback">
                <label class="control-label col-sm-3" for="oldPassword">
                    <spring:message code="myaccount.oldPassword"/>
                </label>
                <div class="col-sm-9">
                    <input id="oldPassword" class="form-control" type="password" name="oldPassword" value="">
                    <div class="form-control-feedback">
                        <i alt="?" title="<spring:message code="myaccount.oldPassword.hint"/>" class="fa fa-question fa-lg"></i>
                    </div>
                    <div id="oldPassword_error" class="registerFieldError help-block error-field"></div>
                </div>
            </li>


            <li id="newPassword_item" class="form-group has-feedback">
                <label class="control-label col-sm-3" for="newPassword">
                    <spring:message code="myaccount.newPassword"/>
                </label>
                <div class="col-sm-9">
                    <input id="newPassword" class="form-control" type="password" name="newPassword" value="">
                    <div class="form-control-feedback">
                        <i alt="?" title="<spring:message code="myaccount.newPassword.hint"/>" class="fa fa-question fa-lg"></i>
                        <span id="newPassword_validOK"></span>
                    </div>
                    <div id="newPassword_error" class="registerFieldError help-block error-field"></div>
                </div>
            </li>

            <li id="confirmNewPassword_item" class="form-group has-feedback">
                <label class="control-label col-sm-3" for="confirmNewPassword">
                    <spring:message code="myaccount.confirmNewPassword"/>
                </label>
                <div class="col-sm-9">
                    <input id="confirmNewPassword" class="form-control" type="password" name="confirmNewPassword" value="">
                    <div class="form-control-feedback">
                        <i alt="?" title="<spring:message code="myaccount.confirmNewPassword.hint"/>" class="fa fa-question fa-lg"></i>
                        <span id="confirmNewPassword_validOK"></span>
                    </div>
                    <div id="confirmNewPassword_error" class="registerFieldError help-block error-field"></div>
                </div>
            </li>

            <li class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="submit fancyboxBirthdayPopup btn btn-primary btn-lg" href="#birthdayConfirm"><spring:message code="save"/></div>
                </div>
            </li>

        </ul>

    </fieldset>

</form>