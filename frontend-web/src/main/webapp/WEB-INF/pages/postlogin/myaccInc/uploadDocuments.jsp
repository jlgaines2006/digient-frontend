<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form id="uploadDocuments" class="myAccForm form-horizontal" action="/billfold-api/player/document" method="POST" onsubmit="return validateFile(this)" enctype="multipart/form-data">

    <fieldset>

        <p id="uploadDocuments_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

        <div class="form-group">
            <label id="uploadDocumentsDesc" class="control-label col-sm-3">
                <spring:message code="upload.desc"/>
            </label>
            <div class="col-sm-9">
                <input type="file" name="file" />
                <small id="uploadDocumentsDescComment" class="help-block"><spring:message code="upload.desc.comment"/></small>
            </div>
        </div>

        <div id="uploadOptions">

            <div class="uploadOptionWrapper form-group">
                <label class="control-label col-sm-3" for="uploadNameField"><spring:message code="upload.docname"/></label>
                <div class="col-sm-9">
                    <input id="uploadNameField" class="form-control" type="text" name="name" value="" maxlength="256">
                </div>
            </div>

            <div class="uploadOptionWrapper form-group">
                <label class="control-label col-sm-3" for="uploadTypeSelector"><spring:message code="upload.typeselect"/></label>
                <div class="col-sm-9">
                    <select id="uploadTypeSelector" class="form-control" name="documentType">
                        <option value="address"><spring:message code="upload.address"/></option>
                        <option value="identity"><spring:message code="upload.identity"/></option>
                        <option value="creditcardfront"><spring:message code="upload.credit.card.front"/></option>
                        <option value="creditcardback"><spring:message code="upload.credit.card.back"/></option>
                    </select>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <input type="submit" class="btn btn-primary btn-lg" value="<spring:message code="upload.submit"/>" id="uploadDocumentButton">
            </div>
        </div>

        <table id="fileTable" class="table table-striped" style="display: none;">

            <thead>

                <tr>
                    <th>
                        <spring:message code="upload.filetable.name"/>
                    </th>
                    <th>
                        <spring:message code="upload.filetable.type"/>
                    </th>
                    <th>
                        <spring:message code="upload.filetable.approved"/>
                    </th>
                </tr>

            </thead>

            <tbody id="fileTableBody">
            </tbody>

        </table>

    </fieldset>

</form>

<script type="text/javascript">

    $(document).ready(function() {
        populateFileTable();
    });

</script>