<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="footer-fluid" class="row">
    <div class="container">
        <div id="footer" class="row">
            <div class="col-xs-12">

                <div id="aboutUsPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/aboutus.html" />
                </div>

                <div id="termsPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/terms.html" />
                </div>

                <div id="termsAcceptancePopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/termsAcceptance.html" />
                </div>

                <div id="responsibleGamblingPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/responsibleGambling.html" />
                </div>

                <div id="policyPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/policy.html" />
                </div>

                <div id="contactPopup" class="popup" style="display: none;">
                    <c:import url="/WEB-INF/pages/inc/contact.jsp" />
                </div>

                <div id="faqPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/faq.html" />
                </div>

                <div id="promotionsPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/promotions.html" />
                </div>

                <div id="affiliatesPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/affiliates.html" />
                </div>

                <div id="rulesPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/rules.html" />
                </div>

                <div id="bankingPopup" class="popup" style="display: none;">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/banking.html" />
                </div>

                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/footer/footerContentPopup.html" />

            </div>
        </div><%-- #footer --%>
    </div>
</div><%-- #footer-fluid --%>