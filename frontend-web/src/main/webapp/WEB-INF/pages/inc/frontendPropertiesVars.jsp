<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">

    // Update the variables used in whole the site (defined in frontend.properties)
    var userIsLogged = ${loginUser != null},
        passwordStrength = "${features.passwordStrength}",
        gameWindowOpening = "${features.gameWindowOpening}",
        liveGameWindowOpening = "${features.liveGameWindowOpening}",
        styledInputs = ${features.styledInputs},
        jackpotDisplayType = "${features.jackpotDisplayType}",
        firstDepositLock = ${features.firstDepositLock},
        promoLockRuleEnabled = ${features.promoLockRule},
        freeSpinsEnabled = ${features.freeSpins},
        languageSelectorType = "${features.languageSelectorType}",
        registerType = "${features.registerType}",
        defaultLanguage = "${features.defaultLanguage}",
        activationProcess = ${features.activationProcess},
        loginMode = ${features.loginMode},
        isDemoGame = "${param.thisIsDemo}",
        liveGameName = "${param.name}",
        gameProvider = "${param.provider}",
        promoLock = true,
        /**
         * Define type of loading games for "View more" button. Possible values are 'all' and 'pages'.
         * @params: "all", "pages"
         * If set to "all" all games will be loaded in 1 click of "View more" button.
         * In case of "pages" each click of "View more" button will be loaded new portion of games according to "FE_NUMBER_OF_GAMES_ON_HOME" property.
         */
        viewMoreType = "all",
        /**
         * Define type of opening Demo games for pre-logged in users
         * @params: "game", "loginRegisterPopup"
         */
        demoGameOpeningType = "game",
        /**
         * Is infinite scroll enabled/disabled
         * Amount of loaded games depends on viewMoreType option.
         */
        infiniteScroll = false;

    // Variables, related to Games List
    var oGamesList = {
        hasPagination: ${features.gameListPagination},
        itemsPerPage: parseInt('${features.numberOfGamesOnHome}') || 0,
        activeGroup: {
            array: [],
            name: '',
            isPaginated: true
        },
        currentList: {
            array: [],
            name: '',
            isPaginated: true,
            page: 0
        }
    };

    // Variables, related to fields
    var oFields = {
        identification: {  // format: { fieldId: 'identificationGroup' }
            passportNumber: 'passportNumber',
            ssn: 'ssn',
            citizenshipNumber: 'citizenshipNumber',
            foreignerNumber: 'foreignerNumber',
            documentType: 'other',
            documentNumber: 'other',
            documentIssuingCountry: 'other',
            documentIssuedDate: 'other',
            documentIssuedBy: 'other',
            residentialAddress: 'other',
            residentialHouseNumber: 'other',
            residentialCity: 'other',
            residentialZipCode: 'other'
        }
    };

</script>