<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!-- Available Bonuses   -->
<div id="availableBonuses" class="bonusesElement">

    <table id="availableBonusesTable_freespins" class="availableBonusesTable table table-striped" >
        <tbody id="availableBonusesTableBody_freespins"></tbody>
    </table>

</div>

<script>
    var freespinsGamesList = [];

    $(function () {
        $.when(ajaxCalls.getBonusInfo('active')).done(function (data) {
            var bonuses = data.bonuses || {},
                transactions = bonuses.transactions || [],
                availableFreeSpinsNumber = 0,
                tableHtml = '';

            for (var i = 0; i < transactions.length; i++) {
                var transaction = transactions[i],
                    name = transaction.name,
                    amount = transaction.freeSpins != null ? transaction.freeSpins : bonusfreeSpinsNA,
                    turnover = transaction.turnoverFactor || 1,
                    bonusId = transaction.freeSpinBonusId;

                if (transaction.games.length > 0) {
                    tableHtml +=
                        '<tr>' +
                            '<td>' +
                                '<a href="/postlogin/campaigns?bonuses" class="availableBonusLink">' + name + '</a>' +
                            '</td>' +
                            '<td>' + amount + '</td>' +
                            '<td class="availableBonusHint">' +
                                '<span class="progress-hint">' +
                                    '<i class="fa fa-question fa-lg"></i>' +
                                    '<span class="alert alert-info">' + replaceParams(availableBonusHint, [turnover]) + '</span>' +
                                '</span>' +
                            '</td>' +
                            '<td>' +
                                '<a onclick="showFreespinsGames(' + bonusId + ')">' + bonusGamesLink + '</a>' +
                            '</td>' +
                        '</tr>';

                    availableFreeSpinsNumber += (transaction.freeSpins || 0);
                }
                freespinsGamesList[bonusId] = extendGamesProperties(transaction.games, 'freeSpins');
            }

            $('#availableBonusesTableBody_freespins').html(tableHtml);
            $('#bonus_balance_value').html(availableFreeSpinsNumber);
            $('#bonus_span').toggleClass('hidden', !availableFreeSpinsNumber).toggleClass('is-shown', !!availableFreeSpinsNumber);
            $('#bonus_balance_wrapper').toggleClass('dropdown-off', !availableFreeSpinsNumber);
        });
    });

    function showFreespinsGames(id) {
        var $games = $('#games'),
            groupName = "freeSpins?freeSpinsId=" + id;

        if (!$games.length) {
            show_tab_content(groupName, true);
        }
        else {
            updateTabContent(freespinsGamesList[id], 'freeSpins');

            $('html, body').stop(true, true).animate({
                scrollTop: $games.offset().top
            }, 600);
        }
    }
</script>