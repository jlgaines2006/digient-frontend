<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="gamesSearchSortingWrapper">
    <select name="sorting" class="selectpicker" data-width="auto">
        <option value=""><spring:message code="gamesSearch.sortingUnsorted"/></option>
        <option value="new"><spring:message code="gamesSearch.sortingByNew"/></option>
        <option value="name"><spring:message code="gamesSearch.sortingByName"/></option>
    </select>
</div>