<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="availableCampaignsIndicator" class="hidden">

    <a id="availableCampaignsLink" href="/postlogin/campaigns?promotions">
        <span id="availableCampaignsNumber"></span> <span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
    </a>

    <script>
        $(function () {
            var $indicator = $('#availableCampaignsIndicator'),
                $campaignsNumber = $('#availableCampaignsNumber'),
                campaignsNumber = 0;

            $.when(getPaymentProviders()).done(function (map) {
                var campaigns = map.campaigns || [];

                for (var i = 0; i < campaigns.length; i++) {
                    var campaign = campaigns[i];
                    if (campaign.campaignId && !campaign.promoCode) {
                        campaignsNumber++;
                    }
                }

                $indicator.toggleClass('hidden', !campaignsNumber).toggleClass('is-shown', !!campaignsNumber);
                $campaignsNumber.html(campaignsNumber);
            });
        });
    </script>

</div>