<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<c:choose>

    <c:when test="${loginUser == null || loginUser.receiveEmail != true }">
        <c:import url="/WEB-INF/pages/inc/newsletter.jsp" />
    </c:when>

    <c:otherwise>
        <div id="loggedInBanner2">
            <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/banners/loggedInBanner2.html" />
        </div>
    </c:otherwise>

</c:choose>