<%@ taglib uri="http://granule.com/tags" prefix="g" %>
<!-- CSS files compressed with g:compress-->

<g:compress>

<!-- Bootstrap -->
<link href="/resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Select -->
<link href="/resources/css/bootstrap-select.min.css" rel="stylesheet">

<!-- Flex Slider -->
<link rel="stylesheet" href="/resources/css/flexslider.css" type="text/css" />

<!-- Fancybox -->
<link rel="stylesheet" href="/resources/css/jquery.fancybox.css" type="text/css" />

<!-- JavaScript Image Combobox (for language switcher) -->
<link rel="stylesheet" href="/resources/css/dd.css" type="text/css" />

<!-- jQuery UI -->
<link rel="stylesheet" href="/resources/css/jquery-ui-1.9.2.custom.min.css" type="text/css" />

<!-- Transaction page's pagination styles -->
<link rel="stylesheet" href="/resources/css/pagination.css" type="text/css" />

<!-- Main stylesheets -->
<link rel="stylesheet" type="text/css" href="/resources/css/application.css" media="screen" title="Normal" />

</g:compress>

<link rel="stylesheet" type="text/css" href="${cmsRoot}/css/customer.css" media="screen" title="Normal" />

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="${cmsRoot}/css/ie.css" media="screen" title="Normal" />
<![endif]-->

<!-- Font Awesome -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

