<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">
    $(function () {
        setTimeout(function () {
            registerOnload('${_lang == null ? features.defaultLanguage : _lang}');
        }, 10);
    });
</script>

<div id="registerSuccess" style="display: none">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-result.html" />
</div>

<div id="registerWrapper">

    <div id="registerWrapper_curtain" style="display: none"></div>

    <h4 class="h1"><spring:message code="register.form.title"/></h4>
    <p><spring:message code="register.form.description"/></p>

    <form id="register_form" class="registration" action="">

        <p id="register_error" class="error alert alert-danger alert-dismissable"></p>

        <div class="row">
            <div id="register_firstcol" class="register-col col-sm-4">
                <ul class="list-unstyled">

                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/email.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/password.jsp"/>

                    <div id="registerSeparator"></div>

                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/firstName.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/birthPlace.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/address.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/country.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/currency.jsp"/>

                </ul>
            </div>
            <div id="register_secondcol" class="register-col col-sm-4">
                <ul class="list-unstyled">

                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/confirmEmail.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/confirmPassword.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/lastName.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/maidenName.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/birthDate.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/city.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/zipCode.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/phoneNumber.jsp"/>

                </ul>
            </div>
            <div id="register_thirdcol" class="register-col col-sm-4">
                <ul class="list-unstyled">

                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/nickName.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/userName.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/gender.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/middleName.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/skypeId.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/weChatId.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/qqId.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/identification.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/passportNumber.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/ssn.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/citizenshipNumber.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/foreignerNumber.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/documentType.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/documentNumber.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/documentIssuingCountry.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/documentIssuedDate.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/documentIssuedBy.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/residentialAddress.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/residentialCity.jsp"/>
                    <jsp:include page="/WEB-INF/pages/inc/registrationFields/residentialZipCode.jsp"/>

                </ul>
            </div>
        </div>

        <div id="register_footer" class="row">
            <div class="col-xs-8">

                <input type="hidden" name="ageChecked" id="ageChecked" />

                <jsp:include page="/WEB-INF/pages/inc/registrationFields/policyChecked.jsp"/>
                <jsp:include page="/WEB-INF/pages/inc/registrationFields/receiveEmail.jsp"/>

            </div>
            <div class="col-xs-4">

                <jsp:include page="/WEB-INF/pages/inc/registrationFields/campaigns.jsp"/>
                <jsp:include page="/WEB-INF/pages/inc/registrationFields/promoCode.jsp"/>

            </div>
            <div class="col-xs-12">
                <a id="signUpButton" class="btn btn-primary btn-lg disabled" href="javascript: register();"><spring:message code="register.form.registerBtn"/></a>
            </div>
        </div>

        <div id="registerFormMandatory"><spring:message code="register.form.mandatory"/></div>
        <p class="note"><spring:message code="register.form.note"/></p>

    </form>

</div>
