<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="quickDepositFormWrapper">

    <form id="quickDepositForm" action="/postlogin/payment/deposit" method="get">
        <div class="input-group">
            <input id="quickDepositAmount" class="form-control" type="text" name="amount" value="" placeholder="<spring:message code='header.cashier.placeholder'/>">

            <div class="input-group-btn">
                <div class="btn-group">
                    <button id="quickDepositCurrentMethod" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="display:none">
                        <span class="providerTitle"></span> <span class="caret"></span>
                    </button>

                    <div id="quickDepositMethodsDropdown" class="dropdown-menu pull-right">
                        <div id="quickDepositMethods" class="btn-group-vertical" data-toggle="buttons"></div>
                    </div>
                </div>

                <input id="quickDepositSubmit" class="btn btn-primary disabled" type="submit" value="<spring:message code='header.cashier.btn' />">
            </div>
        </div>
    </form>

    <script>
        $(function () {
            var $form = $('#quickDepositForm'),
                $amount = $('#quickDepositAmount'),
                $methods = $('#quickDepositMethods'),
                $currentMethod = $('#quickDepositCurrentMethod'),
                $currentMethodTitle = $('.providerTitle', $currentMethod),
                $submit = $('#quickDepositSubmit'),
                isAmountValidationDisabled = false;

            $.when(getPaymentProvidersForFooter()).done(function (map) {
                if (!hasErrors(map) && map.methods.length) {
                    var htmlDepositMethods = '',
                        hasDefaultMethod = false;

                    for (var i = map.methods.length - 1; i >= 0; i--) {
                        var methodID = map.methods[i].method,
                            providerID = map.methods[i].provider,
                            providerTitle = getPaymentProv(providerID, methodID),
                            isDefault = map.methods[i].defaultMethod;

                        if (i == 0 && !isDefault && !hasDefaultMethod) {
                            isDefault = true;
                        }

                        htmlDepositMethods =
                            '<label class="btn btn-default' + (isDefault ? ' active' : '') + '">' +
                                '<input type="radio" name="method" value="' + methodID + '"' + (isDefault ? ' checked="checked"' : '') + '> ' +
                                '<span class="providerTitle">' + providerTitle + '</span>' +
                            '</label>' + htmlDepositMethods;

                        if (isDefault) {
                            $currentMethodTitle.html(providerTitle);
                            hasDefaultMethod = true;
                        }
                    }

                    $methods.html(htmlDepositMethods).find(':radio').on('change', function () {
                        $currentMethodTitle.html( $(this).next('.providerTitle').html() );
                        isAmountValidationDisabled = $(this).val() === '150001';  // disable decimal number validation if method is "promo code"
                        checkAmount();
                    });

                    if (htmlDepositMethods) {
                        $currentMethod.show();
                    }
                }
            });

            $amount.on('keyup blur input paste', function () {
                checkAmount();
            }).on('keypress', function (event) {
                if (!isAmountValidationDisabled) {
                    validateInputs(event, 'decimal');
                }
            });

            $form.on('submit', function (event) {
                if (!checkAmount()) {
                    event.preventDefault();
                }
            });

            function checkAmount() {
                var amount = $amount.val(),
                    parsedAmount = parseFloat(amount) || 0,
                    isValid = isAmountValidationDisabled ? !!amount : !!parsedAmount && /^[0-9]+[.]?[0-9]*$/.test(amount);
                $submit.toggleClass('disabled', !isValid);
                return isValid;
            }
        });
    </script>

</div>
