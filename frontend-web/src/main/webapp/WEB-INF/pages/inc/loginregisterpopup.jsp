<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="loginRegisterPopup_wrapper" class="row">

    <div id="loginRegisterPopup_left" class="col-sm-6">

        <h4 class="h1"><spring:message code="login.title" /></h4>
        <p><spring:message code="login.h1" /></p>

        <form id="loginRegisterPopup_form" action="" autocomplete="off">

            <input type="hidden" name="action" value="LoginAction" />

            <p id="loginRegisterPopup_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

            <fieldset>

                <ul id="loginRegisterPopup_fields" class="list-unstyled">

                    <li class="form-group">
                        <label class="control-label" for="loginRegisterPopup_email"><spring:message code="email"/></label>
                        <input id="loginRegisterPopup_email" class="form-control" type="text" name="email" />
                    </li>

                    <li class="form-group">
                        <label class="control-label" for="loginRegisterPopup_password"><spring:message code="password"/></label>
                        <input id="loginRegisterPopup_password" class="form-control" type="password" name="password" />
                    </li>

                </ul>

                <a id="loginRegisterPopup_loginbutton" class="btn btn-primary btn-lg" href="javascript: loginFromPopup();" title="<spring:message code="header.loginLabel"/>" ><spring:message code="header.loginLabel"/></a>
                <a id="loginRegisterPopup_forgotpassword" class="fancyboxForgotPwd btn btn-link" href="#forgotpasswordPopup" onclick="javascript: emptyForgotElement();"><spring:message code="header.login.forgotPassword" /></a>

            </fieldset>

        </form>

    </div>

    <div id="loginRegisterPopup_right" class="col-sm-6">

        <h4 class="h1"><spring:message code="register.popup.title"/></h4>
        <p><spring:message code="register.popup.desc"/></p>

        <a id="loginRegisterPopup_registerbutton" class="fancyboxPopupRegister btn btn-primary btn-lg" onclick="javascript: setPromoCode('');" href="#registerPopup"><spring:message code="register.title" /></a>

    </div>

</div>