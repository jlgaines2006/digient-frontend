<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="mainMenuTemplate" type="text/x-jquery-tmpl">

    {{each games}}
    {{if $value.gameDisabled != true}}

    <c:choose>

        <c:when test="${loginUser != null}">

            {{if $value.tabName == "live" || $value.liveGame }}
                <li><a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}" class="liveGameLink promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}}">{{= $value.title}}</a></li>
            {{else}}
                <li><a href="/realGame?name={{= $value.name}}&provider={{= $value.provider}}" class="gameLink promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}}">{{= $value.title}}</a></li>
            {{/if}}

        </c:when>
        <c:otherwise>

            {{if $value.hideDemoUrl != true}}

                {{if $value.tabName == "live" || $value.liveGame }}
                    <li><a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="liveGameLink gameLink_provider_{{= $value.provider}}">{{= $value.title}}</a></li>
                {{else}}
                    <li><a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="gameLink gameLink_provider_{{= $value.provider}}">{{= $value.title}}</a></li>
                {{/if}}

            {{else}}
                <li><a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','real')" class="fancyboxPopup">{{= $value.title}}</a></li>
            {{/if}}

        </c:otherwise>

    </c:choose>

    {{/if}}
    {{/each}}

</script>

<ul id="menu" ${features.gameGroupDropdownsType != 'accordion' ? 'class="nav nav-pills"' : ''}>
</ul>

<script type="text/javascript">
    populate_gameGroupItems(${features.gameGroupDropdowns},'${features.gameGroupDropdownsType}','${features.excludeFromGameGroupMenu}','${features.numberOfGamesOnHome}','${features.maxNumberOfDropDownItems}',${features.gameListPagination});
</script>

