<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="contactUsWrapper">
<div id="contacttitletab"></div>

<div id="contactSuccess" style="display: none">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/contact-us/contactus-thanks.html" />
</div>

<div id="contactWrapper">

    <div id="contactWrapper_curtain" style="display: none"></div>

    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/contact-us/contactus-header.html" />

    <p id="contact_error" class="error alert alert-danger alert-dismissable" style="display: none"></p>

    <form id="contactForm" class="contactForm form-horizontal" action="">

        <fieldset>

            <ul class="list-unstyled">

                <li class="form-group has-feedback">
                    <label class="control-label col-sm-3" for="contact_firstName">
                        <spring:message code="firstName"/>
                    </label>
                    <div class="col-sm-9">
                        <c:if test="${loginUser != null && loginUser.firstName != null}">
                            <input id="contact_firstName" class="form-control" disabled="disabled" type="text" name="firstName" value="${loginUser.firstName}"/>
                        </c:if>
                        <c:if test="${loginUser != null && loginUser.firstName == null}">
                            <input id="contact_firstName" class="form-control" type="text" name="firstName" value=""/>
                        </c:if>
                        <c:if test="${loginUser == null}">
                            <input id="contact_firstName" class="form-control" maxlength="45" type="text" name="firstName" value=""/>
                        </c:if>
                        <span id="contact_firstName_validOK" class="form-control-feedback"></span>
                        <span id="contact_firstName_error" class="contactFieldError help-block error-field"></span>
                    </div>
                </li>

                <li class="form-group has-feedback">
                    <label class="control-label col-sm-3" for="contact_lastName">
                        <spring:message code="lastName"/>
                    </label>
                    <div class="col-sm-9">
                        <c:if test="${loginUser != null && loginUser.lastName != null}">
                            <input id="contact_lastName" class="form-control" disabled="disabled" type="text" name="lastName" value="${loginUser.lastName}"/>
                        </c:if>
                        <c:if test="${loginUser != null && loginUser.lastName == null}">
                            <input id="contact_lastName" class="form-control" type="text" name="lastName" value=""/>
                        </c:if>
                        <c:if test="${loginUser == null}">
                            <input id="contact_lastName" class="form-control" maxlength="45" type="text" name="lastName" value=""/>
                        </c:if>
                        <span id="contact_lastName_validOK" class="form-control-feedback"></span>
                        <span id="contact_lastName_error" class="contactFieldError help-block error-field"></span>
                    </div>
                </li>

                <li class="form-group has-feedback">
                    <label class="control-label col-sm-3" for="contact_email">
                        <spring:message code="email"/>
                    </label>
                    <div class="col-sm-9">
                        <c:if test="${loginUser != null}">
                            <input id="contact_email" class="form-control" disabled="disabled" type="text" name="email" value="${loginUser.email}"/>
                        </c:if>
                        <c:if test="${loginUser == null}">
                            <input id="contact_email" class="form-control" type="text" maxlength="45" name="email" value=""/>
                        </c:if>
                        <span id="contact_email_validOK" class="form-control-feedback"></span>
                        <span id="contact_email_error" class="contactFieldError help-block error-field"></span>
                    </div>
                </li>

                <li id="li_subjectId" class="form-group has-feedback">
                    <label class="control-label col-sm-3" for="contact_subjectId">
                        <spring:message code="contact.subject"/>
                    </label>
                    <div class="col-sm-9">
                        <div class="select">
                            <select id="contact_subjectId" class="form-control" name="subjectId">
                                <option value="" selected="true" disabled="disabled"><spring:message code="contact.subject.choose"/></option>
                                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/contact-us/contactus-subject.html" />
                            </select>
                        </div>
                        <span id="contact_subjectId_validOK" class="form-control-feedback"></span>
                        <span id="contact_subjectId_error" class="contactFieldError help-block error-field"></span>
                    </div>
                </li>

                <li id="li_specifySubjectId" class="form-group has-feedback">
                    <label class="control-label col-sm-3" for="contact_specifySubject">
                        <spring:message code="contact.specify"/>
                    </label>
                    <div class="col-sm-9">
                        <input id="contact_specifySubject" class="form-control" maxlength="80" type="text" name="subject" value="<spring:message code='contact.title'/>"/>
                        <span id="contact_specifySubject_validOK" class="form-control-feedback"></span>
                        <span id="contact_specifySubject_error" class="contactFieldError help-block error-field"></span>
                    </div>
                </li>

                <li class="form-group has-feedback">
                    <label class="control-label col-sm-3" for="contact_message">
                        <spring:message code="contact.message"/>
                    </label>
                    <div class="col-sm-9">
                        <textarea id="contact_message" class="form-control" maxlength="1024" rows="10" name="body" value=""></textarea>
                        <span id="contact_message_validOK" class="form-control-feedback"></span>
                        <span id="contact_message_error" class="contactFieldError help-block error-field"></span>
                    </div>
                </li>

                <li id="li_sendMeCopy" class="row">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div class="checkbox">
                            <label for="contact_sendMeCopy">
                                <input id="contact_sendMeCopy" type="checkbox" name="sendMeCopy" checked="checked"/>
                                <spring:message code="contact.copy"/>
                            </label>
                        </div>
                    </div>
                </li>

                <input type="hidden" name="action"  value="RequestContactUsAction"/>
                <input type="hidden" name="language"  value="${_lang == null ? features.defaultLanguage : _lang}"/>

                <li class="row">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div id="contact_sendMessage" class="btn btn-primary btn-lg" onclick="javascript: submitContactUsForm();">
                            <spring:message code="contact.btn.sent"/>
                        </div>
                    </div>
                </li>

            </ul>

        </fieldset>

        <c:choose>

            <c:when test="${loginUser == null}">
                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/banners/preLoginContactUsBanner.html" />
            </c:when>

            <c:otherwise>
                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/banners/loggedInContactUsBanner.html" />
            </c:otherwise>

        </c:choose>

    </form>

</div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        contactOnload();
    });

</script>