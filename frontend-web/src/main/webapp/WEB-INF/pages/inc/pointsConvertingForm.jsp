<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="pointsConvertingFormWrapper">

    <form id="pointsConvertingForm" action="">
        <div class="row">
            <div class="col-xs-6">
                <div class="input-group">
                    <input id="pointsConvertingAmount" class="form-control" type="text" value="" onkeypress="validateInputs(event, 'decimal');">

                    <div class="input-group-btn">
                        <button id="pointsRefreshButton" class="btn btn-default" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
                    </div>
                </div>

                <div id="pointsConvertingRate"></div>
            </div>

            <div class="col-xs-6">
                <button id="pointsConvertingSubmit" class="btn btn-primary disabled" type="submit">
                    <spring:message code="header.claim.default" />
                </button>
            </div>
        </div>
    </form>

    <script>
        $(function () {
            var $form = $('#pointsConvertingForm'),
                $rate = $('#pointsConvertingRate'),
                $amount = $('#pointsConvertingAmount'),
                $submit = $('#pointsConvertingSubmit'),
                $refresh = $('#pointsRefreshButton'),
                totalAmount = 0;

            $.when(getLoyaltyRate(), getBalance()).done(function (rate, balance) {
                rate = rate[0];
                balance = balance[0];
                if (!hasErrors(rate) && !hasErrors(balance)) {
                    var value = rate.value || 0,
                        type = rate.type || '',  // "cash" or "promo"
                        examplePoints = 500,

                        exampleCash = getCurrencySymbol(balance.currency) + ' ' + (examplePoints * value).toFixed(2);
                    if (type == "cash") {
                        $("#pointsConvertingSubmit").html('<spring:message code="header.claim.cash" />');
                    }
                    else if (type == "promo") {
                        $("#pointsConvertingSubmit").html('<spring:message code="header.claim.promo" />');
                    }
                    else {
                        $("#pointsConvertingSubmit").html('<spring:message code="header.claim.default" />');
                    }
                    $rate.html(replaceParams(window['pointsExchangeRate' + (type ? type[0].toUpperCase() + type.slice(1) : '')], [examplePoints, exampleCash]));
                }
                if (!hasErrors(balance)) {
                    $amount.val(totalAmount = balance.awardPoints);
                    checkAmount();
                }
            });

            $amount.on('keyup', function () {
                checkAmount();
            }).on('keydown', function () {
                changeSubmitButtonStatus();
            }).on('input paste', function () {
                checkAmount();
                changeSubmitButtonStatus();
            }).on('focus', function () {
                $(this).val('');
                checkAmount();
            }).on('blur', function () {
                if (!$(this).val().length) $(this).val(totalAmount);
                checkAmount();
                changeSubmitButtonStatus();
            });

            $refresh.on('click', function () {
                refreshPoints();
            });

            $form.on('submit', function (event) {
                event.preventDefault();

                if (checkAmount()) {
                    $submit.addClass('disabled');
                    changeSubmitButtonStatus();

                    $.when(convertLoyaltyPoints(parseFloat($amount.val()) || 0)).done(function (response) {
                        if (!hasErrors(response)) {
                            refreshPoints(true);
                            showBalance();
                            changeSubmitButtonStatus('success');
                        }
                        else {
                            changeSubmitButtonStatus('error');
                        }
                    }).fail(function () {
                        changeSubmitButtonStatus('error');
                    }).always(function () {
                        $submit.removeClass('disabled');
                    });
                }
            });

            function refreshPoints(forcibly) {
                $refresh.addClass('disabled');
                $.when(getBalance(forcibly)).done(function (balance) {
                    if (!hasErrors(balance)) {
                        $amount.val(totalAmount = balance.awardPoints);
                        checkAmount();
                    }
                }).always(function () {
                    $refresh.removeClass('disabled');
                });
                changeSubmitButtonStatus();
            }

            function checkAmount() {
                var amount = $amount.val(),
                    parsedAmount = parseFloat(amount) || 0,
                    isValid = !!parsedAmount && /^[0-9]+[.]?[0-9]*$/.test(amount) && parsedAmount > 0 && parsedAmount <= totalAmount;
                $submit.toggleClass('disabled', !isValid);
                return isValid;
            }

            function changeSubmitButtonStatus(status) {
                switch (status) {
                    case 'success':
                        $submit.removeClass('btn-primary btn-danger').addClass('btn-success');
                    break;
                    case 'error':
                        $submit.removeClass('btn-primary btn-success').addClass('btn-danger');
                    break;
                    default:
                        $submit.removeClass('btn-danger btn-success').addClass('btn-primary');
                    break;
                }
            }
        });
    </script>

</div>
