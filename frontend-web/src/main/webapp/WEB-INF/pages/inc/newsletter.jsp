<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<div id="newsLetterJoin">

    <form id="newsletter_form">

        <fieldset>
            <spring:message code="newsletter.title" />
            <br />

            <c:choose>
                <c:when test="${loginUser == null}">
                    <input type="text" name="email" id="newEmail" maxlength="45" value="" placeholder="<spring:message code="newsletter.email" />"/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="email" id="newEmail" value="${loginUser.email}" />
                </c:otherwise>
            </c:choose>

            <input type="hidden" name="language" value="${_lang == null ? features.defaultLanguage : _lang}">
            <a title="" id="newsletterButton" class="fancyboxPopup" href="#newsletterConfirm" onclick="javascript: isNewsEmail();"><spring:message code="newsletter.get" /></a>

        </fieldset>

    </form>

</div>

<div id="newsletterConfirm" class="popup" style="display: none;">

    <div id="newsletterConfirm_content">

        <p id="newsMsg"></p>
        <div id="newComfirmNo" class="confirm" title="" onclick="javascript: $.fancybox.close();" ><spring:message code="newsletter.confirm.no" /></div>
        <div id="newComfirmyes" class="confirm" title="" onclick="javascript: submitNewsForm();" ><spring:message code="newsletter.confirm.yes" /></div>
        <div id="newCancel" class="confirm" title="" onclick="javascript: $.fancybox.close(); $('#newEmail').focus();" style="display:none"><spring:message code="birthdate.question.cancel" /></div>

        <c:choose>
            <c:when test="${loginUser == null}">
                <div id="newsletterOk" class="confirm" title="" onclick="javascript: $.fancybox.close(); $('#newEmail').val('');" style="display:none">OK</div>
            </c:when>
            <c:otherwise>
                <div id="newsletterOk" class="confirm" title="" onclick="javascript: $.fancybox.close(); $('#newsLetterJoin').hide('');" style="display:none">OK</div>
            </c:otherwise>
        </c:choose>

    </div>

</div>