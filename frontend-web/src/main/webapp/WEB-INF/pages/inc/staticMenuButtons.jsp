<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/subheader/staticMenuButtons.html" />

<script type="text/javascript">

    $(document).ready(function() {

        // If url parameter currentMenuButton is found, make the correct button "active" (not on home page, though)
        var urlParam = getParamValue("currentMenuButton");

        if(urlParam != null && $("#slider").length == 0) {
            $("#"+urlParam+"_toplink").addClass("staticLinkActive");
        }

        /* login pop for live games in case user is not logged in */
        <c:choose>
            <c:when test="${loginUser != null}">
                $("#ezugiLive_toplink").addClass("liveGameListLink gameLink_provider_4 promoMoneyEnabled_true")
                    .attr("href","/liveGameWindow?name=LiveBlackJack&provider=4");

                $("#gglLive_toplink").addClass("liveGameListLink gameLink_provider_19 promoMoneyEnabled_true")
                    .attr("href","/liveGameWindow?name=GglBlackjack&provider=19");
            </c:when>

            <c:otherwise>
                $("#ezugiLive_toplink,#gglLive_toplink,#liveCasino_toplink").addClass("fancyboxPopup")
                    .attr("href","#loginregisterPopup");
            </c:otherwise>
        </c:choose>

    });

</script>