<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="header-fluid" class="row">
    <div class="container">
        <div id="header" class="row">

            <div id="logoPlaceholder" class="col-sm-6">
                <a href="javascript: homeLink();" id="logo">

                    <div id="slogan">
                        <spring:message code="header.slogan"/>
                    </div>
                </a>
            </div>

            <div id="headerFormPlaceholder" class="col-sm-6">
                <div id="headerchat">
                    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/pages/boldChat.html" />
                </div>

                <c:choose>
                    <c:when test="${loginUser == null}">
                        <form action="" id="login_form" class="form-inline" autocomplete="on" onkeypress="return event.keyCode != 13;">

                            <input type="hidden" name="action" value="LoginAction" />
                            <h4 class="loginformTitle"><spring:message code="header.login.connexion" /></h4>
                            <div id="login_error" class="error alert alert-danger alert-dismissable" style="display: none;"></div>

                            <fieldset>

                                <div id="loginEmailWrapper" class="form-group inline-block-xs block-sm inline-block-md">
                                    <label class="sr-only" for="loginEmail"><spring:message code="email"/></label>
                                    <input id="loginEmail" class="form-control" type="text" name="email" placeholder="<spring:message code="email"/>" />
                                </div>
                                <div id="loginPasswordWrapper" class="form-group inline-block-xs block-sm inline-block-md">
                                    <label class="sr-only" for="password"><spring:message code="password"/></label>
                                    <input id="password" class="form-control" type="password" name="password" placeholder="<spring:message code="password"/>" />
                                </div>

                                <a id="loginButton" class="loginformButton loginButtonBkg btn btn-primary" href="javascript: login();" title="<spring:message code="header.loginLabel"/>" ><spring:message code="header.loginLabel"/></a>

                                <div id="rememberMeBox" class="checkbox inline-block-xxs">
                                    <label for="rememberMe">
                                        <input id="rememberMe" name="check" type="checkbox" value="None" /> <spring:message code="header.login.rememberMe" />
                                    </label>
                                </div>

                                <a id="registerButton" class="fancyboxPopupRegister loginformButton loginButtonBkg btn btn-default" onclick="setPromoCode('');" href="#registerPopup" title="<spring:message code="register.form.title" />"><spring:message code="register.title" /></a>

                                <a id="forgotPass" class="fancyboxForgotPwd" href="#forgotpasswordPopup" onclick="emptyForgotElement();" title="<spring:message code="forgetpassword.title" />"><spring:message code="header.login.forgotPassword" /></a>

                                <div id="languageWrapper_login">
                                    <c:choose>
                                        <c:when test="${features.languageSelectorType == 'dropdown'}">
                                            <select id="changeLanguage">
                                            </select>
                                        </c:when>
                                        <c:when test="${features.languageSelectorType == 'buttons'}">
                                            <div id="changeLanguage">
                                            </div>
                                        </c:when>
                                    </c:choose>
                                </div>

                                <button type="submit" id="loginSubmitHidden"></button>

                            </fieldset>

                        </form>

                        <div id="registerPopup" class="popup" style="display: none;">
                        <c:choose>
                            <c:when test="${features.registerType == '2steps'}">
                                <c:import url="/WEB-INF/pages/inc/registerSteps.jsp" />
                            </c:when>
                            <c:otherwise>
                                <c:import url="/WEB-INF/pages/inc/register.jsp" />
                            </c:otherwise>
                        </c:choose>
                        </div>

                        <div id="forgotpasswordPopup" class="popup" style="display: none;">
                            <c:import url="/WEB-INF/pages/inc/forgotpassword.jsp" />
                        </div>

                        <div id="loginregisterPopup" class="popup" style="display: none;">
                            <c:import url="/WEB-INF/pages/inc/loginregisterpopup.jsp" />
                        </div>

                    </c:when>
                    <c:otherwise>
                        <%-- we have 2 different pot login forms. Standard view with all balances is postLoginForm.jsp. New form with balance and bonus dropdown is postLoginFormModern.jsp --%>
                        <c:import url="/WEB-INF/pages/inc/postLoginFormModern.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>

            <c:if test="${loginUser != null}">
                <div id="quickDepositFormPlaceholder" class="col-sm-6">
                    <c:if test="${features.quickDepositForm}">
                        <c:import url="/WEB-INF/pages/inc/quickDepositForm.jsp" />
                    </c:if>
                </div>

                <div id="pointsConvertingFormPlaceholder" class="col-sm-6">
                    <c:if test="${features.pointsConvertingForm}">
                        <c:import url="/WEB-INF/pages/inc/pointsConvertingForm.jsp" />
                    </c:if>
                </div>
            </c:if>

            <c:if test="${features.footerType == 'Static'}">
                <div id="termsPopup" class="popup" style="display: none;">
                </div>

                <div id="termsAcceptancePopup" class="popup" style="display: none;">
                </div>

                <div id="policyPopup" class="popup" style="display: none;">
                </div>
            </c:if>

            <div id="lockPopup" class="popup" style="display: none;">
                <c:import url="/WEB-INF/pages/inc/lockPopup.jsp" />
            </div>

            <div id="timeReachedPopup" class="popup" style="display: none;">
                <c:import url="/WEB-INF/pages/postlogin/limitsInc/timeReachedConfirm.jsp" />
            </div>

        </div><%-- #header --%>
    </div>
</div><%-- #header-fluid --%>

<script type="text/javascript">

    $(document).ready(function(){

        // Prevent the default submit of login form
        $('#login_form').submit(function() {
            return false;
        });

        // Populate language selector in the header
        populateLanguageMenu('${_lang == null ? features.defaultLanguage : _lang}');

        // If in the url logout - then logging out
        var url = window.location.href.toString();
        if (url.indexOf("?logout") != -1) logout();

        // Update balance
        <c:if test="${loginUser != null}">

            // Refresh free spins
            <c:if test="${features.freeSpins != 'false'}">
                //refreshFreeSpins();
            </c:if>

            showBalance();
            balanceTimer();
        </c:if>

        // Update footer with the payment providers
        updatePaymentProvidersFooter();

        // T&C, Privacy Policy. Add content to fancybox
        populatePopupsViaAjax(["terms", "policy", "termsAcceptance"], "${cmsRoot}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/");
    });

</script>