<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="totaljackpot">

    <%--<div id="totaljackpotleft">
        <spring:message code="totalJackPot.total"/>

        <div id="totaljackpotlefttext">
            <spring:message code="totalJackPot.jackpot"/>
        </div>
    </div>

    <div id="totaljackpotcenter">
        $107,993.23
    </div>

    <div id="totaljackpotbutton">
        <spring:message code="totalJackPot.button"/>
    </div>--%>

    <div id="totaljackpotTitle">
        <img src="${cmsRoot}/languages/${_lang == null ? features.defaultLanguage : _lang}/images/jackpotLogo.png" alt="<spring:message code="totalJackPot.jackpot"/>" />
    </div>

    <div id="totaljackpotValue">
    </div>

</div>

<script type="text/javascript">

    // Get total jackpot
    <c:choose>

        <c:when test="${loginUser == null}">

            $.getJSON("/billfold-api/config/currencies", null, function(map) {

                if(map != null) {
                    getTotalJackpot(map.currencies[0]);
                }
            });

        </c:when>
        <c:otherwise>

            getTotalJackpot('${loginUser.registrationCurrency}');

        </c:otherwise>

    </c:choose>

</script>
