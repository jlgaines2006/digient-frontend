<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="one-click-deposit-widget" style="display:none">

    <div id="one-click-deposit" class="dropup">

        <div id="one-click-deposit-dropdown" class="one-click-deposit dropdown-toggle" data-toggle="dropdown">
            <span class="btn btn-primary"><spring:message code='ocdWidget.label'/></span>
        </div>

        <form id="one-click-deposit-form" action="" class="one-click-deposit-details dropdown-menu">
            <div id="one-click-deposit-title"><spring:message code='ocdWidget.label'/></div>

            <div class="form-group">
                <label id="one-click-deposit-label" for="one-click-deposit-amount"><spring:message code='ocdAmount'/></label>
                <input id="one-click-deposit-amount" class="form-control" type="text" name="amount" value="" placeholder="<spring:message code='ocdWidget.placeholder'/>">
            </div>

            <div class="form-group">
                <label id="one-click-deposit-current-card-label"><spring:message code='ocdCard'/></label>
                <div id="one-click-deposit-cards-dropdown">
                    <button id="one-click-deposit-current-card" class="btn btn-default" type="button" style="display:none">
                        <span class="card-title"></span> <span class="caret"></span>
                    </button>

                    <div id="one-click-deposit-cards" class="btn-group-vertical" data-toggle="buttons"></div>
                </div>
            </div>

            <div id="one-click-deposit-campaigns-wrapper" class="hidden form-group">
                <label id="one-click-deposit-campaign-label" for="one-click-deposit-campaigns"><spring:message code='ocdBonus'/></label>
                <div id="one-click-deposit-campaigns-dropdown" class="has-feedback">
                    <select id="one-click-deposit-campaigns" class="form-control"></select>
                    <div class="form-control-feedback">
                        <i id="one-click-deposit-campaigns-hint" alt="?" title="<spring:message code='deposit.hint.campaigns'/>" class="fa fa-question fa-lg control-hint"></i>
                    </div>
                </div>
            </div>
            <input id="one-click-deposit-submit" class="btn btn-primary disabled" type="submit" value="<spring:message code='ocdWidget.submit' />">
        </form>
    </div>

    <script>
        $(function () {
            var $widget = $('#one-click-deposit-widget'),
                $form = $('#one-click-deposit-form'),
                $amount = $('#one-click-deposit-amount'),
                $cards = $('#one-click-deposit-cards'),
                $currentCard = $('#one-click-deposit-current-card'),
                $currentCardTitle = $('.card-title', $currentCard),
                $campaignList = $('#one-click-deposit-campaigns-wrapper'),
                $campaign = $('#one-click-deposit-campaigns'),
                $submit = $('#one-click-deposit-submit'),
                amountPlaceholder = $amount.attr('placeholder') || '',
                currency = '',
                dropdownObj = {},
                currentReference,
                campaigns;

            $.when(getPaymentProviders()).done(function (map) {
                var methods = map.methods || [],
                    htmlStoredData = '';
                campaigns = map.campaigns || [];
                currency = getCurrencySymbol(map.currency || '');

                for (var i = 0; i < methods.length; i++) {
                    var method = methods[i];

                    if (method.stored && method.oneClickDeposit) {
                        var storedData = method.storedData || [];

                        for (var j = 0; j < storedData.length; j++) {
                            var reference = storedData[j].reference,
                                identifier = storedData[j].cardEnding || storedData[j].accountNumber,
                                isDefault = !htmlStoredData;

                            if (!reference || !identifier) continue;

                            dropdownObj[reference] = {
                                method: method.method,
                                minAmount: method.minAmount,
                                maxAmount: method.maxAmount
                            };

                            htmlStoredData +=
                                '<label class="btn btn-default' + (isDefault ? ' active' : '') + '">' +
                                    '<input type="radio" name="reference" value="' + reference + '"' + (isDefault ? ' checked="checked"' : '') + '> ' +
                                    '<span class="card-title">' + identifier + '</span>' +
                                '</label>';

                            if (isDefault) {
                                $currentCardTitle.html(identifier);
                                currentReference = reference;
                            }
                        }
                    }
                }

                $cards.html(htmlStoredData).find(':radio').on('change', function () {
                    $currentCardTitle.html( $(this).next('.card-title').html() );
                    currentReference = $(this).val();
                    $('label', $cards).removeClass('active');
                    $(this).closest('label').addClass('active');
                    updatePlaceholder();
                    checkAmount();
                    changeSubmitButtonStatus();
                });

                if (htmlStoredData) {
                    updatePlaceholder();
                    $currentCard.show();
                    $widget.addClass('active').show();
                }

                if (campaigns) {
                    for (var i = 0; i < campaigns.length; i++) {
                        var campaign = campaigns[i];

                        if (campaign.promoCode) {
                            campaigns.splice(i, 1);
                            i--;  // step back
                        }
                    }
                    if (campaigns.length > 0) {
                        populateCampaignDropdown(campaigns, 'one-click-deposit-campaigns-dropdown', '', {}, currency);
                        $campaignList.removeClass('hidden');
                    }
                }
            });

            $amount.on('keyup', function () {
                checkAmount();
                populateCampaignDropdown(campaigns, 'one-click-deposit-campaigns-dropdown', '', { amount: $amount.val() }, currency);
            }).on('keydown', function () {
                changeSubmitButtonStatus();
            }).on('keypress', function (event) {
                validateInputs(event, 'decimal');
            }).on('input paste', function () {
                checkAmount();
                changeSubmitButtonStatus();
                populateCampaignDropdown(campaigns, 'one-click-deposit-campaigns-dropdown', '', { amount: $amount.val() }, currency);
            });

            $form.on('submit', function (event) {
                event.preventDefault();

                if (checkAmount()) {
                    $submit.addClass('disabled');
                    changeSubmitButtonStatus();

                    $.when(confirmDepositAjax({
                        amount: parseFloat($amount.val()) || 0,
                        method:	dropdownObj[currentReference].method,
                        campaignId: $campaign.val() || '',
                        storedDetailReference: currentReference
                    })).done(function (map) {
                        if (!hasErrors(map) && /[^a-zA-Z](ok|pending|success)[^a-zA-Z]/.test(map.redirectUrl || '')) {
                            $amount.val('');
                            checkAmount();
                            changeSubmitButtonStatus('success');
                        }
                        else {
                            changeSubmitButtonStatus('error');
                        }
                    }).fail(function () {
                        changeSubmitButtonStatus('error');
                    }).always(function () {
                        $submit.removeClass('disabled');
                    });
                }
            });

            function checkAmount() {
                var amount = $amount.val(),
                    parsedAmount = parseFloat(amount) || 0,
                    min = dropdownObj[currentReference].minAmount || 0,
                    max = dropdownObj[currentReference].maxAmount || Infinity,
                    isValid = !!parsedAmount && /^[0-9]+[.]?[0-9]*$/.test(amount) && parsedAmount >= min && parsedAmount <= max;
                $submit.toggleClass('disabled', !isValid);
                return isValid;
            }

            function changeSubmitButtonStatus(status) {
                switch (status) {
                    case 'success':
                        $submit.removeClass('btn-primary btn-danger').addClass('btn-success');
                    break;
                    case 'error':
                        $submit.removeClass('btn-primary btn-success').addClass('btn-danger');
                    break;
                    default:
                        $submit.removeClass('btn-danger btn-success').addClass('btn-primary');
                    break;
                }
            }

            function updatePlaceholder() {
                var min = dropdownObj[currentReference].minAmount || 0,
                    max = dropdownObj[currentReference].maxAmount || 9999;
                $amount.attr('placeholder', amountPlaceholder + ' (' + currency + min + ' - ' + currency + max + ')');
            }

            // Prevent closing of widget onclick inside the widget
            $form.on('click', function(event) {
                event.stopPropagation();
            });
        });
    </script>

</div>
