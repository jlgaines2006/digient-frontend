<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="lockPopupWrapper">

    <h4 class="h1"><spring:message code='lockpopup.desc'/></h4>
    <div id="lockPopupButton" class="btn btn-primary btn-lg" onclick="payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');"><spring:message code='navigation.deposit'/></div>

</div>