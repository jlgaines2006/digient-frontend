<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script id="gameGroupTemplate" type="text/x-jquery-tmpl">

    {{each games}}

    <div class="livegame">

        <div class="liveGameElementContent">
            <img src="${features.useDefaultGameThumbnails == 'true' ? desktopThumbnailUrl : cmsRoot}/images/games/provider-id-{{= $value.provider}}/{{= $value.name}}.png" onerror="this.src = '${cmsRoot}/images/games/gameDefault.png'" class="liveGameElementContentImg" alt="{{= $value.title}}"/>
        </div>

        <div class="liveGameElementContent_playButtonWrapper">

            <div class="liveGameTitleWrapper">
                <div class="liveGameTitleContent">
                    {{= $value.title}}
                </div>
            </div>

            <c:choose>
                <c:when test="${loginUser != null}">

                    <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}" class="gameListLink gameLink_provider_{{= $value.provider}} liveGameElement_realButton" title="{{= $value.title}}">
                        <spring:message code='game.play.real'/>
                    </a>

                </c:when>
                <c:otherwise>

                    <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','real')" class="fancyboxPopup liveGameElement_realButton">
                        <spring:message code="game.play.real" />
                    </a>

                </c:otherwise>
            </c:choose>
        </div>

    </div>

    {{/each}}

</script>

<div id="livecasinotitletab"></div>

<div id="games">
</div>

<script type="text/javascript">

    $(document).ready(function() {

        show_tab_content('live');

    });

</script>