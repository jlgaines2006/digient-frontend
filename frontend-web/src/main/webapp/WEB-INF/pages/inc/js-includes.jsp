<%@ taglib uri="http://granule.com/tags" prefix="g" %>

<!-- jQuery -->
<script	type="text/javascript" src="/resources/js/jquery/1.8.2/jquery.min.js"></script>

<!-- jQuery template (for game listing) -->
<script type="text/javascript" src="/resources/js/jquery/jquery.tmpl.min.js"></script>

<!-- jQuery mousewheel -->
<script type="text/javascript" src="/resources/js/jquery/jquery.mousewheel.js"></script>

<!-- jQuery Flex Slider (for responsive slideshow) -->
<script type="text/javascript" src="/resources/js/jquery/jquery.easing.js"></script>
<script type="text/javascript" src="/resources/js/jquery/jquery.flexslider.min.js"></script>

<!-- jQuery Cycle slider (can be used for example in the main slider or in jackpot scroller) -->
<script type="text/javascript" src="/resources/js/jquery/jquery.cycle.all.js"></script>

<!-- jQuery Fancybox (for pop-ups) -->
<script type="text/javascript" src="/resources/js/jquery/jquery.fancybox.js?v=2.1.3"></script>

<!-- JavaScript Image Combobox (for language switcher) -->
<script type="text/javascript" src="/resources/js/jquery/jquery.dd.min.js"></script>

<!-- Winner list scroller -->
<script type="text/javascript" src="/resources/js/jquery/jquery-scroller-v1.min.js"></script>

<!-- Transaction page's pagination library -->
<script type="text/javascript" src="/resources/js/jquery/jquery.pagination.js"></script>

<!-- jQuery serialize plugin that can handle checkboxes -->
<script type="text/javascript" src="/resources/js/jquery/jquery.serialize.js"></script>

<!-- jQuery checkbox styler -->
<script type="text/javascript" src="/resources/js/jquery/jquery-checkbox-2.0.js"></script>

<!-- jQuery UI -->
<script type="text/javascript" src="/resources/js/jquery/1.9.2/jquery-ui.min.js"></script>

<!-- Masked input when filling certain bank fields -->
<script type="text/javascript" src="/resources/js/jquery/jquery.maskedinput.min.js"></script>

<!-- Preload all the images in css -->
<!-- <script type="text/javascript" src="/resources/js/jquery/jquery.preLoadCss.js"></script> -->

<!-- Create device-specific fingerprint -->
<script type="text/javascript" src="/resources/js/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript" src="/resources/js/jquery/_core_v20130926.js"></script>
<script type="text/javascript" src="/resources/js/jquery/_md5.js"></script>

<!-- Google Analytics -->
<script type="text/javascript" src="${cmsRoot}/pages/googleAnalytics.js"></script>

<!-- Betinaction provider-->
<script type="text/javascript" src="/resources/js/game-providers/betinaction.js"></script>

<g:compress>

<!-- Other JS -->
<script type="text/javascript" src="/resources/js/gameWindowAspectRatios.js"></script>
<script type="text/javascript" src="/resources/js/telephoneCodes.js"></script>
<script type="text/javascript" src="/resources/js/commonTranslations.js"></script>
<script type="text/javascript" src="/resources/js/languageSelector.js"></script>
<script type="text/javascript" src="/resources/js/util/ajaxCalls.js"></script>
<script type="text/javascript" src="/resources/js/application.js"></script>
<script type="text/javascript" src="/resources/js/gameOpeningLogic.js"></script>
<script type="text/javascript" src="/resources/js/validation.js"></script>
<script type="text/javascript" src="/resources/js/myacc.js"></script>
<script type="text/javascript" src="/resources/js/campaigns.js"></script>
<script type="text/javascript" src="/resources/js/limits.js"></script>
<script type="text/javascript" src="/resources/js/payment.js"></script>
<script type="text/javascript" src="/resources/js/transactions.js"></script>
<script type="text/javascript" src="/resources/js/jackPots.js"></script>
<script type="text/javascript" src="/resources/js/cookieHandler.js"></script>
<script type="text/javascript" src="/resources/js/deviceFingerprint.js"></script>

</g:compress>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
