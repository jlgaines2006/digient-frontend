<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="gamesSearchSortingWrapper" class="btn-group" data-toggle="buttons">
    <label class="btn btn-default active">
        <input id="gamesSearchUnsorted" type="radio" name="sorting" value="" checked="checked"><spring:message code="gamesSearch.sortingUnsorted"/>
    </label>
    <label class="btn btn-default">
        <input id="gamesSearchSortingNew" type="radio" name="sorting" value="new"><spring:message code="gamesSearch.sortingByNew"/>
    </label>
    <label class="btn btn-default">
        <input id="gamesSearchSortingName" type="radio" name="sorting" value="name"><spring:message code="gamesSearch.sortingByName"/>
    </label>
</div>