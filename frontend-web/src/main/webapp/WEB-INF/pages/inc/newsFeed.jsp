<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<div class="feedBlock" id="newsFeed">

    <div class="newsFeedElem" id="newsFeedElem1"></div>
    <div class="newsFeedElem" id="newsFeedElem2"></div>
    <div class="newsFeedElem" id="newsFeedElem3"></div>

</div>

<script type="text/javascript">

    // List the month names (from spring tags) and convert them to JS-variables
    <c:forEach var="month" begin="1" end="12" step="1">
        <c:set var="monthWithZero" value="0${month}"/>
        var monthName_${month} = "<spring:message code='month.${month < 10 ? monthWithZero : month}'/>";
    </c:forEach>

    // Get the current language
    var langParam = '${param.lang}';

    if (langParam == 'null' || langParam == '') {
        langParam = '${_lang == null ? features.defaultLanguage : _lang}';
    }

    // Populate the content of single news block, based on the given date
    function getNewsFeedContent(date,elemName) {

        $("#"+elemName).load("${cmsRoot}/languages/"+langParam+"/news/news_"+date+".html", function(responseText, statusText, xhr) {

            if(statusText == "success") {

                // Show the news feed block just after everything is loaded
                $("#"+elemName).show();

                if(responseText.indexOf("Error") >= 0 ) {
                    $("#"+elemName).html("");
                }
                else {

                    // Get the original text from the first paragraph of the news-file
                    var firstParagraphText = $("#newsFeed p:first").html();

                    // Show just the first words of the newspage's first paragraph (if it's too long)
                    if(firstParagraphText.length > 200) {

                        var splittedContent = firstParagraphText.split(" ");

                        firstParagraphText = "";
                        var counter = 0;

                        // Max 15 words per paragraph
                        while(counter<splittedContent.length && counter < 15) {

                            firstParagraphText = firstParagraphText+splittedContent[counter]+" ";
                            counter++;
                        }

                        // Trim the paragraph and add three dots to the end
                        if(firstParagraphText.charAt(firstParagraphText.length-2) === "," || firstParagraphText.charAt(firstParagraphText.length-2) === ".") {
                            firstParagraphText = firstParagraphText.substring(0, firstParagraphText.length - 2);
                        }
                        else {
                            firstParagraphText = firstParagraphText.substring(0, firstParagraphText.length - 1);
                        }

                        firstParagraphText += "...";
                    }

                    // Parse and structure the date text (day + month)
                    var dateArray = date.split("-");
                    var monthText = window['monthName_'+dateArray[1]];

                    if(window['monthName_'+dateArray[1]].length > 3) {
                        monthText = monthText.substring(0,3);
                    }

                    var dateText = "<span class='newsDay'>"+dateArray[0]+"</span> " +
                            "<span class='newsMonth'>"+monthText+"</span>";

                    // Append the date-, title-, and paragraph -elements to the page
                    var newsDate = $("<div>", {
                        "class": "newsDate"
                    });
                    newsDate.html(dateText);

                    var newsContentWrapper = $("<div>", {
                        "class": "newsContentWrapper"
                    });

                    var newsTitle = $("<div>", {
                        "class": "newsTitle",
                        "text": $("#newsFeed .newsFeedTitle").html()
                    }).appendTo(newsContentWrapper);

                    var newsParagraph = $("<div>", {
                        "class": "newsParagraph",
                        "text": firstParagraphText
                    }).appendTo(newsContentWrapper);

                    // Append the "Read more" -link to the text
                    var readMoreLink = "/staticPageHandler?pageName=news_"+date;
                    var readMoreLinkElem = $("<a>", {
                        "class": "newsReadMoreLink",
                        "href": readMoreLink,
                        "text": "<spring:message code="newsblock.readmore"/> >"
                    }).appendTo(newsParagraph);

                    $("#"+elemName).html("");

                    newsDate.appendTo($("#"+elemName));
                    newsContentWrapper.appendTo($("#"+elemName));
                }
            }
            else if(statusText != "success") {

                $("#"+elemName).html("");
            }
        });
    }

    // Get the latest 3 news from the newsList-file, and show the content on the site
    $(document).ready(function() {

        $("#newsFeedElem1").load("${cmsRoot}/pages/newsList.html", function(responseText, statusText, xhr) {

            if(statusText == "success") {

                if(responseText.indexOf("Error") >= 0 ) {
                    $("#newsFeedElem1").html("");
                }
                else {

                    var newsList = responseText.split("*");
                    var counter = 0;

                    while(counter<newsList.length && counter < 3) {

                        getNewsFeedContent(newsList[counter],"newsFeedElem"+(counter+1));
                        counter++;
                    }
                }
            }
            else if(statusText != "success") {

                $("#newsFeedElem1").html("");
            }
        });

    });

</script>
