<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<div id="forgotpasswordSuccess" style="display: none">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/forget-pwd/forget-pwd-thanks.html" />
</div>

<div id="forgotpasswordWrapper">

    <div id="forgotPwdWrapper_curtain" style="display: none"></div>

    <h4 class="h1"><spring:message code='forgetpassword.title'/></h4>
    <p><spring:message code='forgetpassword.h1'/></p>

    <form id="forgetpasswordForm" class="registration form-horizontal" action="">

        <p id="forgotPwdHead_error" class="error alert alert-danger alert-dismissable"></p>

        <fieldset>

            <ul class="list-unstyled">
                <li class="form-group">
                    <label class="control-label col-sm-4" for="forgotpasswordBirthday">
                        <spring:message code='register.form.birthDate'/>&nbsp;<span class="birthdatetitle">
                        <div class="clearfix hidden-xs"></div>
                        <spring:message code='register.form.birthDateFormat'/></span>
                    </label>
                    <div id="forgotpassword_birthdayWrapper" class="col-sm-8">

                        <select id="forgotpasswordBirthday" class="form-control" name="day-birthDate">
                            <option value="1" selected="selected">01</option>
                            <c:forEach var="day" begin="2" end="31" step="1">
                                <c:set var="dayWithZero" value="0${day}"/>
                                <option value="${day}">${day < 10 ? dayWithZero : day}</option>
                            </c:forEach>
                        </select>
                        <select id="forgotpasswordBirthmonth" class="form-control" name="month-birthDate">
                            <option value="1" selected="selected"><spring:message code='month.01'/></option>
                            <c:forEach var="month" begin="2" end="12" step="1">
                                <c:set var="monthWithZero" value="0${month}"/>
                                <option value="${month}"><spring:message code='month.${month < 10 ? monthWithZero : month}'/></option>
                            </c:forEach>
                        </select>
                        <select id="forgotpasswordBirthyear" class="form-control" name="year-birthDate">
                        </select>

                    </div>
                </li>
                <li class="form-group">
                    <label class="control-label col-sm-4" for="forgotpassword_email"><spring:message code='email'/></label>
                    <div id="forgotpassword_emailWrapper" class="col-sm-8">
                        <input id="forgotpassword_email" class="registerField form-control" type="text" name="email" value="" onblur="javascript: validateForgotpwdEmail();" />
                        <p id="forgotPwdEmail_error" class="help-block error-field" style="display: none">&nbsp;</p>
                    </div>
                </li>
                <input type="hidden" name="action" value="ForgetPasswordAction"/>

                <a id="forgotpassword_button" class="btn btn-primary btn-lg" onclick="javascript: submitForgotPassword();"><spring:message code='forgetpassword.submitBtn'/></a>
            </ul>

        </fieldset>

    </form>

</div>