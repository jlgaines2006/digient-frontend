<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="gamesSearchWrapper">
    <form id="gamesSearchForm" class="form-inline">

        <div id="gamesSearchSorting">

            <div class="form-group">
                <label class="control-label hide"><spring:message code="gamesSearch.sortingLabel"/></label>
            </div>

            <%-- There are 2 different types of view for filtering of games: buttons and dropdown. Use gamesSearchSortingButtons.jsp for buttons view and gamesSearchSortingDropdown.jsp for dropdown --%>
            <c:import url="/WEB-INF/pages/inc/gamesSearchSortingButtons.jsp" />
            <%--<c:import url="/WEB-INF/pages/inc/gamesSearchSortingDropdown.jsp" />--%>

            <div id="gamesSearchSortingReverseWrapper" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default">
                    <input id="gamesSearchSortingReverse" type="checkbox" name="sortingIsReversed" value="true"><spring:message code="gamesSearch.sortingReverse"/>
                </label>
            </div>

        </div>

        <div id="gamesListView" class="btn-group" data-toggle="buttons">
            <label class="btn btn-default active">
                <input id="gamesListViewThumbnails" type="radio" name="view" value="thumbnails" checked="checked"><span class="glyphicon glyphicon-th-large"></span>
            </label>
            <label class="btn btn-default">
                <input id="gamesListViewText" type="radio" name="view" value="text"><span class="glyphicon glyphicon-th-list"></span>
            </label>
        </div>

        <div id="gamesSearchFiltering">

            <div id="gamesSearchKeywordWrapper" class="form-group has-feedback">
                <label class="control-label hide" for="gamesSearchKeyword"><spring:message code="gamesSearch.filteringLabel"/></label>
                <input id="gamesSearchKeyword" class="form-control" type="text" name="keyword" placeholder="<spring:message code="gamesSearch.title"/>">
                <div class="form-control-feedback">
                    <span id="gamesSearchKeywordIconSearch" class="glyphicon glyphicon-search"></span>
                    <span id="gamesSearchKeywordIconRemove" class="glyphicon glyphicon-remove" style="display:none"></span>
                </div>
            </div>

        </div>

    </form>
</div><%-- #gamesSearchWrapper --%>

<script>
    $(function () {

        var gamesSearchDelay = 1000,
            gamesSearchTimeoutId;

        // Load the list of all games
        $.when(getGamesListAll()).done(function (map) {

            $('#gamesSearchForm').submit(function (event) {
                event.preventDefault();
            });

            // Run search after the changing of sorting method
            $('#gamesSearchSorting').find(':radio, :checkbox, select').on('change', function (event) {
                updateTabContent();
            });

            // Run search after the changing of type of list view
            $('#gamesListView').find(':radio').on('change', function (event) {
                updateTabContent();
            });

            // Run search after the changing of search keyword
            $('#gamesSearchKeyword').on('keyup input paste', function (event) {
                var self = this,
                    runUpdateFunction = function () {
                        if ($(self).val() == '') {
                            updateTabContent(oGamesList.activeGroup.array, oGamesList.activeGroup.name, oGamesList.activeGroup.isPaginated, 0);
                        }
                        else {
                            updateTabContent(map.games, 'all', true, 0);
                        }
                    };
                clearTimeout(gamesSearchTimeoutId);
                if (event.which == 13) {
                    runUpdateFunction();
                }
                else {
                    gamesSearchTimeoutId = setTimeout(function () {
                        runUpdateFunction();
                    }, gamesSearchDelay);
                }
            });

            // Contraption for the search field clearing
            $('#gamesSearchKeywordIconRemove').on('click', function () {
                if ($('#gamesSearchKeyword').val() != '') {
                    $('#gamesSearchKeyword').val('');
                    updateTabContent(oGamesList.activeGroup.array, oGamesList.activeGroup.name, oGamesList.activeGroup.isPaginated, 0);
                }
            });

        });

    });
</script>