<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">

    setTimeout(function () {
        registerOnload('${_lang == null ? features.defaultLanguage : _lang}');
    }, 10);

</script>

<div id="registerSuccess" style="display: none">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-result.html"/>
</div>

<div id="registerWrapper">

    <div id="registerWrapper_curtain" style="display: none"></div>

    <h4 class="h1"><spring:message code="register.form.title"/></h4>

    <p><spring:message code="register.form.description"/></p>

    <form id="register_form" class="registration" action="">

        <p id="register_error" class="error alert alert-danger alert-dismissable"></p>

        <fieldset id="register-step-1">

            <div class="row">

                <div id="pre_register_firstcol" class="register-col col-sm-6">

                    <ul class="list-unstyled">

                        <li id="li_email" class="form-group has-feedback" data-register-field="email" style="display: none">
                            <label class="registerLabel control-label" for="registerEmail"><spring:message code="email"/><span
                                    id="star_email" style="display: none"> *</span></label>
                            <input id="registerEmail" class="registerField form-control" type="text" value="" name="email"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.form.email.title"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerEmail_validOK"></span>
                            </div>
                            <div id="registerEmail_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_password" class="form-group has-feedback" data-register-field="password" style="display: none">
                            <label class="registerLabel control-label" for="registerPassword"><spring:message code="password"/><span
                                    id="star_password" style="display: none"> *</span></label>
                            <input id="registerPassword" class="registerField form-control" type="password" value="" name="password"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.hint.password"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerPassword_validOK"></span>
                            </div>
                            <div id="registerPassword_error" class="registerFieldError help-block error-field"></div>
                        </li>

                    </ul>

                </div>

                <div id="pre_register_secondcol" class="register-col col-sm-6">

                    <ul class="list-unstyled">

                        <li id="li_confirmEmail" class="form-group has-feedback" data-register-field="confirmEmail" style="display: none">
                            <label class="registerLabel control-label" for="registerConfirmEmail"><spring:message
                                    code="register.form.confirmEmail"/><span id="star_confirmEmail"
                                                                             style="display: none"> *</span></label>
                            <input id="registerConfirmEmail" class="registerField form-control" type="text" value=""
                                   name="confirmEmail"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.hint.confirmEmail"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerConfirmEmail_validOK"></span>
                            </div>
                            <div id="registerConfirmEmail_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_confirmPassword" class="form-group has-feedback" data-register-field="confirmPassword" style="display: none">
                            <label class="registerLabel control-label" for="registerConfirmPassword"><spring:message
                                    code="register.form.confirmPassword"/><span id="star_confirmPassword"
                                                                                style="display: none"> *</span></label>
                            <input id="registerConfirmPassword" class="registerField form-control" type="password" value=""
                                   name="confirmPassword"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.hint.confirmPassword"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerConfirmPassword_validOK"></span>
                            </div>
                            <div id="registerConfirmPassword_error" class="registerFieldError help-block error-field"></div>
                        </li>

                    </ul>

                </div>

            </div>

            <div id="register_footer" class="row">

                <div class="col-xs-8">

                    <input type="hidden" name="ageChecked" id="ageChecked" data-register-field="ageChecked"/>

                    <div id="li_policyChecked" class="checkbox" data-register-field="policyChecked" style="display: none">
                        <label for="accept_user_terms">
                            <input type="checkbox" name="policyChecked" id="accept_user_terms"/>

                            <p id="registerTerms1Text"><spring:message code="register.form.terms1"/>&nbsp;
                                <a href="#termsPopup" class="fancyboxReturnToParentPopup">
                                    <spring:message code="register.form.terms2"/>
                                </a> &amp;
                                <a href="#policyPopup" class="fancyboxReturnToParentPopup">
                                    <spring:message code="register.form.policy"/>
                                </a><span id="star_policyChecked" style="display: none"> *</span>
                            </p>
                        </label>

                        <div id="accept_user_terms_error" class="registerFieldError_checkbox help-block error-field"></div>
                    </div>

                    <div id="li_receiveEmail" class="checkbox" data-register-field="receiveEmail" style="display: none">
                        <label for="receiveEmail">
                            <input id="receiveEmail" type="checkbox" value="1" name="receiveEmail" checked="true"/>
                            <spring:message code="register.form.receivePromo"/><span id="star_receiveEmail"
                                                                                     style="display: none"> *</span>
                        </label>
                    </div>

                </div>

                <div class="col-xs-4">

                    <div id="li_promoCode" class="form-group has-feedback" data-register-field="promoCode" style="display: none">
                        <label class="registerLabel control-label" for="registrationPromoCode"><spring:message
                                code="register.form.promocode"/><span id="star_promoCode" style="display: none"> *</span></label>

                        <div id="registerPromocodeWrapper">
                            <input id="registrationPromoCode" class="registerField form-control" type="text" name="promoCode"
                                   maxLength="32"/>

                            <div id="registerPromoHint" class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.form.promoHint"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registrationPromoCode_validOK"></span>
                            </div>
                            <div id="registrationPromoCode_error" class="registerFieldError help-block error-field"></div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-12">

                    <a id="continueSignUpButton" class="btn btn-primary btn-lg disabled" href="javascript: continueRegister();"><spring:message
                            code="register.form.continueBtn"/></a>

                </div>

            </div>

            <div id="registerFormMandatory"><spring:message code="register.form.mandatory"/></div>
            <p class="note"><spring:message code="register.form.note"/></p>

        </fieldset><%-- #register-step-1 --%>

        <fieldset id="register-step-2" style="display:none;">

            <div class="row">

                <div id="register_firstcol" class="register-col col-sm-4">

                    <ul class="list-unstyled">

                        <li id="li_firstName" class="form-group has-feedback" data-register-field="firstName" style="display: none">
                            <label class="registerLabel control-label" for="registerFirstName"><spring:message code="firstName"/><span
                                    id="star_firstName" style="display: none"> *</span></label>
                            <input id="registerFirstName" class="registerField form-control" type="text" value="" name="firstName"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="firstName.hint"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerFirstName_validOK"></span>
                            </div>
                            <div id="registerFirstName_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_birthPlace" class="form-group has-feedback" data-register-field="birthPlace" style="display: none">
                            <label class="registerLabel control-label" for="registerBirthPlace"><spring:message
                                    code="register.form.placeofbirth"/><span id="star_birthPlace"
                                                                             style="display: none"> *</span></label>
                            <input id="registerBirthPlace" class="registerField form-control" type="text" value="" name="birthPlace"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.form.placeofbirth.title"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerBirthPlace_validOK"></span>
                            </div>
                            <div id="registerBirthPlace_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_address" class="form-group has-feedback" style="display: none;">

                            <label class="registerLabel control-label" for="registerAddress"><spring:message code="address"/><span
                                    id="star_address" style="display: none"> *</span></label>

                            <div class="row">
                                <div class="col-xs-8">
                                    <input id="registerAddress" class="registerField form-control" type="text" value="" name="address"
                                           maxlength="256" data-register-field="address"/>
                                </div>
                                <div class="col-xs-4">
                                    <input id="registerHouseNumber" class="registerField form-control" type="text" value=""
                                           name="houseNumber" maxlength="20" data-register-field="houseNumber"/>
                                </div>
                            </div>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="address.hint"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerAddress_validOK"></span>
                            </div>
                            <div id="registerAddress_error" class="registerFieldError help-block error-field"></div>

                        </li>

                        <li id="li_country" class="form-group" data-register-field="country" style="display: none">
                            <label class="registerLabel control-label" for="registerCountrySelector"><spring:message
                                    code="country"/><span id="star_country" style="display: none"> *</span></label>

                            <select id="registerCountrySelector" class="form-control" name="country">
                                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-country.html"/>
                            </select>
                        </li>

                    </ul>

                </div><%-- #register_firstcol --%>

                <div id="register_secondcol" class="register-col col-sm-4">

                    <ul class="list-unstyled">

                        <li id="li_lastName" class="form-group has-feedback" data-register-field="lastName" style="display: none">
                            <label class="registerLabel control-label" for="registerLastName"><spring:message code="lastName"/><span
                                    id="star_lastName" style="display: none"> *</span></label>
                            <input id="registerLastName" class="registerField form-control" type="text" value="" name="lastName"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="lastName.hint"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerLastName_validOK"></span>
                            </div>
                            <div id="registerLastName_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_birthDate" class="form-group" data-register-field="birthDate" style="display: none">
                            <label class="registerLabel control-label" for="registerBirthday">
                                <spring:message code="register.form.birthDate"/><span id="star_birthDate" style="display: none"> *</span>
                                <span class="birthdatetitle"><spring:message code='register.form.birthDateFormat'/></span>
                            </label>

                            <div id="registerBirthdateWrapper">

                                <table>
                                    <tbody>
                                        <tr>

                                            <td>
                                                <select id="registerBirthday" class="form-control" name="day-birthDate">
                                                    <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.day'/></option>
                                                    <c:forEach var="day" begin="1" end="31" step="1">
                                                        <c:set var="dayWithZero" value="0${day}"/>
                                                        <option value="${day}">${day < 10 ? dayWithZero : day}</option>
                                                    </c:forEach>
                                                </select>

                                            </td>

                                            <td style="text-align:center;">

                                                <select id="registerBirthmonth" class="form-control" name="month-birthDate">
                                                    <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.month'/></option>
                                                    <c:forEach var="month" begin="1" end="12" step="1">
                                                        <c:set var="monthWithZero" value="0${month}"/>
                                                        <option value="${month}"><spring:message
                                                                code='month.${month < 10 ? monthWithZero : month}'/></option>
                                                    </c:forEach>
                                                </select>
                                            </td>

                                            <td style="text-align:right;">
                                                <select id="registerBirthyear" class="form-control" name="year-birthDate">
                                                    <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.year'/></option>
                                                </select>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>

                            </div>

                            <div id="birthdayField_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_city" class="form-group has-feedback" data-register-field="city" style="display: none;">
                            <label class="registerLabel control-label" for="registerCity"><spring:message code="myaccount.city"/><span
                                    id="star_city" style="display: none"> *</span></label>
                            <input id="registerCity" class="registerField form-control" type="text" value="" name="city"
                                   maxlength="64"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="myaccount.city.hint"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerCity_validOK"></span>
                            </div>
                            <div id="registerCity_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_currency" class="form-group" data-register-field="currency" style="display: none">
                            <label class="registerLabel control-label" for="registerCurrencySelector"><spring:message
                                    code="register.form.currency"/><span id="star_currency" style="display: none"> *</span></label>
                            <select id="registerCurrencySelector" class="form-control" name="registrationCurrency">
                            </select>
                        </li>

                    </ul>

                </div><%-- #register_secondcol --%>

                <div id="register_thirdcol" class="register-col col-sm-4">

                    <ul class="list-unstyled">

                        <li id="li_nickName" class="form-group has-feedback" data-register-field="nickName" style="display: none">
                            <label class="registerLabel control-label" for="registerNickname"><spring:message code="nickname"/><span
                                    id="star_nickName" style="display: none"> *</span></label>
                            <input id="registerNickname" class="registerField form-control" type="text" value="" name="nickName"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="register.form.nickname.title"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerNickname_validOK"></span>
                            </div>
                            <div id="registerNickname_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_passportNumber" class="form-group has-feedback" style="display: none">
                            <label class="radio-inline">
                                <input id="ssn" class="ssnRadio" type="radio" name="passportNumberRadio" value="ssn" checked="checked"
                                       onclick="selectSsnOrPassportNumber();"><spring:message code="myaccount.ssn"/><span id="star_ssn"
                                                                                                                          style="display: none"> *</span>
                            </label>
                            <label class="radio-inline">
                                <input id="passportNumber" class="ssnRadio" type="radio" name="passportNumberRadio"
                                       value="passportNumber" onclick="selectSsnOrPassportNumber();"><spring:message
                                    code="myaccount.passportNumber"/><span id="star_passportNumber" style="display: none"> *</span>
                            </label>

                            <div id="passportNumberDiv" data-register-field="passportNumber" style="display: none;">

                                <input id="registerPassportNumber" class="registerField form-control" type="text" value=""
                                       name="passportNumber" onkeypress="validateInputs(event, 'bankInfo');"/>

                                <div class="registerIcons form-control-feedback">
                                    <i alt="?" title="<spring:message code="myaccount.passportNumber.hint"/>" class="fa fa-question fa-lg"></i>
                                    <span class="registerValidOK" id="registerPassportNumber_validOK"></span>
                                </div>
                                <div id="registerPassportNumber_error" class="registerFieldError help-block error-field"></div>

                            </div>

                            <div id="ssnDiv" data-register-field="ssn">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <input id="ssnYear" class="registerFieldSsn form-control" maxlength="2"
                                               onkeypress="validateInputs(event, 'number'); moveToNextField('ssnYear');"
                                               placeholder="<spring:message code="year"/>"/>
                                        <span class="ssnDot">.</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input id="ssnMonth" class="registerFieldSsn form-control" maxlength="2"
                                               onkeypress="validateInputs(event, 'number'); moveToNextField('ssnMonth');"
                                               placeholder="<spring:message code="month"/>"/>
                                        <span class="ssnDot">.</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input id="ssnDate" class="registerFieldSsn form-control" maxlength="2"
                                               onkeypress="validateInputs(event, 'number'); moveToNextField('ssnDate');"
                                               placeholder="<spring:message code="date"/>"/>
                                        <span class="ssnDash">-</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input id="ssnNumber" class="registerFieldSsn registerFieldSsnNumber form-control" maxlength="5"
                                               onkeypress="validateInputs(event, 'number');"
                                               placeholder="<spring:message code="number"/>"/>
                                    </div>
                                </div>
                                <input id="registerSsn" class="registerField form-control" type="text" value="" name="ssn"
                                       style="display: none"/>

                                <div class="registerIcons form-control-feedback">
                                    <i alt="?" title="<spring:message code="myaccount.ssn.hint"/>" class="fa fa-question fa-lg"></i>
                                    <span class="registerValidOK" id="registerSsn_validOK"></span>
                                </div>
                                <div id="registerSsn_error" class="registerFieldError help-block error-field"></div>

                            </div>
                        </li>

                        <li id="li_zipCode" class="form-group has-feedback" data-register-field="zipCode" style="display: none;">
                            <label class="registerLabel control-label" for="registerZipCode"><spring:message
                                    code="myaccount.zipCode"/><span id="star_zipCode" style="display: none"> *</span></label>
                            <input id="registerZipCode" class="registerField form-control" type="text" value="" name="zipCode"
                                   maxlength="16" onkeypress="validateInputs(event, 'zipCode');"/>

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" title="<spring:message code="myaccount.zipCode.hint"/>" class="fa fa-question fa-lg"></i>
                                <span class="registerValidOK" id="registerZipCode_validOK"></span>
                            </div>
                            <div id="registerZipCode_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_gender" class="form-group" data-register-field="gender" style="display: none">
                            <label class="registerLabel control-label"><spring:message code="register.form.gender"/><span
                                    id="star_gender" style="display: none"> *</span></label>

                            <div id="genderItems" class="labels">
                                <label id="genderMaleLabel" class="radio-inline">
                                    <input id="registerGenderMale" type="radio" name="gender" value="male"><spring:message
                                        code="register.form.gender.male"/>
                                </label>
                                <label id="genderFemaleLabel" class="radio-inline">
                                    <input id="registerGenderFemale" type="radio" name="gender" value="female"><spring:message
                                        code="register.form.gender.female"/>
                                </label>
                            </div>
                            <div id="registerGender_error" class="registerFieldError help-block error-field"></div>
                        </li>

                        <li id="li_phoneNumber" class="form-group has-feedback" data-register-field="phoneNumber" style="display: none;">
                            <label class="registerLabel control-label" for="registerPhoneNumber"><spring:message code="myaccount.phoneNumber"/><span id="star_phoneNumber" style="display: none"> *</span></label>
                            <input id="registerPhoneNumber" class="registerField form-control" name="phoneNumber" type="text" value="" maxlength="20" onkeypress="validateInputs(event, 'phone');" />

                            <div class="registerIcons form-control-feedback">
                                <i alt="?" id="registerPhoneNumberHint" class="fa fa-question fa-lg"></i>
                                <span id="registerPhoneNumber_validOK" class="registerValidOK"></span>
                            </div>
                            <div id="registerPhoneNumber_error" class="registerFieldError help-block error-field"></div>
                        </li>

                    </ul>

                </div><%-- #register_thirdcol --%>

                <div class="col-xs-12">

                    <a id="signUpButton" class="btn btn-primary btn-lg disabled" href="javascript: register();"><spring:message
                            code="register.form.registerBtn"/></a>

                </div>

            </div>

        </fieldset><%-- #register-step-2 --%>

    </form>

</div><%-- #registerWrapper --%>