<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="footer-fluid" class="row">
    <div class="container">
        <div id="footer" class="row">
            <div class="col-xs-12">

                <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/footer/footerContentStatic.html" />

            </div>
        </div><%-- #footer --%>
    </div>
</div><%-- #footer-fluid --%>