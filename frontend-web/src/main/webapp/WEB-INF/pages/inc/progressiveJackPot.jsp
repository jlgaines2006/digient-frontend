<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script id="progressiveJackpotTemplate" type="text/x-jquery-tmpl">

    {{if showInSlider == true}}

        <div class="jackpotitem">

            <c:choose>
                <c:when test="${loginUser != null}">

                    {{if provider == "4"}}
                        <a class="liveGameLink freeSpinsEnabled_{{= freeSpinsEnabled}} promoMoneyEnabled_{{= promoMoneyEnabled}}" href="/liveGameWindow?name={{= name}}&provider={{= provider}}">
                            <img src="${features.useDefaultGameThumbnails == 'true' ? jackpotThumbnailUrl : cmsRoot}/images/jackpotGameThumbs/{{= name}}.png" alt="{{= name}}" onerror="this.src = '${cmsRoot}/images/jackpotGameThumbs/gameDefault.png'"/>
                        </a>
                    {{else}}
                        <a class="gameLink freeSpinsEnabled_{{= freeSpinsEnabled}} promoMoneyEnabled_{{= promoMoneyEnabled}}" href="/realGame?name={{= name}}&provider={{= provider}}">
                            <img src="${features.useDefaultGameThumbnails == 'true' ? jackpotThumbnailUrl : cmsRoot}/images/jackpotGameThumbs/{{= name}}.png" alt="{{= name}}" onerror="this.src = '${cmsRoot}/images/jackpotGameThumbs/gameDefault.png'"/>
                        </a>
                    {{/if}}

                </c:when>
                <c:otherwise>

                    {{if hideDemoUrl != true}}

                        {{if provider == "4"}}
                            <a class="liveGameLink" href="/liveGameWindow?name={{= name}}&provider={{= provider}}&thisIsDemo=1">
                                <img src="${features.useDefaultGameThumbnails == 'true' ? jackpotThumbnailUrl : cmsRoot}/images/jackpotGameThumbs/{{= name}}.png" alt="{{= name}}" onerror="this.src = '${cmsRoot}/images/jackpotGameThumbs/gameDefault.png'"/>
                            </a>
                        {{else}}
                            <a class="gameLink" href="/demoGame?name={{= name}}&provider={{= provider}}&thisIsDemo=1">
                                <img src="${features.useDefaultGameThumbnails == 'true' ? jackpotThumbnailUrl : cmsRoot}/images/jackpotGameThumbs/{{= name}}.png" alt="{{= name}}" onerror="this.src = '${cmsRoot}/images/jackpotGameThumbs/gameDefault.png'"/>
                            </a>
                        {{/if}}

                    {{else}}
                        <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= name}}','real')" class="fancyboxPopup">
                            <img src="${features.useDefaultGameThumbnails == 'true' ? jackpotThumbnailUrl : cmsRoot}/images/jackpotGameThumbs/{{= name}}.png" alt="{{= name}}" onerror="this.src = '${cmsRoot}/images/jackpotGameThumbs/gameDefault.png'"/>
                        </a>
                    {{/if}}

                </c:otherwise>
            </c:choose>

            <div class="jackpotitemvalue">
                {{= currency}}<span class="jackpotitemvaluenumber jackpotgame_{{= name}}">{{= previousValue}}</span>
            </div>

            <div class="jackpotitemname">{{= displayName}}</div>

        </div>

    {{/if}}

</script>

<div id="progressivejackpot">

    <h4 class="h3" id="progresivejackpottitle">
        <spring:message code="progressiveJackpot.title"/>
    </h4>

    <div id="jackpotrow">

        <div id="jackpotslider">

            <div id="jackpotslidertextwrapper">
            </div>

        </div>

    </div>

</div>

<script type="text/javascript">

    // Define winner list scroller
    $(document).ready(function() {

        <c:choose>
            <c:when test="${loginUser == null}">

                $.getJSON("/billfold-api/config/currencies", null, function(map) {

                    if(map != null) getGameJackpots(map.currencies[0]);
                });

            </c:when>
            <c:otherwise>

                getGameJackpots('${loginUser.registrationCurrency}');

            </c:otherwise>
        </c:choose>

    });

</script>