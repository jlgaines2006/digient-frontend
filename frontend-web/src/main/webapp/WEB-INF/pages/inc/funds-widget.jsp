<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="funds-widget" class="dropup">

    <div class="funds btn btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="text"><spring:message code="fundsWidget.label" /></span> <span class="total-value"></span>
    </div>

    <ul class="funds-details dropdown-menu">
        <li class="title">
            <span class="text"><spring:message code="fundsWidget.detailsLabel" /></span> <span class="refresh-btn"><i class="fa fa-refresh"></i></span>
        </li>
        <li class="award">
            <span class="text"><spring:message code="fundsWidget.awardLabel" /></span> <span class="award-value"></span>
        </li>
        <li class="wagering">
            <span class="text"><spring:message code="fundsWidget.wageringLabel" /></span> <span class="wagering-value"></span>
        </li>
        <li class="real">
            <span class="text"><spring:message code="fundsWidget.realLabel" /></span> <span class="real-value"></span>
        </li>
        <li class="promo">
            <span class="text"><spring:message code="fundsWidget.promoLabel" /></span> <span class="promo-value"></span>
        </li>
        <li class="total">
            <span class="text"><spring:message code="fundsWidget.totalLabel" /></span> <span class="total-value"></span>
        </li>
    </ul>

    <script>
        $(function () {
            var $widget = $('#funds-widget'),
                $realValue = $widget.find('.real-value'),
                $promoValue = $widget.find('.promo-value'),
                $totalValue = $widget.find('.total-value'),
                $awardValue = $widget.find('.award-value'),
                $wageringValue = $widget.find('.wagering-value'),
                $wagering = $widget.find('.wagering'),
                $refresh = $widget.find('.refresh-btn');

            var updateFunds = function () {
                return getBalance().done(function (map) {
                    if (!map.hasOwnProperty('balance')) return;

                    var realCash = (map.cash || 0).toFixed(2),
                        promoCash = (map.promo || 0).toFixed(2),
                        totalCash = (map.balance || 0).toFixed(2),
                        awardPoints = map.awardPoints || 0,
                        wageringRequirement = calculateWageringRequirement(map.currentTurnover, map.totalTurnover),
                        currency = getCurrencySymbol(map.currency);

                    $realValue.html(replaceParams(amountMode, [realCash, currency]));
                    $promoValue.html(replaceParams(amountMode, [promoCash, currency]));
                    $totalValue.html(replaceParams(amountMode, [totalCash, currency]));
                    $awardValue.html(awardPoints);
                    $wageringValue.html(wageringRequirement != null ? wageringRequirement + '%' : '');
                    $wagering.toggle(wageringRequirement != null);
                });
            };

            (function updateFundsTimer() {
                updateFunds().always(function () {
                    setTimeout(updateFundsTimer, 5000);
                });
            })();

            $refresh.click(updateFunds);

            // Prevent closing of widget onclick inside the widget
            $('.funds-details').on('click', function(event) {
                event.stopPropagation();
            });
        });
    </script>

</div>