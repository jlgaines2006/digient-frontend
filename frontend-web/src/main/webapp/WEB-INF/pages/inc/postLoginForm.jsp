<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form action="" id="postlogin_form" class="form-inline">

    <h4 class="loginformTitle"><spring:message code="header.login.myAccount" /></h4>
    <div id="header_error" class="error alert alert-danger alert-dismissable" style="display: none"></div>

    <div id="postlogin_form_account">

        <c:choose>
            <c:when test="${features.displayName == 'userName'}">
                <a id="postlogin_username" class="headerLink" href="javascript: redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/myacc');">${loginUser.userName}</a>
            </c:when>
            <c:otherwise>
                <a id="postlogin_nickname" class="headerLink" href="javascript: redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/myacc');">${loginUser.nickName}</a>
            </c:otherwise>
        </c:choose>

        <div id="mainUserLoyaltyTier">
            <c:choose>
                <c:when test="${loginUser.vip == true }">
                    <c:choose>
                        <c:when test="${loginUser.loyaltyTier == 'bronze'}">
                            <a class="headerLink" href="javascript: redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="userVIP" src="/cms/images/icons/profile-loyalty/player-icon-bronze-vip.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategoryBronze" /> </span>
                            </a>
                        </c:when>
                        <c:when test="${loginUser.loyaltyTier == 'silver'}">
                            <a class="headerLink" href="javascript: redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="userVIP" src="/cms/images/icons/profile-loyalty/player-icon-silver-vip.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategorySilver" /> </span>
                            </a>
                        </c:when>
                        <c:when test="${loginUser.loyaltyTier == 'gold'}">
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="userVIP" src="/cms/images/icons/profile-loyalty/player-icon-gold-vip.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategoryGold" /> </span>
                            </a>
                        </c:when>
                        <c:when test="${loginUser.loyaltyTier == 'platinum'}">
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="userVIP" src="/cms/images/icons/profile-loyalty/player-icon-platinum-vip.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategoryPlatinum" /> </span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="userVIP" src="/cms/images/icons/profile-loyalty/player-icon-vip.png" />
                            </a>
                        </c:otherwise>
                    </c:choose>
                </c:when>

                <c:otherwise>
                    <c:choose>
                        <c:when test="${loginUser.loyaltyTier == 'bronze'}">
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="imageUserCategory" src="/cms/images/icons/profile-loyalty/player-icon-bronze.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategoryBronze" /> </span>
                            </a>
                        </c:when>
                        <c:when test="${loginUser.loyaltyTier == 'silver'}">
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="imageUserCategory" src="/cms/images/icons/profile-loyalty/player-icon-silver.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategorySilver" /> </span>
                            </a>
                        </c:when>
                        <c:when test="${loginUser.loyaltyTier == 'gold'}">
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="imageUserCategory" src="/cms/images/icons/profile-loyalty/player-icon-gold.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategoryGold" /> </span>
                            </a>
                        </c:when>
                        <c:when test="${loginUser.loyaltyTier == 'platinum'}">
                            <a class="headerLink" href="javascript:redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/campaigns?loyalty');">
                                <img id="imageUserCategory" src="/cms/images/icons/profile-loyalty/player-icon-platinum.png" alt=""/>
                                <span id="userLoyaltyTier"> <spring:message code="header.userCategoryPlatinum" /> </span>
                            </a>
                        </c:when>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </div>

    </div>

    <div id="postlogin_form_deposit">

                                <span id="balance_span">
                                    <div id="cash_balance_wrapper" class="form-group inline-block-xs block-sm inline-block-md">
                                        <span id="cash_balance" class="form-control input-sm">
                                            <spring:message code="header.cashBalLabel" /> <span id="cash_balance_value"></span>
                                        </span>
                                    </div>
                                    <div id="promo_balance_wrapper" class="form-group inline-block-xs block-sm inline-block-md dropdown dropdown-off">
                                        <span id="promo_balance" class="form-control input-sm" data-toggle="dropdown">
                                            <spring:message code="header.promoBalLabel" /> <span id="promo_balance_value"></span>
                                        </span>

                                        <div class="dropdown-menu">
                                            <div class="dropdown-menu-content">
                                                <div id="wagering_requirement_wrapper" style="display: none;">
                                                    <span class="progress-label"><spring:message code="header.wageringRequirementLabel" /></span>
                                                    <span class="progress-hint"><i class="fa fa-question fa-lg"></i><span class="alert alert-info"></span></span>
                                                    <div id="wagering_requirement" class="progress">
                                                        <div id="wagering_requirement_value" class="progress-bar"></div>
                                                    </div>
                                                </div>
                                                <div id="rollover_requirement_wrapper" style="display: none;">
                                                    <span class="progress-label"><spring:message code="header.rolloverRequirementLabel" /></span>
                                                    <span class="progress-hint"><i class="fa fa-question fa-lg"></i><span class="alert alert-info"></span></span>
                                                    <div id="rollover_requirement" class="progress">
                                                        <div id="rollover_requirement_value" class="progress-bar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="total_balance_wrapper" class="form-group inline-block-xs block-sm inline-block-md">
                                        <span id="total_balance" class="form-control input-sm">
                                            <spring:message code="header.totalBalLabel" /> <span id="total_balance_value"></span>
                                        </span>
                                    </div>
                                </span>

    </div>

    <a id="accountButton" class="postloginButton loginButtonBkg btn btn-primary" href="javascript: redirect('/${_lang == null ? features.defaultLanguage : _lang}/postlogin/myacc');"><spring:message code="header.login.manageAccount" /></a>
    <a id="depositButton" class="postloginButton loginButtonBkg btn btn-primary" href="javascript: payment_redirect('/postlogin/payment/prefill?deposit', '/postlogin/payment/deposit', 'deposit');"><spring:message code="login.bank" /></a>
    <a id="signoutButton" class="postloginButton loginButtonBkg btn btn-default" href="javascript: logout();"><spring:message code="login.signout" /></a>

    <div id="languageWrapper_postlogin">
        <c:choose>
            <c:when test="${features.languageSelectorType == 'dropdown'}">
                <select id="changeLanguage">
                </select>
            </c:when>
            <c:when test="${features.languageSelectorType == 'buttons'}">
                <div id="changeLanguage">
                </div>
            </c:when>
        </c:choose>
    </div>

</form>