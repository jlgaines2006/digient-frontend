<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="winnerListTemplate" type="text/x-jquery-tmpl">

    <div class="winnerListItem">

        <div class="winnerListItemLeft">

            {{if country != ""}}

                {{if country == "NAN"}}
                    <img src="/resources/images/countryflags/International.gif" alt="" />
                {{else}}
                    <img src="/resources/images/countryflags/{{= country}}.gif" alt="{{= country}}" onerror="this.src = '/resources/images/countryflags/International.gif'"/>
                {{/if}}

            {{else}}
                <img src="/resources/images/countryflags/International.gif" alt="" />
            {{/if}}

            <span class="winnerListPlayer">{{= nickName}}</span>
        </div>

        <div class="winnerListItemRight">

            <span class="winnerListCurrency">{{= currency}}</span>
            <span class="winnerListAmount">{{= amount}}</span>

            <c:choose>
                <c:when test="${loginUser != null}">

                    {{if provider == "4" || liveGame}}
                        <a class="winnerListLink liveGameLink freeSpinsEnabled_{{= freeSpinsEnabled}} promoMoneyEnabled_{{= promoMoneyEnabled}}" href="/liveGameWindow?name={{= name}}&provider={{= provider}}">{{= title}}</a>&nbsp;
                    {{else}}
                        <a class="winnerListLink gameLink freeSpinsEnabled_{{= freeSpinsEnabled}} promoMoneyEnabled_{{= promoMoneyEnabled}}" href="/realGame?name={{= name}}&provider={{= provider}}">{{= title}}</a>&nbsp;
                    {{/if}}

                </c:when>
                <c:otherwise>

                   {{if hideDemoUrl != true}}

                        {{if provider == "4" || liveGame}}
                            <a class="winnerListLink liveGameLink" href="/liveGameWindow?name={{= name}}&provider={{= provider}}&thisIsDemo=1">{{= title}}</a>&nbsp;
                        {{else}}
                            <a class="winnerListLink gameLink" href="/demoGame?name={{= name}}&provider={{= provider}}&thisIsDemo=1">{{= title}}</a>&nbsp;
                        {{/if}}

                    {{else}}
                        <a class="winnerListLink fancyboxPopup" href="#loginregisterPopup" onclick="setGameForAutostart('{{= name}}','real')">{{= title}}</a>&nbsp;
                    {{/if}}

                </c:otherwise>
            </c:choose>

        </div>

    </div>

</script>

<div id="winnerRow">

    <div id="winnerrowleft">
        <spring:message code="horizontalWinnerListLeft.recent"/>

        <div id="winnerrowlefttext">
            <spring:message code="horizontalWinnerListLeft.winners"/>
        </div>
    </div>

    <div id="winnerSlider">

        <div id="winnerSliderTextWrapper">
        </div>

    </div>

</div>

<script type="text/javascript">

    // Define winner list scroller
    $(document).ready(function() {

        populate_winnerList(${features.winnerListScrollSpeed},'${features.winnerListDirection}','${features.winnerListStartFrom}');
    });

</script>