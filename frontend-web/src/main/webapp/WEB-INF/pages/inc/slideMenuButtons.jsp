<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/subheader/staticMenuButtons.html" />

<div id="aboutus_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/aboutus.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="terms_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/terms.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="responsibleGambling_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/responsibleGambling.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="policy_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/policy.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="contact_SlideElement" class="slideelement" style="display: none;">
    <c:import url="/WEB-INF/pages/inc/contact.jsp" />
    <div class="slidecloseicon"></div>
</div>

<div id="faq_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/faq.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="promotions_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/promotions.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="affiliates_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/affiliates.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="rules_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/rules.html" />
    <div class="slidecloseicon"></div>
</div>

<div id="banking_SlideElement" class="slideelement" style="display: none;">
    <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/info/banking.html" />
    <div class="slidecloseicon"></div>
</div>

<script type="text/javascript">

    currentSlidePageOpen = null;

    // Close slide pages when clicked "Close" -icon
    $(document).ready(function() {

        $(".slidecloseicon").click(function() {

            currentSlidePageOpen = null;

            $(".slideelement").slideUp(500);
            $(".staticlink").removeClass("staticLinkActive");
        });

        var slideUrlParam = getParamValue("currentSlide");

        if(slideUrlParam && slideUrlParam != "null") {

            if(slideUrlParam === "contact") {
                contactOnload();
            }

            $(".staticlink").removeClass("staticLinkActive");
            $("#"+slideUrlParam+"_toplink").addClass("staticLinkActive");
            $("#"+slideUrlParam+"_SlideElement").show();

            currentSlidePageOpen = slideUrlParam;
        }

    });

    // Show the slide menu elements (on those sites that use it)
    function showSlideMenuElem(page,isPreFooter) {

        window.scroll(0,0);

        if(currentSlidePageOpen == page && typeof isPreFooter == "undefined") {

            currentSlidePageOpen = null;

            $(".slideelement").slideUp(500);
            $(".staticlink").removeClass("staticLinkActive");
        }
        else {

            if(page === "contact") {
                contactOnload();
            }

            $(".staticlink").removeClass("staticLinkActive");
            $("#"+page+"_toplink").addClass("staticLinkActive");

            $(".slideelement").hide();
            $("#"+page+"_SlideElement").slideDown(500);

            currentSlidePageOpen = page;
        }
    }

</script>