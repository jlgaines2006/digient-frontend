<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script id="gameGroupTemplate" type="text/x-jquery-tmpl">

    {{each games}}

        {{if $value.metadata.promoted != "true" && $value.gameDisabled != true}}

            <div class="{{= ($value.tabName == 'live' ? 'col-xs-6' : 'col-md-3 col-sm-4 col-xs-6') }} {{= ($value.newGame ? 'game new' : 'game') }}">
                <div class="gameInner">

                    <c:choose>

                        <c:when test="${features.showGameProviderInList == true}">

                            <div class="gameElementContent">
                                <img class="gameElementContentMainImg img-responsive img-thumbnail center-block" src="${features.useDefaultGameThumbnails == 'true' ? desktopThumbnailUrl : cmsRoot}/images/games/provider-id-{{= $value.provider}}/{{= $value.name}}.png" onerror="this.src = '${cmsRoot}/images/games/gameDefault.png'" alt="{{= $value.title}}"/>
                                {{if $value.newGame == true}}
                                    <div class="gameElementContentNewImgWrapper">
                                        <img class="gameElementContentNewImg img-responsive img-thumbnail center-block" src="${cmsRoot}/images/games/gameNew.png"/>
                                    </div>
                                {{/if}}
                                {{if $value.freeSpins == true}}
                                    <div class="gameElementContentFreespinsImgWrapper">
                                        <img class="gameElementContentFreespinsImg img-responsive img-thumbnail center-block" src="${cmsRoot}/images/games/gameFreespins.png"/>
                                    </div>
                                {{/if}}
                            </div>

                            <div class="gameTitleWrapper">
                                <div class="gameTitleContent">
                                    {{= $value.title}}
                                </div>

                                <div class="gameProviderName">
                                    {{= $value.providerName}}
                                </div>

                                <div class="gameRTP{{= $value.rtp == null ? ' hidden' : ''}}">
                                    RTP: {{= $value.rtp}}
                                </div>
                            </div>

                        </c:when>

                        <c:otherwise>

                            <div class="gameTitleWrapper">
                                <div class="gameTitleContent">
                                    {{= $value.title}}
                                </div>

                                <div class="gameRTP{{= $value.rtp == null ? ' hidden' : ''}}">
                                    RTP: {{= $value.rtp}}
                                </div>
                            </div>

                            <div class="gameElementContent">
                                <img class="gameElementContentMainImg img-responsive img-thumbnail center-block" src="${features.useDefaultGameThumbnails == 'true' ? desktopThumbnailUrl : cmsRoot}/images/games/provider-id-{{= $value.provider}}/{{= $value.name}}.png" onerror="this.src = '${cmsRoot}/images/games/gameDefault.png'" alt="{{= $value.title}}"/>
                                {{if $value.newGame == true}}
                                    <div class="gameElementContentNewImgWrapper">
                                        <img class="gameElementContentNewImg img-responsive img-thumbnail center-block" src="${cmsRoot}/images/games/gameNew.png"/>
                                    </div>
                                {{/if}}
                                {{if $value.freeSpins == true}}
                                    <div class="gameElementContentFreespinsImgWrapper">
                                        <img class="gameElementContentFreespinsImg img-responsive img-thumbnail center-block" src="${cmsRoot}/images/games/gameFreespins.png"/>
                                    </div>
                                {{/if}}

                        </c:otherwise>

                    </c:choose>

                        {{if $value.jackpotGame == true}}
                            <div id="gameJackpot_{{= $value.name}}" class="gameJackpot">
                                <div class="gameJackpotTitle"><spring:message code="gameJackpotCounter.title"/></div>
                                <div class="gameJackpotValue" id="gameListJackpot_{{= $value.name}}"></div>
                            </div>
                        {{/if}}

                        <div class="gameElementContent_playButtonWrapper">
                            <c:choose>
                                <c:when test="${loginUser != null}">

                                    {{if $value.hideDemoUrl != true}}

                                        {{if $value.tabName == "live" || $value.liveGame}}
                                            <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default liveGameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                <spring:message code='game.play.fun'/>
                                            </a>
                                        {{else}}
                                            <a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default gameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                <spring:message code="game.play.fun" />
                                            </a>
                                        {{/if}}

                                    {{/if}}

                                    {{if $value.tabName == "live" || $value.liveGame}}
                                        <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}" class="btn btn-primary liveGameListLink freeSpinsEnabled_{{= $value.freeSpinsEnabled}} promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}} gameTab_real_{{= $value.tabName}} gameElementContent_playButton gameElement_realButton" title="{{= $value.title}}">
                                            <spring:message code='game.play.real'/>
                                        </a>
                                    {{else}}
                                        <a href="/realGame?name={{= $value.name}}&provider={{= $value.provider}}" class="btn btn-primary gameListLink freeSpinsEnabled_{{= $value.freeSpinsEnabled}} promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}} gameTab_real_{{= $value.tabName}} gameElementContent_playButton gameElement_realButton" title="{{= $value.title}}">
                                            <spring:message code='game.play.real'/>
                                        </a>
                                    {{/if}}

                                    <i class="favouriteGameIcon btn btn-default glyphicon{{= $value.favourite ? ' active' : ''}}" onclick="toggleFavouriteGame(this, {{= $value.gameId}})"></i>

                                </c:when>
                                <c:otherwise>

                                    {{if $value.hideDemoUrl != true}}

                                        {{if demoGameOpeningType == "loginRegisterPopup"}}

                                            <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','demo')" class="btn btn-default fancyboxPopup gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                <spring:message code="game.demo" />
                                            </a>

                                        {{else}}

                                            {{if $value.tabName == "live" || $value.liveGame}}
                                                <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default liveGameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                    <spring:message code="game.demo" />
                                                </a>
                                            {{else}}
                                                <a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default gameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                    <spring:message code="game.demo" />
                                                </a>
                                            {{/if}}

                                        {{/if}}

                                    {{/if}}

                                    <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','real')" class="btn btn-primary fancyboxPopup gameTab_real_{{= $value.tabName}} gameElementContent_playButton gameElement_realButton" title="{{= $value.title}}">
                                        <spring:message code="game.play.real.preLogin" />
                                    </a>

                                </c:otherwise>
                            </c:choose>
                        </div>

                    <c:choose>
                        <c:when test="${features.showGameProviderInList != true}">
                        </div>
                        </c:when>
                    </c:choose>

                </div>
            </div>

        {{/if}}

    {{/each}}

</script>

<%-- Same as gameGroupTemplate, but may have another structure --%>
<script id="gameGroupTemplateViewText" type="text/x-jquery-tmpl">

    {{each games}}

        {{if $value.metadata.promoted != "true" && $value.gameDisabled != true}}

            <div class="col-md-4 col-sm-6 col-xs-12 game{{= ($value.newGame ? ' new' : '') }}">
                <div class="gameInner">

                    <c:choose>

                        <c:when test="${features.showGameProviderInList == true}">

                            <div class="gameTitleWrapper">
                                <div class="gameTitleContent">
                                    {{= $value.title}}
                                </div>

                                <div class="gameProviderName">
                                    {{= $value.providerName}}
                                </div>

                                <div class="gameRTP{{= $value.rtp == null ? ' hidden' : ''}}">
                                    RTP: {{= $value.rtp}}
                                </div>
                            </div>

                        </c:when>

                        <c:otherwise>

                            <div class="gameTitleWrapper">
                                <div class="gameTitleContent">
                                    {{= $value.title}}
                                </div>

                                <div class="gameRTP{{= $value.rtp == null ? ' hidden' : ''}}">
                                    RTP: {{= $value.rtp}}
                                </div>
                            </div>

                            <div class="gameElementContent">

                        </c:otherwise>

                    </c:choose>

                        {{if $value.jackpotGame == true}}
                            <div id="gameJackpot_{{= $value.name}}" class="gameJackpot">
                                <div class="gameJackpotTitle"><spring:message code="gameJackpotCounter.title"/></div>
                                <div class="gameJackpotValue" id="gameListJackpot_{{= $value.name}}"></div>
                            </div>
                        {{/if}}

                        <div class="gameElementContent_playButtonWrapper">
                            <c:choose>
                                <c:when test="${loginUser != null}">

                                    {{if $value.hideDemoUrl != true}}

                                        {{if $value.tabName == "live" || $value.liveGame}}
                                            <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default liveGameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                <spring:message code='game.play.fun'/>
                                            </a>
                                        {{else}}
                                            <a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default gameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                <spring:message code="game.play.fun" />
                                            </a>
                                        {{/if}}

                                    {{/if}}

                                    {{if $value.tabName == "live" || $value.liveGame}}
                                        <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}" class="btn btn-primary liveGameListLink freeSpinsEnabled_{{= $value.freeSpinsEnabled}} promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}} gameTab_real_{{= $value.tabName}} gameElementContent_playButton gameElement_realButton" title="{{= $value.title}}">
                                            <spring:message code='game.play.real'/>
                                        </a>
                                    {{else}}
                                        <a href="/realGame?name={{= $value.name}}&provider={{= $value.provider}}" class="btn btn-primary gameListLink freeSpinsEnabled_{{= $value.freeSpinsEnabled}} promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}} gameTab_real_{{= $value.tabName}} gameElementContent_playButton gameElement_realButton" title="{{= $value.title}}">
                                            <spring:message code='game.play.real'/>
                                        </a>
                                    {{/if}}

                                    <i class="favouriteGameIcon btn btn-default glyphicon{{= $value.favourite ? ' active' : ''}}" onclick="toggleFavouriteGame(this, {{= $value.gameId}})"></i>

                                </c:when>
                                <c:otherwise>

                                    {{if $value.hideDemoUrl != true}}

                                        {{if demoGameOpeningType == "loginRegisterPopup"}}

                                            <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','demo')" class="btn btn-default fancyboxPopup gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                <spring:message code="game.demo" />
                                            </a>

                                        {{else}}

                                            {{if $value.tabName == "live" || $value.liveGame}}
                                                <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default liveGameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                    <spring:message code="game.demo" />
                                                </a>
                                            {{else}}
                                                <a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default gameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} gameElementContent_playButton gameElement_demoButton" title="{{= $value.title + ' ' + demo}}">
                                                    <spring:message code="game.demo" />
                                                </a>
                                            {{/if}}

                                        {{/if}}

                                    {{/if}}

                                    <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','real')" class="btn btn-primary fancyboxPopup gameTab_real_{{= $value.tabName}} gameElementContent_playButton gameElement_realButton" title="{{= $value.title}}">
                                        <spring:message code="game.play.real.preLogin" />
                                    </a>

                                </c:otherwise>
                            </c:choose>
                        </div>

                    <c:choose>
                        <c:when test="${features.showGameProviderInList != true}">
                        </div>
                        </c:when>
                    </c:choose>

                </div>
            </div>

        {{/if}}

    {{/each}}

</script>

<script id="promotedGameGroupTemplate" type="text/x-jquery-tmpl">

    {{each games}}

        {{if $value.metadata.promoted == "true" && $value.gameDisabled != true}}

            <div class="promotedGame col-md-4 col-xs-6">

                <c:choose>

                    <c:when test="${features.showGameProviderInList == true}">

                    <div class="promotedGameElementContent">

                        <img src="${features.useDefaultGameThumbnails == 'true' ? desktopThumbnailUrl : cmsRoot}/images/games/provider-id-{{= $value.provider}}/{{= $value.name}}_promoted.png" onerror="this.src = '${cmsRoot}/images/games/gameDefault.png'" class="img-responsive img-thumbnail center-block promotedGameElementContentImg" alt="{{= $value.title}}"/>

                        <div class="promotedGameTitleWrapper">
                            <div class="promotedGameTitleContent">
                                {{= $value.title}}
                            </div>

                            <div class="gameProviderName">
                                {{= $value.providerName}}
                            </div>
                        </div>

                    </c:when>

                    <c:otherwise>

                        <div class="promotedGameTitleWrapper">
                            <div class="promotedGameTitleContent">
                                {{= $value.title}}
                            </div>
                        </div>

                        <div class="promotedGameElementContent">
                            <img src="${features.useDefaultGameThumbnails == 'true' ? desktopThumbnailUrl : cmsRoot}/images/games/provider-id-{{= $value.provider}}/{{= $value.name}}_promoted.png" onerror="this.src = '${cmsRoot}/images/games/gameDefault.png'" class="img-responsive img-thumbnail center-block promotedGameElementContentImg" alt="{{= $value.title}}"/>
                        </div>

                    </c:otherwise>

                </c:choose>

                {{if $value.jackpotGame == true}}
                    <div id="gameJackpot_{{= $value.name}}" class="gameJackpot">
                        <div class="gameJackpotTitle"><spring:message code="gameJackpotCounter.title"/></div>
                        <div class="gameJackpotValue" id="gameListJackpot_{{= $value.name}}"></div>
                    </div>
                {{/if}}

                <div class="promotedGame_playButtonWrapper">

                    <c:choose>
                        <c:when test="${loginUser != null}">

                            {{if $value.hideDemoUrl != true}}

                                {{if $value.tabName == "live" || $value.liveGame}}
                                    <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default liveGameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} promotedGame_playButton promotedGame_demoButton" title="{{= $value.title + ' ' + demo}}">
                                        <spring:message code="game.play.fun" />
                                    </a>
                                {{else}}
                                    <a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default gameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} promotedGame_playButton promotedGame_demoButton" title="{{= $value.title + ' ' + demo}}">
                                        <spring:message code="game.play.fun" />
                                    </a>
                                {{/if}}

                            {{/if}}

                            {{if $value.tabName == "live" || $value.liveGame}}
                                <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}" class="btn btn-primary liveGameListLink freeSpinsEnabled_{{= $value.freeSpinsEnabled}} promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}} gameTab_real_{{= $value.tabName}} promotedGame_playButton promotedGame_realButton" title="{{= $value.title}}">
                                    <spring:message code='game.play.real'/>
                                </a>
                            {{else}}
                                <a href="/realGame?name={{= $value.name}}&provider={{= $value.provider}}" class="btn btn-primary gameListLink freeSpinsEnabled_{{= $value.freeSpinsEnabled}} promoMoneyEnabled_{{= $value.promoMoneyEnabled}} gameLink_provider_{{= $value.provider}} gameTab_real_{{= $value.tabName}} promotedGame_playButton promotedGame_realButton" title="{{= $value.title}}">
                                    <spring:message code='game.play.real'/>
                                </a>
                            {{/if}}

                        </c:when>
                        <c:otherwise>

                            {{if $value.hideDemoUrl != true}}

                                {{if demoGameOpeningType == "loginRegisterPopup"}}

                                    <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','demo')" class="btn btn-default fancyboxPopup gameTab_demo_{{= $value.tabName}} promotedGame_playButton promotedGame_demoButton" title="{{= $value.title + ' ' + demo}}">
                                        <spring:message code="game.demo" />
                                    </a>

                                {{else}}

                                    {{if $value.tabName == "live" || $value.liveGame}}
                                        <a href="/liveGameWindow?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default liveGameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} promotedGame_playButton promotedGame_demoButton" title="{{= $value.title + ' ' + demo}}">
                                            <spring:message code="game.demo" />
                                        </a>
                                    {{else}}
                                        <a href="/demoGame?name={{= $value.name}}&provider={{= $value.provider}}&thisIsDemo=1" class="btn btn-default gameListLink gameLink_provider_{{= $value.provider}} gameTab_demo_{{= $value.tabName}} promotedGame_playButton promotedGame_demoButton" title="{{= $value.title + ' ' + demo}}">
                                            <spring:message code="game.demo" />
                                        </a>
                                    {{/if}}

                                {{/if}}

                            {{/if}}

                            <a href="#loginregisterPopup" onclick="setGameForAutostart('{{= $value.name}}','real')" class="btn btn-primary fancyboxPopup gameTab_real_{{= $value.tabName}} promotedGame_playButton promotedGame_realButton" title="{{= $value.title}}">
                                <spring:message code="game.play.real.preLogin" />
                            </a>

                        </c:otherwise>
                    </c:choose>

                    <c:choose>
                        <c:when test="${features.showGameProviderInList == true}">
                        </div>
                        </c:when>
                    </c:choose>

                </div>

            </div>

        {{/if}}

    {{/each}}

</script>

<div id="games" class="row">
</div>

<c:choose>
    <c:when test="${features.gameListPagination == true}">

        <div id="gamelistpaginator" class="gamespagination col-xs-12"></div>

    </c:when>
    <c:otherwise>

        <div id="viewmorebutton" class="gamespagination col-xs-12"></div>

    </c:otherwise>
</c:choose>

<script type="text/javascript">

    // If url parameter gamegroup is found, open the correct group with slide animation
    var urlParam='${param.gamegroup}';
    var freeSpinsId = getUrlParameter(this.href, 'freeSpinsId');
    $(document).ready(function() {
        if(freeSpinsId) {
            // Set delay for filling array with games
            setTimeout(function() {
                updateTabContent(freespinsGamesList[freeSpinsId], 'freeSpins');
            },1500);

            if ($("#games").length > 0) {
                $('html, body').stop(true, true).animate({
                    scrollTop: $("#games").offset().top
                }, 600);
            }
        }
        else {
            // Populate gamelist
            if(urlParam != '') {
                show_tab_content(urlParam);
            }
            else if (window.isLiveCasinoPage) {
                show_tab_content('live');
            }
            else {
                show_tab_content('${features.defaultGameGroup}');
            }
        }
        // Show jackpots
        <c:choose>
            <c:when test="${loginUser == null}">

                $.getJSON("/billfold-api/config/currencies", null, function(map) {
                    if(map != null) getGameJackpots(map.currencies[0]);
                });

            </c:when>
            <c:otherwise>

                getGameJackpots('${loginUser.registrationCurrency}');

            </c:otherwise>
        </c:choose>

    });

</script>