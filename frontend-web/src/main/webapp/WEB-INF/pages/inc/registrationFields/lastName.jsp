<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_lastName" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerLastName"><spring:message code="lastName"/><span id="star_lastName" style="display: none;"> *</span></label>
    <input id="registerLastName" class="registerField form-control" type="text" value="" name="lastName" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="lastName.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerLastName_validOK"></span>
    </div>
    <div id="registerLastName_error" class="registerFieldError help-block error-field"></div>
</li>