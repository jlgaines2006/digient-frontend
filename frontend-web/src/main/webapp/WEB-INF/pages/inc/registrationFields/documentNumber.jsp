<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_documentNumber" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerDocumentNumber"><spring:message code="myaccount.documentNumber"/><span id="star_documentNumber" style="display: none"> *</span></label>
    <input id="registerDocumentNumber" class="registerField form-control" type="text" value="" name="documentNumber" maxlength="256" onkeypress="validateInputs(event, 'bankInfo');"/>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.documentNumber.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerDocumentNumber_validOK"></span>
    </div>
    <div id="registerDocumentNumber_error" class="registerFieldError help-block error-field"></div>
</li>