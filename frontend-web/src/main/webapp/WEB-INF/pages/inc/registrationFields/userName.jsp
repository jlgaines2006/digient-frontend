<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_userName" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerUserName"><spring:message code="userName"/><span id="star_userName" style="display: none"> *</span></label>
    <input id="registerUserName" class="registerField form-control" type="text" value="" name="userName" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="userName.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerUserName_validOK"></span>
    </div>
    <div id="registerUserName_error" class="registerFieldError help-block error-field"></div>
</li>