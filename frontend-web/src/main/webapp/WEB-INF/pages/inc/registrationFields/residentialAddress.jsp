<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_residentialAddress" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerResidentialAddress"><spring:message code="myaccount.residentialAddress"/><span id="star_residentialAddress" style="display: none"> *</span></label>
    <div class="row">
        <div class="col-xs-8">
            <input id="registerResidentialAddress" class="registerField form-control" type="text" value="" name="residentialAddress" maxlength="256" />
        </div>
        <div class="col-xs-4">
            <input id="registerResidentialHouseNumber" class="registerField form-control" type="text" value="" name="residentialHouseNumber" maxlength="20" />
        </div>
    </div>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.residentialAddress.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerResidentialAddress_validOK"></span>
    </div>
    <div id="registerResidentialAddress_error" class="registerFieldError help-block error-field"></div>
</li>