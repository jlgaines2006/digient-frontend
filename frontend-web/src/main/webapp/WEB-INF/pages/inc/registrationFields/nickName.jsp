<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_nickName" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerNickname"><spring:message code="nickname"/><span id="star_nickName" style="display: none"> *</span></label>
    <input id="registerNickname" class="registerField form-control" type="text" value="" name="nickName" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.form.nickname.title"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerNickname_validOK"></span>
    </div>
    <div id="registerNickname_error" class="registerFieldError help-block error-field"></div>
</li>