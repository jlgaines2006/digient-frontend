<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="li_promoCode" class="form-group has-feedback hidden">
    <label class="registerLabel control-label" for="registrationPromoCode"><spring:message code="register.form.promocode"/><span id="star_promoCode" style="display: none"> *</span></label>
    <div id="registerPromocodeWrapper">
        <input id="registrationPromoCode" class="registerField form-control" type="text" name="promoCode" maxLength="32" />

        <div id="registerPromoHint" class="registerIcons form-control-feedback">
            <i alt="?" title="<spring:message code="register.form.promoHint"/>" class="fa fa-question fa-lg"></i>
            <span class="registerValidOK" id="registrationPromoCode_validOK"></span>
        </div>
        <div id="registrationPromoCode_error" class="registerFieldError help-block error-field"></div>
    </div>
</div>