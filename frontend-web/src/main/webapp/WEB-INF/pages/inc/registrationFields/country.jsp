<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_country" class="form-group" style="display: none;">
    <label class="registerLabel control-label" for="registerCountrySelector"><spring:message code="country"/><span id="star_country" style="display: none;"> *</span></label>
    <select id="registerCountrySelector" class="form-control" name="country"></select>
    <script type="text/javascript">
        (function () {
            if ($('#registerCountrySelector').has('option').length == 0) $('#registerCountrySelector').load('${cmsRoot}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-country.html');
        })();
    </script>
</li>