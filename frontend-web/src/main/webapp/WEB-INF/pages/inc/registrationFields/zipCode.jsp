<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_zipCode" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerZipCode"><spring:message code="myaccount.zipCode"/><span id="star_zipCode" style="display: none"> *</span></label>
    <input id="registerZipCode" class="registerField form-control" type="text" value="" name="zipCode" maxlength="16" onkeypress="validateInputs(event, 'zipCode');"/>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.zipCode.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerZipCode_validOK"></span>
    </div>
    <div id="registerZipCode_error" class="registerFieldError help-block error-field"></div>
</li>