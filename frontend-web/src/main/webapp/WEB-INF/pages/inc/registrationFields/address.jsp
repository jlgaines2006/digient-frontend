<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_address" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerAddress"><spring:message code="address"/><span id="star_address" style="display: none"> *</span></label>
    <div class="row">
        <div class="col-xs-8">
            <input id="registerAddress" class="registerField form-control" type="text" value="" name="address" maxlength="256" />
        </div>
        <div class="col-xs-4">
            <input id="registerHouseNumber" class="registerField form-control" type="text" value="" name="houseNumber" maxlength="20" />
        </div>
    </div>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="address.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerAddress_validOK"></span>
    </div>
    <div id="registerAddress_error" class="registerFieldError help-block error-field"></div>
</li>