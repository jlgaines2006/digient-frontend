<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_confirmPassword" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerConfirmPassword"><spring:message code="register.form.confirmPassword"/><span id="star_confirmPassword" style="display: none"> *</span></label>
    <input id="registerConfirmPassword" class="registerField form-control" type="password" value="" name="confirmPassword" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.hint.confirmPassword"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerConfirmPassword_validOK"></span>
    </div>
    <div id="registerConfirmPassword_error" class="registerFieldError help-block error-field"></div>
</li>