<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="li_receiveEmail" class="checkbox" style="display: none">
    <label for="receiveEmail">
        <input id="receiveEmail" type="checkbox" value="1" name="receiveEmail" checked="true" />
        <spring:message code="register.form.receivePromo"/><span id="star_receiveEmail" style="display: none"> *</span>
    </label>
</div>