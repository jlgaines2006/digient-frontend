<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_residentialCity" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerResidentialCity"><spring:message code="myaccount.residentialCity"/><span id="star_residentialCity" style="display: none"> *</span></label>
    <input id="registerResidentialCity" class="registerField form-control" type="text" value="" name="residentialCity" maxlength="64" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.residentialCity.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerResidentialCity_validOK"></span>
    </div>
    <div id="registerResidentialCity_error" class="registerFieldError help-block error-field"></div>
</li>