<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_maidenName" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registermaidenName"><spring:message code="maidenName"/><span id="star_maidenName" style="display: none;"> *</span></label>
    <input id="registermaidenName" class="registerField form-control" type="text" value="" name="maidenName" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="maidenName.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registermaidenName_validOK"></span>
    </div>
    <div id="registermaidenName_error" class="registerFieldError help-block error-field"></div>
</li>