<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_city" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerCity"><spring:message code="myaccount.city"/><span id="star_city" style="display: none"> *</span></label>
    <input id="registerCity" class="registerField form-control" type="text" value="" name="city" maxlength="64" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.city.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerCity_validOK"></span>
    </div>
    <div id="registerCity_error" class="registerFieldError help-block error-field"></div>
</li>