<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_gender" class="form-group" style="display: none">
    <label class="registerLabel control-label"><spring:message code="register.form.gender"/><span id="star_gender" style="display: none"> *</span></label>
    <div id="genderItems" class="labels">
        <label id="genderMaleLabel" class="radio-inline">
            <input id="registerGenderMale" type="radio" name="gender" value="male"><spring:message code="register.form.gender.male"/>
        </label>
        <label id="genderFemaleLabel" class="radio-inline">
            <input id="registerGenderFemale" type="radio" name="gender" value="female"><spring:message code="register.form.gender.female"/>
        </label>
    </div>
    <div id="registerGender_error" class="registerFieldError help-block error-field"></div>
</li>