<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_qqId" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerQqId"><spring:message code="qqId"/><span id="star_qqId" style="display: none"> *</span></label>
    <input id="registerQqId" class="registerField form-control" type="text" value="" name="qqId" maxlength="32" onkeypress="validateInputs(event, 'userAccounts');" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="qqId.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerQqId_validOK"></span>
    </div>
    <div id="registerQqId_error" class="registerFieldError help-block error-field"></div>
</li>
