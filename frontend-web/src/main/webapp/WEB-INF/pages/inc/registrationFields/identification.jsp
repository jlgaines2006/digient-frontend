<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_identification" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerIdentificationSelector"><spring:message code="myaccount.identification"/><span id="star_identification" style="display: none;"> *</span></label>
    <select id="registerIdentificationSelector" class="form-control"></select>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.identification.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerIdentificationSelector_validOK"></span>
    </div>
    <div id="registerIdentificationSelector_error" class="registerFieldError help-block error-field"></div>
</li>