<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_weChatId" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerWeChatId"><spring:message code="weChatId"/><span id="star_weChatId" style="display: none"> *</span></label>
    <input id="registerWeChatId" class="registerField form-control" type="text" value="" name="weChatId" maxlength="32" onkeypress="validateInputs(event, 'userAccounts');" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="weChatId.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerWeChatId_validOK"></span>
    </div>
    <div id="registerWeChatId_error" class="registerFieldError help-block error-field"></div>
</li>
