<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_foreignerNumber" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerForeignerNumber"><spring:message code="myaccount.foreignerNumber"/><span id="star_foreignerNumber" style="display: none"> *</span></label>
    <input id="registerForeignerNumber" class="registerField form-control" type="text" value="" name="foreignerNumber" maxlength="256" onkeypress="validateInputs(event, 'bankInfo');"/>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.foreignerNumber.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerForeignerNumber_validOK"></span>
    </div>
    <div id="registerForeignerNumber_error" class="registerFieldError help-block error-field"></div>
</li>