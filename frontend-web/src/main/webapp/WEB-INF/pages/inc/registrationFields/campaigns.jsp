<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="li_campaigns" class="form-group has-feedback hidden">
    <label class="registerLabel control-label" for="registerCampaignSelector"><spring:message code="register.form.campaigns"/></label>
    <select id="registerCampaignSelector" class="form-control" name="campaignId"></select>

    <div class="registerIcons form-control-feedback">
        <i id="registerCampaignSelector_hint" alt="?" title="<spring:message code='register.hint.campaigns'/>" class="fa fa-question fa-lg control-hint"></i>
    </div>
    <div id="registerCampaignSelector_error" class="registerFieldError help-block error-field"></div>
</div>