<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_email" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerEmail"><spring:message code="email"/><span id="star_email" style="display: none"> *</span></label>
    <input id="registerEmail" class="registerField form-control" type="text" value="" name="email" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.form.email.title"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerEmail_validOK"></span>
    </div>
    <div id="registerEmail_error" class="registerFieldError help-block error-field"></div>
</li>