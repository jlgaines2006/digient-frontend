<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_citizenshipNumber" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerCitizenshipNumber"><spring:message code="myaccount.citizenshipNumber"/><span id="star_citizenshipNumber" style="display: none"> *</span></label>
    <input id="registerCitizenshipNumber" class="registerField form-control" type="text" value="" name="citizenshipNumber" onkeypress="validateInputs(event, 'number');" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.citizenshipNumber.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerCitizenshipNumber_validOK"></span>
    </div>
    <div id="registerCitizenshipNumber_error" class="registerFieldError help-block error-field"></div>
</li>