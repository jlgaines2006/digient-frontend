<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_firstName" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerFirstName"><spring:message code="firstName"/><span id="star_firstName" style="display: none"> *</span></label>
    <input id="registerFirstName" class="registerField form-control" type="text" value="" name="firstName" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="firstName.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerFirstName_validOK"></span>
    </div>
    <div id="registerFirstName_error" class="registerFieldError help-block error-field"></div>
</li>