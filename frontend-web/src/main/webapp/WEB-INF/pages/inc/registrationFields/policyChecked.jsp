<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="li_policyChecked" class="checkbox" style="display: none">
    <label for="accept_user_terms">
        <input type="checkbox" name="policyChecked" id="accept_user_terms" />
        <p id="registerTerms1Text"><spring:message code="register.form.terms1"/>&nbsp;
            <a href="#termsPopup" class="fancyboxShowTermsOrPolicy">
                <spring:message code="register.form.terms2"/>
            </a> &amp;
            <a href="#policyPopup" class="fancyboxShowTermsOrPolicy">
                <spring:message code="register.form.policy"/>
            </a><span id="star_policyChecked" style="display: none"> *</span>
        </p>
    </label>
    <div id="accept_user_terms_error" class="registerFieldError_checkbox help-block error-field"></div>
</div>