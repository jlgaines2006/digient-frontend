<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_password" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerPassword"><spring:message code="password"/><span id="star_password" style="display: none"> *</span></label>
    <input id="registerPassword" class="registerField form-control" type="password" value="" name="password" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.hint.password"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerPassword_validOK"></span>
    </div>
    <div id="registerPassword_error" class="registerFieldError help-block error-field"></div>
</li>