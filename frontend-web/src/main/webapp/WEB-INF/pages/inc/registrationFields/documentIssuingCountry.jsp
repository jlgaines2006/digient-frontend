<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_documentIssuingCountry" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerDocumentIssuingCountry"><spring:message code="myaccount.documentIssuingCountry"/><span id="star_documentIssuingCountry" style="display: none;"> *</span></label>
    <select id="registerDocumentIssuingCountry" class="registerField form-control" name="documentIssuingCountry"></select>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.documentIssuingCountry.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerDocumentIssuingCountry_validOK"></span>
    </div>
    <div id="registerDocumentIssuingCountry_error" class="registerFieldError help-block error-field"></div>

    <script type="text/javascript">
        (function () {
            $('#registerDocumentIssuingCountry').load('${cmsRoot}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/register/register-country.html', function () {
                $(this).prepend('<option value="" selected="selected" disabled="disabled">' + registerSelectDocumentIssuingCountry + '</option>');
            });
        })();
    </script>
</li>