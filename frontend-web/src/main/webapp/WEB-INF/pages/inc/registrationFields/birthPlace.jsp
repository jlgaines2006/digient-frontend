<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_birthPlace" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerBirthPlace"><spring:message code="register.form.placeofbirth"/><span id="star_birthPlace" style="display: none"> *</span></label>
    <input id="registerBirthPlace" class="registerField form-control" type="text" value="" name="birthPlace" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.form.placeofbirth.title"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerBirthPlace_validOK"></span>
    </div>
    <div id="registerBirthPlace_error" class="registerFieldError help-block error-field"></div>
</li>