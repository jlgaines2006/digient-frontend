<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_skypeId" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerSkypeId"><spring:message code="skypeId"/><span id="star_skypeId" style="display: none"> *</span></label>
    <input id="registerSkypeId" class="registerField form-control" type="text" value="" name="skypeId" maxlength="32" onkeypress="validateInputs(event, 'userAccounts');" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="skypeId.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerSkypeId_validOK"></span>
    </div>
    <div id="registerSkypeId_error" class="registerFieldError help-block error-field"></div>
</li>
