<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_phoneNumber" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerPhoneNumber"><spring:message code="myaccount.phoneNumber"/><span id="star_phoneNumber" style="display: none"> *</span></label>
    <input id="registerPhoneNumber" class="registerField form-control" name="phoneNumber" type="text" value="" maxlength="20" onkeypress="validateInputs(event, 'phone');" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" id="registerPhoneNumberHint" class="fa fa-question fa-lg"></i>
        <span id="registerPhoneNumber_validOK" class="registerValidOK"></span>
    </div>
    <div id="registerPhoneNumber_error" class="registerFieldError help-block error-field"></div>
</li>