<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_middleName" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerMiddleName"><spring:message code="middleName"/><span id="star_middleName" style="display: none"> *</span></label>
    <input id="registerMiddleName" class="registerField form-control" type="text" value="" name="middleName" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="middleName.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerMiddleName_validOK"></span>
    </div>
    <div id="registerMiddleName_error" class="registerFieldError help-block error-field"></div>
</li>