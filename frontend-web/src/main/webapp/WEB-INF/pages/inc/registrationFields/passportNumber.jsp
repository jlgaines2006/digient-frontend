<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_passportNumber" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerPassportNumber"><spring:message code="myaccount.passportNumber"/><span id="star_passportNumber" style="display: none"> *</span></label>
    <div id="passportNumberDiv">
        <input id="registerPassportNumber" class="registerField form-control" type="text" value="" name="passportNumber" onkeypress="validateInputs(event, 'bankInfo');" />

        <div class="registerIcons form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.passportNumber.hint"/>" class="fa fa-question fa-lg"></i>
            <span class="registerValidOK" id="registerPassportNumber_validOK"></span>
        </div>
        <div id="registerPassportNumber_error" class="registerFieldError help-block error-field"></div>
    </div>
</li>