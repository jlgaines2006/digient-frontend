<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_currency" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerCurrencySelector"><spring:message code="register.form.currency"/><span id="star_currency" style="display: none"> *</span></label>
    <select id="registerCurrencySelector" class="form-control" name="registrationCurrency"></select>

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.currency.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerCurrencySelector_validOK"></span>
    </div>
    <div id="registerCurrencySelector_error" class="registerFieldError help-block error-field"></div>
</li>