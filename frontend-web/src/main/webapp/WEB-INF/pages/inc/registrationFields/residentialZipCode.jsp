<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_residentialZipCode" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerResidentialZipCode"><spring:message code="myaccount.residentialZipCode"/><span id="star_residentialZipCode" style="display: none"> *</span></label>
    <input id="registerResidentialZipCode" class="registerField form-control" type="text" value="" name="residentialZipCode" maxlength="16" onkeypress="validateInputs(event, 'zipCode');" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.residentialZipCode.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerResidentialZipCode_validOK"></span>
    </div>
    <div id="registerResidentialZipCode_error" class="registerFieldError help-block error-field"></div>
</li>