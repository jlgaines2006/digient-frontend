<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_documentType" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label"><spring:message code="myaccount.documentType"/><span id="star_documentType" style="display: none"> *</span></label>
    <div id="documentTypeItems" class="labels">
        <label id="documentTypeIdCardLabel" class="radio-inline">
            <input id="registerDocumentTypeIdCard" type="radio" name="documentType" value="idCard"><spring:message code="myaccount.documentType.idCard"/>
        </label>
        <label id="documentTypePassportLabel" class="radio-inline">
            <input id="registerDocumentTypePassport" type="radio" name="documentType" value="passport"><spring:message code="myaccount.documentType.passport"/>
        </label>
        <label id="documentTypeOtherLabel" class="radio-inline">
            <input id="registerDocumentTypeOther" type="radio" name="documentType" value="other"><spring:message code="myaccount.documentType.other"/>
        </label>
    </div>
    <div id="registerDocumentType_error" class="registerFieldError help-block error-field"></div>
</li>