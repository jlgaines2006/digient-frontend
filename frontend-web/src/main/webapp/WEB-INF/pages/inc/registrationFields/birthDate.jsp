<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_birthDate" class="form-group" style="display: none">
    <label class="registerLabel control-label" for="registerBirthday">
        <spring:message code="register.form.birthDate"/><span id="star_birthDate" style="display: none"> *</span>
        <span class="birthdatetitle"><spring:message code='register.form.birthDateFormat'/></span>
    </label>
    <div id="registerBirthdateWrapper">
        <table>
            <tbody>
              <tr>
                  <td>
                      <select id="registerBirthday" class="form-control" name="day-birthDate">
                          <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.day'/></option>
                          <c:forEach var="day" begin="1" end="31" step="1">
                              <c:set var="dayWithZero" value="0${day}"/>
                              <option value="${day}">${day < 10 ? dayWithZero : day}</option>
                          </c:forEach>
                      </select>
                  </td>

                  <td style="text-align:center;">
                      <select id="registerBirthmonth" class="form-control" name="month-birthDate">
                          <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.month'/></option>
                          <c:forEach var="month" begin="1" end="12" step="1">
                              <c:set var="monthWithZero" value="0${month}"/>
                              <option value="${month}"><spring:message code='month.${month < 10 ? monthWithZero : month}'/></option>
                          </c:forEach>
                      </select>
                  </td>

                  <td style="text-align:right;">
                      <select id="registerBirthyear" class="form-control" name="year-birthDate">
                          <option value="" selected="selected" disabled="disabled"><spring:message code='register.form.birthDate.year'/></option>
                      </select>
                  </td>
              </tr>
            </tbody>
        </table>
    </div>
    <div id="birthdayField_error" class="registerFieldError help-block error-field"></div>
</li>