<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_documentIssuedBy" class="form-group has-feedback" style="display: none;">
    <label class="registerLabel control-label" for="registerDocumentIssuedBy"><spring:message code="myaccount.documentIssuedBy"/><span id="star_documentIssuedBy" style="display: none"> *</span></label>
    <input id="registerDocumentIssuedBy" class="registerField form-control" type="text" value="" name="documentIssuedBy" maxlength="256" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="myaccount.documentIssuedBy.hint"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerDocumentIssuedBy_validOK"></span>
    </div>
    <div id="registerDocumentIssuedBy_error" class="registerFieldError help-block error-field"></div>
</li>