<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_ssn" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="ssnYear"><spring:message code="myaccount.ssn"/><span id="star_ssn" style="display: none"> *</span></label>
    <div id="ssnDiv">
        <div class="row">
            <div class="col-xs-3">
                <input id="ssnYear" class="registerFieldSsn form-control" maxlength="2" onkeypress="validateInputs(event, 'number'); moveToNextField('ssnYear');" placeholder="<spring:message code="year"/>"/>
                <span class="ssnDot">.</span>
            </div>
            <div class="col-xs-3">
                <input id="ssnMonth" class="registerFieldSsn form-control" maxlength="2" onkeypress="validateInputs(event, 'number'); moveToNextField('ssnMonth');" placeholder="<spring:message code="month"/>"/>
                <span class="ssnDot">.</span>
            </div>
            <div class="col-xs-3">
                <input id="ssnDate" class="registerFieldSsn form-control" maxlength="2" onkeypress="validateInputs(event, 'number'); moveToNextField('ssnDate');" placeholder="<spring:message code="date"/>"/>
                <span class="ssnDash">-</span>
            </div>
            <div class="col-xs-3">
                <input id="ssnNumber" class="registerFieldSsn registerFieldSsnNumber form-control" maxlength="5" onkeypress="validateInputs(event, 'number');" placeholder="<spring:message code="number"/>"/>
            </div>
        </div>
        <input id="registerSsn" class="registerField form-control" type="text" value="" name="ssn" style="display: none" />
        <div class="registerIcons form-control-feedback">
            <i alt="?" title="<spring:message code="myaccount.ssn.hint"/>" class="fa fa-question fa-lg"></i>
            <span class="registerValidOK" id="registerSsn_validOK"></span>
        </div>
        <div id="registerSsn_error" class="registerFieldError help-block error-field"></div>
    </div>
</li>