<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li id="li_confirmEmail" class="form-group has-feedback" style="display: none">
    <label class="registerLabel control-label" for="registerConfirmEmail"><spring:message code="register.form.confirmEmail"/><span id="star_confirmEmail" style="display: none"> *</span></label>
    <input id="registerConfirmEmail" class="registerField form-control" type="text" value="" name="confirmEmail" />

    <div class="registerIcons form-control-feedback">
        <i alt="?" title="<spring:message code="register.hint.confirmEmail"/>" class="fa fa-question fa-lg"></i>
        <span class="registerValidOK" id="registerConfirmEmail_validOK"></span>
    </div>
    <div id="registerConfirmEmail_error" class="registerFieldError help-block error-field"></div>
</li>