<div id="sbtech-betting__wrapper" class="row">
    <div id="sbtech-betting__container" class="container">
        <div id="sbtech-betting" class="row">
            <iframe id="sbtech-betting__iframe" frameborder="0" width="100%" height="100%"></iframe>
        </div>
    </div>

    <script>
        (function () {
            'use strict';

            var gameId = 350001,
                mode = userIsLogged ? 'real' : 'demo',
                $iframe = $('#sbtech-betting__iframe'),
                url = (getGameParams(gameId, mode) || {}).url || '';

            $iframe.attr('src', url);

            $(window).on('message onmessage', function (e) {
                var data = e.originalEvent.data || {};

                if (data.height) {
                    $iframe.height(data.height);
                }
            });
        })();
    </script>
</div>