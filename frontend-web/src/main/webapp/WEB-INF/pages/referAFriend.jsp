<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/pages/inc/cookie_language.jsp" %>

<script type="text/javascript">

    $(document).ready(function() {
        referAFriendOnload();
    });

    // Check if there is cookie that will launch the game immediately
    checkGameCookie("gamestart");

</script>

<div id="main-fluid" class="row">
    <div class="container">
        <div id="main" class="row">

            <div id="main_leftpart" class="col-md-${features.mainBannersOthers != 'false' ? '9' : '12'}">

                <div id="content">

                    <!-- Title tab -->
                    <div class="titletab">

                        <div class="titletabcontent">
                            <spring:message code="referAFriend.titletab"/>
                        </div>
                    </div>

                    <img src="/cms/languages/en/images/banners/referAFriendBanner.png" alt="" />

                    <div id="referAFriendSuccess" style="display: none">
                        <c:import charEncoding="UTF-8" url="${cmsRootLocal}/languages/${_lang == null ? features.defaultLanguage : _lang}/pages/contact-us/contactus-thanks.html" />
                    </div>

                    <div id="referAFriendWrapper">

                        <div id="referAFriendWrapper_curtain" style="display: none"></div>

                        <form action="" id="referAFriendForm">

                            <fieldset id="referAFriendDetails">

                                <legend><spring:message code="referAFriend.legend"/></legend>

                                <p id="referAFriend_error" class="error" style="display: none"></p>

                                <ul id="referAFriendDetails_ul">

                                    <li id="yourName_item">

                                        <label for="referAFriend_yourName"><spring:message code="referAFriend.yourName"/>
                                        </label>
                                        <input type="text" name="yourName" value="" id="referAFriend_yourName" maxlength="256" />
                                        <div class="referAFriendFieldError" id="referAFriend_yourName_error"></div>

                                    </li>

                                    <li id="friendsName_item">

                                        <label for="referAFriend_friendsName"><spring:message code="referAFriend.friendsName"/>
                                        </label>
                                        <input type="text" name="friendsName" value="" id="referAFriend_friendsName" maxlength="256" />
                                        <div class="referAFriendFieldError" id="referAFriend_friendsName_error"></div>

                                    </li>

                                    <li id="friendsEmail_item">

                                        <label for="referAFriend_friendsEmail"><spring:message code="referAFriend.friendsEmail"/>
                                        </label>
                                        <input type="text" name="friendsEmail" value="" id="referAFriend_friendsEmail" maxlength="256" />
                                        <div class="referAFriendFieldError" id="referAFriend_friendsEmail_error"></div>

                                    </li>

                                    <li id="yourMessage_item">

                                        <label for="referAFriend_yourMessage"><spring:message code="referAFriend.yourMessage"/>
                                        </label>
                                        <textarea name="yourMessage" value="" id="referAFriend_yourMessage" maxlength="256" ></textarea>
                                        <div class="referAFriendFieldError" id="referAFriend_yourMessage_error"></div>

                                    </li>

                                    <li>
                                        <div id="referAFriend_saveButton" class="submit btn btn-primary btn-lg" onclick="submitReferAFriendForm();"><spring:message code="save"/></div>
                                    </li>

                                </ul>


                            </fieldset>

                        </form>

                    </div>

                </div>

            </div>

            <c:choose>

                <c:when test="${features.mainBannersOthers != 'false'}">

                    <div id="main_rightpart" class="col-md-3">

                        <div id="mainbanners">

                            <c:set var="mainBannersOthersArray" value="${features.mainBannersOthers}"/>

                                <%-- JSTL foreach tag example to loop an array in jsp --%>
                            <c:if test="${!empty mainBannersOthersArray}">
                                <c:forEach var="mainBannersOthersArray" items="${fn:split(mainBannersOthersArray, ',')}">
                                    <c:import url="/WEB-INF/pages/inc/${mainBannersOthersArray}.jsp" />
                                </c:forEach>
                            </c:if>

                        </div>

                    </div>

                </c:when>

            </c:choose>

        </div><%-- #main --%>
    </div>
</div><%-- #main-fluid --%>