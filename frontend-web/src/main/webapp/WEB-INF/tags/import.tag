<%@tag pageEncoding="UTF-8"%>
<%@tag import="java.net.URL"%>
<%@tag import="java.net.HttpURLConnection"%>
<%@tag import="java.io.File"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="url" required="true"%>

<%
    boolean isFile;
    if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
        int statusCode = ((HttpURLConnection) new URL(url).openConnection()).getResponseCode();
        isFile = statusCode >= 200 && statusCode <= 299;
    }
    else {
        isFile = new File(application.getRealPath((String) url)).exists();
    }
    // Import file if exists, otherwise use the tag body
    if (isFile) {
        %> <c:import charEncoding="UTF-8" url="${url}"/> <%
    }
    else {
        %> <jsp:doBody/> <%
    }
%>