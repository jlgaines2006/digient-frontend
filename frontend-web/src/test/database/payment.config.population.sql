set @siteId=1;

-- Let's add provider data for deposit:
INSERT INTO `PRODUCT` (`PK_PRODUCT_ID`, `PRODUCT_CATEGORY`, `PRODUCT_NAME`) VALUES (11137, 0, 'ap');
INSERT INTO PAYMENT_PROVIDER (PK_PROVIDER_ID, NAME) VALUES (50, 'ap');
INSERT INTO PAYMENT_METHOD (PK_PAYMENT_METHOD_ID, PROVIDER, FK_PRODUCT_ID, DIRECTION, DESCRIPTION) VALUES(320001, 50, 11137, 0, 'Asia Payment');
INSERT INTO `PAYMENT_METHOD_FIELD` (`FK_PAYMENT_METHOD_ID`, `FIELD`, `OPTIONAL`, `MIN_LENGTH`, `MAX_LENGTH`, `FIELD_TYPE`, `PAYMENT_FIELD`) VALUES(320001, 'PaymentMethodId',   0,   0, 100, 'text', 'PaymentMethodId');
INSERT INTO `PAYMENT_METHOD_FIELD` (`FK_PAYMENT_METHOD_ID`, `FIELD`, `OPTIONAL`, `MIN_LENGTH`, `MAX_LENGTH`, `FIELD_TYPE`, `PAYMENT_FIELD`) VALUES(320001, 'BankCode',          0,   0, 100,  'text', 'BankCode');
INSERT INTO PSP_CONFIG (`FK_PAYMENT_METHOD_ID`, `METHOD_ORDER`, `DIRECTION`, `PROVIDER`, `FK_PRODUCT_ID`, `INCLUDE_COUNTRY`, `SITE_ID`, `MIN_AMOUNT`, `MAX_AMOUNT`, `AMOUNT_LIST`) VALUES (320001,  1, 0, 50, 11137, 'ALL', 1, '5.00', '1000.00','5.00,10.00,100.00,1000.00');

-- Let's add provider data for withdrawals:
INSERT INTO `PRODUCT` (`PK_PRODUCT_ID`, `PRODUCT_CATEGORY`, `PRODUCT_NAME`) VALUES (11427, 0, 'ap_withdrawal');
INSERT INTO PAYMENT_PROVIDER (PK_PROVIDER_ID, NAME) VALUES (51, 'ap_withdrawal');
INSERT INTO PAYMENT_METHOD (PK_PAYMENT_METHOD_ID, PROVIDER, FK_PRODUCT_ID, DIRECTION, DESCRIPTION) VALUES(1033, 51, 11427, 1, 'Asia Payment payout');
INSERT INTO PSP_CONFIG (`FK_PAYMENT_METHOD_ID`, `METHOD_ORDER`, `DIRECTION`, `PROVIDER`, `FK_PRODUCT_ID`, `INCLUDE_COUNTRY`,  `SITE_ID`, `MIN_AMOUNT`, `MAX_AMOUNT`) VALUES (1033, 1, 1, 51, 11427, 'ALL', 1, '10.00', '1000.00');

-- Let's add Asia Payment properties:
INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_AP_API_URL_TEST', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_AP_API_URL_TEST',  'http://ppsapi-test.elasticbeanstalk.com/');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_AP_API_USERNAME', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_AP_API_USERNAME',  'Id41e2ff9a-694b-413f-a57c-318948cf7f0c');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_AP_API_PASSWORD', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_AP_API_PASSWORD',  'Keyd7767e23-ed4a-4f1e-be3c-595ecf7199fc');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_AP_SECRET_KEY', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_AP_SECRET_KEY',    'some-key');

-- Let's add common configurations properties for all providers:
INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_MODE', 1, 0);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_MODE',  'false');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_URL_OK', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_URL_OK', 'http://localhost:8080/postlogin/payment/deposit?depositStatus=ok');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_URL_CANCELLED', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_URL_CANCELLED', 'http://localhost:8080/postlogin/payment/deposit?depositStatus=cancelled');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_URL_FAILED', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_URL_FAILED', 'http://localhost:8080/postlogin/payment/deposit?depositStatus=failure');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_URL_REJECTED', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_URL_REJECTED', 'http://localhost:8080/postlogin/payment/deposit?depositStatus=rejected');

INSERT INTO BILLFOLD_CONFIG (PROPERTY_KEY, SITE, PROPERTY_TYPE) VALUES ('PAYMENT_URL_NOTIFICATION_BASE', 1, 3);
INSERT INTO BILLFOLD_PROPERTY (FK_APPLICATION_NAME_ID, PROPERTY_KEY, PROPERTY_VALUE) VALUES (@siteId, 'PAYMENT_URL_NOTIFICATION_BASE', 'http://localhost:8080/status-web');
