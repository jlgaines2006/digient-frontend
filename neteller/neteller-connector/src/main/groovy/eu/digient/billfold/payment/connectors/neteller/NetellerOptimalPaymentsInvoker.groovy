package eu.digient.billfold.payment.connectors.neteller

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.connector.AbstractBaseConnector
import eu.digient.sdk.lang.BigDecimals
import eu.digient.sdk.util.StringUtil
import eu.digient.sdk.util.Base64Util
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

interface NetellerOptimalPaymentsInvoker {
    /**
     * Obtain an access token from Neteller
     * <p>
     * @param siteId - Id of the site
     * @return authentication token in case of success
     * @throws PaymentGatewayException in case of the failure
     */
    String obtainOAuth2Token(Integer siteId) throws PaymentGatewayException

    /**
     * Sends deposit to neteller
     * <p>
     * @param authToken - Authentication token used by the api
     * @param siteId - Site id where this payout was created
     * @param amount - Amount of the payout
     * @param currency - Currency of the payout
     * @param netAccountId - Net account id
     * @param secureId - Secure Id
     * @param reference - Reference of this payment
     * @return Data returned from neteller
     * @throws PaymentGatewayException - In case of something goes wrong
     */
    Netdirect transferIn(String authToken, Integer siteId, BigDecimal amount, String currency, String netAccountId, String secureId, String reference) throws PaymentGatewayException

    /**
     * Sends withdrawal to neteller
     * <p>
     * @param authToken - Authentication token used by the api
     * @param siteId - Site id where this payout was created
     * @param amount - Amount of the payout
     * @param currency - Currency of the payout
     * @param netAccountId - Net account id
     * @param secureId - Secure Id
     * @param reference - Reference of this payment
     * @return Data returned from neteller
     * @throws PaymentGatewayException - In case of something goes wrong
     */
    Netdirect transferOut(String authToken, Integer siteId, String language, BigDecimal amount, String currency, String netellerAccountId, String reference)  throws PaymentGatewayException
}

@Component
class NetellerOptimalPaymentsInvokerImpl extends AbstractBaseConnector implements NetellerOptimalPaymentsInvoker {
    private static final def LOG = LoggerFactory.getLogger(NetellerOptimalPaymentsInvoker)
    private static final def INPUT_OUTPUT = LoggerFactory.getLogger('INPUT_OUTPUT_NETELLER')

    @Autowired NetellerHttpUtil http

    NetellerOptimalPaymentsInvokerImpl() {
        name = Payment.PaymentProviderName.neteller
    }

    String obtainOAuth2Token(final Integer siteId)
            throws PaymentGatewayException
    {
        long x1 = System.currentTimeMillis()
        boolean error = false
        String errorMsg = null
        def url = null
        Map<String, String> headers = null
        def response = null
        try {
            url = getApiUrl(siteId) + '/oauth2/token?grant_type=client_credentials'
            def clientId = getApiUsername(siteId)
            def clientSecret = getApiPassword(siteId)
            headers = [
                    'Authorization': "Basic ${new String(Base64Util.encode(clientId + ':' + clientSecret))}",
                    'Content-Type' : 'application/json',
                    'Cache-Control': 'no-cache'
            ]
            response = http.post(url, headers, null)
            def accessToken = response.accessToken
            if (accessToken?.size() < 1) {
                errorMsg = response?.error?.toString()
                LOG.error('[obtainOAuth2Token: {}] no token in response, error: [{}]', clientId, errorMsg)
                throw new PaymentGatewayException(errorMessage: response)
            }
            return accessToken
        } catch (PaymentGatewayException e) {
            error = true
            throw e
        } catch (IllegalArgumentException e) {
            error = true
            errorMsg = e.message
            throw new PaymentGatewayException(errorMessage: 'Unable to parse json response from Neteller server. Message: ' + e.message)
        } catch (Exception e) {
            LOG.error('[obtainOAuth2Token: {}] Something went wrong', clientId, e)
            error = true
            errorMsg = e.toString()
            throw  createNewException(e)
        } finally {
            def time = System.currentTimeMillis() - x1
            if (error) {
                INPUT_OUTPUT.error('[OAUTH2][URL]{}[HEADERS]{}[ERROR]{}[RESPONSE]{}[TIME][{}][END]', url, headers, errorMsg, response, time)
            } else {
                INPUT_OUTPUT.debug('[OAUTH2][URL]{}[HEADERS]{}[RESPONSE]{}[TIME][{}][END]', url, headers, headers, response, time)
            }
        }
    }

    Netdirect transferIn(final String authToken, final Integer siteId, final BigDecimal amount, final String currency, final String netAccountId, final String secureId, final String reference)
            throws PaymentGatewayException
    {
        long x1 = System.currentTimeMillis()
        def url = null
        Map<String, String> headers = null
        Map<String, Object> request = [:]
        def response = null
        try {
            url = getApiUrl(siteId) + '/transferIn'

            headers = [
                    'Authorization': "Bearer ${authToken}",
                    'Content-Type' : 'application/json',
            ]

            request.paymentMethod = [type: 'neteller', value: netAccountId]
            request.transaction = [merchantRefId: reference, amount: BigDecimals.toCents(amount), currency: currency]
            request.verificationCode = secureId

            response = http.post(url, headers, request)

            def id = response?.transaction?.id?.toString()
            def status = response?.transaction?.status?.toString()
            if (id?.size() > 0 && status == 'accepted') {
                return new Netdirect(approval: Approval.yes, transId: id)
            }
            else {
                return new Netdirect(approval: Approval.no, error: 'return data does not contain id of transaction, return data: [' + response + ']')
            }
        } catch (Exception e) {
            throw createNewException(e)
        } finally {
            def time = System.currentTimeMillis() - x1
            INPUT_OUTPUT.debug('[OAUTH2][URL]{}[HEADERS]{}[REQUEST]{}[RESPONSE]{}[TIME][{}][END]', url, headers, request, response, time)
        }

    }

    Netdirect transferOut(final String authToken, final Integer siteId, final String language, final BigDecimal amount, final String currency, final String netellerAccountId, final String reference)
            throws PaymentGatewayException
    {
        long x1 = System.currentTimeMillis()
        boolean error = false
        String errorMsg = null
        def url = null
        Map<String, String> headers = null
        Map<String, Object> request = [:]

        def response = null
        try {
            url = getApiUrl(siteId) + '/transferOut'

            headers = [
                    'Authorization': "Bearer ${authToken}",
                    'Content-Type' : 'application/json',
            ]

            if (StringUtil.isEmailID(netellerAccountId)) {
                request.payeeProfile = [email: netellerAccountId]
            } else {
                request.payeeProfile = [accountId: netellerAccountId]
            }

            request.transaction = [amount: BigDecimals.toCents(amount), currency: currency, merchantRefId: reference]
            request.message = getMessage('payment.neteller.transferOut', language)

            response = http.post(url, headers, request)

            def id = response?.transaction?.id?.toString()
            def status = response?.transaction?.status?.toString()
            if (id?.size() > 0 && status == 'accepted') {
                return new Netdirect(approval: Approval.yes, transId: id, errorMessage: response?.toString())
            }
            else {
                return new Netdirect(approval: Approval.no, errorMessage: 'return data does not contain id of transaction, return data: [' + response?.toString() + ']')
            }
        } catch (Exception e) {
            error = true
            errorMsg = e.toString()
            throw e
        } finally {
            def time = System.currentTimeMillis() - x1
            if (error != null) {
                INPUT_OUTPUT.debug('[OAUTH2][URL]{}[HEADERS]{}[ERROR]{}[RESPONSE]{}[TIME][{}][END]', url, headers, errorMsg, headers, response, time)
            } else {
                INPUT_OUTPUT.debug('[OAUTH2][URL]{}[HEADERS]{}[RESPONSE]{}[TIME][{}][END]', url, headers, headers, response, time)
            }
        }
    }
}
