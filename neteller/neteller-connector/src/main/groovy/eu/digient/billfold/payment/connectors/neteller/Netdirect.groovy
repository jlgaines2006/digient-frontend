package eu.digient.billfold.payment.connectors.neteller

import eu.digient.sdk.lang.annotation.Equals
import eu.digient.sdk.lang.annotation.EqualsStrategy
import eu.digient.sdk.lang.annotation.HashCode
import eu.digient.sdk.lang.annotation.HashCodeStrategy
import eu.digient.sdk.lang.annotation.ToString
import eu.digient.sdk.lang.annotation.ToStringStrategy

class Netdirect implements Serializable {
    private static final long serialVersionUID = 1L
    @ToString @Equals @HashCode Approval approval
    @ToString @Equals @HashCode String transId
    @ToString @Equals @HashCode String errorMessage
    @ToString @Equals @HashCode String error

    @Override
    String toString() {
        ToStringStrategy.byAnnotation(Netdirect, this)
    }

    @Override
    boolean equals(Object that) {
        EqualsStrategy.byAnnotation(Netdirect, this, that)
    }

    @Override
    int hashCode() {
        HashCodeStrategy.byAnnotation(Netdirect, this)
    }

}
