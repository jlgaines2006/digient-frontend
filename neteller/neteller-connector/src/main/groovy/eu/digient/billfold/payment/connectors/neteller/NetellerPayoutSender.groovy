package eu.digient.billfold.payment.connectors.neteller

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.PayoutSendResult
import eu.digient.billfold.payment.PayoutSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
@Qualifier(value = 'neteller_withdrawal')
class NetellerPayoutSender implements PayoutSender {
    @Autowired NetellerOptimalPaymentsInvoker invoker

    PayoutSendResult send(final Payment payment) {
        try {
            def authToken = invoker.obtainOAuth2Token(payment.siteId)
            Netdirect r = invoker.transferOut(authToken, payment.siteId, payment.language, payment.bankAmount, payment.bankCurrency, payment.netellerAccountId, payment.reference)

            def map = [:]
            map.message = r.errorMessage
            map.approval =  r.approval

            if (r.approval == Approval.yes) {
                return new PayoutSendResult(success: true, providerReference: r.transId, extraInfo: map.toString())
            } else {
                return new PayoutSendResult(success: false, errorText: map.toString() + ' (' + r.errorMessage + ')')
            }
        } catch (PaymentGatewayException e) {
            return new PayoutSendResult(success: false, errorText: e.errorMessage)
        }
    }

    Payment.PaymentProviderName getName() {
        Payment.PaymentProviderName.neteller_withdrawal
    }
}
