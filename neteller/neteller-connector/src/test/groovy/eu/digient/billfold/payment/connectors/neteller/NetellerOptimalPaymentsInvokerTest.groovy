package eu.digient.billfold.payment.connectors.neteller

import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.sdk.util.Base64Util
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.context.MessageSource

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertTrue
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

@RunWith(MockitoJUnitRunner)
class NetellerOptimalPaymentsInvokerTest {
    NetellerOptimalPaymentsInvoker target

    @Mock BillfoldConfigService configService
    @Mock MessageSource messageSource
    @Mock NetellerHttpUtil http

    Integer siteId = 1

    @Before
    void setup() {
        target = new NetellerOptimalPaymentsInvokerImpl(http: http, configService: configService, messageSource: messageSource)

        when(configService.getPropertyAsBoolean(siteId, 'PAYMENT_MODE')).thenReturn(false)
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_API_URL_TEST')).thenReturn('some-url')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_API_USERNAME')).thenReturn('some-user')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_API_PASSWORD')).thenReturn('some-pwd')
    }

    @Test
    void obtainOAuth2Token() {
        def url = 'some-url/oauth2/token?grant_type=client_credentials'
        def headers = [
                'Authorization': "Basic ${new String(Base64Util.encode('some-user:some-pwd'))}",
                'Content-Type' : 'application/json',
                'Cache-Control': 'no-cache'
        ]

        when(http.post(url, headers, null)).thenReturn([accessToken: 'some-token'])
        def r = target.obtainOAuth2Token(siteId)

        assertEquals 'some-token', r
    }

    @Ignore
    @Test
    void transferIn() {
        def url = 'some-url/transferIn'
        def headers = [
                'Authorization': "Bearer some-token",
                'Content-Type' : 'application/json',
        ]

        def map = [:]
        map.paymentMethod = [type: 'neteller', value: 'some-net-id']
        map.transaction = [merchantRefId: 'some-ref', amount: 100, currency: 'some-cur']
        map.verificationCode = 'some-secure-id'

        when(http.post(url, headers, map)).thenReturn([transaction: [id: 'some-id', status: 'accepted']])

        def r = target.transferIn('some-token', siteId, new BigDecimal('1'), 'some-cur', 'some-net-id', 'some-secure-id', 'some-ref')
        verify(http).post(url, headers, map)

        assertEquals Approval.yes, r.approval
        assertEquals 'some-id', r.transId
    }

    @Test
    void transferIn_not_approved() {
        def r = target.transferIn('some-token', siteId, new BigDecimal('1'), 'some-cur', 'some-net-id', 'some-secure-id', 'some-ref')
        assertEquals Approval.no, r.approval
    }

    @Ignore
    @Test
    void transferOut() {
        when(messageSource.getMessage('payment.neteller.transferOut', null, new Locale('en'))).thenReturn('some-trans')

        def url = 'some-url/transferOut'
        def headers = [
                'Authorization': 'Bearer some-token',
                'Content-Type' : 'application/json',
        ]

        def map = [:]
        map.payeeProfile = [email: 'some-net-id']
        map.transaction = [amount: 100, currency: 'some-cur', merchantRefId: 'some-ref']
        map.message = 'some-trans'

        when(http.post(url, headers, map)).thenReturn([transaction: [id: 'some-id', status: 'accepted']])

        def r = target.transferOut('some-token', siteId, 'en', new BigDecimal('1'), 'some-cur', 'some-net-id', 'some-ref')
        assertEquals Approval.yes, r.approval
        assertEquals 'some-id', r.transId
    }


}

@Ignore
@RunWith(MockitoJUnitRunner)
class NetellerOptimalPaymentsInvokerIntTest {
    NetellerOptimalPaymentsInvoker target

    @Mock BillfoldConfigService configService
    @Mock MessageSource messageSource

    Integer siteId = 1

    @Before
    void setup() {
        target = new NetellerOptimalPaymentsInvokerImpl(http: new NetellerHttpUtilImpl(), configService: configService, messageSource: messageSource)

        when(configService.getPropertyAsBoolean(siteId, 'PAYMENT_MODE')).thenReturn(false)
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_API_URL_TEST')).thenReturn('https://test.api.neteller.com/v1')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_API_USERNAME')).thenReturn('AAABTpQVgt5NJ-IT')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_API_PASSWORD')).thenReturn('0.Hxu6QqL_abPYbuzNzmRKnmZCGRZRh53PLBZDvgALW60.dL2jkQA7I8J3vUufMshY--rE_OA')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_NETELLER_MERCHANT_ID')).thenReturn('35421')
        when(messageSource.getMessage('payment.neteller.transferIn', null, new Locale('en'))).thenReturn('some-trans')
    }

    @Test void transferIn() {
        def siteId = 1
        def authToken = target.obtainOAuth2Token(siteId)
        assertNotNull(authToken)

        def amount = new BigDecimal('3.00')
        def currency = 'EUR'
        def netId = 'netellertest_EUR@neteller.com'
        def secureId = '908379'
        def reference = System.currentTimeMillis().toString()

        def approval = target.transferIn(authToken, siteId, amount, currency, netId, secureId, reference)
        assertEquals(Approval.yes, approval.approval)
        assertNotNull(approval.transId)
    }

    @Test void transferIn_wrong_credentials() {
        def siteId = 1
        def authToken = target.obtainOAuth2Token(siteId)
        assertNotNull(authToken)

        def amount = new BigDecimal('10')
        def currency = 'EUR'
        def netId = 'wrong@neteller.com'
        def secureId = '123456'
        def reference = System.currentTimeMillis().toString()

        def approval = target.transferIn(authToken, siteId, amount, currency, netId, secureId, reference)
        assertEquals(Approval.no, approval.approval)
        assertTrue(approval.error.contains('20007'))
        assertTrue(approval.error.contains('Invalid accountId or email'))
    }

    @Test void transferOut() {
        def siteId = 1
        def authToken = target.obtainOAuth2Token(siteId)
        assertNotNull(authToken)

        def amount = new BigDecimal('4.00')
        def currency = 'EUR'
        def netId = 'netellertest_EUR@neteller.com'
        def lang = 'en'
        def reference = System.currentTimeMillis().toString()

        def approval = target.transferOut(authToken, siteId, lang, amount, currency, netId, reference)
        assertEquals(Approval.yes, approval.approval)
        assertNotNull(approval.transId)
    }


}