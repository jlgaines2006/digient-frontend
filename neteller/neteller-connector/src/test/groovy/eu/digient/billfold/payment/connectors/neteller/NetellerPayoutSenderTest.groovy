package eu.digient.billfold.payment.connectors.neteller

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.test.AbstractBaseTest
import org.easymock.EasyMock
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class NetellerPayoutSenderTest extends AbstractBaseTest {
    NetellerPayoutSender target

    NetellerOptimalPaymentsInvoker invoker

    @Before void setUp() {
        target = new NetellerPayoutSender()

        invoker = EasyMock.createMock(NetellerOptimalPaymentsInvoker)
        addMock(target, 'invoker', invoker)
    }

    @Test void send() {
        EasyMock.expect(invoker.obtainOAuth2Token(1)).andReturn('some-token')
        EasyMock.expect(invoker.transferOut('some-token', 1, 'some-lang', new BigDecimal('10'), 'some-cur', 'some-account-id', 'some-reference')).andReturn(new Netdirect(transId: 'some-provider-reference', approval: Approval.yes))

        replay()
        def r = target.send(new Payment(siteId: 1, reference: 'some-reference', language: 'some-lang', bankAmount: new BigDecimal('10'), bankCurrency: 'some-cur', netellerAccountId: 'some-account-id'))
        assertEquals('some-provider-reference', r.providerReference)
        assertTrue r.success
        assertEquals '[message:null, approval:yes]', r.extraInfo
    }

    @Test void send_failed() {
        EasyMock.expect(invoker.obtainOAuth2Token(1)).andThrow(new PaymentGatewayException(errorMessage: 'some-error'))

        replay()
        def r = target.send(new Payment(siteId: 1, reference: 'some-reference', bankAmount: new BigDecimal('10'), bankCurrency: 'some-cur', netellerAccountId: 'some-account-id'))
        assertEquals('some-error', r.errorText)
        assertFalse r.success
    }

}
