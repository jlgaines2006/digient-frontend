package eu.digient.billfold.payment.providers.neteller

import eu.digient.billfold.payment.NoPaymentDetailsFilledException
import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.payment.connectors.neteller.Netdirect
import eu.digient.billfold.payment.connectors.neteller.NetellerOptimalPaymentsInvoker
import eu.digient.billfold.payment.connectors.neteller.Approval
import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.billfold.user.User
import eu.digient.billfold.user.service.PaymentDetailService
import eu.digient.billfold.user.service.UserService
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.context.MessageSource

import static org.junit.Assert.assertEquals
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.verifyZeroInteractions
import static org.mockito.Mockito.when

@Ignore
@RunWith(MockitoJUnitRunner)
class NetellerProviderTest {

    @Mock NetellerOptimalPaymentsInvoker invoker
    @Mock UserService userService
    @Mock BillfoldConfigService configService
    @Mock MessageSource messageSource
    @Mock PaymentDetailService paymentDetailService
    NetellerProvider target

    @Before void setUp(){
        target = new NetellerProvider()
        target.invoker = invoker
        target.userService = userService
        target.configService = configService
        target.messageSource = messageSource
    }

    @Test void createDeposit() {
        when(invoker.obtainOAuth2Token(1)).thenReturn('some-auth-token')
        when(invoker.transferIn('some-auth-token', 1, new BigDecimal('1'), 'some-cur', 'some-account', 'some-secure-id', 'some-ref')).thenReturn(new Netdirect(approval: Approval.yes, transId: "trans_id"))
        when(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).thenReturn('some-url')

        User user = new User(userId: 1L, siteId: 1, language: 'fi')
        Payment payment = new Payment(paymentIp: 'ip', amount: new BigDecimal('1'), currency: 'some-cur', reference: 'some-ref')
        def userInput = [netellerAccountId: 'some-account', secureId: 'some-secure-id', password: 'some-pwd']

        def r = target.createDeposit(payment, user, userInput)
        assertEquals(PaymentStatus.ok, r.synchronousStatus)
        assertEquals(true, r.synchronous)
        assertEquals('trans_id', r.providerReference)
        assertEquals('some-url', r.redirectUrl)
        verify(paymentDetailService).updateNetellerAccountId(1L, 'some-account')
    }

    @Test void createDeposit_netAccount_from_PaymentData_same_accountId() {
        when(invoker.obtainOAuth2Token(1)).thenReturn('some-auth-token')
        when(invoker.transferIn('some-auth-token', 1, new BigDecimal('1'), 'some-cur', 'some-account', 'some-secure-id', 'some-ref')).thenReturn(new Netdirect(approval: Approval.yes, transId: "trans_id"))
        when(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).thenReturn('some-url')

        User user = new User(siteId: 1, language: 'fi', netellerAccountId: 'some-account')
        Payment payment = new Payment(paymentIp: 'ip', amount: new BigDecimal('1'), currency: 'some-cur', reference: 'some-ref')
        def userInput = [netellerAccountId: 'some-account', secureId: 'some-secure-id', password: 'some-pwd']

        def r = target.createDeposit(payment, user, userInput)
        assertEquals(PaymentStatus.ok, r.synchronousStatus)
        assertEquals(true, r.synchronous)
        assertEquals('trans_id', r.providerReference)
        assertEquals('some-url', r.redirectUrl)
        verifyZeroInteractions(userService)
    }

    @Test void createDeposit_netAccount_from_User() {
        when(invoker.obtainOAuth2Token(1)).thenReturn('some-auth-token')
        when(invoker.transferIn('some-auth-token', 1, new BigDecimal('1'), 'some-cur', 'some-account', 'some-secure-id', 'some-ref')).thenReturn(new Netdirect(approval: Approval.yes, transId: "trans_id"))
        when(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).thenReturn('some-url')

        User user = new User(siteId: 1, language: 'fi', netellerAccountId: 'some-account')
        Payment payment = new Payment(paymentIp: 'ip', amount: new BigDecimal('1'), currency: 'some-cur', reference: 'some-ref')
        def userInput = [secureId: 'some-secure-id', password: 'some-pwd']

        def r = target.createDeposit(payment, user, userInput)
        assertEquals(PaymentStatus.ok, r.synchronousStatus)
        assertEquals(true, r.synchronous)
        assertEquals('trans_id', r.providerReference)
        assertEquals('some-url', r.redirectUrl)
        verifyZeroInteractions(userService)
    }

    @Test
    void createDeposit_netAccount_no_netAccount_filled(){
        try {
            target.createDeposit(new Payment(), new User(), null)
        } catch (NoPaymentDetailsFilledException e) {
            assertEquals('netellerAccountId', e.fieldName)
        }
    }

    @Test
    void createDeposit_netAccount_no_secureId_filled(){
        try {
            target.createDeposit(new Payment(), new User(netellerAccountId: 'some-id'), null)
        } catch (NoPaymentDetailsFilledException e) {
            assertEquals('secureId', e.fieldName)
        }
    }

}
