package eu.digient.billfold.payment.providers.neteller

import eu.digient.billfold.account.Product
import eu.digient.billfold.payment.CreateDepositResult
import eu.digient.billfold.payment.NoPaymentDetailsFilledException
import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentException
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.PaymentMethod
import eu.digient.billfold.payment.PaymentMethodAttribute
import eu.digient.billfold.payment.PaymentProvider
import eu.digient.billfold.payment.PspProvider
import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.payment.connectors.neteller.Netdirect
import eu.digient.billfold.payment.connectors.neteller.NetellerOptimalPaymentsInvoker
import eu.digient.billfold.payment.connectors.neteller.Approval
import eu.digient.billfold.payment.provider.AbstractPspProvider
import eu.digient.billfold.support.annotations.BillfoldPropertyConfig
import eu.digient.billfold.support.annotations.BillfoldPropertyValue
import eu.digient.billfold.support.annotations.BillfoldPropertyValueContainer
import eu.digient.billfold.user.User
import eu.digient.billfold.user.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component(value = 'neteller')
@BillfoldPropertyConfig
class NetellerProvider extends AbstractPspProvider implements PspProvider {
    private static final def LOG = LoggerFactory.getLogger(NetellerProvider)
    private static final def PROVIDER_LOG = LoggerFactory.getLogger('PROVIDER_LOG')

    @Autowired NetellerOptimalPaymentsInvoker invoker
    @Autowired UserService userService

    NetellerProvider() {
        name = Payment.PaymentProviderName.neteller
    }

    @Override
    CreateDepositResult createDeposit(final Payment payment, final User user, final Map<String, Object> userInput) throws PaymentGatewayException, NoPaymentDetailsFilledException, PaymentException {
        String secureId = userInput?.secureId
        String usedId = userInput?.netellerAccountId
        def oldId = user.paymentDetail.netellerAccountId
        if (usedId == null) {
            usedId = user.paymentDetail.netellerAccountId
        }

        if (usedId == null) {
            LOG.warn '[{}] [userId: {}] netellerAccountId is not provided', payment.reference, user.userId
            throw new NoPaymentDetailsFilledException(fieldName: 'netellerAccountId')
        }

        if (secureId == null) {
            LOG.warn '[{}] [userId: {}] secureId is not provided', payment.reference, user.userId
            throw new NoPaymentDetailsFilledException(fieldName: 'secureId')
        }

        def authToken = invoker.obtainOAuth2Token(user.siteId)
        Netdirect netDirect = invoker.transferIn(authToken, user.siteId, payment.amount, payment.currency, usedId, secureId, payment.reference)

        def r = new CreateDepositResult(providerReference: netDirect?.transId, synchronous: true, updatePayment: [netellerAccountId: usedId])

        if (netDirect.approval == Approval.yes) {
            LOG.debug '[paymentId: {}] Payment approved', payment.reference
            r.providerReference = netDirect.transId
            r.redirectUrl = getOkUrl(user.siteId)
            r.synchronousStatus = PaymentStatus.ok
            if (oldId != usedId) {
                paymentDetailService.updateNetellerAccountId(user.userId, usedId)
            }
            PROVIDER_LOG.info '[{}] [{}] createDeposit [SUCCESS] [{}]', payment.reference, name, payment.amount
        }
        else {
            LOG.debug '[paymentId: {}] Payment declined', payment.reference
            r.errorText = 'Rejected by Neteller! Error: ' + netDirect.error + ', errorMessage: ' + netDirect.errorMessage
            if (r.errorText?.size() > 0 && !r.errorText.endsWith('.')) {
                r.errorText += '.'
            }
            r.redirectUrl = getRejectedUrl(user.siteId)
            r.synchronousStatus = PaymentStatus.rejected
            PROVIDER_LOG.info '[{}] [{}] createDeposit [REJECTED] [{}]', payment.reference, name, payment.amount
        }
        r
    }

    @Override
    Collection<Product> getProductConfiguration() {
        [
                new Product(productId: 11118L, name: 'neteller', productCategory: Product.ProductCategory.payment),
                new Product(productId: 11417L, name: 'neteller_withdrawal', productCategory: Product.ProductCategory.payment)
        ]
    }

    @Override
    Collection<PaymentProvider> getProviderConfiguration()  {
        [
                new PaymentProvider(provider: Payment.PaymentProviderName.neteller, name: 'neteller',
                        paymentMethods: [
                                new PaymentMethod(paymentMethodId: 110002L, productId: 11118L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'NETELLER', direction: Payment.PaymentDirection.deposit, paymentDetailType: 4, helpLinkTarget: '_blank', helpLinkMethod: 'GET', helpLinkTemplate: 'https://www.neteller.com/member/quickSignup?lang={language}&merchantid=11923&currency={currency}&firstname={firstName}&lastname={lastName}&email={email}&country={country2letter}&linkbackurl=/postlogin/payment/deposit', oneClickDeposit: true,
                                        fields: [
                                                new PaymentMethodAttribute(field: 'netellerAccountId',  optional: false, minLength: 1, maxLength: 100, fieldType: 'text', userField: 'netellerAccountId', targetField: 'netellerAccountId', paymentField: 'netellerAccountId'),
                                                new PaymentMethodAttribute(field: 'secureId',           optional: false, minLength: 6, maxLength: 6, fieldType: 'text', userField: 'secureId', targetField: 'secureId')
                                        ]
                                )
                        ]
                ),

                new PaymentProvider(provider: Payment.PaymentProviderName.neteller_withdrawal, name: 'neteller_withdrawal',
                        paymentMethods: [
                                new PaymentMethod(paymentMethodId: 1017L, productId: 11417L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'Neteller payout', supportDirect: true, directEnabled: true, direction: Payment.PaymentDirection.withdraw, paymentDetailType: 4,
                                        fields: [
                                                new PaymentMethodAttribute(field: 'netellerAccountId',  optional: false, minLength: 1, maxLength: 100, fieldType: 'text', userField: 'netellerAccountId', targetField: 'netellerAccountId')
                                        ]
                                )
                        ]
                )
        ]
    }

    @BillfoldPropertyValueContainer(values = [
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 27,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_API_URL_PROD',
                    defaultValue = 'https://api.neteller.com/v1'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 27,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_API_URL_TEST',
                    defaultValue = 'https://test.api.neteller.com/v1'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 27,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_API_USERNAME',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 27,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_API_PASSWORD',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 27,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_RESULT_URL_PREFIX',
                    defaultValue = 'http://web.finnplay.com/pos tlogin/payment/deposit?depositStatus='
            ),

            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 28,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_PAYOUT_MERCHANT_ID',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 28,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_PAYOUT_MERCHANT_KEY',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 28,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_PAYOUT_MERCHANT_PASS',
                    defaultValue = 'CONFIGURE-ME'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 28,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_PAYOUT_URL',
                    defaultValue = 'https://test.api.neteller.com/instantpayout'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 28,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_PAYOUT_ENCODING',
                    defaultValue = 'UTF-8'
            ),
            @BillfoldPropertyValue(
                    group = 1,
                    subGroup = 28,
                    version = '1.36',
                    key = 'PAYMENT_NETELLER_PAYOUT_VERSION',
                    defaultValue = '4.1'
            )
    ])
    void billfoldProperties() {}

}
