package eu.digient.billfold.payment.providers.makati

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.payment.connectors.ap.AsiaPaymentServiceInvoker
import eu.digient.billfold.payment.providers.ap.AsiaPaymentProvider
import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.billfold.user.User
import eu.digient.test.AbstractBaseTest
import org.easymock.EasyMock
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals

class AsiaPaymentProviderTest extends AbstractBaseTest {
    AsiaPaymentProvider target

    AsiaPaymentServiceInvoker invoker
    BillfoldConfigService configService

    @Before void setUp() {
        target = new AsiaPaymentProvider(invoker: invoker)

        invoker = EasyMock.createMock(AsiaPaymentServiceInvoker)
        addMock(target, 'invoker', invoker)

        configService = EasyMock.createMock(BillfoldConfigService)
        addMock(target, 'configService', configService)
    }

    @Test void createDeposit() {
        EasyMock.expect(invoker.deposit(1, 'some-payment-method-id', new BigDecimal('2'), 'some-bank-code', 'some-ref', 'some-cur')).andReturn('some-string')
        EasyMock.expect(configService.getPropertyAsString(1, 'PAYMENT_URL_OK')).andReturn('some-url')

        replay()
        def r = target.createDeposit(new Payment(reference: 'some-ref', amount: new BigDecimal('2'), currency: 'some-cur'), new User(siteId: 1), [BankCode: 'some-bank-code', 'PaymentMethodId': 'some-payment-method-id'])
        assertEquals('some-string', r.providerReference)
        assertEquals('some-url', r.redirectUrl)
        assertEquals(true, r.synchronous)
        assertEquals(PaymentStatus.ok, r.synchronousStatus)
    }
}
