package eu.digient.billfold.payment.providers.ap

import eu.digient.billfold.account.Product
import eu.digient.billfold.payment.CreateDepositResult
import eu.digient.billfold.payment.CreateWithdrawResult
import eu.digient.billfold.payment.NoPaymentDetailsFilledException
import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.PaymentException
import eu.digient.billfold.payment.PaymentGatewayException
import eu.digient.billfold.payment.PaymentMethod
import eu.digient.billfold.payment.PaymentMethodAttribute
import eu.digient.billfold.payment.PaymentProvider
import eu.digient.billfold.payment.PaymentStatus
import eu.digient.billfold.payment.PspProvider
import eu.digient.billfold.payment.connectors.ap.AsiaPaymentServiceInvoker
import eu.digient.billfold.payment.provider.AbstractPspProvider
import eu.digient.billfold.support.annotations.BillfoldPropertyConfig
import eu.digient.billfold.user.User
import eu.digient.billfold.user.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component(value = 'ap')
@BillfoldPropertyConfig
class AsiaPaymentProvider extends AbstractPspProvider implements PspProvider {
    private static final def PROVIDER_LOG = LoggerFactory.getLogger('PROVIDER_LOG')
    private static final def SUCCESS = '00001'

    @Autowired AsiaPaymentServiceInvoker invoker
    @Autowired UserService userService

    AsiaPaymentProvider() {
        name = Payment.PaymentProviderName.ap
    }

    @Override
    CreateDepositResult createDeposit(final Payment payment, final User user, final Map<String, Object> userInput) throws PaymentGatewayException, NoPaymentDetailsFilledException, PaymentException {
        String paymentMethodId = userInput?.PaymentMethodId
        String bankCode = userInput?.BankCode

        String providerReference = invoker.deposit(user.siteId, paymentMethodId, payment.amount, bankCode, payment.reference, payment.currency)
        PROVIDER_LOG.info '[{}] [{}] createDeposit [SUCCESS] [{}]', payment.reference, name, payment.amount
        new CreateDepositResult(synchronous: true, synchronousStatus: PaymentStatus.ok, providerReference: providerReference, redirectUrl: getOkUrl(user.siteId))
    }

    CreateWithdrawResult createWithdraw(final Payment payment, final User user, final Map<String, Object> userInput) throws PaymentGatewayException, NoPaymentDetailsFilledException, PaymentException {
        String paymentMethodId = userInput?.PaymentMethodId
        String bankCode=userInput?.BankCode

        String result = invoker.deposit(user.siteId, paymentMethodId, payment.amount, bankCode, payment.reference, payment.currency)

        if (result.equals(SUCCESS)) {
            return new CreateWithdrawResult(success: true)
        } else {
            return new CreateWithdrawResult(success: false)
        }
    }

    @Override
    Collection<Product> getProductConfiguration() {
        [
                new Product(productId: 11137L, name: 'ap', productCategory: Product.ProductCategory.payment),
                new Product(productId: 11427L, name: 'ap_withdrawal', productCategory: Product.ProductCategory.payment)
        ]
    }

    @Override
    Collection<PaymentProvider> getProviderConfiguration()  {
        [
                new PaymentProvider(provider: Payment.PaymentProviderName.ap, name: 'ap',
                        paymentMethods: [
                                new PaymentMethod(paymentMethodId: 320001L, productId: 11137L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'Asia Payment', direction: Payment.PaymentDirection.deposit,
                                        fields: [
                                                new PaymentMethodAttribute(field: 'PaymentMethodId',  optional: false, minLength: 1, maxLength: 100, fieldType: 'text', paymentField: 'PaymentMethodId'),
                                                new PaymentMethodAttribute(field: 'BankCode',         optional: false, minLength: 1, maxLength: 100, fieldType: 'text', paymentField: 'BankCode')
                                        ]
                                )
                        ]
                ),

                new PaymentProvider(provider: Payment.PaymentProviderName.ap_withdrawal, name: 'ap_withdrawal',
                        paymentMethods: [
                                new PaymentMethod(paymentMethodId: 1033L, productId: 11427L, includeCountries: 'ALL',  excludeCountries: '',  currencies: 'ALL', description: 'Asia Payment payout', supportDirect: true, directEnabled: true, direction: Payment.PaymentDirection.withdraw)
                        ]
                )
        ]
    }

}