package eu.digient.billfold.payment.connectors.ap

import eu.digient.sdk.util.HttpClientUtil
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

interface AsiaPaymentHttpUtil {
    def post(String url, Map<String, String> headers, Map<String, Object> params)
}

@Component
class AsiaPaymentHttpUtilImpl implements AsiaPaymentHttpUtil {
    private static final def LOG = LoggerFactory.getLogger(AsiaPaymentHttpUtil)

    def post(final String url, final Map<String, String> headers, final Map<String, Object> params) {
        try {
            String body = null
            if (params?.size() > 0) {
                body = new JsonBuilder(params).toString()
            }
            def r = HttpClientUtil.httpPostWithSimpleResponse(url, headers, body)
            return new JsonSlurper().parseText(r.entity)
        } catch (Exception e) {
            LOG.error('[url: {}], unable to parse response from asia payment', url, e)
            throw new IllegalArgumentException()
        }
    }
}
