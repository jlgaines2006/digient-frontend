package eu.digient.billfold.payment.connectors.ap

import eu.digient.billfold.payment.Payment
import eu.digient.billfold.payment.connector.AbstractBaseConnector
import eu.digient.sdk.util.HMACUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import eu.digient.billfold.payment.PaymentGatewayException

interface AsiaPaymentServiceInvoker {
    String deposit(Integer siteId, String paymentMethodId, BigDecimal transactionAmount, String bankCode, String transactionId, String currency) throws PaymentGatewayException

    String withdraw(Integer siteId, String paymentMethodId, BigDecimal transactionAmount, String bankCode, String transactionId, String currency) throws PaymentGatewayException

    String getStatus(Integer siteId, String transactionId) throws PaymentGatewayException

}

@Component
class AsiaPaymentServiceInvokerImpl extends AbstractBaseConnector  implements AsiaPaymentServiceInvoker  {
    private static final def LOG = LoggerFactory.getLogger(AsiaPaymentServiceInvoker)
    private static final def INPUT_OUTPUT = LoggerFactory.getLogger('INPUT_OUTPUT_AP')
    private static final def HEADERS = [
            'Content-Type' : 'application/json',
            'Accept' : 'application/json'
    ]

    @Autowired AsiaPaymentHttpUtil http

    AsiaPaymentServiceInvokerImpl() {
        name = Payment.PaymentProviderName.ap
    }

    String deposit(final Integer siteId, final String paymentMethodId, final BigDecimal transactionAmount, final String bankCode, final String transactionId, String currency) throws PaymentGatewayException {
        long x1 = System.currentTimeMillis()
        def url = null
        Map<String, String> headers = null
        Map<String, Object> request = [:]
        def response = null
        try {
            url = getApiUrl(siteId) + '/deposit'

            def operatorId = getApiUsername(siteId)
            def operatorKey = getApiPassword(siteId)
            def hmac = calculateHmac(siteId, [operatorId, operatorKey, transactionAmount.toString(), paymentMethodId, transactionId])

            request.Request = [
                    Operator:
                            [
                                    Id: operatorId,
                                    Key:operatorKey
                            ],
                    PaymentMethodId: paymentMethodId,
                    Amount: transactionAmount,
                    BankCode: bankCode,
                    Currency: currency,
                    TransactionId: transactionId,
                    CallbackUrl: getStatusUrl(siteId,'ap'),
                    Hmac: hmac
            ]
            response = http.post(url, HEADERS, request)

            def id = response?.response?.transactionId?.toString()
            def message = response?.response?.result?.message?.toString()
            //if (id?.size() > 0 && message == 'OK') {
                return id
            //}
            LOG.warn('[transactionId: {}] invalid response from the service provider. Response was: [{}]', transactionId, response)

            throw new IllegalArgumentException()
        } catch (Exception e) {
            throw createNewException(e)
        } finally {
            def time = System.currentTimeMillis() - x1
            INPUT_OUTPUT.debug('[DEPOSIT][URL]{}[HEADERS]{}[REQUEST]{}[RESPONSE]{}[TIME][{}][END]', url, headers, request, response, time)
        }

    }

    String withdraw(final Integer siteId, final String paymentMethodId, final BigDecimal transactionAmount, final String bankCode, final String transactionId, final String currency) throws PaymentGatewayException {
        long x1 = System.currentTimeMillis()
        def url = null
        Map<String, String> headers = null
        Map<String, Object> request = [:]
        def response = null
        try {
            url = getApiUrl(siteId) + '/withdraw'

            def operatorId = getApiUsername(siteId)
            def operatorKey = getApiPassword(siteId)
            def hmac = calculateHmac(siteId, [operatorId, operatorKey, transactionAmount.toString(), paymentMethodId, transactionId])

            request.Request = [
                    Operator:
                            [
                                    Id:  operatorId,
                                    Key: operatorKey
                            ],
                    PaymentMethodId: paymentMethodId,
                    Amount: transactionAmount,
                    BankCode: bankCode,
                    Currency: currency,
                    TransactionId: transactionId,
                    CallbackUrl: getStatusUrl(siteId,'ap'),
                    Hmac: hmac
            ]
            response = http.post(url, HEADERS, request)

            def id = response?.response?.transactionId?.toString()
            def message = response?.response?.result?.message?.toString()
            if (id?.size() > 0 && message == 'OK') {
                return id
            }
            LOG.warn('[transactionId: {}] invalid response from the service provider. Response was: [{}]', transactionId, response)

            throw new IllegalArgumentException()
        } catch (Exception e) {
            throw createNewException(e)
        } finally {
            def time = System.currentTimeMillis() - x1
            INPUT_OUTPUT.debug('[WITHDRAW][URL]{}[HEADERS]{}[REQUEST]{}[RESPONSE]{}[TIME][{}][END]', url, headers, request, response, time)
        }
    }

    String getStatus(final Integer siteId, final String transactionId) throws PaymentGatewayException {
        long x1 = System.currentTimeMillis()
        def url = null
        Map<String, String> headers = null
        Map<String, Object> request = [:]
        def response = null
        try {
            url = getApiUrl(siteId) + '/status'

            def operatorId = getApiUsername(siteId)
            def operatorKey = getApiPassword(siteId)
            def hmac = calculateHmac(siteId, [operatorId, operatorKey, transactionId])

            request = [
                    Operator:
                            [
                                    Id: operatorId,
                                    Key: operatorKey
                            ],
                    TransactionId: transactionId,
                    Hmac: hmac
            ]
            response = http.post(url, HEADERS, request)

            def id = response?.response?.transactionId?.toString()
            def status = response?.response?.result?.status?.toString()
            if (id?.size() > 0) {
                return status
            }
            LOG.warn('[transactionId: {}] invalid response from the service provider. Response was: [{}]', transactionId, response)

            throw new IllegalArgumentException()
        } catch (Exception e) {
            throw createNewException(e)
        } finally {
            def time = System.currentTimeMillis() - x1
            INPUT_OUTPUT.debug('[STATUS][URL]{}[HEADERS]{}[REQUEST]{}[RESPONSE]{}[TIME][{}][END]', url, headers, request, response, time)
        }
    }

    private String calculateHmac(final Integer siteId, final List<String> params) {
        def plainText = ''
        params.each {
            plainText += it
         }
        HMACUtil.calculateHmacSHA256(plainText, getPropertyAsString(siteId, 'SECRET_KEY'))
    }

}
