package eu.digient.billfold.payment.connectors.ap

import eu.digient.billfold.support.service.BillfoldConfigService
import eu.digient.sdk.util.HMACUtil
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

import static org.junit.Assert.assertEquals
import static org.mockito.Mockito.when

@RunWith(MockitoJUnitRunner)
class AsiaPaymentServiceInvokerTest {
    AsiaPaymentServiceInvoker target

    @Mock BillfoldConfigService configService
    @Mock AsiaPaymentHttpUtil http

    Integer siteId = 1

    @Before
    void setup() {
        target = new AsiaPaymentServiceInvokerImpl(http: http, configService: configService)

        when(configService.getPropertyAsBoolean(siteId, 'PAYMENT_MODE')).thenReturn(false)
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_API_URL_TEST')).thenReturn('some-url')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_API_USERNAME')).thenReturn('some-username')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_API_PASSWORD')).thenReturn('some-pwd')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_SECRET_KEY')).thenReturn('some-key')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_URL_NOTIFICATION_BASE')).thenReturn("some-status-url")
    }

    @Test void deposit() {
        def siteId = 1
        def paymentMethodId = "1"

        def transactionAmount = new BigDecimal('3.00')
        def bankCode = 'some-bank-code'
        def transactionId = "some-transaction-id"
        def currency = "PHP"

        def headers = [
            'Content-Type' : 'application/json',
            'Accept' : 'application/json'
        ]

        def hmac = HMACUtil.calculateHmacSHA256('some-username' + 'some-pwd' + transactionAmount +
                paymentMethodId + transactionId, 'some-key')

        def request = [:]
        request.Request = [
                Operator:
                        [
                                Id: 'some-username',
                                Key: 'some-pwd'
                        ],
                PaymentMethodId: paymentMethodId,
                Amount: transactionAmount,
                BankCode: bankCode,
                Currency: currency,
                TransactionId: transactionId,
                CallbackUrl: 'some-status-url/ap',
                Hmac: hmac
        ]

        when(http.post('some-url/deposit', headers, request)).thenReturn([response: [transactionId: "some-trans-id", result: [message: "OK"]]])

        def r = target.deposit(siteId, paymentMethodId, transactionAmount, bankCode, transactionId,currency)
        assertEquals(r, "some-trans-id")
    }

    @Test void withdraw() {
        def siteId = 1
        def paymentMethodId = "1"

        def transactionAmount = new BigDecimal('3.00')
        def bankCode = 'some-bank-code'
        def transactionId = "some-transaction-id"
        def currency = "PHP"

        def headers = [
                'Content-Type' : 'application/json',
                'Accept' : 'application/json'
        ]

        def request = [:]
        request.Request = [
                Operator:
                        [
                                Id: 'some-username',
                                Key: 'some-pwd'
                        ],
                PaymentMethodId: paymentMethodId,
                Amount: transactionAmount,
                BankCode: bankCode,
                Currency: currency,
                TransactionId: transactionId,
                CallbackUrl: 'some-status-url/ap',
                Hmac: 'c390f4467003cb687c099d736235810069f2344d8d837098b112ab1d97225a36'
        ]

        when(http.post('some-url/withdraw', headers, request)).thenReturn([response: [transactionId: "some-trans-id", result: [message: "OK"]]])

        def r = target.withdraw(siteId, paymentMethodId, transactionAmount, bankCode, transactionId,currency)
        assertEquals(r, "some-trans-id")
    }


    @Test void getStatus() {
        def siteId = 1

        def transactionId = "some-transaction-id"

        def headers = [
                'Content-Type' : 'application/json',
                'Accept' : 'application/json'
        ]

        def request = [
                Operator:
                        [
                                Id: 'some-username',
                                Key: 'some-pwd'
                        ],
                TransactionId: transactionId,
                Hmac: '3cf3dbf1097f1cba98734ee8b78d6976f05a5d830bf66e309a2f87505f4b430b'
        ]

        when(http.post('some-url/status', headers, request)).thenReturn([response: [transactionId: "some-trans-id", result: [status: "00001"]]])

        def r = target.getStatus(siteId, transactionId)
        assertEquals(r, "00001")
    }

}

@RunWith(MockitoJUnitRunner)
class AsiaPaymentServiceInvokerConnectionTest {
    AsiaPaymentServiceInvoker target

    @Mock BillfoldConfigService configService

    Integer siteId = 1

    @Before
    void setup() {
        target = new AsiaPaymentServiceInvokerImpl(http: new AsiaPaymentHttpUtilImpl(), configService: configService)

        when(configService.getPropertyAsBoolean(siteId, 'PAYMENT_MODE')).thenReturn(false)
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_API_URL_TEST')).thenReturn('http://ppsapi-test.elasticbeanstalk.com/')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_API_USERNAME')).thenReturn('Id41e2ff9a-694b-413f-a57c-318948cf7f0c')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_API_PASSWORD')).thenReturn('Keyd7767e23-ed4a-4f1e-be3c-595ecf7199fc')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_AP_SECRET_KEY')).thenReturn('some-key')
        when(configService.getPropertyAsString(siteId, 'PAYMENT_URL_NOTIFICATION_BASE')).thenReturn("some-status-url")
    }

    @Test void deposit() {
        def siteId = 1
        def paymentMethodId = "1"

        def transactionAmount = new BigDecimal('3.00')
        def bankCode = 'some-bank-code'
        def transactionId = "some-transaction-id"
        def currency = "PHP"

        def r = target.deposit(siteId, paymentMethodId, transactionAmount, bankCode, transactionId,currency)
        assertEquals(r, "some-transaction-id")
    }

    @Test void withdraw() {
        def siteId = 1
        def paymentMethodId = "1"

        def transactionAmount = new BigDecimal('3.00')
        def bankCode = 'some-bank-code'
        def transactionId = "some-transaction-id"
        def currency = "PHP"

        def r = target.withdraw(siteId, paymentMethodId, transactionAmount, bankCode, transactionId,currency)
        assertEquals(r, "some-transaction-id")
    }

    @Test void getStatus() {
        def siteId = 1

        def transactionId = "10001"

        def r = target.getStatus(siteId,transactionId)
        assertEquals(r, "00005")
    }

}
